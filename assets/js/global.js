//initialize tooltip
var base_url = $("#base_url").val();
var assets_url = $("#assets_url").val();

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});  

 /*$.backstretch([
      assets_url+"img/bg.jpeg"
  ], {duration: 3000, fade: 750}); */
  
var mySpinner = null;

 
function SubmitDialog(thisBtn,thisform, onSuccess, onValidate)
{
    var btn = $(thisBtn);
    var frm = $(thisform);

    console.log(frm);

    $('.help-block').remove();
    $('.form-group').removeClass('has-error').removeClass('has-success');

    if (btn.find('img').length > 0) return false;

    if (typeof onValidate == 'function')
        onValidate();

    var required = frm.find('.required').filter(function(){
        return (typeof $(this).prop('name') != typeof undefined) && ($(this).val() == '');
    });

    if (required.length > 0) {
        required
            .after('<div class="help-block">Required field</div>')
            .parent().addClass('has-error');
        required[0].focus();
        return false;
    }

    if (frm.find('.has-error').length > 0)
        return false;

    $.ajax({
        type : 'POST',
        url : frm.attr('action'),
        data : frm.serialize(),
		enctype: 'multipart/form-data',
        dataType : 'json',
        cache : false,
        async : true,
        timeout: 10000,

        beforeSend : function() {
        	//
            btn.prepend('<img src="'+assets_url+'img/white-loader.svg" height="20" style="fill:#fff">');
            btn.attr('disabled',true);
        },

        success : function(response)
        {
            if (response.success) {
				$(".msgx").html(response.msg);
				frm.trigger("reset");
            }  else {
				$(".msgx").html(response.msg);
            } 
        },

        complete : function()
        {
            btn.find('img').remove();
            btn.removeAttr('disabled');         
        }
    });

    return false;
}

$("#desc").elastic();
$("#ingrid").elastic();
$("#vol").elastic();

//add qty
//itmqty
$(document).on('mouseup keyup change','.itmqty',function(e){
    var a = 0;
    $(".itmqty").each(function() {
        a += parseInt($(this).val());
    });
    $("#total_qty").val(a);
    var ototal=parseInt($("#total_qty").val())*parseInt($("#price").val()); 
    if(isNaN(ototal)) ototal="0";
    /*$("#ototal").val(ototal);*/
    ototal=ototal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'); 
    //$(this).closest("table").find("input.tqty").val(ototal);
    $("#total_price").text("Php "+ototal);
});
$(document).on('mouseup keyup change','.tqty',function(e){

    if($("#options input[name='opt_choice[]']").length > 0){

        var sum = 0;
        $("#options input.itmqty").each(function(i,item){
            sum += parseInt($(item).val());
        });

        $(".tqty").val(sum);
    }

    var ototal=parseInt($(".tqty").val())*parseInt($("#price").val());

    if(isNaN(ototal)) ototal="0";
    $("#ototal").val(ototal);

    
    ototal=ototal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'); 
    $("#total_price").text("Php "+ototal);     
});

$("#link_checkout").click(function(e){

    e.preventDefault();

    $.ajax({
        url: base_url + 'home/index/ajax_check_session',
        dataType: "json",
        success: function(result){

            if(result.logged_in == "0"){
                //popup modal
                $("#login-btn").click();
                $("#redirect_to").val('checkout');
            }else{
                //continue to transaction
               window.location.href= base_url + 'checkout';
            }
        },
        error: function(e){
            console.error("Something terribly went wrong. " + e);
        }

    });
    //check login
 
});