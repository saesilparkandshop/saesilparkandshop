var base_urlx = $("#base_urlx").val();
var promo = $('#promo_table').dataTable( {
		"responsive": true,
		"orderClasses": false,
		"processing":true,
        "sPaginationType": "bootstrap",
        "aLengthMenu": [[10, 50, 100, -1], [10, 50, 100, 'All']],     		
        "ajax": { 
        	url:base_urlx+"admin/table/promo",
        	type:"POST"
       },
        "deferRender": true,
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});
$(".date").datepicker();  	
//select promo type
$("select[multiple]").select2();
$("#promo_type").change(function(){

	if($(this).val()=='bundle' || $(this).val()=='item' ){
		$("#items_div").show();
		$("#brands_div").hide();
		
	} else if($(this).val() == 'brand'){
 
		$("#items_div").hide();
		$("#brands_div").show();
	}else {
		$("#brands_div").hide();
		$("#items_div").hide();
	}
	
});  
$("#upload-item").click(function(){
	$("#btn_upload").trigger("click");
});
function showImage(input) { 
var _URL = window.URL || window.webkitURL;	
var base_url = document.getElementById('hid_base_url').value;
	//ehcking file type
	var ext = input.value.match(/\.(.+)$/)[1];
    switch (ext) {
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'JPG':
        case 'JPEG':
        case 'PNG':        
        //case 'gif':
			    var file, img;
			    
			    if ((file = input.files[0])) {
			        img = new Image();
			        img.onload = function (e) {
			          // if(this.width=='312' && this.height=='475'){
							if (input.files && input.files[0]) { 
							         var reader = new FileReader(); 
							         reader.onload = function (e) { 
							                 $('#img-prev').attr('src', e.target.result); 
							         }
							         reader.readAsDataURL(input.files[0]); 
							      } 			            	
			           /* } else {
			            	alert("This Image has a resolution of "+this.width + " " + this.height+", Image must have 312x475 resolution.");
			            	$('#img-prev').attr('src', base_url+"assets/img/td.PNG"); 
            				input.value = '';
			            }*/
			        };
			        img.src = _URL.createObjectURL(file);
			    }          
        
            $('#btn_upload').attr('disabled', false);
       
            
            break;
        default:
            alert('This is not an allowed file type.');
            $('#img-prev').attr('src', base_url+"assets/img/td.PNG"); 
            input.value = '';
    }

	
  } 
$( "form" ).submit(function( event ) {
var $img=$('#img-prev');
var src=$('#img-prev').attr('src');
	/*if($('#btn_upload')[0].files.length == 0){
		alert("Please Upload Image First.");	
		return false;	
	} */
if(src==assets+'img/item_covers/td.PNG') {
    $img.addClass("highlight");
    // or
    $img.css("border", "3px solid #f04124");
    return false;
} 
	$( ':input[required]', ".formx" ).each( function () {
	    if ( this.value.trim() === '' ) {
	        ctr++;
	    }
	});
	if(ctr==0){
	$(".modal").modal("show");		
	} 
});	
	  
  	