var users = $('#dashboard_table').dataTable( {
		"responsive": true,
		"orderClasses": false,
		"processing":true,
        "sPaginationType": "bootstrap",
        "aLengthMenu": [[10, 50, 100, -1], [10, 50, 100, 'All']],         
		"order":[[2,'desc']],
        "ajax": { 
        	url:base_url+"admin/table/activity",
        	type:"POST"
       },
        "deferRender": true,
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});
$.backstretch([
      assets_url+"img/bg.jpeg"
  ], {duration: 3000, fade: 750});    	
	   	