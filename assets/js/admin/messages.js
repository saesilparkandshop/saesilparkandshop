var messages = $('#messages_table').dataTable( {
		"responsive": true,
		"orderClasses": false,
		"processing":true,
        "sPaginationType": "bootstrap",
        "aLengthMenu": [[10, 50, 100, -1], [10, 50, 100, 'All']],     
        "ajax": { 
        	url:base_url+"admin/table/messages",
        	type:"POST"
       },
        "deferRender": true,
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});
    	
/*var options = {
    target: '#price',
    change: true,
    base:   'USD',
    symbols: {
    	'USD' : '$',
        'WON' : '₩',
        'PHP' : '₩',
    },
    customCurrency: {
        'USD': 1,
        'WON': 1136.14,
        'PHP': 46.60,
    }  
 }     	
$(function(){ // document ready alias
    $('#price_changer').curry(options); // init curry
});	 */

   	
