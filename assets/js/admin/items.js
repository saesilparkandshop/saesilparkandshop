var assets = $("#assets_url").val();
var items = $('#items_table').dataTable( {
		"responsive": true,
		"orderClasses": false,
		"processing":true,
		"sPaginationType": "bootstrap",
		"aLengthMenu": [[10, 50, 100, -1], [10, 50, 100, 'All']],
        "ajax": { 
        	url:base_url+"admin/table/items",
        	type:"POST"
       },
        "deferRender": true,
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});
    	
/*var options = {
    target: '#price',
    change: true,
    base:   'USD',
    symbols: {
    	'USD' : '$',
        'WON' : '₩',
        'PHP' : '₩',
    },
    customCurrency: {
        'USD': 1,
        'WON': 1136.14,
        'PHP': 46.60,
    }  
 }     	
$(function(){ // document ready alias
    $('#price_changer').curry(options); // init curry
});	 */
/*
$("#upload-item").click(function(){
	$("#btn_upload").trigger("click");
});*/

$(document).on('click','.upload-item',function(){
	var uc=$(this).attr('uc');
	$("#btn_upload"+uc).trigger("click");
});

function showImage(input) { 
var _URL = window.URL || window.webkitURL;	
var base_url = document.getElementById('hid_base_url').value;
var cnt = input.getAttribute("buc");
//alert(cnt);
	//ehcking file type
	var ext = input.value.match(/\.(.+)$/)[1];
    switch (ext) {
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'JPG':
        case 'JPEG':
        case 'PNG':        
        //case 'gif':
			    var file, img;
			    
			    if ((file = input.files[0])) {
			        img = new Image();
			        img.onload = function (e) {
			          // if(this.width=='312' && this.height=='475'){
							if (input.files && input.files[0]) { 
							         var reader = new FileReader(); 
							         reader.onload = function (e) { 
							                 $('#img-prev'+cnt).attr('src', e.target.result); 
							         }
							         reader.readAsDataURL(input.files[0]); 
							      } 			            	
			           /* } else {
			            	alert("This Image has a resolution of "+this.width + " " + this.height+", Image must have 312x475 resolution.");
			            	$('#img-prev').attr('src', base_url+"assets/img/td.PNG"); 
            				input.value = '';
			            }*/
			        };
			        img.src = _URL.createObjectURL(file);
			    }          
        
            //$('#btn_upload').attr('disabled', false);
       
            
            break;
        default:
            alert('This is not an allowed file type.');
            $('#img-prev').attr('src', base_url+"assets/img/td.PNG"); 
            input.value = '';
    }

	
  }


  $("#brand").change(function() {
	//alert(id);
		var id = $("#brand option:selected").attr("data-b");
		$("#brand_id").val(id);
	}); 
//for brand
/*$("#brand").change(function() {
	//alert(id);
	var id = $("#brand option:selected").attr("data-b");
	
	//reset sub category when brand is re selected.
	str = str + "<option value=''>--Please Select Category First--</option>";
	$('#sub_category').html(str);	
		if(id!=''){
			$.post(base_url+"admin/admin/getCategory/"+id, function(data){
				var json_data = $.parseJSON(data);
				var str = "";
				if(json_data.length > 0 ){
				str = str + "<option data-x='' value=''>--Category--</option>";
				
				$.each(json_data, function(i, item){
					str = str + "<option data-x='"+item.cat_id+"' value='"+item.cat_name+"'>"+item.cat_name+"</option>";
				});
				$('#category').html(str);
				}else {
					alert("Please set Category for this brand.");
					str = str + "<option data-x='' value=''>--Please set Category for this brand.--</option>";
					$('#category').html(str);
				}
			});
		} else if (id==''){
				//alert("Please set Category for this brand.");
				var str = "";				
				str = str + "<option data-x='' value=''>--Please Select Brand First--</option>";			
				$('#category').html(str); 
		}
});  */
//for category
$("#category").change(function() {
	//alert(id);
	var id = $("#category option:selected").attr("data-x");
		if(id!=''){
			$.post(base_url+"admin/admin/getSubcategory/"+id, function(data){
				var json_data = $.parseJSON(data);
				var str = "";
				if(json_data.length > 0 ){
					str = str + "<option value=''>--Sub Category--</option>";
					
					$.each(json_data, function(i, item){
						str = str + "<option  value='"+item.scat_name+"'>"+item.scat_name+"</option>";
					});
					$('#sub_category').html(str);					
				} else {
					alert("Please set Sub Category for this Category.");
					str = str + "<option value=''>--Please set Sub Category for this Category.--</option>";
					$('#sub_category').html(str);
				}
			});
		} else if (id==''){
				//alert("Please set Sub Category for this Category.");
				 var str = "";				
				str = str + "<option value=''>--Sub Category--</option>";			
				$('#sub_category').html(str); 
		}
}); 

//is promo
$("#on_promo").change(function(){
	
if ( $("#on_promo").prop( "checked" ) ){
	$("#is_hidpromo").val(0);
	$("#p_desc").show();
} else {
	$("#is_hidpromo").val(1);
	$("#p_desc").hide();
}

//end of change checked	
});	

$( "form" ).submit(function( event ) {
var ctr=0;
var img_ctr=0;
/*var $img1 = $("#img-prev1");
var $img2 = $("#img-prev2");
var $img3 = $("#img-prev3");
var src1 = $img1.attr("src");
var src2 = $img2.attr("src");
var src3 = $img3.attr("src");

if(src1==assets+'img/item_covers/td.PNG') {
    $img1.addClass("highlight");
    // or
    $img1.css("border", "3px solid #f04124");
    return false;
} */
$(".img-responsive").each( function () {
	var src=$(this).attr("src");
	if(src===assets+'img/item_covers/td.PNG'){
	    //$(this).addClass("highlight");
	    // or
	    $(this).css("border", "2px solid #f04124");	
	    img_ctr++;	
	}else if(src!=assets+'img/item_covers/td.PNG'){
		$(this).css("border", "0px solid #fff");
		img_ctr--;
	}

});
	if(img_ctr>0){
		return false;
	}
	$( ':input[required]', ".formx" ).each( function () {
	    if ( this.value.trim() === '' ) {
	        ctr++;
	    }
	});
	if(ctr==0){
	$(".modal").modal("show");		
	} 
	/*setTimeout(function(){
	  $(".modal").modal("hide");
	}, 5000); */
	
}); 

// $(".options").select2({
//   placeholder: "Select a Item Options",
// });
$("#on_hand").click(function(){
	if($(this).is(":checked")){
		$("#on_hand_val").val('1');
		$(".isonhand").show();
		
	} else {
		$("#on_hand_val").val('0');
		$(".isonhand").hide();
	}
});
	   	
$('#add_option').on('keypress click', function(e){
	var str ='';
    if (e.which === 13 || e.type === 'click') {
		if($(".io").length>0){
		 $("#nodata").remove();
			str+='<tr class="io">';
			str+='<td><input type="text" name="item_options[]" class="form-control text_spacer input-sm"/></td>';
			str+='<td><select  class="form-control text_spacer input-sm" name="option_stock[]">';
			str+='<option value="0">In Stock</option>';
			str+='<option value="1">Out of Stock</option>';
			str+='</select></td>';
			str+='<td><button type="button" class="btn btn-danger btn-sm item_op"><span class="glyphicon glyphicon-trash"></span></button></td>';
			str+='</tr>';	
			$("#options").append(str);		 			
		}else {
		str+='<tr class="io">';
		str+='<td><input type="text" name="item_options[]" class="form-control text_spacer input-sm"/></td>';
			str+='<td><select  class="form-control text_spacer input-sm" name="option_stock[]">';
			str+='<option value="0">In Stock</option>';
			str+='<option value="1">Out of Stock</option>';
			str+='</select></td>';		
		str+='<td><button type="button" class="btn btn-danger btn-sm item_op"><span class="glyphicon glyphicon-trash"></span></button></td>';
		str+='</tr>';	
		$("#options").append(str);
		}    	
	
    }
});
$(document).on('click','.item_op',function(){
	var str ='';	
	if($(".io").length==1){
		str+='<tr class="io" id="nodata">';
		str+='<td colspan="3">No option added.</td>';
		str+='</tr>';	
		$("#options").html(str);			
	}else {
	$(this).closest('tr').remove();		
	}

});


$(".elastica").elastic();
$("#brand").select2({
	 theme: "bootstrap",
	 dropdownAutoWidth: 'true'
});
$("#category").select2({
	 theme: "bootstrap",
	 dropdownAutoWidth: 'true'
});
$("#sub_category").select2({
	 theme: "bootstrap",
	 dropdownAutoWidth: 'true'
});

$(document).on('click','.remove_img',function(){
	var count=$(this).attr('cnt');
	$("#btn_upload"+count).val('');
	$('#img-prev'+count).attr('src', base_url+'assets/img/item_covers/TD.png'); 
	//console.log($("#btn_upload"+count).val());
});

//setup before functions
var typingTimer;                //timer identifier
var doneTypingInterval = 500;  //time in ms, 5 second for example
var $input = $('#item_name');

//on keyup, start the countdown
$input.on('keyup', function () {
  clearTimeout(typingTimer);
  typingTimer = setTimeout(doneTyping, doneTypingInterval);
});

//on keydown, clear the countdown 
$input.on('keydown', function () {
  clearTimeout(typingTimer);
});

//user is "finished typing," do something
function doneTyping() {
  $.ajax({
  	url: base_url+'admin/admin/item_exist',
  	type: 'POST',
  	dataType: 'json',
  	data: {item: $('#item_name').val()},
  	
    beforeSend : function() {
    	$('#item_name').attr('disabled',true).focus();
    },

    success : function(response)
    {
       if(response.success){
       	   alert(response.msg);
		   $('#item_name').parent().addClass('has-error')
		   .focus();
       } else{
		   $('#item_name').parent().removeClass('has-error')
		   .focus();       	     	
       }
    },

    complete : function()
    {    
    $('#item_name').removeAttr('disabled').foucs();   
    }  	
  });
}



