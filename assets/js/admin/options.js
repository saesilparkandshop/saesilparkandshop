var options = $('#options_table').dataTable( {
		"responsive": true,
		"orderClasses": false,
		"processing":true,
        "sPaginationType": "bootstrap",
        "aLengthMenu": [[10, 50, 100, -1], [10, 50, 100, 'All']],     		
        "ajax": { 
        	url:base_url+"admin/table/options",
        	type:"POST"
       },
        "deferRender": true,
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});
	   	