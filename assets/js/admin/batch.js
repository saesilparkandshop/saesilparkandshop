var users = $('#batch_table').dataTable( {
		"responsive": true,
		"orderClasses": false,
		"processing":true,
        "sPaginationType": "bootstrap",
        "aLengthMenu": [[10, 50, 100, -1], [10, 50, 100, 'All']],     		
		"order":[[0,'desc']],
        "ajax": { 
        	url:base_url+"admin/table/batch",
        	type:"POST"
       },
        "deferRender": true,
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});
var datex= new Date();
datex.setDate(datex.getDate());    	
$(".date").datepicker({
	format:'yyyy-mm-dd',
	startDate:datex
});  		   	