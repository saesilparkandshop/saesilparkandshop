var bid =$("#bid").val();
var active = $('#active_table').dataTable( {
		"responsive": true,
		"orderClasses": false,
		"processing":true,
        "sPaginationType": "bootstrap",
        "aLengthMenu": [[10, 50, 100, -1], [10, 50, 100, 'All']],     		
        "ajax": { 
        	url:base_url+"admin/orders_grid_table/active/"+bid,
        	type:"POST"
       },
        "deferRender": true,
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});


//for delete option
$(document).on('mouseup keyup change','.opqtyt',function(){
	//recompute the total qty

	var cnt=$(this).attr("itemn"); //count
	var idn= $("#total_qty"+cnt); //total qty
	var price_each = $("#price_each"+cnt);
	var sub_total = $("#subtotal"+cnt);
	var total_qty=0;
	var oldval =  $(this).attr("oldval");
	if($(this).val()=="0"){
	bootbox.alert("You cannot set zero.");	
	//$(this).val(oldval);
	
	return false;	
	}else{
	$(".opqty"+cnt).each(function(){
	if($(this).val()==''){
		return false;
	}else{
	total_qty+=parseFloat($(this).val());	
	}	
				
	});
	//console.log(total_qty);
	idn.val(total_qty);
	//alert(total_qty);
	//recompute total price of the item
	var st=parseFloat(price_each.val())*parseFloat(total_qty);	
	//alert(st.toFixed(2));
	$("#subtotal"+cnt).val(st.toFixed(2));	
	//alert(sub_total);
	
	
	//recompute total
	var pretotal= 0;
	$(".subtotal").each(function(){
	pretotal+=parseFloat($(this).val());	
	
				
	});	
	
	var gtotal = parseFloat($("#intl_fee").val())+parseFloat($("#shipping_fee").val())+parseFloat(pretotal);
	//alert(gtotal);
	$("#gtotal").val(gtotal.toFixed(2));
	}
	
});
//delete option
    	
$(document).on('click','.deloption',function(){
	var cnt=$(this).attr("itemn"); //count
	var opcnt=  $(this).attr("option_ctr"); 
	//check length
	var idn= $("#total_qty"+cnt); //total qty
	var price_each = $("#price_each"+cnt);
	var total_qty=0;
	if($(".option_div"+cnt).length=='1'){
		bootbox.alert("You cannot delete all item options.");
	} else{
	$("#option_div"+opcnt).remove();		
	$(".opqty"+cnt).each(function(){
	//alert($(this).val());
	total_qty+=parseFloat($(this).val());	
		
				
	});		
	idn.val(total_qty);
	var st=parseFloat(price_each.val())*parseFloat(total_qty);	
	//alert(st.toFixed(2));
	$("#subtotal"+cnt).val(st.toFixed(2));	
	//alert(sub_total);
	
	
	//recompute total
	var pretotal= 0;
	$(".subtotal").each(function(){
	pretotal+=parseFloat($(this).val());	
	
				
	});	
	
	var gtotal = parseFloat($("#intl_fee").val())+parseFloat($("#shipping_fee").val())+parseFloat(pretotal);
	//alert(gtotal);
	$("#gtotal").val(gtotal.toFixed(2));	
	
	}
	
});	
$(document).on('click','.itemsdel',function(){
	var cnt=$(this).attr("itemn"); //count
	if($(".itemnum").length=='1'){
		bootbox.alert("You cannot delete all items.");
	} else{
	$("#itemnum"+cnt).remove();	
	var pretotal= 0;
	$(".subtotal").each(function(){
	pretotal+=parseFloat($(this).val());	
	
				
	});	
	
	var gtotal = parseFloat($("#intl_fee").val())+parseFloat($("#shipping_fee").val())+parseFloat(pretotal);
	//alert(gtotal);
	$("#gtotal").val(gtotal.toFixed(2));				
	}	
});	   	

$(document).on('mouseup keyup change','#intl_fee',function(){
	//recompute the total qty

	var cnt=$(this).attr("itemn"); //count
	var idn= $("#total_qty"+cnt); //total qty
	var price_each = $("#price_each"+cnt);
	var sub_total = $("#subtotal"+cnt);
	var total_qty=0;
	var oldval =  $(this).attr("oldval");
	
	
	//recompute total
	var pretotal= 0;
	$(".subtotal").each(function(){
	pretotal+=parseFloat($(this).val());	
	
				
	});	
	
	var gtotal = parseFloat($("#intl_fee").val())+parseFloat($("#shipping_fee").val())+parseFloat(pretotal);
	//alert(gtotal);
	$("#gtotal").val(gtotal.toFixed(2));
	
	
});
$(document).on('mouseup keyup change','#shipping_fee',function(){
	//recompute the total qty

	var cnt=$(this).attr("itemn"); //count
	var idn= $("#total_qty"+cnt); //total qty
	var price_each = $("#price_each"+cnt);
	var sub_total = $("#subtotal"+cnt);
	var total_qty=0;
	var oldval =  $(this).attr("oldval");
	
	
	//recompute total
	var pretotal= 0;
	$(".subtotal").each(function(){
	pretotal+=parseFloat($(this).val());	
	
				
	});	
	
	var gtotal = parseFloat($("#intl_fee").val())+parseFloat($("#shipping_fee").val())+parseFloat(pretotal);
	//alert(gtotal);
	$("#gtotal").val(gtotal.toFixed(2));
	
	
});

	$(".payment-thumb").fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 50,
				height	: 50
			}
		}
	});