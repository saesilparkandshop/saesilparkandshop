var tripsummary_table = $('#bor_table').dataTable( {
		destroy:true,
		responsive: false,
		bFilter:false,
		  dom: 'T<"clear">lfrtip',
		"oTableTools": {
	        "sSwfPath": "//cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf",
	        "aButtons": [ 
	            {
	                "sExtends": "xls",
	                "sFileName": "BOR.csv",
	                "bFooter": true
	            },
	            {
	                "sExtends": "pdf",
	                "sFileName": "BOR.pdf",
	                "sPdfOrientation": "landscape"
	            }
	        ]
	    },		  
		//colVis: { exclude: [0] },
		ajax: {
			type:"POST",
		    url: base_url+"report/report/table/bor",
		    data: function(data){
		      data['start'] = $('#start').val();
		     data['end'] = $('#end').val();
		    },	
	    },    
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		},
    	});
//filter
$("#filter").click(function(){
	tripsummary_table.api().ajax.reload(null, false);
});
    	
$(".date").datepicker();    	
	   	