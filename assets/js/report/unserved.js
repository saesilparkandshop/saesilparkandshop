var unserved_table = $('#forbooking_table').dataTable( {
		destroy:true,
		responsive: false,
		bFilter:false,
		  dom: 'T<"clear">lfrtip',
		"oTableTools": {
	        "sSwfPath": "//cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf",
	        "aButtons": [ 
	            {
	                "sExtends": "xls",
	                "sFileName": "UNSERVED.csv",
	                "bFooter": true
	            },
	            {
	                "sExtends": "pdf",
	                "sFileName": "UNSERVED.pdf",
	                "sPdfOrientation": "landscape"
	            }
	        ]
	    },		  
		//colVis: { exclude: [0] },
		ajax: {
			type:"POST",
		    url: base_url+"dashboard/dashboard/table/for-booking",
		    data: function(data){
		      //data['start'] = $('#start').val();
		     // data['end'] = $('#end').val();
		    },	
	    },    
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		},
    	});
//filter
$("#filter").click(function(){
	tripsummary_table.api().ajax.reload(null, false);
});
    	
$(".date").datepicker();    	
	   	