var tripsummary_table = $('#tripsummary_table').dataTable( {
		destroy:true,
		responsive: true,
		bFilter:false,
		  dom: 'T<"clear">lfrtip',
		"oTableTools": {
	        "sSwfPath": "//cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf",
	        "aButtons": [ 
	            {
	                "sExtends": "xls",
	                "sFileName": "TRIPSUMMARY.csv",
	                "bFooter": true
	            },
	            {
	                "sExtends": "pdf",
	                "sFileName": "TRIPSUMMARY.pdf"
	            }
	        ]
	    },		  
		colVis: { exclude: [0] },
		ajax: {
			type:"POST",
		    url: base_url+"report/report/table/tripsummary",
		    data: function(data){
		      data['start'] = $('#start').val();
		      data['end'] = $('#end').val();
		    },	
	    },    
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		},
		footerCallback: function ( row, data, start, end, display ) {
            var api = this.api();
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
 
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
                         
            };
            // Total over all pages
 			//for total
                if (api.column(2).data().length){
                var total = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                return intVal(a) + intVal(b);
                } ) }
                else{ total = 0};
                 
  
            // Total over this page            
            if (api.column(2).data().length){
            var pageTotal = api
                .column( 2, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                } ) }
                else{ pageTotal = 0};
  
            // Update footer
            $( api.column(2).footer() ).html(
                pageTotal
            );
            
  				///for %
                if (api.column(3).data().length){
                var total = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                return parseFloat(a) + parseFloat(b);
                } ) }
                else{ total = 0};
                 
  
            // Total over this page            
            if (api.column(3).data().length){
            var pageTotal = api
                .column( 3, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return parseFloat(a) + parseFloat(b);
                } ) }
                else{ pageTotal = 0};
  
            // Update footer
            $( api.column(3).footer() ).html(
                pageTotal
            );           
        },	
    	});
//filter
$("#filter").click(function(){
	tripsummary_table.api().ajax.reload(null, false);
});
    	
$(".date").datepicker();    	
	   	