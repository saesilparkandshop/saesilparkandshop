var unbilled_table = $('#unbilled_table').dataTable( {
		destroy:true,
		responsive: true,
		bFilter:false,
		  dom: 'T<"clear">lfrtip',
		"oTableTools": {
	        "sSwfPath": "//cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf",
	        "aButtons": [ 
	            {
	                "sExtends": "xls",
	                "sFileName": "UNBILLED.csv",
	                "bFooter": true
	            },
	            {
	                "sExtends": "pdf",
	                "sFileName": "UNBILLED.pdf"
	            }
	        ]
	    },		  
		colVis: { exclude: [0] },
		ajax: {
			type:"POST",
		    url: base_url+"report/report/table/unbilled/",
		    data: function(data){
		      data['start'] = $('#start').val();
		      data['end'] = $('#end').val();
		    },	
	    },    
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		},
		footerCallback: function ( row, data, start, end, display ) {
            var api = this.api();
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
 
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
                         
            };
  
            // Total over all pages
 				//total
                if (api.column(6).data().length){
                var total = api
                .column( 6)
                .data()
                .reduce( function (a, b) {
                return intVal(a) + intVal(b);
                } ) }
                else{ total = 0};
                 
  
            // Total over this page
             
            if (api.column(6).data().length){
            var pageTotal = api
                .column( 6, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                } ) }
                else{ pageTotal = 0};
  
            // Update footer
            $( api.column(6).footer() ).html(
                pageTotal
            );                 
         
        },	
    	});
//filter
$("#filter").click(function(){
	tripsummary_table.api().ajax.reload(null, false);
});
    	
$(".date").datepicker();    	
	   	