var salesreport_table = $('#fuel_revenue_table').dataTable( {
		destroy:true,
		responsive: true,
		bFilter:false,
		  dom: 'T<"clear">lfrtip',
		"oTableTools": {
	        "sSwfPath": "//cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf",
	        "aButtons": [ 
	            {
	                "sExtends": "xls",
	                "sFileName": "FUEL_REVENUE.csv",
	                "bFooter": true
	            },
	            {
	                "sExtends": "pdf",
	                "sFileName": "FUEL_REVENUE.pdf"
	            }
	        ]
	    },		  
		colVis: { exclude: [0] },
		ajax: {
			type:"POST",
		    url: base_url+"report/report/table/fuel_revenue/",
		    data: function(data){
		      data['start'] = $('#start').val();
		      data['end'] = $('#end').val();
		    },	
	    },    
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		},
		footerCallback: function ( row, data, start, end, display ) {
            var api = this.api();
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
 
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
                         
            };
  
            // Total over all pages
 				//total
                if (api.column(2).data().length){
                var total = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                return intVal(a) + intVal(b);
                } ) }
                else{ total = 0};
                 
  
            // Total over this page
             
            if (api.column(2).data().length){
            var pageTotal = api
                .column( 2, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                } ) }
                else{ pageTotal = 0};
  
            // Update footer
            $( api.column(2).footer() ).html(
                pageTotal
            );
              // Total over this page
             //total %          
                if (api.column(3).data().length){
                var total = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                return parseFloat(a) + parseFloat(b);
                } ) }
                else{ total = 0};
                 
  

            if (api.column(3).data().length){
            var pageTotal = api
                .column(3, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return parseFloat(a) + parseFloat(b);
                } ) }
                else{ pageTotal = 0};
  
            // Update footer
            $( api.column(3).footer() ).html(
                pageTotal+"%"
            );           
        },	
    	});
//filter
$("#filter").click(function(){
	salesreport_table.api().ajax.reload(null, false);
});
$("#excel").click(function(){
	
});
    	
$(".date").datepicker();    	
	   	