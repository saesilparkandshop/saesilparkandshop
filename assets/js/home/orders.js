var active = $('#activeorders_table').dataTable( {
		"responsive": true,
		"orderClasses": false,
		"processing":true,
        "sPaginationType": "bootstrap",
        "aLengthMenu": [[10, 50, 100, -1], [10, 50, 100, 'All']],             
        "ajax": { 
        	url:base_url+"home/home_orders_table/active",
        	type:"POST"
       },
        "deferRender": true,
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});
	   	
var predelivery_table = $('#predelivery_table').dataTable( {
		"responsive": true,
		"orderClasses": false,
		"processing":true,
        "sPaginationType": "bootstrap",
        "aLengthMenu": [[10, 50, 100, -1], [10, 50, 100, 'All']],             
        "ajax": { 
        	url:base_url+"home/home_orders_table/predelivery",
        	type:"POST"
       },
        "deferRender": true,
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});
    	
var onshipping = $('#onshipping_table').dataTable( {
		"responsive": true,
		"orderClasses": false,
		"processing":true,
        "sPaginationType": "bootstrap",
        "aLengthMenu": [[10, 50, 100, -1], [10, 50, 100, 'All']],             
        "ajax": { 
        	url:base_url+"home/home_orders_table/onshipping",
        	type:"POST"
       },
        "deferRender": true,
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});

var ondelivery = $('#ondelivery_table').dataTable( {
		"responsive": true,
		"orderClasses": false,
		"processing":true,
        "sPaginationType": "bootstrap",
        "aLengthMenu": [[10, 50, 100, -1], [10, 50, 100, 'All']],             
        "ajax": { 
        	url:base_url+"home/home_orders_table/ondelivery",
        	type:"POST"
       },
        "deferRender": true,
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});   
    	
var completed = $('#completed_table').dataTable( {
		"responsive": true,
		"orderClasses": false,
		"processing":true,
        "sPaginationType": "bootstrap",
        "aLengthMenu": [[10, 50, 100, -1], [10, 50, 100, 'All']],             
        "ajax": { 
        	url:base_url+"home/home_orders_table/completed",
        	type:"POST"
       },
        "deferRender": true,
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});        
    	
var void_table = $('#void_table').dataTable( {
		"responsive": true,
		"orderClasses": false,
		"processing":true,
        "sPaginationType": "bootstrap",
        "aLengthMenu": [[10, 50, 100, -1], [10, 50, 100, 'All']],             
        "ajax": { 
        	url:base_url+"home/home_orders_table/void",
        	type:"POST"
       },
        "deferRender": true,
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});        		 	