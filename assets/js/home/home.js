$('[data-submenu]').submenupicker();
function get_action(form) {
var v = grecaptcha.getResponse();
if(v.length == 0)
{
    document.getElementById('captcha').innerHTML="You can't leave Captcha Code empty";
    return false;
}
if(v.length != 0)
{
    document.getElementById('captcha').innerHTML="Captcha completed";
    return true; 
}
}

// Can also be used with $(document).ready()
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide"
  });
});
//for login btn
$('a[data-toggle=modal][data-target]').click(function () {
    var target = $(this).attr('href');
    $('a[data-toggle=tab][href=' + target + ']').tab('show');
})
/*
$('#login-btn').click(function(){
	//alert("s");
	$("#login-modal").modal('show');
	$("#login").addClass('active');
	$("#loginx").attr('aria-expanded','true');
});
$('#register-btn').click(function(){
	//alert("s");
	$("#login-modal").modal('show');
	$("#register").addClass('active');
	$("#registerx").attr('aria-expanded','true');
}); */
//submit on php
$(".formx").submit(function(){
	 $(".submit").prepend('<img src="'+assets_url+'img/white-loader.svg" height="20" style="fill:#fff">');
     $(".submit").attr('disabled',true);	
});

//submit on ajax
$("#submit").click(function(){
  get_action(this);
  var form = $(this).parents('form:first');
  SubmitDialog(this,form,
    function(){ // onSuccess

    },
    function(){ // onValidate -- validate the product description

    }
  );	
});
$("#submit").keypress(function(e){
	if(e.event=='13'){
		$("#submit").click();
	}
});


$(".option").select2();

function showImage(input) { 
var _URL = window.URL || window.webkitURL;	
var base_url = document.getElementById('hid_base_url').value;
	//ehcking file type
	var ext = input.value.match(/\.(.+)$/)[1];
    switch (ext) {
        case 'jpg':
        case 'jpeg':
        case 'png':
        //case 'gif':
			    var file, img;
			    
			    if ((file = input.files[0])) {
			        img = new Image();
			        img.onload = function (e) {
			          // if(this.width=='312' && this.height=='475'){
							if (input.files && input.files[0]) { 
							         var reader = new FileReader(); 
							         reader.onload = function (e) { 
							                 $('#img-prev').attr('src', e.target.result); 
							         }
							         reader.readAsDataURL(input.files[0]); 
							      } 			            	
			           /* } else {
			            	alert("This Image has a resolution of "+this.width + " " + this.height+", Image must have 312x475 resolution.");
			            	$('#img-prev').attr('src', base_url+"assets/img/td.PNG"); 
            				input.value = '';
			            }*/
			        };
			        img.src = _URL.createObjectURL(file);
			    }          
        
            $('#btn_upload').attr('disabled', false);
       
            
            break;
        default:
            alert('This is not an allowed file type.');
            $('#img-prev').attr('src', base_url+"assets/img/td.PNG"); 
            input.value = '';
    }

	
  } 
  
$("#options_menu, .options_menu").change(function(){
var val=$(this).val();
var str='';

	if(val!=''){
		str+='<tr class="io">';	
		str+='<td><input type="text" class="form-control input-sm" name="opt_choice[]" value="'+val+'" disabled/></td>';
		str+='<td><input type="number" class="itmqty form-control input-sm" min="0" value="0" /></td>';
		str+='<td><button type="button" class="btn btn-danger btn-sm item_op" opt="'+val+'"><span class="glyphicon glyphicon-remove"></span></button></td>';
		str+='</tr>';
		/* $("#options").append(str); */
		 $(this).closest('div.list-group').closest('div.list-group').find('table').select('tr.options_body').append(str);
		$('option:selected', this).remove();
	}
});
$(document).on('click','.item_op',function(){
	var opt = $(this).attr('opt');
	var str ='';
	var optx='';	
	optx+='<option value="'+opt+'">'+opt+'</option>';

	 
	$(this).closest('div.list-group').find('select.options_menu').append(optx);		

	$(this).closest('tr').remove();
	/* $("#options_menu").append(optx);		 */

	
});

$("#add_to_cart").click(function(){

	Cart.add_to_cart();

});

$("#add_to_cart2").click(function(){

	Cart.add_to_cart2();

});

$("#continue_shopping").click(function(){
	Cart.reset_content();
});
$("#proceed_to_cart").click(function(){
	Cart.proceed_to_cart();
});

$(".delete_cart_item").click(function(){
	var id = $(this).attr('data-rowid');
	var self = $(this);


 //TOFIX : check delete cart bug
	Cart.delete_cart_item(id, function(){
		
		self.parent().parent().fadeOut(300, function(){
			self.remove();
		}); 
	});
});
/*$(document).on('click','.view',function(){
	var path = $(this).attr('pth');
	$("#prev_item").attr('src',path);
	$('#preview_modal').modal('show');
}); */
//image zoom
/*$("#itm_img").elevateZoom({
	zoomType:"lens",
	lensShape:"round",
	lensSize:180,
}); */

	$(".fancybox-thumb").fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 50,
				height	: 50
			}
		}
	});
