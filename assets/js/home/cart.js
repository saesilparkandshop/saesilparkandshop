var Cart = {

	reset_content : function(){
			window.location.href =  ""; //for the meantime

	},
	proceed_to_cart : function(){
			window.location.href =  base_url + "cart"; //for the meantime

	},
	add_to_cart : function(){
	 	var item_id = $("#item_id").val(), 
			qty = $("#total_qty").val();

		//for options
		if($("#options_menu").length > 0 && $("#options input[name='opt_choice[]']").length == 0){

			alert("Please select an option");
			return;
		}else if($("#total_qty").val() <= 0){
			alert("Please enter quantity");
			return;
		}


		var options = [];

		$("#options input[name='opt_choice[]']").each(function(index, value){

			options.push({
				name : $(this).val(),
				qty : $($("#options input.itmqty")[index]).val()
			});

		});	




		$.ajax({
			url: base_url + 'cart/add_item',
			data : {
				item_id : item_id,
				qty : qty,
				options: options,
				has_promo: 0
			},
			method : "POST",
			dataType 	: "json",
			success : function(response){

				if(response.status == "reenter"){
					$("#add_more").text(response.add_qty);
					$("#bonus_cart_modal").modal()

					return;
				}
				var count = parseInt($("#cart_count").text());
				count += parseInt(qty);

				$("#cart_count").text(count);
				$("#cart_modal").modal();
			},
			error: function(){
				console.log("Error");
			}
		});
	},
 	add_to_cart2 : function(){
	 	var item_id = $("#item_id").val(), 
			qty = $("#total_qty").val();

		//for options
		if($("#options_menu2").length > 0 && $("#options2 input[name='opt_choice[]']").length == 0){

			alert("Please select an option");
			return;
		}else if($("#total_qty").val() <= 0){
			alert("Please enter quantity");
			return;
		} /* if greater than add_more  . . Must not exceed X value */
		

		var options = [];

		$("#options input[name='opt_choice[]'],#options2 input[name='opt_choice[]']").each(function(index, value){

			options.push({
				name : $(this).val(),
				qty : $($("#options input.itmqty")[index]).val()
			});

		});	
 
			 



		$.ajax({
			url: base_url + 'cart/add_item',
			data : {
				item_id : item_id,
				qty : qty,
				options: options,
				has_promo: 1
			},
			method : "POST",
			dataType 	: "json",
			success : function(response){

				if(response.status == "reenter"){

					$("#bonus_cart_modal").modal()
					return;
				}
				var count = parseInt($("#cart_count").text());
				count += parseInt(qty);

				$("#cart_count").text(count);
				$("#cart_modal").modal();
			},
			error: function(){
				console.log("Error");
			}
		});
	},
	delete_cart_item : function(row_id, callback){

		$.ajax({
			url: base_url + 'cart/delete/',
			data : {
				row_id : row_id, 
			 	
			},
			method : "POST",
			success : function(){
				 if(typeof callback === "function"){
				 	callback();
				 }
			},
			error: function(){
				console.log("Error");
			}
		});

	}


	
};

