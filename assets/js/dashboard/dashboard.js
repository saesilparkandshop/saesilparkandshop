//forusers
var user_table = $('#forbooking_table').DataTable( {
		destroy: true,
		responsive: true,
        "ajax": base_url+"dashboard/dashboard/table/for-booking",
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});
//for delivery    	
var delivery_table = $('#fordelivery_table').DataTable( {
		destroy: true,
		responsive: true,
        "ajax": base_url+"dashboard/dashboard/table/for-delivery",
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});
//for completed    	
var completed_table = $('#completed_table').DataTable( {
		destroy:true,
		responsive: true,
        "ajax": base_url+"dashboard/dashboard/table/completed",
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});  
$('#checkall').change( function() {
	if($(this).is(':checked')){
		$('.wbid', completed_table.column().nodes()).prop('checked', true);
	} else {
		$('.wbid', completed_table.column().nodes()).prop('checked', false);
	}      
});
$("#for-billing").click(function(){
var rowcollection =  completed_table.$(".wbid:checked", {"page": "all"});
var arr=[];
if($(".wbid").is(':checked')){
	rowcollection.each(function(index,elem){
	    var checkbox_value = $(elem).val();
	    //Do something with 'checkbox_value'
	    //alert(checkbox_value);
	    arr.push(checkbox_value);
	});
		
	$.ajax({
	     type: "POST",
	     url: base_url+'dashboard/dashboard/forBilling',
	     data: {"ids":arr},  //assign the var here 
	     success: function(msg){
	        $(".msg").html(msg);
	        completed_table.ajax.reload(null, false);
	     }
	});		
}else {
	alert("Check Item First.");
}		
});
//for billing
var billing_table = $('#billing_table').DataTable( {
		destroy:true,
		responsive: true,
        "ajax": base_url+"dashboard/dashboard/table/for-billing",
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});  
var billed_table = $('#billed_table').DataTable( {
		destroy:true,
		responsive: true,
        "ajax": base_url+"dashboard/dashboard/table/billed",
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});      	
//for completed    	
var paid_table = $('#paid_table').DataTable( {
		destroy:true,
		responsive: true,
        "ajax": base_url+"dashboard/dashboard/table/paid",
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});  
var paid_table = $('#paidview_table').DataTable( {
		bFilter:false,
		destroy:true,
		responsive: true,
        "ajax": base_url+"dashboard/dashboard/tablepaid_view/"+$("#pay_id").text(),
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});      	
$(".datepicker").datepicker();	   	

$("#close").click(function(){
	$('.formx').trigger("reset");
});
$("#pri_cus" ).change(function() {
	var id = $(this).val();
		if(id!=''){
			$.post(base_url+"dashboard/dashboard/sb_data/"+id, function(data){
				var json_data = $.parseJSON(data);
				var str = "";
				
				str = str + "<option value=''>--Sub Customer--</option>";
				
				$.each(json_data, function(i, item){
					str = str + "<option value='"+item.sub_cus_id+"'>"+item.name+"</option>";
				});
				$('#sub_cus').html(str);
			});
		} else if (id==''){
				var str = "";
				
				str = str + "<option value=''>--Sub Customer--</option>";			
				$('#sub_cus').html(str);
		}
});

$("#truck").change(function(){
	var did = $('option:selected', this).attr('did');
	var h1id = $('option:selected', this).attr('h1id');
	var h2id = $('option:selected', this).attr('h2id');
	var h3id = $('option:selected', this).attr('h3id');
	
	$("#driver").val(did);
	$("#helper1").val(h1id);
	$("#helper2").val(h2id);
	$("#helper3").val(h3id);
});

$("#shipper").change(function(){
	var addr = $('option:selected', this).attr('addr');
	
	$("#delivery_addr").val(addr);
});

$("#van_size").change(function(){
	var	selected = $(this).val();
	
	if(selected =='Single' || selected =='40 footer'){
		$("#part1").show();
		$("#part2").hide();
	} else if(selected =='Tandem'){
		$("#part1").show();
		$("#part2").show();		
	} else if(selected ==''){
		$("#part1").hide();
		$("#part2").hide();			
	}
});

//Stack menu when collapsed
$('#bs-example-navbar-collapse-1').on('show.bs.collapse', function() {
    $('.nav-pills').addClass('nav-stacked');
});

//Unstack menu when not collapsed
$('#bs-example-navbar-collapse-1').on('hide.bs.collapse', function() {
    $('.nav-pills').removeClass('nav-stacked');
});
var deliveryremarks_table='';
//showing dr
$(document).on( "click",".del_btn",function() {
	var dr=$(this).data("dr");
	var x=$(this).data("x");
	
	$("#delivery_rem").val(dr);
	$("#delivery_rem").attr("data-this",x);
deliveryremarks_table = $('#deliveryremarks_table').DataTable( {
		destroy:true,
		
		responsive: true,
        "ajax": base_url+"dashboard/dashboard/deliveryRemarksGrid/"+x,
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		},
		"pageLength": 5,
		"lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]]
    	});  	
	
	
	$("#dashboard").modal("show");
});
//s th dr data
$("#s-btn").click(function(){
	var dr_data=$("#delivery_rem").val();
	var this_data=$("#delivery_rem").attr("data-this");	
	
	$.ajax({
	  method : "POST",  
	  url: base_url+"dashboard/dashboard/DeliveryRemarks",
	  data:{dr:dr_data, id:this_data},	
	  cache : false,
      async : true,
      timeout: 7000,	
        
	  success: function( data ) {
	    //$("#dashboard").modal('hide');
	    deliveryremarks_table.ajax.reload(null,false);
	    delivery_table.ajax.reload(null,false);
	  },
	  error:function(){
	  	alert("Something Went Wrong.");
	  }
	});
		//user_table.ajax.reload(null, false);
		
});
//check if rates have a value
	var price =$('option:selected', '#rates').attr('data-amt');
	var vat = parseFloat(price)*parseFloat(0.12);
	var total = parseFloat(price)+parseFloat(vat);
	if(price==''){
	price=0;	
	vat = 0;
	total = 0;	
	$("#amount").val(price.toFixed(2));
	$("#vat").val(vat.toFixed(2));
	$("#total").val(total.toFixed(2));	
	}
	else{
	$("#amount").val(price);
	$("#vat").val(vat.toFixed(2));
	$("#total").val(total.toFixed(2));
	}

$("#rates").change(function(){
	var price =$('option:selected', this).attr('data-amt');
	var vat = parseFloat(price)*parseFloat(0.12);
	var total = parseFloat(price)+parseFloat(vat);
	if(price==''){
	price=0;	
	vat = 0;
	total = 0;	
	$("#amount").val(price.toFixed(2));
	$("#vat").val(vat.toFixed(2));
	$("#total").val(total.toFixed(2));	
	}
	else{
	$("#amount").val(price);
	$("#vat").val(vat.toFixed(2));
	$("#total").val(total.toFixed(2));
	}
});

$("#print_wb").click(function(){
var idx = $("#idx").val();
var sh = (screen.height - 600) / 2;
var sw = (screen.width - 800) / 2;
var win = window.open(base_url+'dashboard/dashboard/PrintWayBill/' + idx, 'Print Waybill', 'width=800, height=600, left='+sw+', top='+sh+', menubar=no, location=no, resizable=yes, scrollbars=yes');	
});
$("#print_wb").keypress(function(e){
	if(e.event=='13'){
		$("#print_wb").click();	
	}	
});
$("#bill-print").click(function(){

	if($(".wbid").is(':checked')){
		//$("#billing").modal('show');
	$("#print_billingx").modal('show');
	
	}else {
		alert("Check Waybill First.");
	}		
});
$("#bill-print").keypress(function(e){
	if(e.event=='13'){
		$("#bill-print").click();	
	}	
});
$("#submit-btn").click(function(){
	var attention = $("#attention").val();
	var address = $("#address").val();
	var rowcollection =  billing_table.$(".wbid:checked", {"page": "all"});
	var arr=[];
	if(attention=='' || address=='' ){
		alert("Please Fill up all fields.");
	}else {
		rowcollection.each(function(index,elem){
		    var checkbox_value = $(elem).val();
		    //Do something with 'checkbox_value'
		    //alert(checkbox_value);
		    arr.push(checkbox_value);
		});
		$.ajax({
		     type: "GET",
		     url: base_url+'/dashboard/dashboard/forBilling/'+arr+'/'+attention+'/'+address,
		     //data: {"ids":arr},  //assign the var here 
		     success: function(msg){
		        $(".msg").html(msg);	
		        //$("#billing").modal('hide');	
		        $("#print_billingx").modal('hide');	          
		        billing_table.ajax.reload(null, false);	
		        
		        //hide modal			
				var sh = (screen.height - 600) / 2;
				var sw = (screen.width - 1000) / 2;
				var win = window.open(base_url+'dashboard/dashboard/PrintWayBilling/' + arr+'/'+attention+'/'+address, 'Print Waybill', 'width=1000, height=600, left='+sw+', top='+sh+', menubar=no, location=no, resizable=yes, scrollbars=yes');				      
		     }
		});	

	}
});
//billed section
$("#paid-print").click(function(){
	var rowcollection =  billed_table.$(".wbid:checked", {"page": "all"});
	var arr=[];
	if($(".wbid").is(':checked')){
		$("#paid").modal('show');
	}else {
		alert("Check Waybill First.");
	}		
});
$("#paid-print").keypress(function(e){
	if(e.event=='13'){
		$("#paid-print").click();	
	}	
}); 
$(".date").datepicker({
	 format: 'yyyy-mm-dd',
});

$("#payment-btn").click(function(){
	var rowcollection =  billed_table.$(".wbid:checked", {"page": "all"});
	var arr=[]; 
	var pay_ref = $("#pay_ref").val();
	var pay_date = $("#pay_date").val();
	var pay_amount = $("#pay_amount").val();
	if(pay_ref=='' || pay_date=='' || pay_amount==''){
		alert("Please Fillup all fields.");
	}else {
		rowcollection.each(function(index,elem){
		    var checkbox_value = $(elem).val();
		    //Do something with 'checkbox_value'
		    //alert(checkbox_value);
		    arr.push(checkbox_value);
		});	
		$.ajax({
		     type: "GET",
		     url: base_url+'dashboard/dashboard/TagPaid/'+arr+'/'+pay_ref+'/'+pay_date+'/'+pay_amount,
		     //data: {"ids":arr},  //assign the var here 
		     success: function(msg){
		        $(".msg").html(msg);	
		        //$("#billing").modal('hide');	        
		        billed_table.ajax.reload(null, false);
		        $("#paid").modal('hide');
		    }   
		});
	}			
});
$("#payment-btn").keypress(function(e){
	if(e.event=='13'){
		$("#payment-btn").click();	
	}		
});
//cancel wb modal
$("#cancel_wb").click(function(){
	$("#cancel_modal").modal('show');
});
$("#cancel_wb").keypress(function(e){
	if(e.event=='13'){
		$("#cancel_wb").click();	
	}		
});
//cancel waybill
$("#confirm-cancel").click(function(){
	var wb_id = $("#waybill_ref").val();
		$.ajax({
		     type: "GET",
		     url: base_url+'dashboard/dashboard/cancelWb/'+wb_id,
		     //data: {"ids":arr},  //assign the var here 
		     success: function(msg){
		        //$(".msg").html(msg);	        
		        $("#cancel_modal").modal('hide');
		        window.location.href=base_url+'dashboard/dashboard';
		    }   
		});	
});
$("#confirm-cancel").keypress(function(e){
	if(e.event=='13'){
		$("#confirm-cancel").click();	
	}		
});

//cancel view
var cancel_table = $('#cancel_table').DataTable( {
		destroy:true,
		responsive: true,
        "ajax": base_url+"dashboard/dashboard/table/cancel",
        "oLanguage": {
		    "sInfoEmpty": 'No entries to show',
		    "sEmptyTable": "No Data.",
		}
    	});  
$("#tag-for-booking").click(function(){
var rowcollection =  cancel_table.$(".wbid:checked", {"page": "all"});
var arr=[];
if($(".wbid").is(':checked')){
	rowcollection.each(function(index,elem){
	    var checkbox_value = $(elem).val();
	    //Do something with 'checkbox_value'
	    //alert(checkbox_value);
	    arr.push(checkbox_value);
	});
		
	$.ajax({
	     type: "POST",
	     url: base_url+'dashboard/dashboard/reBooking/'+arr,
	     data: {"ids":arr},  //assign the var here 
	     success: function(msg){
	        $(".msg").html(msg);
	        cancel_table.ajax.reload(null, false);
	     }
	});		
}else {
	alert("Check Item First.");
}			
});    	
    	
