<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['default_controller'] = "home/index";
//$route['sales'] = "sales/todos"; #temporary
$route['404_override'] = 'error_404';
$route['assets/(:any)'] = 'assets/$1';
//$route['admin'] = "admin/home";
//for admin
$route['admin'] = "admin/admin";
//admin homepage
$route['admin/dashboard'] = "admin/admin/dashboard";
//for admin dynamic form 
$route['admin/form/(:any)/(:any)'] = "admin/admin/form/$1/$2";
//for admin dynamic table
$route['admin/table/(:any)'] = "admin/admin/table/$1";
//admin menu items dynamic route
$route['admin/grid/(:any)'] =  "admin/admin/grid/$1";
$route['admin/orders_table/(:any)/(:any)']= "admin/admin/orders_table/$1/$2";
$route['admin/orders_grid/(:any)/(:any)']= "admin/admin/orders_grid/$1/$2";
$route['admin/orders_grid_table/(:any)/(:any)']= "admin/admin/orders_grid_table/$1/$2";
$route['admin/order_form/(:any)/(:any)']= "admin/admin/order_form/$1/$2";
 
//for home
$route['items/(:any)'] =  "home/index/items/$1";
$route['items_filter/(:any)'] =  "home/index/items_filter/$1/$2/$3";
$route['items_view/(:any)'] =  "home/index/items_view/$1";
$route['items_bytitle/(:any)'] =  "home/index/items_bytitle/$1/$2";
//$route['search'] =  "home/index/search";
$route['search/(:any)/(:any)'] =  "home/index/search/$1/$2";
$route['announcement/(:any)'] =  "home/index/announcement/$1";
$route['promo/(:any)'] =  "home/index/promo/$1";//sha eto ang routes dito nagawa tau alias ng link para hinde mahaba at di obvious na ci gmit ntin
$route['recent_items'] =  "home/index/recent_items";
$route['current_transaction'] =  "home/index/current_transaction";
$route['home/home_orders_table/(:any)'] =  "home/index/home_orders_table/$1";
$route['home/order_form/(:any)/(:any)']= "home/index/order_form/$1/$2";


$route['user_setup'] = "setup/setup";
$route['admin/logout'] = "admin/admin/logout";

/* End of file routes.php */
/* Location: ./application/config/routes.php */