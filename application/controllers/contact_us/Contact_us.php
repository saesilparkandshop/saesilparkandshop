<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends CI_Controller {

	    function __construct(){
        parent::__construct();
		//loading database or global functions
        $this->load->model('admin/admin_tbl','setup');
		//check if user setup pre data...
   		$this->load->model('home/home_tbl','home');
   		$this->load->model('contact_us/contactus_tbl','contact');
		}	
	public function index($msg = NULL){
			$data['title']='Sae sil ParknShop';
			//load menus.
			$data['logo_data']=$this->setup->getlogo();
			$data['brand_data']=$this->home->getBrand();
			$data['category_data']=$this->home->getCategoryMenu();
			//$data['scategory_data']=$this->home->getSCategory();
			//$data['home_model']=$this->home;
			$data['promo_data']=$this->home->promoData();
			$data['announcement_data']=$this->home->announcementData('top');
			
			//items
			$data['newitems_data']=$this->home->itemsData('new');
			$data['bestitems_data']=$this->home->itemsData('best_seller');
			//$data['onhanditems_data']=$this->home->itemsData('on_hand');
			
			
			$data['scripts']=array(assets_url()."js/home/home.js");
			$data['styles']=array(assets_url()."css/home/home.css");
			
			$this->load->view('home/home_header_page',$data);
			$this->load->view('contact_us/contactus_page',$data);
			$this->load->view('home/home_footer_page',$data);	
               		
    }
	
	function saveContacts(){
		 $name = $this->security->xss_clean($this->input->post('name'));
		 $email = $this->security->xss_clean($this->input->post('email'));
		 $subject = $this->security->xss_clean($this->input->post('subject'));
		 $message = $this->security->xss_clean($this->input->post('message'));
		 $date = $this->security->xss_clean($this->input->post('date'));
				
			$data = array(
                              
                              'name' => $name,
                              'email' => $email,
                              'subject' => $subject,
                              'message' => $message,
                              'date' => date('Y-m-d'),
                             
             );
            //var_dump($data);
            $insert_msg=$this->contact->create_contact($data);
			//die($insert_msg);
			if($insert_msg==1){
				$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Message Successfully sent.</div>');						
				redirect('contact_us/contact_us');											
			} else if ($insert_msg==0) {
	            $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong.</div>');	
	            redirect('contact_us/contact_us');					
			}

	}
    


}
