<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

	    function __construct(){
        parent::__construct();
		//phpinfo();
		//loading database or global functions
        $this->load->model('home/home_tbl','home');
		$this->load->model('admin/admin_tbl','setup');
		//check if user setup pre data...
   		$this->load->library('pagination');
   		$this->load->library('discount');
   		//	$this->load->driver('minify');
		//$this->output->enable_profiler(TRUE);
		//$this->session->flashdata('msgx');
		//
	
		//echo $this->encryption->decode("Kor1yiFkRXbpkhhCZ_F_lA34Z0jXKNK_zYNNBQXluQI");
		}		
		function index(){
			//phpinfo();
			$data['title']='Sae sil ParknShop';
			//load menus
			$data['logo_data']=$this->setup->getlogo();
			
			$data['brand_data']=$this->home->getBrand();
			$data['category_data']=$this->home->getCategoryMenu();

			//die(print_r($data['category_data']));
			//$data['scategory_data']=$this->home->getCategoryMenu();
			//die(var_dump($data['scategory_data']));
			//$data['home_model']=$this->home;
			$data['promo_data']=$this->home->promoData();
			$data['announcement_data']=$this->home->announcementData('top');
			
			//items
			$data['newitems_data']=$this->home->itemsData('new');
			$data['bestitems_data']=$this->home->itemsData('best_seller');
			$data['onhanditems_data']=$this->home->itemsData('on_hand');
			$data['images_data']=$this->home->itemsData('on_hand');
			
			//discounts
			$data['discount_conditions'] = $this->home->getDiscountConditions();
			foreach ($data['newitems_data'] as $key => $value) {
					$data['newitems_data'][$key] = $this->discount->setDiscountDisplay($data['discount_conditions'],$value);
			}

	       /* $i=0;
			 //var_dump($home_model);
			$html_menu='';
			foreach($data['brand_data'] as $brd_data){

		        $html_menu.= " <li class='dropdown-submenu'>
		            <a tabindex='0'>".$brd_data['brand_name']."</a>";													
				$html_menu.="<ul class='dropdown-menu'>";	 
		        foreach($data['category_data'] as $cat_data){
	        	// if($cat_data['brand_id']==$brd_data['brand_id']){
		         $html_menu.= " <li class='dropdown-submenu'>
		            <a tabindex='0'>".$cat_data['cat_name']."</a>";	
				// }	
					//$scategory_data=$home_model->getsCategoryMenu($cat_data['cat_id']);
					$html_menu.="<ul class='dropdown-menu'>";
					foreach($data['scategory_data'] as $scat_data){
						if($scat_data['cat_id']==$cat_data['cat_id']){
							$html_menu.= "					            
					              <li><a tabindex='0' href='".base_url('items_filter/'.$this->encryption->encode($brd_data['brand_name'])."/".$this->encryption->encode($cat_data['cat_name'])."/".$this->encryption->encode($scat_data['scat_name'])."'>".$scat_data['scat_name'])."</a></li>							
							";										
						}											
					}
					$html_menu.="</ul>";		
		       $html_menu.="   </li> "; 
		       
		      }
			$html_menu.="</ul>";
			$html_menu.="   </li> "; 		
		  }
		  //die($html_menu);
		  $data['menu']=$html_menu;*/	


			$data['scripts']=array(assets_url()."js/home/home.js");
			$data['styles']=array(assets_url()."css/home/home.css");
			
			$this->load->view('home/home_header_page',$data);
			$this->load->view('home/home_page',$data);
			$this->load->view('home/home_footer_page',$data);	
		}
		function login(){
		$data['login_msg']='';
			if($_POST){	
			$username = $this->security->xss_clean($this->input->post('username'));
			$password = $this->security->xss_clean($this->input->post('password'));
			$redirect_to = $this->security->xss_clean($this->input->post('redirect_to'));
				
				$account_chk=$this->home->AccountCheck($username);
				//if user exist

				if(!empty($account_chk)){
					
					if($account_chk['username']==$username && $this->encryption->decode($account_chk['password'])==$password){
							//check user if its active
							$if_active=$this->home->ActiveCheck($account_chk['status']);
							if($if_active['status']=="inactive"){
						    $data['login_msg']='<div class="alert alert-danger" role="alert">Account is not Active. </div>';								
							} else {					
							//initialize session					
							$this->session->set_userdata('full_name' , $account_chk['full_name']);
							$this->session->set_userdata('username' , $account_chk['username']);
							$this->session->set_userdata('user_id' , $account_chk['user_id']);
							$this->session->set_userdata('access_level' , $account_chk['access_level']);
							if($redirect_to != ""){
								redirect($redirect_to);
							}else{
							redirect("home");	
							}
							
							}												
					} else {
				    	$data['login_msg']='<div class="alert alert-danger" role="alert">Incorrect Username/Password. </div>';											
					}
				
				} else {
					$data['login_msg']='<div class="alert alert-danger" role="alert">User not yet exist.</div>';										
				}
					
			
			
			}//end post
			
			$data['title']='Sae sil ParknShop';
			//load menus
			$data['logo_data']=$this->setup->getlogo();
			
			$data['brand_data']=$this->home->getBrand();
			//$data['category_data']=$this->home->getCategoryMenu();
			//$data['scategory_data']=$this->home->getsCategoryMenu();
			//$data['home_model']=$this->home;
			//$data['promo_data']=$this->home->promoData();
			//$data['announcement_data']=$this->home->announcementData('top');
			
			//items
			//$data['newitems_data']=$this->home->itemsData('new');
			//$data['bestitems_data']=$this->home->itemsData('best_seller');
			//$data['onhanditems_data']=$this->home->itemsData('on_hand');
			
			$data['scripts']=array(assets_url()."js/home/home.js");
			$data['styles']=array(assets_url()."css/home/home.css");			
		            $this->load->view('home/login_header_page',$data);
					$this->load->view('home/login_page',$data);
					$this->load->view('home/home_footer_page',$data);				
		}	
		function search($items,$start=0){
			$search='';
			
			if($_POST){
			$search = $this->security->xss_clean($this->input->post('search'));
			$this->session->set_userdata('search_val',$search);
			} else {
			$search	=$this->session->userdata('search_val');
			}
			//echo $search;
			//die($search);
		        $cnt = $this->home->countSearch($search);

		        $config['base_url'] =base_url('search/items');
		        $config['total_rows'] = $cnt['cnt'];
				//die($cnt['cnt']);
		        $config['per_page'] = 8;
		       //fix pagination when brand new,preloved or search
		       if (count($_GET) > 0) $config['suffix'] = '/0' . http_build_query($_GET, '', "&");
		        $config['first_url'] = $config['base_url'].'/0'.http_build_query($_GET);
		       // $config['uri_segment'] = 3;
		
		            /*for bootstrap integration*/
		        $config['full_tag_open'] = "<ul class='pagination'>";
		        $config['full_tag_close'] ="</ul>";
		        $config['num_tag_open'] = '<li>';
		        $config['num_tag_close'] = '</li>';
		        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		        $config['next_tag_open'] = "<li>";
		        $config['next_tagl_close'] = "</li>";
		        $config['prev_tag_open'] = "<li>";
		        $config['prev_tagl_close'] = "</li>";
		        $config['first_tag_open'] = "<li>";
		        $config['first_tagl_close'] = "</li>";
		        $config['last_tag_open'] = "<li>";
		        $config['last_tagl_close'] = "</li>";
		
		    $this->pagination->initialize($config);
		            
		    $data['pagination'] = $this->pagination->create_links();					
				
			$search_item = $this->home->search($search,8,$start); 
			$data['items_data']=$search_item;
			$data['logo_data']=$this->setup->getlogo();
			$data['brand_data']=$this->home->getBrand();
			$data['category_data']=$this->home->getCategoryMenu();
			//$data['scategory_data']=$this->home->getsCategoryMenu();
			$data['scripts']=array(assets_url()."js/home/home.js");
			$data['styles']=array(assets_url()."css/home/home.css");
			$data['title_filter']='Search';
			$data['title']='Search Items';
			$data['search_value']=$search;	
			//discounts
			$data['discount_conditions'] = $this->home->getDiscountConditions();			
									
			$this->load->view('home/home_header_page',$data);
			$this->load->view('home/items_page',$data);
			$this->load->view('home/home_footer_page',$data);					
			}
		function items_filter($bfilter,$cfilter,$filter,$start=0){
			$bfilter=$this->encryption->decode($bfilter);
			$cfilter=$this->encryption->decode($cfilter);
			$filter=$this->encryption->decode($filter);
			$data['title']='Sae sil ParknShop';
			$data['discount_conditions'] = $this->home->getDiscountConditions();
		        $cnt = $this->home->itemsDatafilteredcountf($bfilter,$cfilter,$filter);
				//var_dump($cnt);
		        $config['base_url'] =base_url('items_filter/'.$bfilter.'/'.$cfilter.'/'.$filter);
		        $config['total_rows'] = $cnt["cnt"];
				//die($cnt['cnt']);
		        $config['per_page'] = 8;
		       //fix pagination when brand new,preloved or search
		       if (count($_GET) > 0) $config['suffix'] = '/0' . http_build_query($_GET, '', "&");
		        $config['first_url'] = $config['base_url'].'/0'.http_build_query($_GET);
		       // $config['uri_segment'] = 3;
		
		            /*for bootstrap integration*/
		        $config['full_tag_open'] = "<ul class='pagination'>";
		        $config['full_tag_close'] ="</ul>";
		        $config['num_tag_open'] = '<li>';
		        $config['num_tag_close'] = '</li>';
		        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		        $config['next_tag_open'] = "<li>";
		        $config['next_tagl_close'] = "</li>";
		        $config['prev_tag_open'] = "<li>";
		        $config['prev_tagl_close'] = "</li>";
		        $config['first_tag_open'] = "<li>";
		        $config['first_tagl_close'] = "</li>";
		        $config['last_tag_open'] = "<li>";
		        $config['last_tagl_close'] = "</li>";
		
		    $this->pagination->initialize($config);
		            
		    $data['pagination'] = $this->pagination->create_links();

			$data['logo_data']=$this->setup->getlogo();
			$data['brand_data']=$this->home->getBrand();
			$data['category_data']=$this->home->getCategoryMenu();
			//$data['scategory_data']=$this->home->getsCategoryMenu();
			$data['items_data']=$this->home->itemsDatafilteredf($bfilter,$cfilter,$filter,8,$start);
			$data['scripts']=array(assets_url()."js/home/home.js");
			$data['styles']=array(assets_url()."css/home/home.css");
			if($filter=='best_seller')
			$data['title_filter']="Best Seller";
			else if($filter=='new')
			$data['title_filter']="New Items";			
			else
			$data['title_filter']=ucwords($filter);	
			
			$data['filter']=$filter;	
			//discounts
			$data['discount_conditions'] = $this->home->getDiscountConditions();			
			$this->load->view('home/home_header_page',$data);
			$this->load->view('home/items_page',$data);
			$this->load->view('home/home_footer_page',$data);					
		}
		function items($filter,$start=0){
			$filter=urldecode($filter);				
			$data['title']='Sae sil ParknShop';
				$data['discount_conditions'] = $this->home->getDiscountConditions();
		        $cnt = $this->home->itemsDatafilteredcount($filter);
				//var_dump($cnt);
		        $config['base_url'] =base_url('items/'.$filter);
		        $config['total_rows'] = $cnt["cnt"];
				//die($cnt['cnt']);
		        $config['per_page'] = 8;
		       //fix pagination when brand new,preloved or search
		       if (count($_GET) > 0) $config['suffix'] = '/0' . http_build_query($_GET, '', "&");
		        $config['first_url'] = $config['base_url'].'/0'.http_build_query($_GET);
		       // $config['uri_segment'] = 3;
		
		            /*for bootstrap integration*/
		        $config['full_tag_open'] = "<ul class='pagination'>";
		        $config['full_tag_close'] ="</ul>";
		        $config['num_tag_open'] = '<li>';
		        $config['num_tag_close'] = '</li>';
		        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		        $config['next_tag_open'] = "<li>";
		        $config['next_tagl_close'] = "</li>";
		        $config['prev_tag_open'] = "<li>";
		        $config['prev_tagl_close'] = "</li>";
		        $config['first_tag_open'] = "<li>";
		        $config['first_tagl_close'] = "</li>";
		        $config['last_tag_open'] = "<li>";
		        $config['last_tagl_close'] = "</li>";
		
		    $this->pagination->initialize($config);
		            
		    $data['pagination'] = $this->pagination->create_links();

			$data['logo_data']=$this->setup->getlogo();
			$data['brand_data']=$this->home->getBrand();
			$data['category_data']=$this->home->getCategoryMenu();
			//$data['scategory_data']=$this->home->getsCategoryMenu();
			$data['items_data']=$this->home->itemsDatafiltered($filter,8,$start);
			$data['scripts']=array(assets_url()."js/home/home.js");
			$data['styles']=array(assets_url()."css/home/home.css");
			if($filter=='best_seller')
			$data['title_filter']="Best Seller";
			else if($filter=='new')
			$data['title_filter']="New Items";			
			else
			$data['title_filter']=ucwords($filter);	
			
			$data['filter']=$filter;	
			//discounts
			$data['discount_conditions'] = $this->home->getDiscountConditions();			
			$this->load->view('home/home_header_page',$data);
			$this->load->view('home/items_page',$data);
			$this->load->view('home/home_footer_page',$data);				
		}
		function items_bytitle($type,$id,$start=0){
			$id=$this->encryption->decode($id);				
			$data['title']='Sae sil ParknShop';
				$data['discount_conditions'] = $this->home->getDiscountConditions();
		        $cnt = $this->home->itemBytypeCount($type,$id);
				//var_dump($cnt);
		        $config['base_url'] =base_url('items_bytitle/'.$type.'/'.$id);
		        $config['total_rows'] = $cnt["cnt"];
				//die($cnt['cnt']);
		        $config['per_page'] = 8;
		       //fix pagination when brand new,preloved or search
		       if (count($_GET) > 0) $config['suffix'] = '/0' . http_build_query($_GET, '', "&");
		        $config['first_url'] = $config['base_url'].'/0'.http_build_query($_GET);
		       // $config['uri_segment'] = 3;
		
		            /*for bootstrap integration*/
		        $config['full_tag_open'] = "<ul class='pagination'>";
		        $config['full_tag_close'] ="</ul>";
		        $config['num_tag_open'] = '<li>';
		        $config['num_tag_close'] = '</li>';
		        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		        $config['next_tag_open'] = "<li>";
		        $config['next_tagl_close'] = "</li>";
		        $config['prev_tag_open'] = "<li>";
		        $config['prev_tagl_close'] = "</li>";
		        $config['first_tag_open'] = "<li>";
		        $config['first_tagl_close'] = "</li>";
		        $config['last_tag_open'] = "<li>";
		        $config['last_tagl_close'] = "</li>";
		
		    $this->pagination->initialize($config);
		            
		    $data['pagination'] = $this->pagination->create_links();

			$data['logo_data']=$this->setup->getlogo();
			$data['brand_data']=$this->home->getBrand();
			$data['category_data']=$this->home->getCategoryMenu();
			//$data['scategory_data']=$this->home->getsCategoryMenu();
			$data['items_data']=$this->home->itemBytype($type,$id,8,$start);
			$data['scripts']=array(assets_url()."js/home/home.js");
			$data['styles']=array(assets_url()."css/home/home.css");
			
			$data['title_filter']=ucwords($type)." (".$id.")";	
			//discounts
			$data['discount_conditions'] = $this->home->getDiscountConditions();			

			
			$this->load->view('home/home_header_page',$data);
			$this->load->view('home/items_page',$data);
			$this->load->view('home/home_footer_page',$data);					
		}
		function items_view($rawid){
			$id=$this->encryption->decode($rawid);
			$data['title']='Sae sil ParknShop';
			$data['title_filter']='Item';
			$data['logo_data']=$this->setup->getlogo();
			$data['brand_data']=$this->home->getBrand();
			$data['category_data']=$this->home->getCategoryMenu();
			//$data['scategory_data']=$this->home->getsCategoryMenu();
			$data['scripts']=array(assets_url()."js/home/home.js", assets_url()."js/home/cart.js"); 
			$data['styles']=array(assets_url()."css/home/home.css");
			//load items
			$data['item_data']=$this->home->getItem($id);
			$data['img_data']=$this->home->getImg($id);
			$data['is_discount'] = false;
			$data['discount_conditions'] = $this->home->getDiscountConditions();

			$data['item_data'] = $this->discount->setDiscountDisplay($data['discount_conditions'],$data['item_data']);

/*			foreach ($data['discount_conditions'] as $key => $value) {
				if($value['promo_type'] == "brand"){
					if($value['value'] == $data['item_data']['brand_id']){
						$data['is_discount'] = true;
						$data['discount_percent'] = $value['discount_percent'];
						break;
					}

				}else if($value['promo_type'] == "item"){
					if($value['value'] == $data['item_data']['item_id']){
						$data['is_discount'] = true;
						$data['discount_percent'] = $value['discount_percent'];
						break;
					}

				} 
			}		*/	
			$this->load->view('home/home_header_page',$data);
			$this->load->view('home/itemsview_page',$data);
			$this->load->view('home/home_footer_page',$data);				
		}
		function announcement($filter,$start=0){
			//filter=top, all or id
			$data['title']="Announcements";
			$data['logo_data']=$this->setup->getlogo();
			$data['brand_data']=$this->home->getBrand();
			$data['category_data']=$this->home->getCategoryMenu();
			//$data['scategory_data']=$this->home->getsCategoryMenu();
			$data['scripts']=array(assets_url()."js/home/home.js");
			$data['styles']=array(assets_url()."css/home/home.css");		
			if($filter=='all'){				
		        $cnt = $this->home->countAnnouncement();

		        $config['base_url'] =base_url('announcement/all');
		        $config['total_rows'] = $cnt;
		        $config['per_page'] = 5;
		       //fix pagination when brand new,preloved or search
		       if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		        $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		       // $config['uri_segment'] = 3;
		
		            /*for bootstrap integration*/
		        $config['full_tag_open'] = "<ul class='pagination'>";
		        $config['full_tag_close'] ="</ul>";
		        $config['num_tag_open'] = '<li>';
		        $config['num_tag_close'] = '</li>';
		        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		        $config['next_tag_open'] = "<li>";
		        $config['next_tagl_close'] = "</li>";
		        $config['prev_tag_open'] = "<li>";
		        $config['prev_tagl_close'] = "</li>";
		        $config['first_tag_open'] = "<li>";
		        $config['first_tagl_close'] = "</li>";
		        $config['last_tag_open'] = "<li>";
		        $config['last_tagl_close'] = "</li>";
		
		       $this->pagination->initialize($config);
		            
		       $data['pagination'] = $this->pagination->create_links();	
			   $data['announce_all']=$this->home->announcementData($filter,5,$start);			
			} else {
				$filter = $this->encryption->decode($filter);
				$data['announce_byid']=$this->home->announcementData($filter,'','');
			}			

			$data['filter']=$filter;
			$this->load->view('home/home_header_page',$data);
			$this->load->view('home/announcement_page',$data);
			$this->load->view('home/home_footer_page',$data);					
		}

		function Process(){
				if($_POST){
					//loading form details
					$uid=$this->security->xss_clean($this->input->post('idx'));
					$UserName = $this->security->xss_clean($this->input->post('UserName'));
					$Password = $this->security->xss_clean($this->input->post('Password'));	
					$Access_level="user";	
					
					$FirstName = $this->security->xss_clean($this->input->post('FirstName'));	
					$MiddleName = $this->security->xss_clean($this->input->post('MiddleName'));	
					$LastName = $this->security->xss_clean($this->input->post('LastName'));
					$gender = $this->security->xss_clean($this->input->post('gender'));
					
					$address = $this->security->xss_clean($this->input->post('address'));	
					$email = $this->security->xss_clean($this->input->post('email'));		
					$contact_no = $this->security->xss_clean($this->input->post('contact_no'));
					$fb_url = $this->security->xss_clean($this->input->post('fb_url'));
					
					$account_name = $this->security->xss_clean($this->input->post('account_name'));
					$account_no = $this->security->xss_clean($this->input->post('account_no'));
										
					$status="inactive";	
					$user_type="regular";
					$FullName=$FirstName." ".$MiddleName." ".$LastName;		
					$created_by='online';	
					//generate user id
					$user_id= $this -> setup -> getUserid();
						if ($user_id) {
							$user_id = $user_id["id"];;
							//$user_id = substr($user_id, -10);
							$uid = "USER" . sprintf("%010d", $user_id + 1);
						} else {
							$uid = "USER" . sprintf("%010d", 1);
						}					
					
						//prepareing data....
						$data=array(
						"user_id"=>$uid,
						"full_name"=>$FullName,
						"first_name"=>$FirstName,
						"middle_name"=>$MiddleName,
						"last_name"=>$LastName,
						"gender"=>$gender,
						"address"=>$address,
						"fb_url"=>$fb_url,
						"account_name"=>$account_name,
						"account_no"=>$account_no,
						"email"=>$email,
						"contact_no"=>$contact_no,						
						"username"=>$UserName,
						"password"=>$this->encryption->encode($Password),
						"access_level"=>$Access_level,
						"user_type"=>$user_type,
						"created_by"=>$created_by,
						"status"=>$status,
						"activation_date"=>date('Y-m-d H:i:s'),	
						"activated_by"=>$created_by			
						);						
						//check if username already exist
						$user_exist= $this -> setup -> UserExist($UserName);
						
						if($user_exist){
							//$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Username already Exist.</div>');						
							//redirect('admin/admin/form/users/a');	
					        echo json_encode([
					        	'success' => false,
					        	'msg' => '<div class="alert alert-danger" role="alert">Username already Exist.</div>'
					        ]);				
						} else {
							//check if email already exist
							$email_exist= $this -> setup -> EmailExist($email);
							if($email_exist){
						        echo json_encode([
						        	'success' => false,
						        	'msg' => '<div class="alert alert-danger" role="alert">Email already Exist.</div>'
						        ]);									
							} else {												
								//insert data
								$insert_data=$this->setup->saveUser($data);
								//
								if($insert_data==1){
								$verification_link = base_url('home/index/verify?key=' . $this->encryption->encode($uid) . '&email=' . $email);
								$email_message = '
			                                Thank you for your Registration. To continue, please click the verification link: <br/>
			                                <a href="' . $verification_link . '" title="Verify Email">' . $verification_link . '</a>
			                                ';
						        $this->load->library('email');
						
						            $config['protocol']    = 'smtp';
						
						            $config['smtp_host']    = 'localhost';//'ssl://smtp.gmail.com';
						
						            $config['smtp_port']    = '25';
						
						            $config['smtp_timeout'] = '60';
						
						            $config['smtp_user']    = 'noreply@saesilparknshop.com';//'saesil.parkenshop2015@gmail.com';
						
						            $config['smtp_pass']    = '032119902ne1';//'Saesil2015';
						
						            $config['charset']    = 'utf-8';//or utf-8
						
						            $config['newline']    = "\r\n";
						
						            $config['mailtype'] = 'html'; // or html
						
						            $config['validation'] = FALSE; // bool whether to validate email or not      
						
						            $this->email->initialize($config);
						
						
						            $this->email->from('saesil.parkenshop-noreply@saesilparknshop.com', 'SaesilParkenshop');
						            $this->email->to($email); 
						
						
						            $this->email->subject('Email Verification');
						
						            $this->email->message($email_message);  
						
						            $this->email->send();
						
						            //echo $this->email->print_debugger();			
								/*if(!){
							        echo json_encode([
							        	'success' => false,
							        	'msg' => '<div class="alert alert-danger" role="alert">
							        	Email already Exist.
							        	</div>'
							        ]);											
								}*/								
								//logging activity for audit..	
								$req=array(
								"action"=>"Create User",
								"action_by"=>$created_by,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);			
								//$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">User Successfully Made.</div>');	
								//redirect('admin/admin/form/users/a');
						        echo json_encode([
						        	'success' => true,
						        	'msg' => '<div class="alert alert-success" role="alert">Thank You for your registering in SaesilParkenshop. Please check your email '.$email.' for confirmation. 
						        	</div>'
						        ]);																									        
								} else if($insert_data==0) {
								//$this->session->set_flashdata('msg','					
								//<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
								//redirect('admin/admin/form/users/a');	
						        echo json_encode([
						        	'success' => false,
						        	'msg' => '<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>'
						        ]);																		
								}
							}					
					   }						
					
					
			 }//end post	
		}
		function verify() {
			
			$idx =urlencode($this -> security -> xss_clean($this -> input -> get('key')));
			$id=$this->encryption->decode($idx);
			//die($id);
			$email = $this -> security -> xss_clean($this -> input -> get('email'));
	
			if ($id != "" && $email != "") {
			$verify	=$this -> home -> verifyAccount($id, $email);
			//die(var_dump($verify));
				if (!empty($verify)) {
					$activate_account=$this -> home -> activateAccount($id);
					if($activate_account==1){
					$data['msgx']='<div class="alert alert-success" role="alert">Your account has been <b>Activated.</b>,You may now <a class="btn-link prof_modal"  data-toggle="modal" href="#loginx"  id="login-btn" data-target="#login-modal" style="color:#fff;">LOGIN</a> .</div>';	
					} else if($activate_account==0){
					$data['msgx']='<div class="alert alert-danger" role="alert">Acount not Activated.</div>';	
					}					
				} else {
					$data['msgx']='<div class="alert alert-danger" role="alert">Acount not Activated.</div>';
				}
			}
			$data['title']='Verify';
			//load menus
			$data['category_data']=$this->home->getCategoryMenu();
			//$data['scategory_data']=$this->home->getsCategoryMenu();
			//$data['home_model']=$this->home;

			//$data['onhanditems_data']=$this->home->itemsData('on_hand')
			$data['scripts']=array(assets_url()."js/home/home.js");
			$data['styles']=array(assets_url()."css/home/home.css");
			
			$this->load->view('home/home_header_page',$data);
			$this->load->view('home/verify_page',$data);
			$this->load->view('home/home_footer_page',$data);		
		}
		function logout(){
			$this->session->sess_destroy();
			redirect("home");
		}		
		function resend_email(){
			$data['title']='Resend Confirmation';
			//load menus
			$data['logo_data']=$this->setup->getlogo();
			$data['brand_data']=$this->home->getBrand();					
			$data['category_data']=$this->home->getCategoryMenu();
			//$data['scategory_data']=$this->home->getsCategoryMenu();
			//$data['home_model']=$this->home;
			//when resending email
			if($_POST){
				$email=$this->security->xss_clean($this->input->post("email"));
				//check email
				$get_email=$this->home->getUserByEmail($email);
				//die($this->encryption->encode($get_email['user_id']));
				if(empty($get_email)){
					$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Email not registered.</div>');							
		            redirect('home/index/resend_email');					
				} else if($get_email['status']=='active'){
					$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">User on this email already Activated.</div>');							
		            redirect('home/index/resend_email');					
				}else {
				$verification_link = base_url('home/index/verify?key=' . $this->encryption->encode($get_email['user_id']) . '&email=' . $email);
				$email_message = '
                            Thank you for your Registration. To continue, please click the verification link: <br/>
                            <a href="' . $verification_link . '" title="Verify Email">' . $verification_link . '</a>
                            ';
		        $this->load->library('email');
		
				            $config['protocol']    = 'smtp';
				
				            $config['smtp_host']    = 'localhost';//'ssl://smtp.gmail.com';
				
				            $config['smtp_port']    = '25';
				
				            $config['smtp_timeout'] = '60';
				
				            $config['smtp_user']    = 'noreply@saesilparknshop.com';//'saesil.parkenshop2015@gmail.com';
				
				            $config['smtp_pass']    = '032119902ne1';//'Saesil2015';
				
				            $config['charset']    = 'utf-8';//or utf-8
				
				            $config['newline']    = "\r\n";
				
				            $config['mailtype'] = 'html'; // or html
				
				            $config['validation'] = FALSE; // bool whether to validate email or not      
				
				            $this->email->initialize($config);
				
				
				            $this->email->from('saesil.parkenshop-noreply@saesilparknshop.com', 'SaesilParkenshop');

		
		            $this->email->subject('Email Verification');
		
		            $this->email->message($email_message);  
		
		            if($this->email->send()){
					$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Confirmation Successfully Resend.</div>');							
		            redirect('home/index/resend_email');		            	
		            } else{
					$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something Went Wrong.</div>');							
		            redirect('home/index/resend_email');			            	
		            }
											
				}
			}

			//$data['onhanditems_data']=$this->home->itemsData('on_hand');
			
			
			$data['scripts']=array(assets_url()."js/home/home.js");
			$data['styles']=array(assets_url()."css/home/home.css");
						
			$this->load->view('home/home_header_page',$data);
			$this->load->view('home/resend_page',$data);
			$this->load->view('home/home_footer_page',$data);				
		}		
		function about(){
			//shannele
			$data['title']='About Us';
			$data['logo_data']=$this->setup->getlogo();			
			//load menus
			$data['brand_data']=$this->home->getBrand();			
			$data['category_data']=$this->home->getCategoryMenu();
			//$data['scategory_data']=$this->home->getsCategoryMenu();
			//$data['home_model']=$this->home;
			//do load the about us data here	
			$data['about_data']=$this->home->getAbout();
			$data['scripts']=array(assets_url()."js/home/home.js");
			$data['styles']=array(assets_url()."css/home/home.css");			
						
			$this->load->view('home/home_header_page',$data);
			$this->load->view('home/about_page',$data);
			$this->load->view('home/home_footer_page',$data);						
		}
		function terms(){
			//shannele
			$data['title']='Terms and Conditions';
			$data['logo_data']=$this->setup->getlogo();			
			//load menus
			$data['brand_data']=$this->home->getBrand();			
			$data['category_data']=$this->home->getCategoryMenu();
			//$data['scategory_data']=$this->home->getsCategoryMenu();
			$data['home_model']=$this->home;
			//do load the about us data here	
			$data['terms_data']=$this->home->getTerms();
			
			$data['scripts']=array(assets_url()."js/home/home.js");
			$data['styles']=array(assets_url()."css/home/home.css");			
						
			$this->load->view('home/home_header_page',$data);
			$this->load->view('home/terms_page',$data);
			$this->load->view('home/home_footer_page',$data);						
		}
		//start developer
		function developer(){
			//shannele
			$data['title']='Developers';
			$data['logo_data']=$this->setup->getlogo();			
			//load menus
			$data['brand_data']=$this->home->getBrand();			
			$data['category_data']=$this->home->getCategoryMenu();
			//$data['scategory_data']=$this->home->getsCategoryMenu();
			$data['home_model']=$this->home;
			//do load the about us data here	
			//$data['developer_data']=$this->home->getdeveloper();
			
			$data['scripts']=array(assets_url()."js/home/home.js");
			$data['styles']=array(assets_url()."css/home/home.css");			
						
			$this->load->view('home/home_header_page',$data);
			$this->load->view('home/developer',$data);
			$this->load->view('home/home_footer_page',$data);						
		}		//end developer
		function promo($id){
			$data['title']='Promo';
			$data['logo_data']=$this->setup->getlogo();			
			//load menus
			$data['brand_data']=$this->home->getBrand();			
			$data['category_data']=$this->home->getCategoryMenu();
			//$data['scategory_data']=$this->home->getsCategoryMenu();
			$data['home_model']=$this->home;
			//do load the promo data here	
			$id= $this->encryption->decode($id);
			$data['promo_data']=$this->home->getPromo($id);
			
			$data['scripts']=array(assets_url()."js/home/home.js");
			$data['styles']=array(assets_url()."css/home/home.css");	
			
			$this->load->view('home/home_header_page',$data);
			$this->load->view('home/promo_view_page',$data);
			$this->load->view('home/home_footer_page',$data);							
		}
		//start of profile
		function profile(){
			//shannele
			$this->home->CheckUserIfLogin();
			$data['title']='Profile';
			$user_id= $this->session->userdata('user_id');
			$data['logo_data']=$this->setup->getlogo();			
			//load menus
			$data['brand_data']=$this->home->getBrand();			
			$data['category_data']=$this->home->getCategoryMenu();
			//$data['scategory_data']=$this->home->getsCategoryMenu();
			$data['home_model']=$this->home;	
			$data['profile_data']=$this->home->getProfile($user_id);
			
			$data['scripts']=array(assets_url()."js/home/home.js");
			$data['scripts']=array(assets_url()."js/home/profile.js");
			$data['styles']=array(assets_url()."css/home/home.css");			
						
			$this->load->view('home/home_header_page',$data);
			$this->load->view('home/profile',$data);
			$this->load->view('home/home_footer_page',$data);	
		}//end profile

		function ajax_check_session(){
		 	if(!$this->session->userdata('user_id')){
			$this->session->set_flashdata('login_status', 'Please log in first before checking out');
			echo json_encode(array("logged_in" => '0'));
			}else{
			echo json_encode(array("logged_in" => '1'));	
			}
		}
		function recent_items(){
				$status = 'completed';
				$user_id=$this->session->userdata('user_id');
				$get_data= $this->home->recent_orders_data($status,$user_id);
				if(empty($get_data)){
					$finalres=array(
					"data"=>''
					);					
				} else {
					foreach($get_data as $table_data){
						$result[]=array(
								ucfirst($table_data['date_created']),
								ucfirst($table_data['batch_id']),
								ucfirst($table_data['order_id']),
								ucfirst($table_data['client_name']),
								ucfirst($table_data['qty']),
								"<a href='".base_url()."admin/order_form/".$status."/".$this->encryption->encode($table_data['order_id'])."' class='btn-link' title='Transaction'>
								<span class='fa fa-pencil-square-o fa-3'></span>
								</a>
								",
						);							
					}
					$finalres=array(
					"data"=>$result
					);
				}		
				echo json_encode($finalres);								
		}
		function current_transaction(){
			$this->home->CheckUserIfLogin();
			$data['title']='Orders List';
			$user_id= $this->session->userdata('user_id');
			$data['logo_data']=$this->setup->getlogo();			
			//load menus
			$data['brand_data']=$this->home->getBrand();			
			$data['category_data']=$this->home->getCategoryMenu();
			//$data['scategory_data']=$this->home->getsCategoryMenu();
			//$data['home_model']=$this->home;	
			$data['scripts']=array(assets_url()."js/home/orders.js");
			
			$this->load->view('home/home_header_page',$data);
			$this->load->view('home/orders_page',$data);
			$this->load->view('home/home_footer_page',$data);							
		}
		function home_orders_table($status){
				$user_id=$this->session->userdata('user_id');
				$get_data= $this->home->recent_orders_data($status,$user_id);
				if(empty($get_data)){
					$finalres=array(
					"data"=>''
					);					
				} else {
					foreach($get_data as $table_data){
						$result[]=array(
								ucfirst($table_data['date_created']),
								ucfirst($table_data['batch_id']),
								ucfirst($table_data['order_id']),	
								ucfirst($table_data['qty']),
								"<a href='".base_url()."home/order_form/".$status."/".$this->encryption->encode($table_data['order_id'])."' class='btn-link' title='Transaction'>
								<span class='fa fa-pencil-square-o fa-3'></span>
								</a>
								",
						);							
					}
					$finalres=array(
					"data"=>$result
					);
				}		
				echo json_encode($finalres);				
		}
		function order_form($status,$order_id){
		$this->home->CheckUserIfLogin();
		$user_id= $this->session->userdata('user_id');
		$data['logo_data']=$this->setup->getlogo();			
		//load menus
		$data['brand_data']=$this->home->getBrand();			
		$data['category_data']=$this->home->getCategoryMenu();
		//$data['scategory_data']=$this->home->getsCategoryMenu();
		//$data['home_model']=$this->home;			
		$order_id = $this->encryption->decode($order_id);
		$data['status']=$status;
		$data['title']= strtoupper("TRANSACTION");
		$data['transaction_mother']=$this->setup->transaction_data_mother($status,$order_id);
		//die(var_dump($data['transaction_mother']));
		$data['transaction_payment']=$this->home->transaction_data_payment($data['transaction_mother']['id']);
		$data['transaction_baby']=$this->setup->transaction_data_baby($status,$order_id);
		$data['couriers']=$this->setup->getCourier();
		
		$data['scripts']=array(assets_url()."js/home/".$status.".js");
	
			$this->load->view('home/home_header_page',$data);
			$this->load->view('home/ordersform_page',$data);
			$this->load->view('home/home_footer_page',$data);							
		}		
		function Process_order($status,$order_id){
			//load form elements...
			if($_POST){
				$trans_code=$this->security->xss_clean($this->input->post('trans_code'));
				$courier=$this->security->xss_clean($this->input->post('courier'));
				$amount=$this->security->xss_clean($this->input->post('amount'));
				$index = $this->security->xss_clean($this->input->post('index'));
				$id =$this->security->xss_clean($this->input->post('idx'));
				$order_id =$this->security->xss_clean($this->input->post('order_id'));
				$payment_id =$this->security->xss_clean($this->input->post('payment_id'));
				$paypic=$this->security->xss_clean($this->input->post('paypic'));
				$paythumbs=$this->security->xss_clean($this->input->post('paythumbs'));
				$pay_index=$this->security->xss_clean($this->input->post('index'));
				$local_courier =$this->security->xss_clean($this->input->post('local_courier'));
				$img_ctr=0;
				$images_data=[];
				if($status=="active" || $status=="predelivery"){
				foreach($_FILES as $key => $value){
							//echo $key;
						//for($i=0;$i<count($_FILES["upload"]['name']);$i++){
						if($_FILES[$key]['name']){
									 $config['image_library'] = 'gd2';
									 $filename="".date("Y")."_".time().$img_ctr."_".$img_ctr.".png";
									 $config['upload_path'] = './assets/img/payments/';
									 $config['allowed_types'] = 'gif|jpg|png|PNG|jpeg|JPG|JPEG';	
									 $config['file_name'] = $filename;
									 $config['encrypt_name']= FALSE;
									 
									 //load ci upload						
									 $this->upload->initialize($config);
									 //$this->upload->do_upload($key);
									if ( ! $this->upload->do_upload($key)) {
										//$this->upload->display_errors();
										$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Upload Failed.</div>');						
										redirect('home/order_form/'.$status.'/'.$this->encryption->encode($order_id).'');	
									} else {
										$lastupload_img=$this->upload->data();
										
										
										//var_dump($lastupload_img);
										//die();
										//die(var_dump($lastupload_img));
										//$lastupload_img = $this->upload->file_name;
										//Image Resizing
										$config['source_image'] = $lastupload_img['full_path'];
										//$config['new_image'] = $filename;
										$config['maintain_ratio'] = FALSE;
										$config['width'] = 225;
										$config['height'] = 333;
										$config['create_thumb'] = FALSE;	
										$config['new_image'] = $lastupload_img['raw_name']."-thumb.png";							
										$this->load->library('image_lib');
										$this->image_lib->initialize($config);
										if ( ! $this->image_lib->resize()){
											$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Image Resizing Failed.</div>');						
											redirect('home/order_form/'.$status.'/'.$this->encryption->encode($order_id).'');	
										} else {
											
										$images_data[]=array(
										"image_name"=>$this->image_lib->file_name,
										"image_thumbs"=>$this->image_lib->new_image,
										"index"=>$img_ctr,
										);	
										
										
										
										
										$img_ctr++;									
										}															
									}							 
	 						     
							} else {
										$images_data[]=array(
										"image_name"=>$paypic[$img_ctr],
										"image_thumbs"=>$paythumbs[$img_ctr],
										"index"=>$pay_index[$img_ctr],	
										);	
										$img_ctr++;									
							}							
							
						}	//end of foreach
						//saving payment...
						 $save_payment='';
						 // //die(var_dump($images_data));
						 for($i=0;$i<$img_ctr;$i++){

							//die(var_dump($payment_id));

								if($payment_id[$i]==''){
							 	$filename='';
								$thumbs=''; 
								$index_data='';									
								 	if(isset($images_data[$i])){
								 		if($images_data[$i]['image_name']!=''){
										 	$filename=$images_data[$i]['image_name'];	
											$thumbs=$images_data[$i]['image_thumbs'];	
											$index_data=$images_data[$i]['index'];								 			
								 		} else{
										 	$filename=$paypic[$i];	
											$thumbs=$paythumbs[$i];
											$index_data=$pay_index[$i];									 			
								 		}
								 	}else{
								 	$filename=$paypic[$i];	
									$thumbs=$paythumbs[$i];
									$index_data=$pay_index[$i];	
								 	} 	
								 	
								 							
								$data=array(
								"order_id"=>$id,
								"control_number"=>$trans_code[$i],
								"courrier"=>$courier[$i],
								"amount"=>$amount[$i],
								"img_filename"=> $filename,
								"img_filename_thumbs"=>$thumbs,
								"index"=>$index_data,
								);
								
									//insert.. data...	
									$save_payment=$this->home->savePayment($data);
					
								} else if($payment_id[$i]!=''){
								 	$filename='';
									$thumbs=''; 
									$index_data='';
									//die(array_key_exists($i,$images_data));
								 	if(isset($images_data[$i])){
								 		//die($images_data[$i]['image_name']);
								 		if($images_data[$i]['image_name']!=''){
										 	$filename=$images_data[$i]['image_name'];	
											$thumbs=$images_data[$i]['image_thumbs'];	
											$index_data=$images_data[$i]['index'];								 			
								 		} else{
										 	$filename=$paypic[$i];	
											$thumbs=$paythumbs[$i];
											$index_data=$pay_index[$i];									 			
								 		}
								 	}else{
								 	$filename=$paypic[$i];	
									$thumbs=$paythumbs[$i];
									$index_data=$pay_index[$i];	
								 	} 		
								 	//die($filename.$thumbs.$index_data);						
									$data=array(
									"control_number"=>$trans_code[$i],
									"courrier"=>$courier[$i],
									"amount"=>$amount[$i],
									"img_filename"=> $filename,
									"img_filename_thumbs"=>$thumbs,
									"index"=>$index_data,
									);	
									//die($filename."-".$thumbs."-".$index_data);	
									//update payment
									$save_payment=$this->home->updatePayment($data,$payment_id[$i]);							
								}
														
						} 
						//update local courier
						$ts=[
						"courier"=>$local_courier,
						];
						$update_order_summary=$this->home->updateorder_summary($ts,$id);


									if($save_payment==1){
										$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Payment Successfully Made.</div>');	
										redirect('home/order_form/'.$status.'/'.$this->encryption->encode($order_id).'');

									}else if($save_payment==0) {
										$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');	
										redirect('home/order_form/'.$status.'/'.$this->encryption->encode($order_id).'');								
									}		
						}//end of if status										
				
			}//end post
			
			
		}
				
} //end of controller	