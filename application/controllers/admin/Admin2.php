<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin2 extends CI_Controller {

	    function __construct(){
        parent::__construct();
		//loading database or global functions
		$this->load->model('admin/admin_tbl','setup');
        $this->load->model('admin/admin_tbl2','setup2');
		$this->load->helper(array('form', 'url'));
		//check if user setup pre data...
   		$this->session->flashdata('msg');
   		
		}
		
		function announcement(){//for loading the view of announcement
			$module= "announcement";
			$data['module']= "announcement";
			$data['title']= "announcement";
			//load admin dynamic js and css when creation go to assets js/admin/then name of module ex items.js same with css
			$data['scripts']=array(assets_url()."js/admin/".$module.".js");
			$data['styles']=array(assets_url()."css/admin/".$module.".css");			
			
			$this->load->view('admin/admin_header_page',$data);
			$this->load->view('admin/announcement/announcement_page',$data); //<-- here the format is application/view/admin/"module name ex items"/items_page.php
			$this->load->view('admin/admin_footer_page',$data);				
		}
		function announcement_table(){
				$get_data= $this->setup2->announcement_table();
				if(empty($get_data)){
					$finalres=array(
					"data"=>''
					);					
				} else {
					foreach($get_data as $table_data){
						$result[]=array(
								ucfirst($table_data['date_announce']),
								ucfirst($table_data['subj']),
								ucfirst($table_data['message']),
								"<a href='".base_url()."admin/form/".$table_data['id'].".aspx' class='btn-link' title='Edit'>
								<span class='glyphicon glyphicon-edit'></span>
								</a>
								"								
								//date('m/d/Y g:i a',strtotime($table_data['action_date'])),						
						);							
					}
					$finalres=array(
					"data"=>$result
					);			
				
				}
		
		}
		function announcement_form($id){
				$module='announcement';
				$data['title']=strtoupper($module);
				
				if($id=='a'){//<--a = add hehehe
				$data['title']="Create ".$module;
				$data['module']=$module;				
				$data['user_data']='';// <-- when creating/adding things this need to be empty.	
				} else {//edit 
				$data['title']="Edit ".$module;
				$data['module']=$module;				
				$data['announcement']=$this->setup->getdata($module,$id);	
				}
		    $this->load->view('admin/admin_header_page',$data);
			$this->load->view('admin/'.$module.'/'.$module.'form_page',$data); //<-- here the format is application/view/admin/"module name ex items"/itemsform_page.php
			$this->load->view('admin/admin_footer_page',$data);							
		}
		
		//about//
		function about_us(){//for loading the view of about_us
			$module= "about_us";
			$data['module']= "about_us";
			$data['title']= "About us";
			//load admin dynamic js and css when creation go to assets js/admin/then name of module ex items.js same with css
			$data['scripts']=array(assets_url()."js/admin/".$module.".js");
			$data['styles']=array(assets_url()."css/admin/".$module.".css");			
			
			$this->load->view('admin/admin_header_page',$data);
			$this->load->view('admin/about_us/about_us_page',$data); //<-- here the format is application/view/admin/"module name ex items"/items_page.php
			$this->load->view('admin/admin_footer_page',$data);				
		}
		function about_table(){
				$get_data= $this->setup2->about_table();
				if(empty($get_data)){
					$finalres=array(
					"data"=>''
					);					
				} else {
					foreach($get_data as $table_data){
						$result[]=array(
								ucfirst($table_data['id']),
								ucfirst($table_data['about']),
								ucfirst($table_data['contacts']),
								"<a href='".base_url()."admin/admin2/about_us_page/".$table_data['id'].".aspx' class='btn-link' title='Edit'>
								<span class='glyphicon glyphicon-edit'></span>
								</a>
								"								
								//date('m/d/Y g:i a',strtotime($table_data['action_date'])),						
						);							
					}
					$finalres=array(
					"data"=>$result
					);			
				
				}
		
		}
		function about_us($id){
				$module='about_us';
				$data['title']=strtoupper($module);
				
				if($id=='a'){//<--a = add hehehe
				$data['title']="Create ".$module;
				$data['module']=$module;				
				$data['user_data']='';// <-- when creating/adding things this need to be empty.	
				} else {//edit 
				$data['title']="Edit ".$module;
				$data['module']=$module;				
				$data['about_us']=$this->setup->getdata($module,$id);	
				}
		    $this->load->view('admin/admin_header_page',$data);
			$this->load->view('admin/'.$module.'/'.$module.'form_page',$data); //<-- here the format is application/view/admin/"module name ex items"/itemsform_page.php
			$this->load->view('admin/admin_footer_page',$data);							
		}
		//end about//
		
		
}		