<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	    function __construct(){
        parent::__construct();
		//loading database or global functions
        $this->load->model('admin/admin_tbl','setup');
        $this->load->model('home/home_tbl','home');
		$this->load->helper(array('form', 'url'));
		//check if user setup pre data...
   		$this->session->flashdata('msg');
		//$data['logo_data']=$this->setup->getlogo();
			//$this->output->enable_profiler(TRUE);
		}
		
		function index(){
			
			//check if is 1st setup...
			$this->setup->CheckIfhaveUser();
			$data['title']='Admin Login';
			
			$data['logo_data']=$this->setup->getlogo();
		//if login
			if($_POST){
				$username = $this->security->xss_clean($this->input->post('username'));
				$password = $this->security->xss_clean($this->input->post('password'));
				
				
				
				$account_chk=$this->setup->AccountCheck($username);
				//if user exist
				if(!empty($account_chk)){
					//$verifyUser=$this->setup->verifyUser($username,$password);
					//die( $this->encryption->decode($account_chk['password'])."-".$password );
					if($account_chk['username']==$username && $this->encryption->decode($account_chk['password'])==$password){
							//check user if its active
							$if_active=$this->setup->ActiveCheck($account_chk['status']);
							if($if_active['status']=="inactive"){
									    $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Account is not Active. </div>');					
							            redirect('admin');				
							} else {					
							//initialize session			
							//die($account_chk['access_level']);		
							$this->session->set_userdata('admin_full_name' , $account_chk['full_name']);
							$this->session->set_userdata('admin_username' , $account_chk['username']);
							$this->session->set_userdata('admin_user_id' , $account_chk['user_id']);
							$this->session->set_userdata('admin_access_level' , $account_chk['access_level']);
							
							redirect('admin/dashboard');	
							}												
					} else {
							    $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Incorrect Username/Password. </div>');					
					            redirect('admin');						
					}
				
				} else {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">User not yet exist.</div>');							
					            redirect('admin');					
				}
				
			}//end of if $_POST			
			$data['scripts']=array(assets_url()."js/admin/dashboard.js");
			
			$this->load->view('layout/header_page',$data);
			$this->load->view('admin/admin_login_page',$data);
			$this->load->view('layout/footer_page',$data);			
			
		}
		function Logout(){
			$this->session->sess_destroy();
			redirect(base_url('admin'));	
		}	
		function dashboard(){
			$this->setup->CheckIfLogin();
			$data['title']='Activities';
			$data['logo_data']=$this->setup->getlogo();
			//load admin dynamic js and css when creation go to assets js/admin/then name of module ex items.js same with css
			$data['scripts']=array(assets_url()."js/admin/dashboard.js");
			//$data['styles']=array(assets_url()."css/admin/dashboard.css");
			
			$this->load->view('admin/admin_header_page',$data);
			$this->load->view('admin/admin_home_page',$data);
			$this->load->view('admin/admin_footer_page',$data);							
		}
		//for page load grid
		function grid($module){
			//here load each page called on admin management you can load and setup pages here			
			//checking if user has login
			$this->setup->CheckIfLogin();	
			
			$data['logo_data']=$this->setup->getlogo();				
			//check module 
			if($module=='users'){
				$data['title']=strtoupper($module);					
			}
			if($module=='brand'){
				$data['title']=strtoupper($module);					
			}						
			if($module=='items'){
				$data['title']=strtoupper($module);					
			}
			if($module=='category'){
				$data['title']=strtoupper($module);					
			}
			if($module=='options'){
				$data['title']=strtoupper($module);
			}			
			if($module=='orders'){
				$data['title']=strtoupper("Order");				
			}
			if($module=='promo'){
				$data['title']=strtoupper($module);					
			}
			if($module=='promo_type'){
				$data['title']=strtoupper("promo scheme");					
			}
			if($module=='logo'){
				$data['title']=strtoupper($module);					
			}						
			if($module=='announcement'){
				$data['title']=strtoupper($module);	
				//shannele here					
			}
			if($module=='reports'){
				$data['title']=strtoupper($module);					
			}
			if($module=='about_us'){
				$data['title']= strtoupper('about us');
				//shannele here										
			}
			if($module=='terms_conditions'){
				$data['title']=strtoupper("terms and condition");		
				//shannele here								
			}	
			if($module=='messages'){
				$data['title']=strtoupper("messages");		
											
			}	
			if($module == "batch"){
				$data['title'] = strtoupper("Batch Management");
			}
			$data['module']= $module;
			//load admin dynamic js and css when creation go to assets js/admin/then name of module ex items.js same with css
			$data['scripts']=array(assets_url()."js/admin/".$module.".js");
			$data['styles']=array(assets_url()."css/admin/".$module.".css");
							
			$this->load->view('admin/admin_header_page',$data);
			$this->load->view('admin/'.$module.'/'.$module.'_page',$data); //<-- here the format is application/view/admin/"module name ex items"/items_page.php
			$this->load->view('admin/admin_footer_page',$data);					
		}
		//for table loads
		function table($module){
			$this->setup->CheckIfLogin();	
				if($module=='activity'){
				$get_data= $this->setup->table_data($module,'');
				if(empty($get_data)){
					$finalres=array(
					"data"=>''
					);					
				} else {
					foreach($get_data as $table_data){
						$result[]=array(
								ucfirst($table_data['action']),
								ucfirst($table_data['action_by']),
								date('m/d/Y g:i a',strtotime($table_data['action_date'])),						
						);							
					}
					$finalres=array(
					"data"=>$result
					);
				}		
				echo json_encode($finalres);					
				}
				else if($module=='users'){
				$get_data= $this->setup->table_data($module,'');
				if(empty($get_data)){
					$finalres=array(
					"data"=>''
					);					
				} else {
					foreach($get_data as $table_data){
						$result[]=array(
								ucfirst($table_data['full_name']),
								ucfirst($table_data['username']),
								ucfirst($table_data['status']),
								"<a href='".base_url()."admin/form/".$module."/".$this->encryption->encode($table_data['user_id'])."' class='btn-link' title='Edit'>
								<span class='glyphicon glyphicon-edit'></span>
								</a>
								<a href='".base_url()."admin/admin/delete_user/".$table_data['user_id'].".aspx' class='btn-link' title='delete'>
								<span class='glyphicon glyphicon-trash'></span>
								</a>										
								"							
						);							
					}
					$finalres=array(
					"data"=>$result
					);
				}		
				echo json_encode($finalres);			
			}
			else if($module=='brand'){
				$get_data= $this->setup->table_data($module,'');
				if(empty($get_data)){
					$finalres=array(
					"data"=>''
					);					
				} else {
					foreach($get_data as $table_data){
						$result[]=array(
								ucfirst($table_data['brand_name']),
								ucfirst($table_data['status']),
								ucfirst($table_data['created_by']),
								"<a href='".base_url()."admin/form/".$module."/".$this->encryption->encode($table_data['brand_id'])."' class='btn-link' title='Edit'>
								<span class='glyphicon glyphicon-edit'></span>
								</a>
								"							
						);							
					}
					$finalres=array(
					"data"=>$result
					);
				}		
				echo json_encode($finalres);					
			}			 
			else if($module=='items'){
				$get_data= $this->setup->table_data($module,'');
				if(empty($get_data)){
					$finalres=array(
					"data"=>''
					);					
				} else {
					foreach($get_data as $table_data){
						$result[]=array(
								ucfirst($table_data['item_name']),
								ucfirst($table_data['brand']),
								ucfirst($table_data['cat_name']),
								ucfirst($table_data['scat_name']),
								ucfirst($table_data['created_by']),
								ucfirst($table_data['status']),								
								"<a href='".base_url()."admin/form/".$module."/".$this->encryption->encode($table_data['item_id'])."' class='btn-link' title='Edit'>
								<span class='glyphicon glyphicon-edit'></span>
								</a>
								<a href='".base_url()."admin/admin/delete_item/".$table_data['item_id'].".aspx' class='btn-link' title='delete'>
								<span class='glyphicon glyphicon-trash'></span>
								</a>
								"							
						);
							
					}
					$finalres=array(
					"data"=>$result
					);
				}		
				echo json_encode($finalres);				
			}
			else if($module=='category'){
				$get_data= $this->setup->table_data($module,'');
				if(empty($get_data)){
					$finalres=array(
					"data"=>''
					);					
				} else {
					foreach($get_data as $table_data){
						$result[]=array(
								ucfirst($table_data['cat_name']),
								ucfirst($table_data['status']),
								ucfirst($table_data['created_by']),
								"<a href='".base_url()."admin/form/".$module."/".$this->encryption->encode($table_data['cat_id'])."' class='btn-link' title='Edit'>
								<span class='glyphicon glyphicon-edit'></span>
								</a><a href='".base_url()."admin/admin/delete_category/".$table_data['cat_id'].".aspx' class='btn-link' title='delete'>
								<span class='glyphicon glyphicon-trash'></span>
								</a>
								"							
						);
							
					}
					$finalres=array(
					"data"=>$result
					);
				}		
				echo json_encode($finalres);			
			}
			else if($module=='sub_category'){
				$get_data_scat= $this->setup->table_data($module,'');
				if(empty($get_data_scat)){
					$finalres_scat=array(
					"data"=>''
					);					
				} else {
					foreach($get_data_scat as $table_data){
						$result_scat[]=array(
								ucfirst($table_data['cat_name']),
								ucfirst($table_data['scat_name']),
								ucfirst($table_data['status']),
								ucfirst($table_data['created_by']),
								"<a href='".base_url()."admin/form/".$module."/".$this->encryption->encode($table_data['scat_id'])."' class='btn-link' title='Edit'>
								<span class='glyphicon glyphicon-edit'></span>
								</a><a href='".base_url()."admin/admin/delete_scategory/".$table_data['scat_id'].".aspx' class='btn-link' title='delete'>
								<span class='glyphicon glyphicon-trash'></span>
								"							
						);
							
					}
					$finalres_scat=array(
					"data"=>$result_scat
					);
				}		
				echo json_encode($finalres_scat);			
			}							
			else if($module=='users'){
				$get_data= $this->setup->table_data($module);//eto nakuha ng laman ng table ..dito kunin data
				if(empty($get_data)){//pag empty nodata lalabas dun
					$finalres=array(
					"data"=>''//<--eto un
					);					
				} else {
					//pag may data ifoforloop para maging list
					foreach($get_data as $table_data){
						///ilagay sa array ang nid ilabas sa table kile sa gnwa ko napansin mo naman dinpo diba?
						$result[]=array(
								ucfirst($table_data['logo_filename']),
								ucfirst($table_data['logo_path']),
								ucfirst($table_data['facebook_url']),
								ucfirst($table_data['twitter_url']),
								"<a href='".base_url()."admin/form/".$module."/".$table_data['logo_id'].".aspx' class='btn-link' title='Edit'>
								<span class='glyphicon glyphicon-edit'></span>
								</a>
								"	//ung may <a yan ung link papunta sa edit mo.di dpt mwla yan kng may edit ggwin mo.							
								//date('m/d/Y g:i a',strtotime($table_data['action_date'])),						
						);							
					}
					$finalres=array(
					"data"=>$result
					);			
				
				}
				echo json_encode($finalres);	//then ipapasa sa table via json..check mo po to sa w3schools.				
			}
			else if($module=='orders'){
		
			}else if($module=='promo'){
				$get_data_promo= $this->setup->table_data($module);
				if(empty($get_data_promo)){
					$finalres_scat=array(
					"data"=>''
					);					
				} else {
					foreach($get_data_promo as $table_data){
						$result_scat[]=array(
								ucfirst($table_data['promo_code']),
								ucfirst($table_data['promo_name']),
								ucfirst($table_data['promo_type']),
								"<a href='".base_url()."admin/form/".$module."/".$this->encryption->encode($table_data['promo_id'])."' class='btn-link' title='Edit'>
								<span class='glyphicon glyphicon-edit'></span>
								</a>
								"							
						);
							
					}
					$finalres_scat=array(
					"data"=>$result_scat
					);
				}		
				echo json_encode($finalres_scat);					
			}
			else if($module=='promo_type'){
				$get_data_promotype= $this->setup->table_data($module,'');
				if(empty($get_data_promotype)){
					$finalres_ptype=array(
					"data"=>''
					);					
				} else {
					foreach($get_data_promotype as $table_data){
						$result_ptype[]=array(
								ucfirst($table_data['ptype_name']),
								ucfirst($table_data['qty']),
								ucfirst($table_data['disc_percent'])."%",
								ucfirst($table_data['status']),
								ucfirst($table_data['created_by']),
								"<a href='".base_url()."admin/form/".$module."/".$this->encryption->encode($table_data['ptype_id'])."' class='btn-link' title='Edit'>
								<span class='glyphicon glyphicon-edit'></span>
								</a>
								"							
						);
							
					}
					$finalres_ptype=array(
					"data"=>$result_ptype
					);
				}		
				echo json_encode($finalres_ptype);			
			}							
			else if($module=='order'){
		
			}

			else if($module=='logo'){
				//eto sa table function dito lalagay ung pag tawag ng mga data sa list ng module mo 
				$get_data= $this->setup->table_data($module);//eto nakuha ng laman ng table ..dito kunin data
				if(empty($get_data)){//pag empty nodata lalabas dun
					$finalres=array(
					"data"=>''//<--eto un
					);					
				} else {
					//pag may data ifoforloop para maging list
					foreach($get_data as $table_data){
						///ilagay sa array ang nid ilabas sa table kile sa gnwa ko napansin mo naman dinpo diba?
						$result[]=array(
								ucfirst($table_data['logo_filename']),
								ucfirst($table_data['logo_path']),
								ucfirst($table_data['facebook_url']),
								ucfirst($table_data['twitter_url']),
								"<a href='".base_url()."admin/form/".$module."/".$this->encryption->encode($table_data['logo_id'])."' class='btn-link' title='Edit'>
								<span class='glyphicon glyphicon-edit'></span>
								</a>
								"	//ung may <a yan ung link papunta sa edit mo.di dpt mwla yan kng may edit ggwin mo.							
								//date('m/d/Y g:i a',strtotime($table_data['action_date'])),						
						);							
					}
					$finalres=array(
					"data"=>$result
					);			
				
				}
				echo json_encode($finalres);	//then ipapasa sa table via json..check mo po to sa w3schools.				
			}
			else if($module=='announcement'){
				//eto sa table function dito lalagay ung pag tawag ng mga data sa list ng module mo 
				$get_data= $this->setup->table_data($module);//eto nakuha ng laman ng table ..dito kunin data
				if(empty($get_data)){//pag empty nodata lalabas dun
					$finalres=array(
					"data"=>''//<--eto un
					);					
				} else {
					//pag may data ifoforloop para maging list
					foreach($get_data as $table_data){
						///ilagay sa array ang nid ilabas sa table kile sa gnwa ko napansin mo naman dinpo diba?
						$result[]=array(
								ucfirst($table_data['date_announce']),
								ucfirst($table_data['subj']),
								ucfirst($table_data['announce_by']),
								"<a href='".base_url()."admin/form/".$module."/".$this->encryption->encode($table_data['announcement_id'])."' class='btn-link' title='Edit'>
								<span class='glyphicon glyphicon-edit'></span>
								</a>
								"	//ung may <a yan ung link papunta sa edit mo.di dpt mwla yan kng may edit ggwin mo.							
								//date('m/d/Y g:i a',strtotime($table_data['action_date'])),						
						);							
					}
					$finalres=array(
					"data"=>$result
					);			
				
				}
				echo json_encode($finalres);	//then ipapasa sa table via json..check mo po to sa w3schools.				
			}
			else if($module=='reports'){
			
			}
			
			//messages
			else if($module=='messages'){
				//eto sa table function dito lalagay ung pag tawag ng mga data sa list ng module mo 
				$get_data= $this->setup->table_data($module);//eto nakuha ng laman ng table ..dito kunin data
				if(empty($get_data)){//pag empty nodata lalabas dun
					$finalres=array(
					"data"=>''//<--eto un
					);					
				} else {
					//pag may data ifoforloop para maging list
					foreach($get_data as $table_data){
						///ilagay sa array ang nid ilabas sa table kile sa gnwa ko napansin mo naman dinpo diba?
						$result[]=array(
								ucfirst($table_data['date']),
								ucfirst($table_data['email']),
								ucfirst($table_data['subject']),
								"<a href='".base_url()."admin/form/".$module."/".$this->encryption->encode($table_data['id'])."' class='btn-link' title='Edit'>
								<span class='glyphicon glyphicon-edit'></span>
								</a>
								"	//ung may <a yan ung link papunta sa edit mo.di dpt mwla yan kng may edit ggwin mo.							
								//date('m/d/Y g:i a',strtotime($table_data['action_date'])),						
						);							
					}
					$finalres=array(
					"data"=>$result
					);			
				
				}
				echo json_encode($finalres);	//then ipapasa sa table via json..check mo po to sa w3schools.				
			}
			
			//end of messages
			else if($module=='about_us'){
				//eto sa table function dito lalagay ung pag tawag ng mga data sa list ng module mo 
				$get_data= $this->setup->table_data($module);//eto nakuha ng laman ng table ..dito kunin data
				if(empty($get_data)){//pag empty nodata lalabas dun
					$finalres=array(
					"data"=>''//<--eto un
					);					
				} else {
					//pag may data ifoforloop para maging list
					foreach($get_data as $table_data){
						///ilagay sa array ang nid ilabas sa table kile sa gnwa ko napansin mo naman dinpo diba?
						$result[]=array(
								ucfirst($table_data['date_aboutus']),
								ucfirst($table_data['contacts']),
								"<a href='".base_url()."admin/form/".$module."/".$this->encrpytion->encode($table_data['about_id']).".aspx' class='btn-link' title='Edit'>
								<span class='glyphicon glyphicon-edit'></span>
								</a>
								"	//ung may <a yan ung link papunta sa edit mo.di dpt mwla yan kng may edit ggwin mo.							
								//date('m/d/Y g:i a',strtotime($table_data['action_date'])),						
						);							
					}
					$finalres=array(
					"data"=>$result
					);			
				
				}
				echo json_encode($finalres);	//then ipapasa sa table via json..check mo po to sa w3schools.				
			} else if($module=='terms_conditions'){
					//eto sa table function dito lalagay ung pag tawag ng mga data sa list ng module mo 
					$get_data= $this->setup->table_data($module);//eto nakuha ng laman ng table ..dito kunin data
					if(empty($get_data)){//pag empty nodata lalabas dun
						$finalres=array(
						"data"=>''//<--eto un
						);					
					} else {
						//pag may data ifoforloop para maging list
						foreach($get_data as $table_data){
							///ilagay sa array ang nid ilabas sa table kile sa gnwa ko napansin mo naman dinpo diba?
							$result[]=array(
									ucfirst($table_data['date_terms']),
									ucfirst($table_data['terms']),
									"<a href='".base_url()."admin/form/".$module."/".$this->encryption->encode($table_data['terms_id']).".aspx' class='btn-link' title='Edit'>
									<span class='glyphicon glyphicon-edit'></span>
									</a>
									"	//ung may <a yan ung link papunta sa edit mo.di dpt mwla yan kng may edit ggwin mo.							
									//date('m/d/Y g:i a',strtotime($table_data['action_date'])),						
							);							
						}
						$finalres=array(
						"data"=>$result
						);			
					
					}
					echo json_encode($finalres);	//then ipapasa sa table via json..check mo po to sa w3schools.	
				} else if($module=='options'){
					//eto sa table function dito lalagay ung pag tawag ng mga data sa list ng module mo 
					$get_data= $this->setup->table_data($module);//eto nakuha ng laman ng table ..dito kunin data
					if(empty($get_data)){//pag empty nodata lalabas dun
						$finalres=array(
						"data"=>''//<--eto un
						);					
					} else {
						//pag may data ifoforloop para maging list
						foreach($get_data as $table_data){
							///ilagay sa array ang nid ilabas sa table kile sa gnwa ko napansin mo naman dinpo diba?
							$result[]=array(
									ucfirst($table_data['option_name']),
									ucfirst($table_data['status']),
									ucfirst($table_data['created_by']),
									"<a href='".base_url()."admin/form/".$module."/".$this->encryption->encode($table_data['id']).".aspx' class='btn-link' title='Edit'>
									<span class='glyphicon glyphicon-edit'></span>
									</a>
									"	//ung may <a yan ung link papunta sa edit mo.di dpt mwla yan kng may edit ggwin mo.							
									//date('m/d/Y g:i a',strtotime($table_data['action_date'])),						
							);							
						}
						$finalres=array(
						"data"=>$result
						);			
					
					}
					echo json_encode($finalres);	//then ipapasa sa table via json..check mo po to sa w3schools.						
				}else if($module == "batch"){

					$get_data= $this->setup->table_data($module); 

					if(empty($get_data)){ 
						$finalres=array(
						"data"=>'' 
						);					
					} else {
						//pag may data ifoforloop para maging list
						foreach($get_data as $table_data){
							///ilagay sa array ang nid ilabas sa table kile sa gnwa ko napansin mo naman dinpo diba?
							$result[]=array(
									ucfirst($table_data['id']),
									ucfirst($table_data['date_from']),
									ucfirst($table_data['date_to']),
									"<a href='".base_url()."admin/form/".$module."/".$this->encryption->encode($table_data['id']).".aspx' class='btn-link' title='Edit'>
									<span class='glyphicon glyphicon-edit'></span>
									</a>
									"										 
							);							
						}
						$finalres=array(
						"data"=>$result
						);			
						//print(json_encode($finalres)); 
					}
					echo json_encode($finalres);	

				}				
								
		}

		//for forms load
		function form($module,$idx){
			//checking if user has login
			$this->setup->CheckIfLogin();
			$id	=$this->encryption->decode($idx);				
			//check module 
			$data['logo_data']=$this->setup->getlogo();
			if($module=='users'){
				$data['title']=strtoupper($module);
				
				if($id=='a'){//<--a = add hehehe
				$data['title']="Create ".$module;
				$data['module']=$module;				
				$data['user_data']='';// <-- when creating/adding things this need to be empty.	
				} else {//edit 
				$data['title']="Edit ".$module;
				$data['module']=$module;				
				$data['user_data']=$this->setup->getdata($module,$id);	
				}								
			}
			else if($module=='brand'){
				$data['title']=strtoupper($module);	
				if($id=='a'){//<--a = add hehehe
				$data['title']="Create ".$module;
				$data['module']=$module;				
				$data['brand_data']='';// <-- when creating/adding things this need to be empty.	
				} else {//edit 
				$data['title']="Edit ".$module;
				$data['module']=$module;				
				$data['brand_data']=$this->setup->getdata($module,$id);	
				}									
			}						
			else if($module=='category'){
				$data['title']=strtoupper($module);		
				if($id=='a'){//<--a = add hehehe
				$data['title']="Create ".$module;
				$data['module']=$module;				
				$data['category_data']='';// <-- when creating/adding things this need to be empty.
				$data['brand_data']=$this->setup->table_data("brand","active");	
				} else {//edit 
				$data['title']="Edit ".$module;
				$data['module']=$module;				
				$data['category_data']=$this->setup->getdata($module,$id);	
				$data['brand_data']=$this->setup->table_data("brand","active");
				}								
			}
			else if($module=='sub_category'){
				$data['title']=strtoupper($module);		
				if($id=='a'){//<--a = add hehehe
				$data['title']="Create Sub Category";
				$data['module']=$module;				
				$data['sub_category_data']='';// <-- when creating/adding things this need to be empty.	
				//category menu
				$data['category_data']=$this->setup->table_data("category","active");		
				} else {//edit 
				$data['title']="Edit Sub Category";
				$data['module']=$module;				
				$data['sub_category_data']=$this->setup->getdata($module,$id);	
				
				//category menu
				$data['category_data']=$this->setup->table_data("category","active");		
				}								
			}
			else if($module=='items'){
				//$data['title']=strtoupper($module);	
				if($id=='a'){//<--a = add hehehe
				$data['title']="Create ".$module;
				$data['module']=$module;				
				$data['items_data']='';// <-- when creating/adding things this need to be empty.	
				$data['images_data']='';
				} else {//edit 
				$data['title']="Edit ".$module;
				$data['module']=$module;				
				$data['items_data']=$this->setup->getdata($module,$id);
				$data['images_data']=$this->setup->getimgdata($id);		
				}
				//load comboboxes need on add and edit module
				$data['brand_data']=$this->setup->table_data("brand","active");
				$data['category_data']=$this->setup->table_data("category","active");
				$data['sub_category_data']=$this->setup->table_data("sub_category","active");
				$data['options_data']=$this->setup->table_data("options","active");											
			}										
			else if($module=='order'){
				$data['title']=strtoupper($module);				
			}
			else if($module=='promo'){
				$data['title']=strtoupper($module);		
				//$id=$this->encryption->decode($id);
				//die($module);
			    if($id=='a'){//<--a = add hehehe
			    //0die($id);
				$data['title']="Create ".$module;
				$data['module']=$module;				
				$data['promo_data']='';
				$data['brands_data'] = $this->setup->getBrand();// <-- when creating/adding things this need to be empty.	
				
				} else {//edit 
				$data['title']="Edit ".$module;
				$data['module']=$module;				
				$data['promo_data']=$this->setup->getdata($module,$id);	
				$data['promo_group'] = $this->setup->getPromoGroup($data['promo_data']['id']);
				
					if($data['promo_data']['promo_type'] == "brand"){
						$data['brands_data'] = $this->setup->getBrandPromo($data['promo_data']['id']);
					}else{
						$data['brands_data'] = $this->setup->getBrand();
					}
				}	
				$data['items_data']=$this->setup->active_items();
				$data['promo_scheme_data']=$this->setup->table_data('promo_type','active');	
		
							
			}
			else if($module=='promo_type'){
				if($id=='a'){//<--a = add hehehe
				$data['title']="Create Promo Scheme";
				$data['module']=$module;				
				$data['promotype_data']='';// <-- when creating/adding things this need to be empty.	
				} else {//edit 
				$data['title']="Edit Promo Scheme";
				$data['module']=$module;			

				$data['promotype_data']=$this->setup->getdata($module,$id);	

				}										
			}
			else if($module=='logo'){
				$data['title']=strtoupper($module);	
				if($id=='a'){//<--a = add hehehe
				$data['title']="Create logo";
				$data['module']=$module;				
				$data['logo_data']='';// <-- when creating/adding things this need to be empty.	eto un
				} else {//edit 
				$data['title']="Edit logo";
				$data['module']=$module;				
				$data['logo_data']=$this->setup->getdata($module,$id);	//eto part na kukuha ka data sa table mo
				}								
			}								
			else if($module=='announcement'){
				$data['title']=strtoupper($module);	
				if($id=='a'){//<--a = add hehehe
				$data['title']="Create Announcement";
				$data['module']=$module;				
				$data['announcement_data']='';// <-- when creating/adding things this need to be empty.	eto un
				} else {//edit 
				$data['title']="Edit Announcement";
				$data['module']=$module;				
				$data['announcement_data']=$this->setup->getdata($module,$id);	//eto part na kukuha ka data sa table mo
				}								
			}
			//messages
			else if($module=='messages'){
				$data['title']=strtoupper($module);	
				if($id=='a'){//<--a = add hehehe
				//$data['title']="Create Messages";
				$data['module']=$module;				
				$data['messages_data']='';// <-- when creating/adding things this need to be empty.	eto un
				} else {//edit 
				$data['title']="Edit Messages";
				$data['module']=$module;				
				$data['messages_data']=$this->setup->getdata($module,$id);	//eto part na kukuha ka data sa table mo
				}								
			}
			//end of messages
			else if($module=='reports'){
				$data['title']=strtoupper($module);					
			}
			else if($module=='about_us'){
				$data['title']=strtoupper($module);	
				if($id=='a'){//<--a = add hehehe
				$data['title']="Create About Us";
				$data['module']=$module;				
				$data['aboutus_data']='';// <-- when creating/adding things this need to be empty.	eto un
				} else {//edit 
				$data['title']="Edit About Us";
				$data['module']=$module;				
				$data['aboutus_data']=$this->setup->getdata($module,$id);	//eto part na kukuha ka data sa table mo
				}								
			}
			else if($module=='terms_conditions'){
				//die("x");
				$data['title']=strtoupper($module);	
				if($id=='a'){//<--a = add heheheterms_conditions
				$data['title']="Create Terms and Conditions";
				$data['module']=$module;				
				$data['termsconditions_data']='';// <-- when creating/adding things this need to be empty.	eto un
				} else {//edit 
				$data['title']="Edit Terms and Conditions";
				$data['module']=$module;				
				$data['termsconditions_data']=$this->setup->getdata($module,$id);	//eto part na kukuha ka data sa table mo
				} 								
			} else if($module=='options'){
				$data['title']=strtoupper($module);	
				if($id=='a'){//<--a = add heheheterms_conditions
				$data['title']="Create Item Options";
				$data['module']=$module;				
				$data['options_data']='';// <-- when creating/adding things this need to be empty.	eto un
				} else {//edit 
				$data['title']="Edit Item Options";
				$data['module']=$module;				
				$data['options_data']=$this->setup->getdata($module,$id);	//eto part na kukuha ka data sa table mo
				} 					
			} else if($module=='batch'){
				//$data['title']=strtoupper($module);	
				if($id=='a'){//<--a = add heheheterms_conditions
				$data['title']="Create Batch";
				//$data['module']=$module;				
				$data['batch_data']='';// <-- when creating/adding things this need to be empty.	eto un
				} else {//edit 
				$data['title']="Edit Batch";
				///$data['module']=$module;				
				$data['batch_data']=$this->setup->getdata($module,$id);	//eto part na kukuha ka data sa table mo
				} 
			}
			//echo $module;
			$data['module']= $module;
			//load admin dynamic js and css when creation go to assets js/admin/then name of module ex items.js same with css
			$data['scripts']=array(assets_url()."js/admin/".$module.".js");
			$data['styles']=array(assets_url()."css/admin/".$module.".css");

		    $this->load->view('admin/admin_header_page',$data);
			$this->load->view('admin/'.$module.'/'.$module.'form_page',$data); //<-- here the format is application/view/admin/"module name ex items"/itemsform_page.php
			$this->load->view('admin/admin_footer_page',$data);	
		} 
	

		//save or update datas
		function process($module,$process){
			//here load each page called on admin management you can load and setup pages here			
			//checking if user has login
			$this->setup->CheckIfLogin();
			//$process=$this->encryption->decode($processx);					
			//check module 
			if($module=='users'){
				if($_POST){
					//loading form details
					$uid=$this->security->xss_clean($this->input->post('idx'));
					$UserName = $this->security->xss_clean($this->input->post('UserName'));
					$Password = $this->encryption->encode($this->security->xss_clean($this->input->post('Password')));	
					$Access_level=$this->security->xss_clean($this->input->post('Access_level'));	
					
					$FirstName = $this->security->xss_clean($this->input->post('FirstName'));	
					$MiddleName = $this->security->xss_clean($this->input->post('MiddleName'));	
					$LastName = $this->security->xss_clean($this->input->post('LastName'));
					$address = $this->security->xss_clean($this->input->post('address'));	
					$email = $this->security->xss_clean($this->input->post('email'));		
					$contact_no = $this->security->xss_clean($this->input->post('contact_no'));
					$fb_url = $this->security->xss_clean($this->input->post('fb_url'));
					
					$account_name = $this->security->xss_clean($this->input->post('account_name'));
					$account_no = $this->security->xss_clean($this->input->post('account_no'));
										
					$status=$this->security->xss_clean($this->input->post('status'));	
					$user_type=$this->security->xss_clean($this->input->post('user_type'));
					$FullName=$FirstName." ".$MiddleName." ".$LastName;		
					$created_by= $this->session->userdata('admin_username');					
					if($process=='a'){
					//generate user id
					$user_id= $this -> setup -> getUserid();
						if ($user_id) {
							$user_id = $user_id["id"];;
							//$user_id = substr($user_id, -10);
							$uid = "USER" . sprintf("%010d", $user_id + 1);
						} else {
							$uid = "USER" . sprintf("%010d", 1);
						}
						//prepareing data....
						$data=array(
						"user_id"=>$uid,
						"full_name"=>$FullName,
						"first_name"=>$FirstName,
						"middle_name"=>$MiddleName,
						"last_name"=>$LastName,
						"address"=>$address,
						"fb_url"=>$fb_url,
						"account_name"=>$account_name,
						"account_no"=>$account_no,
						"email"=>$email,
						"contact_no"=>$contact_no,						
						"username"=>$UserName,
						"password"=>$Password,
						"access_level"=>$Access_level,
						"user_type"=>$user_type,
						"created_by"=>$created_by,
						"status"=>$status,
						"activation_date"=>date('Y-m-d H:i:s'),	
						"activated_by"=>$created_by			
						);	
						//check if username already exist
						$user_exist= $this -> setup -> UserExist($UserName);
						
						if($user_exist){
							$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Username already Exist.</div>');						
							redirect('admin/admin/form/users/'.$this->encryption->encode('a').'');						
						} else {					
							//insert data
							$insert_data=$this->setup->saveUser($data);
							//upload img
							if($insert_data==1){
							//logging activity for audit..	
							$req=array(
							"action"=>"Create User",
							"action_by"=>$created_by,
							"action_date"=>date('Y-m-d H:i:s')
							);	
							$log_activity=$this->setup->activityLog($req);			
							$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">User Successfully Made.</div>');	
							redirect('admin/admin/form/users/'.$this->encryption->encode('a').'');
																						        
							}else if($insert_data==0) {
							$this->session->set_flashdata('msg','					
							<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
							redirect('admin/admin/form/users/'.$this->encryption->encode('a').'');										
							}					
					   }																			
					} else {
						//prepareing data....
						$data=array(
						"user_id"=>$uid,
						"full_name"=>$FullName,
						"address"=>$address,
						"fb_url"=>$fb_url,
						"account_name"=>$account_name,
						"account_no"=>$account_no,						
						"email"=>$email,
						"contact_no"=>$contact_no,
						"first_name"=>$FirstName,
						"middle_name"=>$MiddleName,
						"last_name"=>$LastName,
						"username"=>$UserName,
						"password"=>$Password,
						"access_level"=>$Access_level,
						"user_type"=>$user_type,
						"created_by"=>$created_by,
						"status"=>$status,
						"activation_date"=>date('Y-m-d H:i:s'),	
						"activated_by"=>$created_by			
						);							
						$update_data=$this->setup->UpdateUser($data,$uid);									
						if($update_data==1){
								//logging activity for audit..	
								$req=array(
								"action"=>"Edit User",
								"action_by"=>$created_by,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);									
								$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">User Successfully Updated.</div>');
					            redirect('admin/admin/form/users/'.$this->encryption->encode($uid).'');
						} else if($update_data==0) {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
					            redirect('admin/admin/form/users/'.$this->encryption->encode($uid).'');		
						}							
					}
				}//end of $_POST					
				}						
			else if($module=='category'){
				if($_POST){
					$cid=$this->security->xss_clean($this->input->post('idx'));
					$brand=$this->security->xss_clean($this->input->post('brand'));					
					$status=$this->security->xss_clean($this->input->post('status'));
					$cat_name = $this->security->xss_clean($this->input->post('cat_name'));
					$created_by= $this->session->userdata('admin_username');
					if($process=='a'){
					//generate user id
					$cat_id= $this -> setup -> getCatid();
						if ($cat_id) {
							$cat_id  = $cat_id["cat_id"];
							$cat_id = substr($cat_id, -10);
							$cid = "CAT" . sprintf("%010d", $cat_id + 1);
						} else {
							$cid = "CAT" . sprintf("%010d", 1);
						}
						//data 
						$data=array(
						"brand_id"=>$brand,
						"cat_id"=>$cid,
						"cat_name"=>$cat_name,
						"status"=>"active",
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						//check if category already exist
						$cat_exist= $this -> setup -> CategoryExist($cat_name);
						
						if($cat_exist){
							$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Category already Exist.</div>');						
							redirect('admin/form/category/'.$this->encryption->encode('a').'');						
						} else {					
							//insert data
							$insert_data=$this->setup->saveCategory($data);
							//upload img
							if($insert_data==1){
							//logging activity for audit..	
							$req=array(
							"action"=>"Create Category",
							"action_by"=>$created_by,
							"action_date"=>date('Y-m-d H:i:s')
							);	
							$log_activity=$this->setup->activityLog($req);			
							$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Category Successfully Made.</div>');	
							redirect('admin/form/category/'.$this->encryption->encode('a').'');
																						        
							}else if($insert_data==0) {
							$this->session->set_flashdata('msg','					
							<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
							redirect('admin/form/category/'.$this->encryption->encode('a').'');										
							}					
					   }													
					} else {
						$data=array(
						"brand_id"=>$brand,
						"cat_id"=>$cid,
						"cat_name"=>$cat_name,
						"status"=>'active',
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						$update_data=$this->setup->UpdateCategory($data,$cid);									
						if($update_data==1){
								//logging activity for audit..	
								$req=array(
								"action"=>"Edit Category",
								"action_by"=>$created_by,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);									
								$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Category Successfully Updated.</div>');
					            redirect('admin/form/category/'.$this->encryption->encode($cid).'');
						} else if($update_data==0) {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
					            redirect('admin/form/category/'.$this->encryption->encode($cid).'');		
						}												
					}										
				}
			}
			else if($module=='sub_category'){
				if($_POST){
					$scid=$this->security->xss_clean($this->input->post('idx'));
					$cat_id = $this->security->xss_clean($this->input->post('category'));
					$status=$this->security->xss_clean($this->input->post('status'));
					$scat_name = $this->security->xss_clean($this->input->post('scat_name'));
					$created_by= $this->session->userdata('admin_username');
					if($process=='a'){
					//generate user id
					$scat_id= $this -> setup -> getSCatid();
						if ($scat_id) {
							$scat_id  = $scat_id["scat_id"];
							$scat_id = substr($scat_id, -10);
							$scid = "SCAT" . sprintf("%010d", $scat_id + 1);
						} else {
							$scid = "SCAT" . sprintf("%010d", 1);
						}
						//data 
						$data=array(
						"cat_id"=>$cat_id,
						"scat_id"=>$scid,
						"scat_name"=>$scat_name,
						"status"=>'active',
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						//check if category already exist
						$scat_exist= $this -> setup -> SCategoryExist($scat_name);
						
						if($scat_exist){
							$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Sub Category already Exist.</div>');						
							redirect('admin/form/sub_category/'.$this->encryption->encode('a').'');						
						} else {					
							//insert data
							$insert_data=$this->setup->saveSCategory($data);
							//upload img
							if($insert_data==1){
							//logging activity for audit..	
							$req=array(
							"action"=>"Create Sub Category",
							"action_by"=>$created_by,
							"action_date"=>date('Y-m-d H:i:s')
							);	
							$log_activity=$this->setup->activityLog($req);			
							$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Sub Category Successfully Made.</div>');	
							redirect('admin/form/sub_category/'.$this->encryption->encode('a').'');
																						        
							}else if($insert_data==0) {
							$this->session->set_flashdata('msg','					
							<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
							redirect('admin/form/sub_category/'.$this->encryption->encode('a').'');										
							}					
					   }													
					} else {
						//data 
						$data=array(
						"cat_id"=>$cat_id,
						"scat_id"=>$scid,
						"scat_name"=>$scat_name,
						"status"=>'active',
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						$update_data=$this->setup->UpdateSCategory($data,$scid);									
						if($update_data==1){
								//logging activity for audit..	
								$req=array(
								"action"=>"Edit Sub Category",
								"action_by"=>$created_by,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);									
								$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Sub Category Successfully Updated.</div>');
					            redirect('admin/form/sub_category/'.$this->encryption->encode($scid).'');
						} else if($update_data==0) {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
					            redirect('admin/form/sub_category/'.$this->encryption->encode($scid).'');		
						}												
					}										
				}			
			}
			else if($module=='brand'){
				if($_POST){
					$bid=$this->security->xss_clean($this->input->post('idx'));
					$status=$this->security->xss_clean($this->input->post('status'));
					$brand_name = $this->security->xss_clean($this->input->post('brand_name'));
					$created_by= $this->session->userdata('admin_username');
					if($process=='a'){
					//generate user id
					$brand_id= $this -> setup -> getBrandid();
						if ($brand_id) {
							$brand_id  = $brand_id["brand_id"];
							$brand_id = substr($brand_id, -10);
							$bid = "BRD" . sprintf("%010d", $brand_id + 1);
						} else {
							$bid = "BRD" . sprintf("%010d", 1);
						}
						//data 
						$data=array(
						"brand_id"=>$bid,
						"brand_name"=>$brand_name,
						"status"=>$status,
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						//check if category already exist
						$brand_exist= $this -> setup -> BrandExist($brand_name);
						
						if($brand_exist){
							$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Brand already Exist.</div>');						
							redirect('admin/form/brand/'.$this->encryption->encode('a').'');						
						} else {					
							//insert data
							$insert_data=$this->setup->saveBrand($data);
							//upload img
							if($insert_data==1){
							//logging activity for audit..	
							$req=array(
							"action"=>"Create Brand",
							"action_by"=>$created_by,
							"action_date"=>date('Y-m-d H:i:s')
							);	
							$log_activity=$this->setup->activityLog($req);			
							$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Brand Successfully Made.</div>');	
							redirect('admin/form/brand/'.$this->encryption->encode('a').'');
																						        
							}else if($insert_data==0) {
							$this->session->set_flashdata('msg','					
							<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
							redirect('admin/form/brand/'.$this->encryption->encode('a').'');										
							}					
					   }													
					} else {
						$data=array(
						"brand_id"=>$bid,
						"brand_name"=>$brand_name,
						"status"=>$status,
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						$update_data=$this->setup->UpdateBrand($data,$bid);									
						if($update_data==1){
								//logging activity for audit..	
								$req=array(
								"action"=>"Edit Brand",
								"action_by"=>$created_by,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);									
								$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Brand Successfully Updated.</div>');
					            redirect('admin/form/brand/'.$this->encryption->encode($bid).'');
						} else if($update_data==0) {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
					            redirect('admin/form/brand/'.$this->encryption->encode($bid).'');		
						}												
					}										
				}			
			}				
			else if($module=='items'){
				if($_POST){
					
					$iid=$this->security->xss_clean($this->input->post('idx'));
					$status=$this->security->xss_clean($this->input->post('status'));
					$item_name=$this->security->xss_clean($this->input->post('item_name'));
					$brand=$this->security->xss_clean($this->input->post('brand'));
					$category=$this->security->xss_clean($this->input->post('category'));
					$sub_category=$this->security->xss_clean($this->input->post('sub_category'));
					$description=$this->security->xss_clean($this->input->post('description'));
					$size=$this->security->xss_clean($this->input->post('size'));//=size
					$volume=$this->security->xss_clean($this->input->post('volume'));
					$options =$this->security->xss_clean($this->input->post('item_options'));					
					$option_stock= $this->security->xss_clean($this->input->post('option_stock'));
					$item_status=$this->security->xss_clean($this->input->post('item_status'));
					$on_hand_val=$this->security->xss_clean($this->input->post('on_hand_val'));	
					$onhand_price=$this->security->xss_clean($this->input->post('onhand_price'));								
					$price=$this->security->xss_clean($this->input->post('price'));
					$price_won=$this->security->xss_clean($this->input->post('price_won'));
					//$quantity=$this->security->xss_clean($this->input->post('quantity'));
					$upload=$this->security->xss_clean($this->input->post('upload'));
					$promo_code=$this->security->xss_clean($this->input->post('promo_code'));
					$created_by= $this->session->userdata('admin_username');

					$is_newitem=$this->security->xss_clean($this->input->post('is_newitem'));
					$is_bestseller=$this->security->xss_clean($this->input->post('is_bestseller'));	
					
					//die($is_newitem." x ".$is_bestseller);
					$is_newitem=='' ? $is_newitem=0 : $is_newitem=$this->security->xss_clean($this->input->post('is_newitem'));
					$is_bestseller=='' ? $is_bestseller=0 : $is_bestseller=$this->security->xss_clean($this->input->post('is_bestseller'));	 
					$options!=''? $options_val=implode(',', $options) : $options_val='';
					$option_stock!='' ? $option_stock_val=implode(',', $option_stock) : $option_stock_val=''; 	
					//image batch
					$img_name1= $this->security->xss_clean($this->input->post('img_name1'));
					$img_thumbs1= $this->security->xss_clean($this->input->post('img_thumbs1'));
					$img_name2= $this->security->xss_clean($this->input->post('img_name2'));
					$img_thumbs2= $this->security->xss_clean($this->input->post('img_thumbs2'));	
					$img_name3= $this->security->xss_clean($this->input->post('img_name3'));
					$img_thumbs3= $this->security->xss_clean($this->input->post('img_thumbs3'));																	
					$brand_id= $this->security->xss_clean($this->input->post('brand_id'));
					if($process=='a'){
					//generate user id
					$item_id= $this -> setup -> getitemid();
						if ($item_id) {
							$item_id  = $item_id["item_id"];
							$item_id = substr($item_id, -10);
							$iid = "ITM" . sprintf("%010d", $item_id + 1);
						} else {
							$iid = "ITM" . sprintf("%010d", 1);
						}
						//uploading img
						$filename='';
						$path='';
						$images_data=[];
						$img_ctr=1;
						//die(var_dump($_FILES['upload']['name']));
						foreach($_FILES as $key => $value){
							
						//for($i=0;$i<count($_FILES["upload"]['name']);$i++){
						if($_FILES[$key]['name']){
									//die($key);	
									//old upload
									/*$path_parts = pathinfo($_FILES["upload"]["name"]);
									$ext = $path_parts['extension']; 
						
										 //die(realpath(APPPATH .'assets/item_covers/'). time().".".$ext);
						    
									     move_uploaded_file($_FILES["upload"]["tmp_name"],
									 	 realpath(APPPATH .'assets/item_covers/'). time().".".$ext);
										 
									     
									     $path=realpath(APPPATH .'assets/item_covers/'). time().".".$ext;	*/
									 //ci upload.
									 //$config = array();
									 $config['image_library'] = 'gd2';
									 $filename="".date("Y")."_".time().$img_ctr."_".$img_ctr.".png";
									 $config['upload_path'] = './assets/img/item_covers/';
									 $config['allowed_types'] = '*';	//gif|jpg|png|PNG|jpeg|JPG|JPEG
									 $config['file_name'] = $filename;
									 $config['encrypt_name']= FALSE;
									 
									 //load ci upload						
									 $this->upload->initialize($config);
									 //$this->upload->do_upload($key);
									if ( ! $this->upload->do_upload($key)) {
										//$this->upload->display_errors();
										$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Upload Failed.</div>');						
										redirect('admin/form/items/'.$this->encryption->encode('a').'');	
									} else {
										$lastupload_img=$this->upload->data();
										
										
										//var_dump($lastupload_img);
										//die();
										//die(var_dump($lastupload_img));
										//$lastupload_img = $this->upload->file_name;
										//Image Resizing
										$config['source_image'] = $lastupload_img['full_path'];
										//$config['new_image'] = $filename;
										$config['maintain_ratio'] = FALSE;
										$config['width'] = 225;
										$config['height'] = 333;
										$config['create_thumb'] = FALSE;	
										$config['new_image'] = $lastupload_img['raw_name']."-thumb.png";							
										$this->load->library('image_lib');
										$this->image_lib->initialize($config);
										if ( ! $this->image_lib->resize()){
											$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Image Resizing Failed.</div>');						
											redirect('admin/form/items/'.$this->encryption->encode('a').'');	
										} else {
										$images_data[]=array(
										"image_name"=>$this->image_lib->file_name,
										"image_thumbs"=>$this->image_lib->new_image,
										"index"=>$img_ctr,
										);	
										$img_ctr++;
										//$this->image_lib->clear();
										//unset($config);								
											//die($this->upload->upload_path.$this->upload->file_name);
										/*	$img = substr($lastupload_img['full_path'], 51);
											//die($img);
											$config['image_library'] = 'gd2';
											$config['source_image'] = $lastupload_img['full_path'];
											$config['wm_text'] = 'Sae sil Parknshop';
											$config['wm_type'] = 'text';
											$config['wm_font_path'] = './system/fonts/texb.ttf';
											$config['wm_font_size']	= '10';									
											$config['wm_font_color'] = '#333';
											$config['wm_vrt_alignment'] = 'middle';
											$config['wm_hor_alignment'] = 'center';
											$config['wm_padding'] = '10';
											$config['new_image'] = $filename;									
											$this->image_lib->initialize($config); 																
											if ( ! $this->image_lib->watermark()){
												die($this->image_lib->display_errors());
												$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Image Watermark Failed.</div>');						
												redirect('admin/form/items/'.$this->encryption->encode('a').'');	
											} else{
												//delete after upload new item
												unlink($lastupload_img['full_path']);	
											}	*/										
										}							
									//delete after upload new item
									//unlink($lastupload_img['full_path']);									
									}							 
	 						     
							}							
							
						}
						//die($this->image_lib->new_image. " - ". $this->image_lib->file_name);
						/*
						"image_path"=>assets_url()."img/item_covers/".$this->image_lib->file_name,
						"image_name"=>$this->image_lib->file_name,
						"image_thumbs_path"=>assets_url()."img/item_covers/".$this->image_lib->new_image,
						"image_thumbs_filename"=>$this->image_lib->new_image,	
						 * */
						 
						//die(var_dump($images_data));
						$data=array(
						"item_id"=>$iid,
						"item_name"=>$item_name,
						"brand"=>$brand,
						"category"=>$category,
						"sub_category"=>$sub_category,	
						"description"=>$description,
						//"ingredients"=>$ingredients,			
						"options"=>$options_val,
						"option_stock"=>$option_stock_val,
						"is_bestseller"=>$is_bestseller,
						"is_newitem"=>$is_newitem,
						"is_stock"=>$item_status,
						"is_onhand"=>$on_hand_val,
						"onhand_price"=>$onhand_price,
						"price"=>$price,
						"price_won"=>$price_won,
						"promo_id"=>$promo_code,	
						"volume"=>$volume,//size/volume
						//"size"=>$size,									
						"status"=>$status,
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s'),
						'brand_id' => $brand_id
						);	
						//check if category already exist
						$item_exist= $this -> setup -> ItemExist($item_name);
						
						if($item_exist){
							$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Item already Exist.</div>');						
							redirect('admin/form/items/'.$this->encryption->encode('a').'');						
						} else {					
							//insert data
							$insert_data=$this->setup->saveItem($data);
							//upload img
							if($insert_data==1){
							//save images		
							foreach($images_data as $imgd){
								$data=array(
								"item_id"=>$iid,
								"filename"=>$imgd['image_name'],
								"thumbs"=>$imgd['image_thumbs'],
								"index"=>$imgd['index'],
								);
							$insert_images=	$this->setup->saveItemImages($data);
							}	
							//logging activity for audit..	
							$req=array(
							"action"=>"Create Item",
							"action_by"=>$created_by,
							"action_date"=>date('Y-m-d H:i:s')
							);	
							$log_activity=$this->setup->activityLog($req);			
							$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Item Successfully Made.</div>');	
							redirect('admin/form/items/'.$this->encryption->encode('a').'');
																						        
							}else if($insert_data==0) {
							$this->session->set_flashdata('msg','					
							<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
							redirect('admin/form/items/'.$this->encryption->encode('a').'');										
							}					
					   }													
					} else {							
						//uploading img	
						$images_data=[];
						$img_ctr=1;
						//die(var_dump($_FILES['upload']['name']));
						foreach($_FILES as $key => $value){											
							if($_FILES[$key]['name']){
								//old upload
								/*$path_parts = pathinfo($_FILES["upload"]["name"]);
								$ext = $path_parts['extension']; 
					
									 //die(realpath(APPPATH .'assets/item_covers/'). time().".".$ext);
					    
								     move_uploaded_file($_FILES["upload"]["tmp_name"],
								 	 realpath(APPPATH .'assets/item_covers/'). time().".".$ext);
									 
								     
								     $path=realpath(APPPATH .'assets/item_covers/'). time().".".$ext;	*/
								 //ci upload.
								 $filename="".date("Y")."_".time().$img_ctr."_".$img_ctr.".png";
								 $config['upload_path'] = './assets/img/item_covers/';
								 $config['allowed_types'] = '*';//'gif|jpg|png|PNG|jpeg|JPG|JPEG';	
								 $config['file_name'] = $filename;
								 $config['encrypt_name']= FALSE;
								 
								 //load ci upload						
								 $this->upload->initialize($config);
								 //$this->upload->do_upload('upload');
								if ( ! $this->upload->do_upload($key))
								{
									$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Upload Failed.</div>');						
									redirect('admin/form/items/'.$this->encryption->encode($iid).'');	
								}
								else
								{
									$lastupload_img=$this->upload->data();
									//Image Resizing
									$config['source_image'] = $lastupload_img['full_path'];
									$config['maintain_ratio'] = FALSE;
									$config['width'] = 225;
									$config['height'] = 333;
									$config['new_image'] =  $lastupload_img['raw_name']."-thumb.png";	
									$this->load->library('image_lib');
									$this->image_lib->initialize($config);							
									if ( ! $this->image_lib->resize()){
										$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Image Resizing Failed.</div>');						
										redirect('admin/form/items/'.$this->encryption->encode($iid).'');	
									} else {
									$images_data[]=array(
									"image_name"=>$this->image_lib->file_name,
									"image_thumbs"=>$this->image_lib->new_image,
									"index"=>$img_ctr,
									);	
									if($img_ctr=='1'){
									unlink("./assets/img/item_covers/".$img_name1);
									unlink("./assets/img/item_covers/".$img_thumbs1);										
									}
									if($img_ctr=='2'){
									unlink("./assets/img/item_covers/".$img_name2);
									unlink("./assets/img/item_covers/".$img_thumbs2);									
									}
									if($img_ctr=='3'){
									unlink("./assets/img/item_covers/".$img_name3);
									unlink("./assets/img/item_covers/".$img_thumbs3);																	
									}										
									$img_ctr++;										
									//$this->image_lib->clear();

									//unset($config);								

									/*	//die($this->upload->upload_path.$this->upload->file_name);
										$img = substr($lastupload_img['full_path'], 51);
										//die($img);
										$config['image_library'] = 'gd2';
										$config['source_image'] = $lastupload_img['full_path'];
										$config['wm_text'] = 'Sae sil Parknshop';
										$config['wm_type'] = 'text';
										$config['wm_font_path'] = './system/fonts/texb.ttf';
										$config['wm_font_size']	= '10';									
										$config['wm_font_color'] = '#333';
										$config['wm_vrt_alignment'] = 'middle';
										$config['wm_hor_alignment'] = 'center';
										$config['wm_padding'] = '10';
										$config['new_image'] = $filename;									
										$this->image_lib->initialize($config); 																
										if ( ! $this->image_lib->watermark()){
											die($this->image_lib->display_errors());
											$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Image Watermark Failed.</div>');						
											redirect('admin/form/items/'.$this->encryption->encode('a').'');	
										} else{
											//delete after upload new item
											unlink($lastupload_img['full_path']);	
										}	*/
																													
									}	

								//delete after upload new item
								//die("./assets/img/item_covers/".$img_name);
								/*unlink("./assets/img/item_covers/".$lastupload_img);	
								unlink("./assets/img/item_covers/".$img_name);	 
								$path_thumbs=assets_url()."img/item_covers/".$this->image_lib->new_image;						
								$filename_thumbs=$this->image_lib->new_image;	
								$path_img=assets_url()."img/item_covers/".$this->image_lib->file_name;						
								$filename_img=$this->image_lib->file_name;	*/										
								} 							 					     
							}  else {
								if($img_ctr=='1'){
								$images_data[]=array(
								"image_name"=>$img_name1,
								"image_thumbs"=>$img_thumbs1,
								"index"=>$img_ctr,
								);										
								}
								if($img_ctr=='2'){
								$images_data[]=array(
								"image_name"=>$img_name2,
								"image_thumbs"=>$img_thumbs2,
								"index"=>$img_ctr,
								);										
								}
								if($img_ctr=='3'){
								$images_data[]=array(
								"image_name"=>$img_name3,
								"image_thumbs"=>$img_thumbs3,
								"index"=>$img_ctr,
								);										
								}																

								$img_ctr++;																		
							}
							}	
							//}	
						/*
						"image_path"=>$path_img,
						"image_name"=>$filename_img,
						"image_thumbs_path"=>$path_thumbs,
						"image_thumbs_filename"=>$filename_thumbs,	*/ 	
								//				"qty"=>$quantity,	
						$data=array(
						"item_id"=>$iid,
						"item_name"=>$item_name,
						"brand"=>$brand,
						"category"=>$category,
						"sub_category"=>$sub_category,	
						"description"=>$description,								
						"options"=>$options_val,
						"option_stock"=>$option_stock_val,	
						"is_bestseller"=>$is_bestseller,
						"is_newitem"=>$is_newitem,											
						"is_stock"=>$item_status,
						"is_onhand"=>$on_hand_val,	
						"onhand_price"=>$onhand_price,											
						"price"=>$price,
						"price_won"=>$price_won,
						"volume"=>$volume,//size/volume
						//"size"=>$size,	
			
						"promo_id"=>$promo_code,				
						"status"=>$status,
						'brand_id' => $brand_id
						);	
						$update_data=$this->setup->UpdateItem($data,$iid);									
						if($update_data==1){
						//delete previous images
						$delete_images=	$this->setup->deleteItemImages($iid);	
							//save images		
							foreach($images_data as $imgd){
								$data=array(
								"item_id"=>$iid,
								"filename"=>$imgd['image_name'],
								"thumbs"=>$imgd['image_thumbs'],
								"index"=>$imgd['index'],
								);
							$insert_images=	$this->setup->saveItemImages($data);
							}								
								//logging activity for audit..	
								$req=array(
								"action"=>"Edit Item",
								"action_by"=>$created_by,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);									
								$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Item Successfully Updated.</div>');
					            redirect('admin/form/items/'.$this->encryption->encode($iid).'');
						} else if($update_data==0) {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
					            redirect('admin/form/items/'.$this->encryption->encode($iid).'');		
						}												
					}						
					
				}
			}								
			else if($module=='order'){
			
			}
			else if($module=='promo'){
				if($_POST){
					$id=$this->security->xss_clean($this->input->post('id'));
					$pid=$this->security->xss_clean($this->input->post('idx'));
					$status=$this->security->xss_clean($this->input->post('status'));
					$promo_code=$this->security->xss_clean($this->input->post('promo_code'));
					$promo_scheme=$this->security->xss_clean($this->input->post('promo_scheme'));
					$promo_name=$this->security->xss_clean($this->input->post('promo_name'));
					$promo_type=$this->security->xss_clean($this->input->post('promo_type'));
					$items=$this->security->xss_clean($this->input->post('items'));
					$start=$this->security->xss_clean($this->input->post('start'));
					$end=$this->security->xss_clean($this->input->post('end'));
					$upload=$this->security->xss_clean($this->input->post('upload'));
					$created_by= $this->session->userdata('admin_username');
					$img_name= $this->security->xss_clean($this->input->post('img_name'));				
					$brands= $this->security->xss_clean($this->input->post('brands'));				
					$promo_group_data = array();
					$items2 = $items;

					if($process=='a'){
					//generate user id
					$promo_id= $this -> setup -> getpromoid();
						if ($promo_id) {
							$promo_id  = $promo_id["promo_id"];
							$promo_id = substr($promo_id, -10);
							$pid = "PRM" . sprintf("%010d", $promo_id + 1);
						} else {
							$pid = "PRM" . sprintf("%010d", 1);
						}
						//uploading img
						$filename='';
						$path='';	
							if($_FILES["upload"]['name']){
								//old upload
								/*$path_parts = pathinfo($_FILES["upload"]["name"]);
								$ext = $path_parts['extension']; 
					
									 //die(realpath(APPPATH .'assets/item_covers/'). time().".".$ext);
					    
								     move_uploaded_file($_FILES["upload"]["tmp_name"],
								 	 realpath(APPPATH .'assets/item_covers/'). time().".".$ext);
									 
								     
								     $path=realpath(APPPATH .'assets/item_covers/'). time().".".$ext;	*/
								 //ci upload.
								 $filename="".time().".png";
								 $config['upload_path'] = './assets/img/promo_banner/';
								 $config['allowed_types'] = 'gif|jpg|png';	
								 $config['file_name'] = $filename;
								 $config['encrypt_name']= FALSE;
								 
								 //load ci upload						
								 $this->upload->initialize($config);
								 $this->upload->do_upload('upload');
								if ( ! $this->upload->do_upload('upload'))
								{
									$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Upload Failed.</div>');						
									redirect('admin/form/promo/'.$this->encryption->encode('a').'');	
								}
								else
								{
									$lastupload_img = $this->upload->file_name;
									//Image Resizing
									$config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
									$config['new_image'] = $filename;
									$config['maintain_ratio'] = FALSE;
									$config['width'] = 750;
									$config['height'] = 442;
									$config['create_thumb'] = FALSE;
									$this->load->library('image_lib', $config);
							
									if ( ! $this->image_lib->resize()){
										$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Image Resizing Failed.</div>');						
										redirect('admin/form/promo/'.$this->encryption->encode('a').'');	
									}
								//delete after upload new item
								unlink("./assets/img/promo_banner/".$lastupload_img);									
								}							 
 						     
							}
						if(!empty($items)){
							$items = implode(',',$items);
						} 
						$data=array(
						"promo_id"=>$pid,
						"promo_code"=>$promo_code,
						"promo_scheme"=>$promo_scheme,
						"promo_name"=>$promo_name,
						"promo_type"=>$promo_type,	
						"items"=>$items,
						"promo_path"=>assets_url()."img/promo_banner/".$this->image_lib->new_image,
						"promo_filename"=>$this->image_lib->new_image,
						"promo_from"=>date('Y-m-d', strtotime($start)),
						"promo_to"=>date('Y-m-d',strtotime($end)),				
						"status"=>$status,
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						//check if category already exist
						$promo_exist= $this -> setup -> PromoExist($promo_code);
						
						if($promo_exist){
							$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Promo already Exist.</div>');						
							redirect('admin/form/promo/'.$this->encryption->encode('a').'');						
						} else {					
							//insert data
 
							$insert_data=$this->setup->savePromo($data);
							//upload img
 
							if($insert_data>1){

								if($promo_type == 'item' || $promo_type == 'bundle'){
									foreach ($items2 as $key => $value) {

										$promo_group_data []= array(
											'promo_id'	=> $insert_data,
											'type'		=> $promo_type,
											'value'		=> $value,
											);

									}
								}else if($promo_type == 'brand'){
									foreach ($brands as $key => $value) {

										$promo_group_data []= array(
											'promo_id'	=> $insert_data,
											'type'		=> $promo_type,
											'value'		=> $value,
											);

									}

								}


								$group_status = 1;
								if(!empty($promo_group_data)){
									$group_status = $this->setup->savePromoGroup($promo_group_data);

								}

							//logging activity for audit..	
							$req=array(
							"action"=>"Create Promo",
							"action_by"=>$created_by,
							"action_date"=>date('Y-m-d H:i:s')
							);	
							$log_activity=$this->setup->activityLog($req);			
							$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Promo Successfully Made.</div>');	
							redirect('admin/form/promo/'.$this->encryption->encode('a').'');
																						        
							}else if($insert_data==0 && $group_status == 0) {
							$this->session->set_flashdata('msg','					
							<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
							redirect('admin/form/promo/'.$this->encryption->encode('a').'');										
							}					
					   }													
					} else {
							
						//uploading img
							if($_FILES["upload"]['name']){
								//old upload
								/*$path_parts = pathinfo($_FILES["upload"]["name"]);
								$ext = $path_parts['extension']; 
					
									 //die(realpath(APPPATH .'assets/item_covers/'). time().".".$ext);
					    
								     move_uploaded_file($_FILES["upload"]["tmp_name"],
								 	 realpath(APPPATH .'assets/item_covers/'). time().".".$ext);
									 
								     
								     $path=realpath(APPPATH .'assets/item_covers/'). time().".".$ext;	*/
								 //ci upload.
								 $filename="".time().".png";
								 $config['upload_path'] = './assets/img/promo_banner/';
								 $config['allowed_types'] = 'gif|jpg|png';	
								 $config['file_name'] = $filename;
								 $config['encrypt_name']= FALSE;
								 
								 //load ci upload						
								 $this->upload->initialize($config);
								 $this->upload->do_upload('upload');
								if ( ! $this->upload->do_upload('upload'))
								{
									$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Upload Failed.</div>');						
									redirect('admin/form/promo/'.$this->encryption->encode($pid).'');	
								}
								else
								{
									$lastupload_img = $this->upload->file_name;
									//Image Resizing
									$config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
									$config['maintain_ratio'] = FALSE;
									$config['width'] = 750;
									$config['height'] = 442;
									$config['new_image'] = $filename;
							
									$this->load->library('image_lib', $config);
							
									if ( ! $this->image_lib->resize()){
										$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Image Resizing Failed.</div>');						
										redirect('admin/form/promo/'.$this->encryption->encode($pid).'');	
									}
								//delete after upload new item
								//die("./assets/img/item_covers/".$img_name);
								unlink("./assets/img/promo_banner".$lastupload_img);	
								unlink("./assets/img/promo_banner/".$img_name);	 	
								}							 
								$path=assets_url()."img/promo_banner/".$this->image_lib->new_image;						
								$filenamex=$this->image_lib->new_image;						     
							} else {
								$path=assets_url()."img/promo_banner/".$img_name;						
								$filenamex=$img_name;
							}		
						if(!empty($items)){
							$items = implode(',',$items);
						} 										
						$data=array(
						"promo_id"=>$pid,
						"promo_code"=>$promo_code,
						"promo_scheme"=>$promo_scheme,
						"promo_name"=>$promo_name,
						"promo_type"=>$promo_type,	
						"items"=>$items,
						"promo_path"=>$path,
						"promo_filename"=>$filenamex,
						"promo_from"=>date('Y-m-d', strtotime($start)),
						"promo_to"=>date('Y-m-d',strtotime($end)),			
						"status"=>$status,
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						$update_data=$this->setup->UpdatePromo($data,$pid);	
						if($promo_type == 'item' || $promo_type == 'bundle'){
							foreach ($items2 as $key => $value) {

								$promo_group_data []= array(
									'promo_id'	=> $id,
									'type'		=> $promo_type,
									'value'		=> $value,
									);

							}
						}else if($promo_type == 'brand'){
							foreach ($brands as $key => $value) {

								$promo_group_data []= array(
									'promo_id'	=> $id,
									'type'		=> $promo_type,
									'value'		=> $value,
									);

							}

						}
 

						$group_status = 1;
						if(!empty($promo_group_data)){
							$group_status = $this->setup->updatePromoGroup($promo_group_data, $id);

						}

						if($update_data==1){


								//logging activity for audit..	
								$req=array(
								"action"=>"Edit Promo",
								"action_by"=>$created_by,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);									
								$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Promo Successfully Updated.</div>');
					            redirect('admin/form/promo/'.$this->encryption->encode($pid).'');
						} else if($update_data==0 && $group_status == 0) {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
					            redirect('admin/form/promo/'.$this->encryption->encode($pid).'');		
						}												
					}						
					
				}			
			}
			if($module=='promo_type'){
				if($_POST){
					$ptid=$this->security->xss_clean($this->input->post('idx'));
					$ptpye_name=$this->security->xss_clean($this->input->post('ptpye_name'));
					$ptpye_qty=$this->security->xss_clean($this->input->post('ptpye_qty'));
					$ptpye_disc=$this->security->xss_clean($this->input->post('ptpye_disc'));
					$status=$this->security->xss_clean($this->input->post('status'));
					$created_by= $this->session->userdata('admin_username');
					
					if($process=='a'){
					//generate user id
					$ptype_id= $this -> setup -> getptypeid();
						if ($ptype_id) {
							$ptype_id  = $ptype_id["ptype_id"];
							$ptype_id = substr($ptype_id, -10);
							$ptid = "PS" . sprintf("%010d", $ptype_id + 1);
						} else {
							$ptid = "PS" . sprintf("%010d", 1);
						}
						//data 
						$data=array(
						"ptype_id"=>$ptid,
						"ptype_name"=>$ptpye_name,
						"qty"=>$ptpye_qty,
						"disc_percent"=>$ptpye_disc,					
						"status"=>$status,
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						//check if category already exist
						$ptype_exist= $this -> setup -> PtypeExist($ptpye_name);
						
						if($ptype_exist){
							$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Promo Scheme already Exist.</div>');						
							redirect('admin/form/promo_type/'.$this->encryption->encode('a').'');						
						} else {					
							//insert data
							$insert_data=$this->setup->savePtype($data);
							//upload img

							if($insert_data!=0){



							//logging activity for audit..	
							$req=array(
							"action"=>"Create Promo Scheme",
							"action_by"=>$created_by,
							"action_date"=>date('Y-m-d H:i:s')
							);	
							$log_activity=$this->setup->activityLog($req);			
							$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Promo Scheme Successfully Made.</div>');	
							redirect('admin/form/promo_type/'.$this->encryption->encode('a').'');
																						        
							}else if($insert_data==0 ) {
							$this->session->set_flashdata('msg','					
							<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
							redirect('admin/form/promo_type/'.$this->encryption->encode('a').'');										
							}					
					   }													
					} else {
						//data 
						$data=array(
						"ptype_id"=>$ptid,
						"ptype_name"=>$ptpye_name,
						"qty"=>$ptpye_qty,
						"disc_percent"=>$ptpye_disc,					
						"status"=>$status,
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						$update_data=$this->setup->UpdatePtype($data,$ptid);									
						if($update_data==1){
								//logging activity for audit..	
								$req=array(
								"action"=>"Edit Brand",
								"action_by"=>$created_by,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);									
								$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Promo Scheme Successfully Updated.</div>');
					            redirect('admin/form/promo_type/'.$this->encryption->encode($ptid).'');
						} else if($update_data==0) {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
					            redirect('admin/form/promo_type/'.$this->encryption->encode($ptid).'');		
						}												
					}									
				}									
			}			
			//logo start
			if($module=='logo'){
				if($_POST){
					$lid=$this->security->xss_clean($this->input->post('idx'));
					$logo_filename=$this->security->xss_clean($this->input->post('logo_filename'));
					$logo_path=$this->security->xss_clean($this->input->post('logo_path'));
					$facebook_url=$this->security->xss_clean($this->input->post('facebook_url'));
					$twitter_url=$this->security->xss_clean($this->input->post('twitter_url'));
					$image_path= $this->session->userdata('image_path');
					$image_name= $this->security->xss_clean($this->input->post('image_name'));	
					$created_by= $this->security->xss_clean($this->input->post('created_by'));	
					$date_created= $this->security->xss_clean($this->input->post('date_created'));				
					if($process=='a'){
					//generate user id
					$logo_id= $this -> setup -> getlogoid();
						if ($logo_id) {
							$logo_id  = $logo_id["logo_id"];
							$logo_id = substr($logo_id, -10);
							$lid = "L" . sprintf("%010d", $logo_id + 1);
						} else {
							$lid = "L" . sprintf("%010d", 1);
						}
						//uploading img
						$filename='';
						$path='';	
							if($_FILES["upload"]['name']){
								//old upload
								/*$path_parts = pathinfo($_FILES["upload"]["name"]);
								$ext = $path_parts['extension']; 
					
									 //die(realpath(APPPATH .'assets/item_covers/'). time().".".$ext);
					    
								     move_uploaded_file($_FILES["upload"]["tmp_name"],
								 	 realpath(APPPATH .'assets/item_covers/'). time().".".$ext);
									 
								     
								     $path=realpath(APPPATH .'assets/item_covers/'). time().".".$ext;	*/
								 //ci upload. so dito po part mag upupload tau
								 $filename="".date("Y")."_".time().".png";//<-- eto for filename
								 $config['upload_path'] = './assets/img/logo/';//path
								 $config['allowed_types'] = 'gif|jpg|png';//<--allowed type para kung makalusot sa js	
								 $config['file_name'] = $filename;//filename
								 $config['encrypt_name']= FALSE;
								 
								 //load ci upload dito part mag upupload n sya...						
								 $this->upload->initialize($config);
								 $this->upload->do_upload('upload');
								if ( ! $this->upload->do_upload('upload'))
								{
									$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Upload Failed.</div>');						
									redirect('admin/form/logo/'.$this->encryption->encode('a').'');	
								}
								else
								{
									//then after po kase ng upload ng reresize po tau ng image to minimize data and para maganda tignan images nila
									$lastupload_img = $this->upload->file_name;
									//Image Resizing
									$config['source_image'] = $this->upload->upload_path.$this->upload->file_name;//<kunin file name ng recent uploaded image
									$config['new_image'] = $filename;//new name ng image pag ka resize 
									$config['maintain_ratio'] = FALSE;
									$config['width'] = 200;
									$config['height'] = 100;
									$config['create_thumb'] = FALSE;
									$this->load->library('image_lib', $config);
							
									if ( ! $this->image_lib->resize()){
										$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Image Resizing Failed.</div>');						
										redirect('admin/form/logo/'.$this->encryption->encode('a').'');	
									}
								//delete after upload new logo
								unlink("./assets/img/logo/".$lastupload_img);									
								}							 
 						     
							}

						$data=array(
						"logo_id"=>$lid,
						"logo_filename"=>$logo_filename,
						"logo_path"=>$logo_path,
						"facebook_url"=>$facebook_url,
						"twitter_url"=>$twitter_url,
						"logo_path"=>assets_url()."img/logo/".$this->image_lib->new_image,
						"logo_filename"=>$this->image_lib->new_image,
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						//check if category already exist
						$logo_exist= $this -> setup -> logoExist($logo_filename);
						
						if($logo_exist){
							$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">logo already Exist.</div>');						
							redirect('admin/form/logo/'.$this->encryption->encode('a').'');						
						} else {					
							//insert data
							$insert_data=$this->setup->savelogo($data);
							//upload img
							if($insert_data==1){
							//logging activity for audit..	
							$req=array(
							"action"=>"Create logo",
							"action_by"=>$created_by,
							"action_date"=>date('Y-m-d H:i:s')
							);	
							$log_activity=$this->setup->activityLog($req);			
							$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">logo Successfully Made.</div>');	
							redirect('admin/form/logo/'.$this->encryption->encode('a').'');
																						        
							}else if($insert_data==0) {
							$this->session->set_flashdata('msg','					
							<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
							redirect('admin/form/logo/'.$this->encryption->encode('a').'');										
							}					
					   }													
					} else {
							
						//uploading img
							if($_FILES["upload"]['name']){
								//old upload
								/*$path_parts = pathinfo($_FILES["upload"]["name"]);
								$ext = $path_parts['extension']; 
					
									 //die(realpath(APPPATH .'assets/item_covers/'). time().".".$ext);
					    
								     move_uploaded_file($_FILES["upload"]["tmp_name"],
								 	 realpath(APPPATH .'assets/item_covers/'). time().".".$ext);
									 
								     
								     $path=realpath(APPPATH .'assets/item_covers/'). time().".".$ext;	*/
								 //ci upload.
								 $filename="".date("Y")."_".time().".png";
								 $config['upload_path'] = './assets/img/logo/';
								 $config['allowed_types'] = 'gif|jpg|png';	
								 $config['file_name'] = $filename;
								 $config['encrypt_name']= FALSE;
								 
								 //load ci upload						
								 $this->upload->initialize($config);
								 $this->upload->do_upload('upload');
								if ( ! $this->upload->do_upload('upload'))
								{
									$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Upload Failed.</div>');						
									redirect('admin/form/logo/'.$this->encryption->encode($lid).'');	
								}
								else
								{
									$lastupload_img = $this->upload->file_name;
									//Image Resizing
									$config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
									$config['maintain_ratio'] = FALSE;
									$config['width'] = 200;
									$config['height'] = 100;
									$config['new_image'] = $filename;
							
									$this->load->library('image_lib', $config);
							
									if ( ! $this->image_lib->resize()){
										$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Image Resizing Failed.</div>');						
										redirect('admin/form/logo/'.$this->encryption->encode($lid).'');	
									}
								//delete after upload new item
								//die("./assets/img/item_covers/".$img_name);
								//eto po kaibahan lng... pag ng eedit nid delete ung old image
								unlink("./assets/img/logo/".$lastupload_img);//<eto po ung old image	
								unlink("./assets/img/logo/".$img_name);	 	
								}							 
								$path=assets_url()."img/logo/".$this->image_lib->new_image;						
								$filenamex=$this->image_lib->new_image;						     
							} else {
								$path=assets_url()."img/logo/".$img_name;	//<eto naman pag di nya binago image nya					
								$filenamex=$img_name;
							}	
			
						$data=array(
						"logo_id"=>$lid,
						"logo_filename"=>$logo_filename,
						"logo_path"=>$logo_path,
						"facebook_url"=>$facebook_url,
						"twitter_url"=>$twitter_url,
						"logo_path"=>assets_url()."img/logo/".$this->image_lib->new_image,
						"logo_filename"=>$this->image_lib->new_image,
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						$update_data=$this->setup->Updatelogo($data,$lid);									
						if($update_data==1){
								//logging activity for audit..	
								$req=array(
								"action"=>"Edit logo",
								"action_by"=>$created_by,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);									
								$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">logo Successfully Updated.</div>');
					            redirect('admin/form/logo/'.$this->encryption->encode($lid).'');
						} else if($update_data==0) {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
					            redirect('admin/form/logo/'.$this->encryption->encode($lid).'');		
						}												
					}						
					
				}
			}								

			//end of logo
			else if($module=='announcement'){
				//dito tau ggwa	
				if($_POST){
					//aid means announcement id//
					$aid=$this->security->xss_clean($this->input->post('idx')); //eto po ung mga kukunin ntin sa view etonng idx default sya kase nid ntin sya sa edit
					$date=$this->security->xss_clean($this->input->post('announce_date'));
					$subj=$this->security->xss_clean($this->input->post('subj'));
					$details=$this->security->xss_clean($this->input->post('details'));
					//so ok na po siya
			
					//sonapansin nyo nilagay ko lng ung mga kinuha ko names para matagwag sy at mapasok sa table...
					//$status=$this->security->xss_clean($this->input->post('status'));
					$created_by= $this->session->userdata('admin_username');
					//eto default para ma trace kng cno ng create audit trail ba
					
					if($process=='a'){//dito naman po nalalaman kng ano action ggwin ntin ung nasa view kanila a at e pinapasa sya dito para alm ntin kng add or edit ang action
					//generate user id
					//eto section ng generate ng id ntin 
					$announcement_id= $this -> setup -> getannouncementid();
					//eto part na to gngwa ntin is mag generate ng id pero module ntin ang patterng nya is Acronym ng module then 10 zero ng increment sya.
						if ($announcement_id) {
							//eto pag may id na nagawa susundan nlng ng next id
							$announcement_id  = $announcement_id["announcement_id"];
							$announcement_id = substr($announcement_id, -10);
							$aid = "A" . sprintf("%010d", $announcement_id + 1);
						} else {
							//dito pag wla pang existing so pansin mo 1 sya
							$aid = "A" . sprintf("%010d", 1);
						}
						//after generate ng id sunod eh lalagay mo n mga iinsert mo data sa table (aka sql ) sa array.
						//data 
						//ung nasa "fielnd name ng talbe ex ("annnouncement_id"=>$aid //ung nagenerate na annoucenment id)"
						$data=array(
						"announcement_id"=>$aid,
						"date_announce"=>$date,
						"subj"=>$subj,
						"message"=>$details,
						"announce_by"=>$created_by,
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						//check if category already exist
						$announcement_exist= $this -> setup -> AnnouncementExist($subj);
						
						if($announcement_exist){
							$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Announcement already Exist.</div>');						
							redirect('admin/form/announcement/'.$this->encryption->encode('a').'');						
						} else {					
							//insert data
							$insert_data=$this->setup->saveAnnouncement($data);
							//upload img
							if($insert_data==1){
							//logging activity for audit..	
							$req=array(
							"action"=>"Create Announcement",
							"action_by"=>$created_by,
							"action_date"=>date('Y-m-d H:i:s')
							);	
							$log_activity=$this->setup->activityLog($req);			
							$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Announcement Successfully Made.</div>');	
							redirect('admin/form/announcement/'.$this->encryption->encode('a').'');
																						        
							}else if($insert_data==0) {
							$this->session->set_flashdata('msg','					
							<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
							redirect('admin/form/announcement/'.$this->encryption->encode('a').'');										
							}					
					   }													
					} else {
						//eto sa edit pero try muna ntin kng nakaka pag insert na ...
						//data 
						$data=array(
						"date_announce"=>$date,
						"subj"=>$subj,
						"message"=>$details,
						"announce_by"=>$created_by,
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);
						$update_data=$this->setup->UpdateAnnouncement($data,$aid);									
						if($update_data==1){
								//logging activity for audit..	
								$req=array(
								"action"=>"Edit Announcement",
								"action_by"=>$created_by,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);									
								$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Announcement Successfully Updated.</div>');
					            redirect('admin/form/announcement/'.$this->encryption->encode($aid).'');
						} else if($update_data==0) {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
					            redirect('admin/form/announcement/'.$this->encryption->encode($aid).'');		
						}												
					}					
					
					
				}											
			}
			else if($module=='reports'){
			
			}
			else if($module=='about_us'){
				
				if($_POST){
					$aboutusid=$this->security->xss_clean($this->input->post('idx')); //eto po ung mga kukunin ntin sa view etonng idx default sya kase nid ntin sya sa edit
					$date_aboutus=$this->security->xss_clean($this->input->post('date_aboutus'));
					$contacts=$this->security->xss_clean($this->input->post('contacts'));
					$about=$this->security->xss_clean($this->input->post('about'));
					$created_by= $this->session->userdata('admin_username');
					
					if($process=='a'){
					$aboutus_id= $this -> setup -> getaboutusid();
					if ($aboutus_id) {
							$aboutus_id  = $aboutus_id["about_id"];
							$aboutus_id = substr($aboutus_id, -10);
							$aboutusid = "A" . sprintf("%010d", $aboutus_id + 1);
						} else {
							$aboutusid = "A" . sprintf("%010d", 1);
						}
						$data=array(
						"about_id"=>$aboutusid,
						"date_aboutus"=>$date_aboutus,
						"about"=>$about,
						"contacts"=>$contacts,						
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						$aboutus_exist= $this -> setup -> aboutusExist($about);
						
						if($aboutus_exist){
							$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">About Us already Exist.</div>');						
							redirect('admin/form/about_us/'.$this->encryption->encode('a').'');						
						} else {					
							//insert data
							$insert_data=$this->setup->saveaboutus($data);
							//upload img
							if($insert_data==1){
							//logging activity for audit..	
							$req=array(
							"action"=>"Create About Us",
							"action_by"=>$created_by,
							"action_date"=>date('Y-m-d H:i:s')
							);	
							$log_activity=$this->setup->activityLog($req);			
							$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">About Us Successfully Made.</div>');	
							redirect('admin/form/about_us/'.$this->encryption->encode('a').'');
																						        
							}else if($insert_data==0) {
							$this->session->set_flashdata('msg','					
							<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
							redirect('admin/form/about_us/'.$this->encryption->encode('a').'');										
							}					
					   }													
					} else {
						$data=array(
						"about_id"=>$aboutusid,
						"date_aboutus"=>$date_aboutus,
						"about"=>$about,
						"contacts"=>$contacts,
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);
						$update_data=$this->setup->Updateaboutus($data,$aboutusid);									
						if($update_data==1){
								$req=array(
								"action"=>"Edit About Us",
								"action_by"=>$aboutusid,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);									
								$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">About Us Successfully Updated.</div>');
					            redirect('admin/form/about_us/'.$this->encryption->encode($aboutusid).'');
						} else if($update_data==0) {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
					            redirect('admin/form/about_us/'.$this->encryption->encode($aboutusid).'');		
						}												
					}					
					
					
					}										
			
			} else if($module=='terms_conditions'){
				
				if($_POST){
					$termsid=$this->security->xss_clean($this->input->post('idx')); //eto po ung mga kukunin ntin sa view etonng idx default sya kase nid ntin sya sa edit
					$date_terms=$this->security->xss_clean($this->input->post('date_terms'));
					$terms=$this->security->xss_clean($this->input->post('terms'));
					$created_by= $this->session->userdata('admin_username');
					
					if($process=='a'){
					$terms_id= $this -> setup -> gettermsid();
					if ($terms_id) {
							$terms_id  = $terms_id["terms_id"];
							$terms_id = substr($terms_id, -10);
							$terms_id = "A" . sprintf("%010d", $terms_id + 1);
						} else {
							$terms_id = "A" . sprintf("%010d", 1);
						}
						$data=array(
						"terms_id"=>$terms_id,
						"date_terms"=>$date_terms,
						"terms"=>$terms,					
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						$termsconditions_exist= $this -> setup -> termsconditionsExist($about);
						
						if($termsconditions_exist){
							$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Terms and Conditions already Exist.</div>');						
							redirect('admin/form/terms_conditions/'.$this->encryption->encode('a').'');						
						} else {					
							//insert data
							$insert_data=$this->setup->savetermsconditions($data);
							//upload img
							if($insert_data==1){
							//logging activity for audit..	
							$req=array(
							"action"=>"Create Terms and Conditions",
							"action_by"=>$created_by,
							"action_date"=>date('Y-m-d H:i:s')
							);	
							$log_activity=$this->setup->activityLog($req);			
							$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Terms and Conditions Successfully Made.</div>');	
							redirect('admin/form/terms_conditions/'.$this->encryption->encode('a').'');
																						        
							}else if($insert_data==0) {
							$this->session->set_flashdata('msg','					
							<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
							redirect('admin/form/terms_conditions/'.$this->encryption->encode('a').'');										
							}					
					   }													
					} else {
						$data=array(
						"terms_id"=>$termsid,
						"date_terms"=>$date_terms,
						"terms"=>$terms,
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);
						$update_data=$this->setup->Updatetermsconditions($data,$termsid);									
						if($update_data==1){
								$req=array(
								"action"=>"Edit Terms and Conditions",
								"action_by"=>$termsid,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);									
								$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Terms and Conditions Successfully Updated.</div>');
					            redirect('admin/form/terms_conditions/'.$this->encryption->encode($termsid).'');
						} else if($update_data==0) {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
					            redirect('admin/form/terms_conditions/'.$this->encryption->encode($termsid).'');		
						}												
					}					
					
					}				
			} else if($module=='messages'){
				
				if($_POST){
					$termsid=$this->security->xss_clean($this->input->post('idx')); //eto po ung mga kukunin ntin sa view etonng idx default sya kase nid ntin sya sa edit
					$name=$this->security->xss_clean($this->input->post('name'));
					$email=$this->security->xss_clean($this->input->post('email'));
					$subject= $this->security->xss_clean($this->input->post('subject'));
					$message= $this->security->xss_clean($this->input->post('message'));
					$date=$this->security->xss_clean($this->input->post('date'));
					
					if($process=='a'){
					$id= $this -> setup -> getmessagesid();
					if ($id) {
							$id  = $id["id"];
							$id = substr($id, -10);
							$id = "A" . sprintf("%010d", $id + 1);
						} else {
							$id = "A" . sprintf("%010d", 1);
						}
						$data=array(
						"id"=>$id,
						"name"=>$name,
						"email"=>$email,					
						"subject"=>$subject,
						"date"=>date('Y-m-d H:i:s')
						);	
						$messages_exist= $this -> setup -> messagesExist($subject);
						
						if($messages_exist){
							$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Message already Exist.</div>');						
							redirect('admin/form/messages/'.$this->encryption->encode('a').'');						
						} else {					
							//insert data
							$insert_data=$this->setup->savemessages($data);
							//upload img
							if($insert_data==1){
							//logging activity for audit..	
							$req=array(
							"action"=>"Messages",
							"action_by"=>$created_by,
							"action_date"=>date('Y-m-d H:i:s')
							);	
							$log_activity=$this->setup->activityLog($req);			
							$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Messages Successfully Made.</div>');	
							redirect('admin/form/messages/'.$this->encryption->encode('a').'');
																						        
							}else if($insert_data==0) {
							$this->session->set_flashdata('msg','					
							<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
							redirect('admin/form/messages/'.$this->encryption->encode('a').'');										
							}					
					   }													
					} else {
						$data=array(
						"id"=>$id,
						"name"=>$name,
						"email"=>$email,					
						"subject"=>$subject,
						"date"=>date('Y-m-d H:i:s')
						);
						$update_data=$this->setup->Updatemessages($data,$id);									
						if($update_data==1){
								$req=array(
								"action"=>"Edit Messages",
								"action_by"=>$id,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);									
								$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Messages Successfully Updated.</div>');
					            redirect('admin/form/messages/'.$this->encryption->encode($id).'');
						} else if($update_data==0) {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
					            redirect('admin/form/messages/'.$this->encryption->encode($id).'');		
						}												
					}					
					
					}									
			
			} else if($module=='options'){
					if($_POST){
					$id=$this->security->xss_clean($this->input->post('idx'));
					$status=$this->security->xss_clean($this->input->post('status'));
					$brand_name = $this->security->xss_clean($this->input->post('name'));
					$created_by= $this->session->userdata('admin_username');
					if($process=='a'){
						//data 
						$data=array(
						"option_name"=>ucfirst(ucwords($brand_name)),
						"status"=>$status,
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						//check if category already exist
						$brand_exist= $this -> setup -> OptionExist($brand_name);
						
						if($brand_exist){
							$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Item Option already Exist.</div>');						
							redirect('admin/form/options/'.$this->encryption->encode('a').'');						
						} else {					
							//insert data
							$insert_data=$this->setup->saveOption($data);
							//upload img
							if($insert_data==1){
							//logging activity for audit..	
							$req=array(
							"action"=>"Create Brand",
							"action_by"=>$created_by,
							"action_date"=>date('Y-m-d H:i:s')
							);	
							$log_activity=$this->setup->activityLog($req);			
							$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Item Option Successfully Made.</div>');	
							redirect('admin/form/options/'.$this->encryption->encode('a').'');
																						        
							}else if($insert_data==0) {
							$this->session->set_flashdata('msg','					
							<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
							redirect('admin/form/options/'.$this->encryption->encode('a').'');										
							}					
					   }													
					} else {
						$data=array(
						"option_name"=>ucfirst(ucwords($brand_name)),
						"status"=>$status,
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						$update_data=$this->setup->UpdateOption($data,$id);									
						if($update_data==1){
								//logging activity for audit..	
								$req=array(
								"action"=>"Edit Option",
								"action_by"=>$created_by,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);									
								$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Item Option Successfully Updated.</div>');
					            redirect('admin/form/options'.$this->encryption->encode($id).'');
						} else if($update_data==0) {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
					            redirect('admin/form/options/'.$this->encryption->encode($id).'');		
						}												
					}										
				}			
			} else if($module=='batch'){
				if($_POST){
				$id=$this->security->xss_clean($this->input->post('idx'));	
				$date_from=$this->security->xss_clean($this->input->post('batch_datefrom'));	
				$date_to=$this->security->xss_clean($this->input->post('batch_dateto'));	
				$created_by= $this->session->userdata('admin_username');	
					if($process=='a'){
						//data 
						$data=array(
						"date_from"=>$date_from,
						"date_to"=>$date_to,
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						//check if category already exist
						$batch_exist= $this -> setup -> BatchExist($date_from,$date_to);
						
						if($batch_exist){
							$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Batch with this dates is already Exist.</div>');						
							redirect('admin/form/batch/'.$this->encryption->encode('a').'');						
						} else {					
							//insert data
							$insert_data=$this->setup->saveBatch($data);
							//upload img
							if($insert_data==1){
							//logging activity for audit..	
							$req=array(
							"action"=>"Create Batch",
							"action_by"=>$created_by,
							"action_date"=>date('Y-m-d H:i:s')
							);	
							$log_activity=$this->setup->activityLog($req);			
							$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Batch Successfully Made.</div>');	
							redirect('admin/form/batch/'.$this->encryption->encode('a').'');
																						        
							}else if($insert_data==0) {
							$this->session->set_flashdata('msg','					
							<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
							redirect('admin/form/batch/'.$this->encryption->encode('a').'');										
							}					
					   }																
					} else {
						$data=array(
						"id"=>$id,
						"date_from"=>$date_from,
						"date_to"=>$date_to,
						"created_by"=>$created_by,
						"created_at"=>date('Y-m-d H:i:s')
						);	
						$update_data=$this->setup->UpdateBatch($data,$id);									
						if($update_data==1){
								//logging activity for audit..	
								$req=array(
								"action"=>"Edit Batch",
								"action_by"=>$created_by,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);									
								$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Batch Successfully Updated.</div>');
					            redirect('admin/form/batch'.$this->encryption->encode($id).'');
						} else if($update_data==0) {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
					            redirect('admin/form/batch/'.$this->encryption->encode($id).'');		
						}							
					}					
				}
			}
			//end
			
			//end of messages
	}
	function getCategory($brand){
		$get_data= $this->setup->getCategory($brand);
		
		echo json_encode($get_data);	
	}
	function getSubcategory($category){
		$get_data= $this->setup->getSubCategory($category);
		
		echo json_encode($get_data);	
	}
	function delete_item($id){
		$this->setup->CheckIfLogin();
		$created_by= $this->session->userdata('admin_username');
		$delete_data=$this->setup->DeleteItem($id,$type);	
						if($delete_data==1){
								//logging activity for audit..	
								$req=array(
								"action"=>"Delete Item",
								"action_by"=>$created_by,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);									
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Item Successfully deleted.</div>');
					            redirect('admin/grid/items');
						} else if($delete_data==0) {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
					            redirect('admin/grid/items');		
						}		
			
	}
	function delete_user($id){
		$this->setup->CheckIfLogin();
		$deleted_by= $this->session->userdata('admin_username');
		$delete_data=$this->setup->DeleteUser($id);	
						if($delete_data==1){
								//logging activity for audit..	
								$req=array(
								"action"=>"Delete User",
								"action_by"=>$deleted_by,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);									
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">User Successfully deleted.</div>');
					            redirect('admin/grid/users');
						} else if($delete_data==0) {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
					            redirect('admin/grid/users');		
						}		
			
	}	
	function delete_category($id){
		$this->setup->CheckIfLogin();
		$created_by= $this->session->userdata('admin_username');
		$delete_data=$this->setup->DeleteCategory($id);	
						if($delete_data==1){
								//logging activity for audit..	
								$req=array(
								"action"=>"Delete ".$type,
								"action_by"=>$created_by,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);									
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Category Successfully deleted.</div>');
					            redirect('admin/grid/category');
						} else if($delete_data==0) {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
					            redirect('admin/grid/category');		
						}		
			
	}
	function delete_scategory($id){
		$this->setup->CheckIfLogin();
		$created_by= $this->session->userdata('admin_username');
		$delete_data=$this->setup->DeteleSCategory($id);	
						if($delete_data==1){
								//logging activity for audit..	
								$req=array(
								"action"=>"Delete ".$type,
								"action_by"=>$created_by,
								"action_date"=>date('Y-m-d H:i:s')
								);	
								$log_activity=$this->setup->activityLog($req);									
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Sub Category Successfully deleted.</div>');
					            redirect('admin/grid/category');
						} else if($delete_data==0) {
								$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
					            redirect('admin/grid/category');		
						}		
			
	}
	function item_exist(){
		$item_name=$this->security->xss_clean($this->input->post('item'));	
		//die($id);
		$item_exist= $this -> setup -> ItemExist($item_name);
		//die(var_dump($item_exist));
		if($item_exist){
		$item=[
			"success"=>true,
			"msg"=>"Item already Exist.",
			];	
		 echo json_encode($item);
		} else{
		$item=[
			"success"=>false,
			"msg"=>"Character length not morethan 70.",
			];	
		 echo json_encode($item);			
		}
	}
	function orders_table($status,$batch_id){
				$this->setup->CheckIfLogin();
				$get_data=$this->setup->orders_data($status,$batch_id);
				if(empty($get_data)){
					$finalres=array(
					"data"=>''
					);					
				} else {
					foreach($get_data as $table_data){
						$result[]=array(
								ucfirst($table_data['batch_id']),
								ucfirst($table_data['date_from']),
								ucfirst($table_data['date_to']),
								"<a href='".base_url()."admin/orders_grid/".$status."/".$this->encryption->encode($table_data['batch_id'])."' class='btn-link' title='Items'>
								<span class='glyphicon glyphicon-th-list'></span>
								</a>
								"	,
						);							
					}
					$finalres=array(
					"data"=>$result
					);
				}		
				echo json_encode($finalres);				
	}	
	function orders_grid($status,$id){
		$this->setup->CheckIfLogin();
		$data['logo_data']=$this->setup->getlogo();	
		$data['title']= strtoupper($status);
		$data['batch_id']=$id;	
		$data['module']=$status;

			$data['scripts']=array(assets_url()."js/admin/".$status.".js");
					
			$this->load->view('admin/admin_header_page',$data);
			$this->load->view('admin/orders/'.$status.'/'.$status.'_page',$data); //<-- here the format is application/view/admin/"module name ex items"/items_page.php
			$this->load->view('admin/admin_footer_page',$data);				
	}
	function orders_grid_table($status,$batch_id){
				$this->setup->CheckIfLogin();
				$get_data= $this->setup->orders_data($status,$this->encryption->decode($batch_id));
				if(empty($get_data)){
					$finalres=array(
					"data"=>''
					);					
				} else {
					foreach($get_data as $table_data){
						$result[]=array(
								ucfirst($table_data['date_created']),
								ucfirst($table_data['order_id']),
								ucfirst($table_data['client_name']),
								ucfirst($table_data['qty']),
								"<a href='".base_url()."admin/order_form/".$status."/".$this->encryption->encode($table_data['order_id'])."' class='btn-link' title='Transaction'>
								<span class='fa fa-pencil-square-o fa-3'></span>
								</a>
								",
						);							
					}
					$finalres=array(
					"data"=>$result
					);
				}		
				echo json_encode($finalres);			
	}
	function order_form($status,$order_id){
		$this->setup->CheckIfLogin();
		$data['logo_data']=$this->setup->getlogo();	
		$order_id = $this->encryption->decode($order_id);

		$data['status']=$status;
		$data['title']= strtoupper("TRANSACTION");
		$data['transaction_mother']=$this->setup->transaction_data_mother($status,$order_id);

		//die(var_dump($data['transaction_mother']));
		$data['transaction_payment']=$this->setup->transaction_data_payment($data['transaction_mother']['id']);
		$data['transaction_baby']=$this->setup->transaction_data_baby($status,$order_id);
		$data['couriers']=$this->setup->getCourier();
		
		$data['scripts']=array(assets_url()."js/admin/".$status.".js");
			
			$this->load->view('admin/admin_header_page',$data);
			$this->load->view('admin/orders/ordersform_page',$data);
			$this->load->view('admin/admin_footer_page',$data);					
	}
	function Process_order($status,$order_id){
	//process orders
		if($_POST){
			//values
			$submit=$this->security->xss_clean($this->input->post('submit'));
			//payment
			$sts=$this->security->xss_clean($this->input->post('status'));
			$payment_id=$this->security->xss_clean($this->input->post('payment_id'));

			//mother		
			$order_id=$this->security->xss_clean($this->input->post('order_id'));
			$item_id= $this->security->xss_clean($this->input->post('item_id'));
			$userid= $this->security->xss_clean($this->input->post('user_id'));
			$client_name= $this->security->xss_clean($this->input->post('client_name'));
			$email= $this->security->xss_clean($this->input->post('email'));


			//baby
			$item_name=$this->security->xss_clean($this->input->post('item_name'));
			$price_each=$this->security->xss_clean($this->input->post('price_each'));
			$total_qty=$this->security->xss_clean($this->input->post('total_qty'));
			$subtotal=$this->security->xss_clean($this->input->post('subtotal'));


			//option
			$opname=$this->security->xss_clean($this->input->post('opname'));
			$opqty=$this->security->xss_clean($this->input->post('opqty'));
			//transaction type
			$paymenterror_count=0;
			$this->db->trans_start();
			if($submit=='update'){
			 // //die(var_dump($images_data));
			 for($i=0;$i<3;$i++){
				if($payment_id[$i]!=''){
					$data=[
					"status"=>$sts[$i],
					];

					$save_payment=$this->home->updatePayment($data,$payment_id[$i]);

					if($save_payment==0):
					$paymenterror_count++;
					endif;	
				} 
			 }//end of foreach	

			 $this->db->trans_complete();

				if($paymenterror_count>0) {
					$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');	
					redirect('admin/order_form/'.$status.'/'.$this->encryption->encode($order_id).'');								
				} else {
					//$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Payment Successfully Updated.</div>');	
					//redirect('admin/order_form/'.$status.'/'.$this->encryption->encode($order_id).'');
					//delete babies
					$this->home->deleteBaby($order_id);

					//process transaction baby
					$datatoadd=[];
					$options_data=[];
					for($i=0;$i<count($item_name); $i++){
						

						$datatoadd[]=[
		 				'order_id' => $order_id,
		 				'item_id' => $item_id[$i],
		 				'user_id' => $userid,
		 				'trx_type' => 'active',
		 				'user_name' => $client_name,
		 				'user_email' =>$email,
		 				'item_name' => $item_name[$i],
		 				'remarks' =>'',
		 				'qty' => $total_qty[$i],
		 				'price' => $price_each[$i],
		 				'subtotal' => $subtotal[$i],
		 				'delivery_sched' =>'',
		 				'isdelete' =>'0',
		 				'name' => $name,
		 				'phone_number' => $phone_number,
		 				'address' => $address,
	                    'options' => json_encode( $options ),
	                    'batch_id' => $batch['id']						
						];
					}		
				}			 
			 


		 


			}

		}	
	}					
}	//end controller	