<?php
/*order form*/
class Checkout extends MY_Controller{
	
	function __construct(){
		parent::__construct();
		//$this->load->model('cart_model');

        $this->load->model('home/home_tbl','setup');
        $this->load->model('transaction');
        $this->load->model('transaction_summary');
        $this->load->library('cart');
        
		if(!$this->session->userdata('user_id')){
			$this->session->set_flashdata('login_status', 'Please log in first before checking out');
			redirect('home/index/login');
		}

	}

	function index(){
			$data = [];

            $data['title']='Checkout | Sae sil ParknShop';
			$data['logo_data']=$this->setup->getlogo();			
            //$data['brand_data']=$this->setup->getBrand();
            $data['category_data']=$this->setup->getCategoryMenu();
            //$data['scategory_data']=$this->setup->getSCategory();
            $data['scripts']=array(assets_url()."js/home/home.js", assets_url()."js/home/cart.js"); 
            $data['styles']=array(assets_url()."css/home/home.css");
			$data['discount_conditions'] = $this->setup->getDiscountConditions();
 
            $batch              = $this->setup->getLatestBatch();
            $data['batch_id']   = $batch['id'];
            $user_id            = $this->session->userdata('user_id');
            $data['user']       = $this->setup->getProfile($user_id);
 			
            $this->load->view('home/home_header_page',$data);
            $this->load->view('home/checkout_index.php', $data); 
            $this->load->view('home/home_footer_page',$data);   

		 
	}

	function order(){
			if($_POST){

            $user_id  = $this->session->userdata('user_id');
	 		$userInfo = $this->setup->getProfile($user_id);
        	$user_name = $this->session->userdata('username');
			$discount_conditions = $this->setup->getDiscountConditions();
	 		$name = $this->security->xss_clean($this->input->post('name'));
	 		$phone_number = $this->security->xss_clean($this->input->post('contact_number'));
	 		$address = $this->security->xss_clean($this->input->post('address'));
            $payscheme = $this->security->xss_clean($this->input->post('payscheme'));
            
            $checkifhavedata=$this->setup->getrecent($user_id);
            //die(var_dump($checkifhavedata));
			$flag = TRUE;
	        $dataTmp = $this->cart->contents();
	        $order_id = $this->transaction->generate_order_id();
            if(empty($checkifhavedata)){
              $order_id = "ORD" . str_pad($order_id, 5, 0 , STR_PAD_LEFT); 
              $batch    = $this->setup->getLatestBatch(); 
            } else{
              $order_id=$checkifhavedata['order_id'];
              $batch= $checkifhavedata['batch_id'];
            }
	        
            
        //generate order ID
	 		  /*fetch cart contents*/

	 	//total computations
 	 			$shipping_fee = 0.0;
 	 			$gran_total = 0.0;
 	 			$item_count = $this->cart->total_items();

 	 			if($item_count == 0){
 	 				$this->session->set_flashdata('cart_status', "You currently don't have items in the cart yet");
 	 				redirect('home');
 	 			}

 	 		$trans_items = array();
            $vat = 0;
	 	foreach ($dataTmp as $item) {
       			//load book data
	 			$itemInfo = $this->setup->getItem2($item['id']);
                $price_each = 0;
              

                foreach ($discount_conditions as $key => $value) {
                    $is_discount = false;
                                    if($value['promo_type'] == "item"){ ///TODO : conditions for categories and sub category. NEED dropdown for category/sub in promo add/edit page

                                        if($value['value'] == $itemInfo['item_id']){
                                            $is_discount = true;
                                            $discount_percent = $value['discount_percent'];
                                        }


                                    }else if($value['promo_type'] == 'brand'){
                                        if($value['value'] == $itemInfo['brand_id']){
                                            $is_discount = true;
                                            $discount_percent = $value['discount_percent'];
                                        }          
                                    }
                                }
                                if($is_discount){
                                    $price = $item['price'] - ($item['price'] * ($discount_percent * 0.01));
                                }else{
                                    $price = $item['price'];
                                }  
            	//TODO discount 


                //get options

                $options = array();
                if ($this->cart->has_options($item['rowid']) == TRUE){
                    foreach ($this->cart->product_options($item['rowid']) as $option_name => $option_value){
                         if($option_name != "item_id") {
                            $options [$option_name] = $option_value;
                         }   
                    }
                }

                  
                $sub_price = $price * $item['qty'];

                $price_each = $price;
 

            // }
            //$vat += ($item['price'] * 0.12) * $item['qty'];
            $gran_total += $sub_price  ;

       			//transaction->insert
	 			$transdata = array(
	 				'order_id' => $order_id,
	 				'item_id' => $item['id'],
	 				'user_id' => $user_id,
	 				'trx_type' => 'active',
	 				'user_name' => $user_name,
	 				'user_email' => $userInfo['email'],
	 				'item_name' => $item['name'],
	 				'remarks' =>'',
	 				'qty' => $item['qty'],
	 				'price' => $price_each,
	 				'subtotal' => $sub_price ,
	 				//'vat' =>'0.12',
	 				//'g_total' =>'15.60',
	 				// x`'is_promo' => $itemInfo['ispromo'],
	 				'courier' =>'',
	 				'track_num' =>'',
	 				// 'shipping_fee' =>$shipping_fee,
	 				'delivery_sched' =>'',
	 				'isdelete' =>'0',
	 				'name' => $name,
	 				'phone_number' => $phone_number,
	 				'address' => $address,
                    'options' => json_encode( $options ),
                    'batch_id' => $batch['id']
	 				);

	 			$trans_items []= $transdata;
            // $updated_qty = $bookInfo['qty'] - $item['qty'];
            // $sell_count = is_numeric($bookInfo['sell_count']) ? $bookInfo['sell_count'] : 0  ;
            // $sell_count += $item['qty'];


 
	 			$this->transaction->insert($transdata);


        } 
            //trx_type=0
            
            if(empty($checkifhavedata)){
            $this->transaction_summary->insert( array(
                'order_id' =>$order_id,
                'user_id' => $user_id,
                'client_name' => $user_name,
                'full_name' => $name,
                'email' => $userInfo['email'],
                'cp_num' => $phone_number,
                'qty' => $item_count,
                'sub_total' =>  '',
                'vat' => $vat,
                'shipping_fee' => $shipping_fee,
                'total_price' => $gran_total,
                'payment_scheme'=>$payscheme,
                //'client_control_num' => '',
                //'admin_control_num' => '',
                //'courier' => '',
                //'remarks' => '',
                'trx_type' => 'active',
                'isdelete' => 0,
                'batch_id' => $batch['id']

            ));
            }
         	/*send email*/
    /*     		ob_start();
         		?>

         		<table border="0">
                    <tr>
                        <th>Batch ID: <?php echo $batch['id']; ?></th>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>                    
                    <tr>
                        <td>Order ID: <?php echo $order_id; ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
 					<tr>
 						<td>Name: <?php echo $name;?></td>
                        <td></td>
 						<td></td>
 						<td></td>
 					</tr>
 					<tr>
 						<td>Address: <?php echo $address;?></td>
                        <td></td>
 						<td></td>
 						<td></td>
 					</tr> 					
 					<tr>
 						<td>Contact Number: <?php echo $phone_number;?></td>
                        <td></td>
 						<td></td>
 						<td></td>
 					</tr>
                    <tr>
                        <td>Title</td>
                        <td>Options</td>
                        <td>Qty</td>
                        <td>Sub Total</td>
                    </tr>
 					<?php foreach ($trans_items as $key => $value) {
 						 ?>
 						 <tr>
                            <td><?=$value['item_name'];?></td>
 						 	<td><?php 

                            if(count($options) > 0){
                                foreach ($options as $option_name => $option_value) {
                                     echo $option_name . ": " . $option_value . "<br/>";
                                }

                            }

                            ?></td>
 						 	<td><?=$value['qty'];?></td>
 						 	<td><?=$this->cart->format_number($price); ?></td>
 						 </tr>
 						 <?php
 					}?>
 
 
 					<tr style="border-top:solid 1px #000;">
 						<td><b>Total: </b></td>
                        <td></td>
 						<td></td>
 						<td><b><?php echo $gran_total; ?></b></td>
 					</tr>

         		</table>

         		<?php
         		$content = ob_get_contents();
         		ob_end_clean();
                

         	    $email_message  = $content;
                
                 $this->load->library('email');
         	    $config = Array(
                                    'protocol' => 'smtp',
                                    'smtp_host' => 'ssl://smtp.gmail.com',
                                    'smtp_port' => 465,
                                    'smtp_user' => 'saesil.parkenshop2015@gmail.com',
                                    'smtp_pass' => 'Saesil2015',
                                    'mailtype'  => 'html', 
                                    'smtp_timeout' => 7,
                                    'charset'   => 'utf-8'
                                );                                  
                                $this->email->initialize($config);
                          
                                $this->email->set_newline("\r\n");
                
                                $this->email->from('saesil.parkenshop2015@gmail.com');
                                $this->email->to($userInfo['email']); 

                                $this->email->cc('saesil.parkenshop2015@gmail.com'); 
                                //$this->email->bcc('them@their-example.com'); 
                
                                $this->email->subject('Sae Sil Order');
                                $this->email->message($email_message); 
                
                                $this->email->send();*/
                                // echo 'email'; 
 
                                  // echo $this->email->print_debugger();    
 
        	 $this->cart->destroy();	
                $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">Order '.$order_id.' has been made.</div>');
        	 //redirect('checkout/receipt/' . $order_id);
              redirect('current_transaction');
	 	}

	}


	function receipt($order_id){
             $data['title']='Checkout | Sae sil ParknShop';
            $data['brand_data']=$this->setup->getBrand();
            $data['category_data']=$this->setup->getCategory();
            $data['scategory_data']=$this->setup->getSCategory();
            $data['scripts']=array(assets_url()."js/home/home.js", assets_url()."js/home/cart.js"); 
            $data['styles']=array(assets_url()."css/home/home.css");
 
            
            $this->load->view('home/home_header_page',$data);
            $this->load->view('home/receipt_view.php', $data); 
            $this->load->view('home/home_footer_page',$data);   
	}


	function confirm(){
		$data['title'] = 'Vanilla Pages Bookshop - Confirm'; 
		$data['categories'] = $this->list_categories();
		$this->load->view('layout/header', $data);
		$this->load->view('receipt_view',$data);
      	$this->load->view('layout/footer');


	}
	
}