<?php

class Cart extends CI_Controller{
	
	function __construct(){
		parent::__construct();
	 

		        $this->load->model('home/home_tbl','setup');
		        $this->load->library('cart');
		        
		//check if user setup pre data...
 
   		        // $this->load->model('admin/admin_tbl','setup');

	}

	function index(){
		$data = [];

			$data['title']='Cart | Sae sil ParknShop';
			$data['brand_data']=$this->setup->getBrand();
			$data['category_data']=$this->setup->getCategoryMenu();
			$data['logo_data']=$this->setup->getlogo();	
			//$data['scategory_data']=$this->setup->getSCategory();
 			$data['scripts']=array(assets_url()."js/home/home.js", assets_url()."js/home/cart.js"); 
			$data['styles']=array(assets_url()."css/home/home.css");
			$discount_conditions = $this->setup->getDiscountConditions();
			$batch = $this->setup->getLatestBatch();
			$data['batch_id'] = $batch['id'];

			ob_start();
		 $i = 1; 
		  foreach ($this->cart->contents() as $items): 

			$item_data = $this->setup->getItem($items['options']['item_id']);
		  	?>


				<tr>
			 	 <td>
			<?php 
							$is_discount = false;
							$discount_percent = 0;
								foreach ($discount_conditions as $key => $value) {
									if($value['promo_type'] == "brand"){

										if($value['value'] == $item_data['brand_id']){
											$is_discount = true;

											$discount_percent = $value['discount_percent'];
											break;
										}

									}else if($value['promo_type'] == "item"){ ///TODO : conditions for categories and sub category. NEED dropdown for category/sub in promo add/edit page

								 		if($value['value'] == $items['options']['item_id']){
								 			$is_discount = true;
								 			$discount_percent = $value['discount_percent'];
								 		}


								 	}
								}
							if($is_discount){
								$price = $items['price'] - ($items['price'] * ($discount_percent * 0.01));
							}else{
								$price = $items['price'];
							}	

			?>
				<?php echo form_hidden('rowid[]', $items['rowid']); ?>
				<?php echo form_hidden('id[]', $items['id']); ?>
					<?php echo $items['name']; ?>

						<?php /* if ($this->cart->has_options($items['rowid']) == TRUE): ?>

							<p>
								<?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>

									<strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />

								<?php endforeach; ?>
							</p>

						<?php endif; */ ?>

				  </td>
				 <?php if($is_discount) : ?>
				 	<td  >Php <?php echo $this->cart->format_number($price); ?>
						<s><?php echo $this->cart->format_number($items['price']); ?></s>
				 	</td>
				 <?php else: ?>
				 	<td  >Php <?php echo $this->cart->format_number($items['price']); ?></td>
				 <?php endif; ?>				  
				  <td class="text-center"><?php echo $items['qty'] ?></td>
				  <td>
							<?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>

				<p>
					<?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
						<?php if($option_name != "item_id") : ?>
							<strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />
						<?php endif; ?>
					<?php endforeach; ?>
				</p>

			<?php endif; ?>	
				   </td>
				  <td class="text-center">Php <?php echo $this->cart->format_number($price * $items['qty']); ?></td>
				  <td class="text-center"><button class="btn btn-danger btn-sm delete_cart_item"  data-rowid="<?php echo $items['rowid'];?>"><i class="fa fa-trash-o"></i></button></td>
				</tr>

			<?php $i++; 

			 endforeach;  
			 $output = ob_get_contents();
			 ob_end_clean();

			$data['table_data'] = $output;
			$this->load->view('home/home_header_page',$data);
			$this->load->view('home/cart_index.php', $data);
			$this->load->view('home/home_footer_page',$data);	
	}





	function add_item(){

		//get promo

				//validation for max qty
		//ajax
		if($_POST){
			$this->load->library('discount');
			$id = $this->input->post('item_id');
 			
			$qty = $this->input->post('qty');
			$options = $this->input->post('options');
 

			$itemInfo = $this->setup->fetchItem( $id );

    		$discount_conditions = $this->setup->getDiscountConditions();
    		$has_promo = $this->input->post('has_promo');



 
			if(empty($itemInfo)){
				echo json_encode(array("error" => true, "message"=>"Invalid Item ID"));
				return;
			}

			$itemInfo =  $this->discount->setDiscountDisplay($discount_conditions,$itemInfo);
			if($itemInfo['is_discount'] && $itemInfo['ps_qty'] > 1 && !$has_promo){
				 $add_qty = floor($itemInfo['ps_qty'] * $qty - $qty);

				//TODO count item
				echo json_encode(array(
					'status' => 'reenter',
					'error'  => false,
					'message' => 'Please Enter more items',
					'add_qty' => $add_qty
					));
				return;
			}
			//if has buy 1 take 1, return 
			//   {"status":"reenter", error:"false", message: "Promo! Please reenter more item", "add_qty" => 10}
			//
			$flag = TRUE;
		 	$dataTmp = $this->cart->contents();

		 	foreach ($dataTmp as $item) {

		 		if ($item['id'] == $id) {

		 				$qtyn = $item['qty'] + $qty;

		 				$cart_data = array('id' => $id,'rowid'=>$item['rowid'], 'qty'=>$qtyn, 'options' => $item['options']); 
		 				if(!empty($options)){

		 					// $cart_data['options']['options'] = $options;
		 				
		 	
		 					foreach( $options as $key1 => $option ){
		 						$isNew = true;
		 						foreach($cart_data['options'] as $key => $sub_item){

		 							if($key === $option['name'] && !is_numeric($key)){

		 								$cart_data['options'][$key] += $option['qty'];
		 								$isNew = false;

									 
		 							}else if(is_numeric($key)){

		 							} 
		 						}	
								
		 						if($isNew){
		 						
									$cart_data['options'][ $option['name']] = $option['qty'];
		 						}

		 					}
		  
		 						//todo append if item not part
		 					
		 			
		 					//workaround for updating options
  
		 				}
 
		 				$this->cart->update($cart_data);
		 				$dataTmp = $this->cart->contents();
 						echo json_encode(array(
 							'error' 	=> false,
 							 'message' => "Successfully updated item",
 							 'data' => $dataTmp
 							));
 
		 			$flag = FALSE;
		 			break;
		 		}
		 	}

		 //add instead of update
		if ( $flag ) {


        			$data = array(
                       'id'      => $id,
                       'name'    => $itemInfo['item_name'],
                       'qty'     => $qty,
                       'price'	 => $itemInfo['price'],
                       'options' => array(
                       		'item_id' => $itemInfo['item_id']
                       	)
                    );
 		 				if(!empty($options)){
		 					foreach ($options as $key => $value) {
		 						$data['options'][$value['name']] = $value['qty'];
		 					}
		 				}
		 				 
        			$this->cart->product_name_rules = '[:print:]'; //accept special characters
        			$this->cart->insert($data);
        			$dataTmp = $this->cart->contents();
        		  	echo json_encode(array(
 							'error' 	=> false,
 							 'message' => "Successfully added item",
 							 'data' => $dataTmp //TODO reload items
 							));
        }



		}//if $_POST



		/*if($itemInfo){

			//check if exists in cart
		 $flag = TRUE;
        $dataTmp = $this->cart->contents();

        foreach ($dataTmp as $item) {
            if ($item['id'] == $id) {
                // "Found Id so updating quantity";
          		if( $item['qty'] + 1 > $itemInfo['qty']){
          			$this->session->set_flashdata('cart_status', 'Cannot Add Item. You have exceeded maximum quantity');

          		 
          		}else{
                $qtyn = $item['qty'] + 1;
                $this->cart->update(array('rowid'=>$item['rowid'], 'qty'=>$qtyn));
               
             	$this->session->set_flashdata('cart_status', 'Cart Item  <b>' . $itemInfo['title'] . '</b> Updated');
                }
                 $flag = FALSE;
                   break;
            }
        }

            if( $flag &&   $itemInfo['qty'] <= 0){
                $this->session->set_flashdata('cart_status', 'Item is currently out of stock');


            }elseif ( $flag ) {



        			$data = array(
                       'id'      => $id,
                       'qty'     => 1,
                       'price'   => $itemInfo['price'],
                       'name'    => $itemInfo['title'],
                       'options' => array('Category' => $itemInfo['category'], 
                       					'image' => $itemInfo['image_name'],
                       					'ispromo' => $itemInfo['ispromo'],
                       					'discount' => $itemInfo['promo_disc'])
                    );
        			$this->cart->product_name_rules = '[:print:]'; //accept special characters
        			$this->cart->insert($data);
        			
        			$this->session->set_flashdata('cart_status', 'Added  ' . $itemInfo['title'] . ' to cart');
        }

        			$ref = str_replace("/index#book{$id}","/index",$this->input->server('HTTP_REFERER', TRUE)) . "#book{$id}";


		 
		}else{
			die("Invalid Entry"); //
		}*/

 
	}

	function delete(){
		$rowid = $this->input->post('row_id');
		 

		 	  $cart_data = array( 'rowid'=>$rowid, 'qty'=>0, ); 
		 	 $this->cart->update($cart_data);
		 
		 
 

		 echo json_encode(array("error" => false));

	}
	function view(){
			$data['title']='Sae sil ParknShop';
			//load menus.
			$data['logo_data']=$this->setup->getlogo();
			$data['brand_data']=$this->home->getBrand();
			$data['category_data']=$this->home->getCategoryMenu();
			//$data['scategory_data']=$this->home->getSCategory();
			$data['home_model']=$this->home;
			$data['promo_data']=$this->home->promoData();
			$data['announcement_data']=$this->home->announcementData('top');
			
			//items
			$data['newitems_data']=$this->home->itemsData('new');
			$data['bestitems_data']=$this->home->itemsData('best_seller');
			//$data['onhanditems_data']=$this->home->itemsData('on_hand');
			
			
			$data['scripts']=array(assets_url()."js/home/home.js");
			$data['styles']=array(assets_url()."css/home/home.css");
			
			$this->load->view('home/home_header_page',$data);
			$this->load->view('contact_us/contactus_page',$data);
			$this->load->view('home/home_footer_page',$data);	
	}

	function update(){
		//not working
			$this->load->model('book');

			$id = $this->input->post('id');
			$rowid = $this->input->post('rowid');
			$qty = $this->input->post('qty');

		 	foreach ($id as $key => $value) {


		 		$bookInfo = $this->book->getBookInfo($value);
		 		if($qty[$key] > $bookInfo['qty']){
		 			$this->session->set_flashdata('cart_status', 'Cannot Add Item. You have exceeded maximum quantity');
		 		}else{
		 			
		 			$this->cart->update(array(

		 		 	'rowid' => $rowid[$key],
		 		 	'qty' => $qty[$key]
		 		 	));
		 		}



		 	}
			//print_r($_POST);
			//$this->cart->update($_POST);
			redirect('cart/view');
	}

	function clear(){
		$this->cart->destroy();
       	$ref = $this->input->server('HTTP_REFERER', TRUE);
		redirect($ref);

	}

	#@
	function checkout(){

		$dataTmp = $this->cart->contents();
		    foreach ($dataTmp as $item) {
            if ($item['id'] == $id) {
                // "Found Id so updating quantity";
          		if( $item['qty'] + 1 > $bookInfo['qty']){
          			$this->session->set_flashdata('cart_status', 'Cannot Add Item. You have exceeded maximum quantity');

          		 
          		}else{
                $qtyn = $item['qty'] + 1;
                $this->cart->update(array('rowid'=>$item['rowid'], 'qty'=>$qtyn));
               
             	$this->session->set_flashdata('cart_status', 'Cart Item  <b>' . $bookInfo['title'] . '</b> Updated');
                }
                 $flag = FALSE;
                   break;
            }
        }
	}

}