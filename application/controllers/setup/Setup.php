<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setup extends CI_Controller {

	    function __construct(){
        parent::__construct();
		//loading database or global functions
        $this->load->model('admin/admin_tbl','setup');
		//check if user setup pre data...	
		}

		function index(){
			$data['title']='Super Admin User Setup';
			
			$this->load->view('admin/admin_header_page',$data);
			$this->load->view('setup/first_setup_page',$data);
			$this->load->view('admin/admin_footer_page',$data);
		}	
		
		function ProcessFirstUser(){
			//data
			if($_POST){
			$UserName = $this->security->xss_clean($this->input->post('UserName'));
			$Password = $this->security->xss_clean($this->input->post('Password'));				
			$FirstName = $this->security->xss_clean($this->input->post('FirstName'));	
			$MiddleName = $this->security->xss_clean($this->input->post('MiddleName'));	
			$LastName = $this->security->xss_clean($this->input->post('LastName'));
			$email=$this->security->xss_clean($this->input->post('email'));
			$contact_no=$this->security->xss_clean($this->input->post('contact_no'));
			$Access_level ="super-admin";
			$FullName=$FirstName." ".$MiddleName." ".$LastName;

				//generate user id
				$user_id= $this -> setup -> getUserid();
					if ($user_id) {
						$user_id = $user_id["id"];
						//$user_id = substr($user_id, -10);
						$uid = "USER" . sprintf("%010d", $user_id + 1);
					} else {
						$uid = "USER" . sprintf("%010d", 1);
					}
			
				//prepareing data....
				$data=array(
				"user_id"=>$uid,
				"full_name"=>$FullName,
				"first_name"=>$FirstName,
				"middle_name"=>$MiddleName,
				"last_name"=>$LastName,
				"username"=>$UserName,
				"password"=>$this->encryption->encode($Password),
				"email"=>$email,
				"contact_no"=>$contact_no,
				"access_level"=>$Access_level,
				"created_by"=>$Access_level,
				"status"=>"active",
				"activation_date"=>date('Y-m-d H:i:s'),
				"activated_by"=>$Access_level,				
				);					
				
					//insert data
					$insert_data=$this->setup->saveUser($data);
					$this->session->unset_userdata('UserId');
					//upload img
					if($insert_data==1){		
							$this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">User Successfully Made, You may now Login.</div>');	
							$this->session->unset_userdata('IsSetup');
							redirect('admin');
																	
			        
					}else if($insert_data==0) {
							$this->session->set_flashdata('msg','					
							<div class="alert alert-danger" role="alert">Something went wrong, try again later.</div>');						
							redirect('user_setup');										
					}			
			
			}//end of if post
		}	
		
		
}		