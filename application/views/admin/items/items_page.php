<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-md-12'>
				<!-- put info here -->
					<legend>
						<?php echo strtoupper($title); ?> MANAGEMENT	
						<div class='pull-right'>
							<a href="<?=base_url("admin/form/".$module."/".$this->encryption->encode('a')."")?>" title='add'>
								<span class='glyphicon glyphicon-plus'></span>
							</a>
						</div>												
					</legend>
   						<?php echo $this->session->flashdata('msg'); ?>					
						<table id="<?php echo $module;?>_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
					        <thead>
					            <tr>
					                <th>Name</th>
					                <th>Brand</th>
					                <th>Category</th>
					                <th>Sub Category</th>
					                <th>Date Created</th>
					                <th>Status</th>
					                <th>Action</th>
					            </tr>
					        </thead>
							<tbody>
								
					        </tbody> 
					        <tfoot>
					        	
					        </tfoot>			           		        				
						</table>					    					
			</div>
			
		</div>
	</div>	
</div>
