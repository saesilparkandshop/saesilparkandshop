<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-md-12'>
					<legend>
						<?php echo $title;?>
						<div class='pull-right'>
							<a href='<?=base_url()?>admin/grid/<?php echo $module; ?>.aspx'>
								<span class='glyphicon glyphicon-chevron-left'>Back</span>
							</a>
						</div>						
					</legend>
				<div class='col-md-12 '>
					<?=$this->session->flashdata('msg');?>	
					<?php
					$hasUser=$items_data;
					if($hasUser!=''){$process ='e'; }
					else if($hasUser==''){$process ='a'; }
					?>					
					<form class='form-horizontal formx' method="post" action="<?=base_url('admin/admin/Process/'.$module.'/'.$process)?>" enctype="multipart/form-data" data-toggle="validator" role="form">
						<div class="media">
						  <div class="media-body">
							  	<div class='col-md-4'>
							  		<?php
							  		$img_thumbs1='td.PNG'; 
									$img_filename1='td.PNG'; 
							  		$img_thumbs2='td.PNG'; 
									$img_filename2='td.PNG'; 
							  		$img_thumbs3='td.PNG'; 
									$img_filename3='td.PNG';
																									  		
									if(!empty($images_data)){
										foreach($images_data as $img_val){
										if($img_val['index']=="1"){
									  		if(!empty($img_val['thumbs']))
									  		$img_thumbs1 = $img_val['thumbs'];	
											
									  		if(!empty($img_val['filename']))
									  		$img_filename1 = $img_val['filename'];													
										}	
										if($img_val['index']=="2"){
									  		if(!empty($img_val['thumbs']))
									  		$img_thumbs2 = $img_val['thumbs'];	
											
									  		if(!empty($img_val['filename']))
									  		$img_filename2 = $img_val['filename'];													
										}
										if($img_val['index']=="3"){
									  		if(!empty($img_val['thumbs']))
									  		$img_thumbs3 = $img_val['thumbs'];	
											
									  		if(!empty($img_val['filename']))
									  		$img_filename3 = $img_val['filename'];													
										}																			
											
										}	
									}
							  		?>
							  		<div class='col-md-12'>
										<div class="form-group">
											<center>
											  <input type="hidden" value="<?php echo  $img_thumbs1; ?>" name='img_thumbs1' />	
											  <input type="hidden" value="<?php echo  $img_filename1; ?>" name='img_name1' />	
											  <a id='upload-item' class="img-thumbnail upload-item" uc='1' title='must  225x333' style=" height:333px; width:225px;  cursor:pointer;">
											      <img style="height:100%; width:100%;" id="img-prev1" class="img-responsive imgcnt" src="<?=assets_url()."img/item_covers/".$img_thumbs1;?>" alt="...">
											  </a>
											  <br>
											  <button type="button" class="btn btn-danger btn-xs remove_img" cnt="1"><i class="" title="remove image"><i class="fa fa-times" aria-hidden="true"></i></button>
											  <input type="file" id='btn_upload1' buc='1' class="btn-upload" accept="image/*" required=""  name='upload1' onchange="showImage(this)" />
											</center>									
										</div>							  			
							  		</div>															  									  		
							  	</div>
							  	<div class='col-md-4'>
							  		<div class='col-md-12'>
										<div class="form-group">
											<center>
											  <input type="hidden" value="<?php echo  $img_thumbs2; ?>" name='img_thumbs2' />	
											  <input type="hidden" value="<?php echo  $img_filename2; ?>" name='img_name2' />	
											  <a id='upload-item' class="img-thumbnail upload-item" uc='2' title='must  225x333' style=" height:333px; width:225px;  cursor:pointer;">
											      <img style="height:100%; width:100%;" id="img-prev2" class="img-responsive imgcnt" src="<?=assets_url()."img/item_covers/".$img_thumbs2;?>" alt="...">
											  </a>
											  <br>
											  <button type="button" class="btn btn-danger btn-xs remove_img" cnt="2"><i class="" title="remove image"><i class="fa fa-times" aria-hidden="true"></i></button>											  
											  <input type="file" id='btn_upload2' buc='2' class="btn-upload" accept="image/*" required=""  name='upload2' onchange="showImage(this)" />
											</center>									
										</div>							  			
							  		</div>															  									  		
							  	</div>
							  	<div class='col-md-4'>
							  		<div class='col-md-12'>
										<div class="form-group">
											<center>
											  <input type="hidden" value="<?php echo  $img_thumbs3; ?>" name='img_thumbs3' />	
											  <input type="hidden" value="<?php echo  $img_filename3; ?>" name='img_name3' />	
											  <a id='upload-item' class="img-thumbnail upload-item" uc='3' title='must  225x333' style=" height:333px; width:225px;  cursor:pointer;">
											      <img style="height:100%; width:100%;" id="img-prev3" class="img-responsive imgcnt" src="<?=assets_url()."img/item_covers/".$img_thumbs3;?>" alt="...">
											  </a>
											  <br>
											  <button type="button" class="btn btn-danger btn-xs remove_img" cnt="3"><i class="" title="remove image"><i class="fa fa-times" aria-hidden="true"></i></button>											  
											  <input type="file" id='btn_upload3' buc='3' class="btn-upload" accept="image/*" required=""  name='upload3' onchange="showImage(this)" />
											</center>									
										</div>							  			
							  		</div>															  									  		
							  	</div>							  								  	
							  <div class='col-md-12'>	
							  <div class="form-group">										  	
			                    <label for="status" class="col-lg-2 control-label">Status</label>
			                    <div class="col-lg-10">
									<select  title='Account Status' class='form-control custom_textbox_xs ' required="" id='status' name='status' placeholder='Account status' >
										<option value=''>--Status--</option>
										<?php
										if($hasUser!=""){													
											if($items_data['status']=='active'){
												echo "
										          <option selected value='active'>Active</option>
										          <option value='inactive'>Inactive</option>															
												";								
											} else if($items_data['status']=='inactive'){
												echo "
										          <option value='active'>Active</option>
										          <option selected value='inactive'>Inactive</option>															
												";													
											}
										} else {
												echo "
										          <option value='active'>Active</option>
										          <option value='inactive'>Inactive</option>															
												";																
											}													
										?>														
									</select>		
									<div class="help-block with-errors"></div>							                   
			                    </div>
			                  </div>				              							  	
								<?php 
								 
								$item_name_val='';
								if($hasUser!=""){ $item_name_val=$items_data['item_name']; }
								?>	
								  <div class="form-group">
				                    <label for="cat_name" class="col-lg-2 control-label">Item name</label>
				                    <div class="col-lg-10">
				                      <input type="text" title='Fill up your desire Username.' maxlength="70"  class='form-control custom_textbox_xs text_spacer' required id='item_name' name='item_name' value="<?php echo $item_name_val; ?>"  placeholder="Item Name" />
				                      <div class="help-block with-errors">Character length not morethan 70.</div>
				                    </div>
				                  </div>	
							  <div class="form-group">									  	
			                    <label for="status" class="col-lg-2 control-label">Brand</label>
			                    <div class="col-lg-10">
									<select  title='Account Status' class='form-control custom_textbox_xs ' required="" id='brand' name='brand' placeholder='Account status' >
										<option data-b='' value=''>--Brand--</option>
										<?php
																			
										foreach($brand_data as $brand_val){
											
											if($hasUser!=""){													
												if($items_data['brand']==$brand_val['brand_name']){
													echo "
											          <option data-b=".$brand_val['brand_id']." selected value='".$brand_val['brand_name']."'>".$brand_val['brand_name']."</option>															
													";								
												} else {
													echo "
													<option data-b=".$brand_val['brand_id']."  value='".$brand_val['brand_name']."'>".$brand_val['brand_name']."</option>																
													";													
												}
											} else {
												echo "
												<option data-b=".$brand_val['brand_id']."  value='".$brand_val['brand_name']."'>".$brand_val['brand_name']."</option>															
												";																
											}												
										}										
										?>														
									</select>		
									<div class="help-block with-errors"></div>							                   
			                    </div>
			                  </div>
							  <div class="form-group">										  	
			                    <label for="status" class="col-lg-2 control-label">Category</label>
			                    <div class="col-lg-10">
									<select  title='Account Status' class='form-control custom_textbox_xs ' required="" id='category' name='category' >
										<option data-x='' value=''>--Select Category--</option>
										<?php									
										foreach($category_data as $category_val){
											if($hasUser!=""){
												//if(!empty($items_data['brand'])){													
													if($items_data['category']==$category_val['cat_name']){
														echo "
												          <option selected data-x='".$category_val['cat_id']."'  value='".$category_val['cat_name']."'>".$category_val['cat_name']."</option>															
														";								
													} else {
														echo "
														<option data-x='".$category_val['cat_id']."' value='".$category_val['cat_name']."'>".$category_val['cat_name']."</option>																
														";													
													}
												//}
											} else {
														echo "
														<option data-x='".$category_val['cat_id']."' value='".$category_val['cat_name']."'>".$category_val['cat_name']."</option>																
														";													
											}												
										}																						
										?>														
									</select>		
									<div class="help-block with-errors"></div>							                   
			                    </div>
			                  </div>	
							  <div class="form-group">										  	
			                    <label for="status" class="col-lg-2 control-label">Sub-Category</label>
			                    <div class="col-lg-10">
									<select  title='Account Status' class='form-control custom_textbox_xs ' required="" id='sub_category' name='sub_category' placeholder='Account status' >
										<option value=''>--Please Select Category First--</option>
										<?php
										if($hasUser!=""){													
											if(!empty($items_data['category'])){
												foreach($sub_category_data as $scategory_val){
													if($items_data['sub_category']==$scategory_val['scat_name']){
														echo "
												          <option selected value='".$scategory_val['scat_name']."'>".$scategory_val['scat_name']."</option>															
														";								
													} else {
														echo "
														<option  value='".$scategory_val['scat_name']."'>".$scategory_val['scat_name']."</option>																
														";													
													}
												}												
											}
										} 												
										?>														
									</select>		
									<div class="help-block with-errors"></div>							                   
			                    </div>
			                  </div>				                  		                  				                  													  							
							</div>	<!-- end of col-md-8-->	
							<div class="col-md-12">
								<?php
								$description_val='';
								if($hasUser!=""){ $description_val=$items_data['description']; }
								?>	
								  <div class="form-group">
				                    <label for="cat_name" class="col-lg-2 control-label">Item Description</label>
				                    <div class="col-lg-10">
				                      <textarea  class='form-control custom_textbox_xs text_spacer elastica' style="resize:none; min-height:100px; " required id='description' name='description' placeholder="Description"><?php echo $description_val; ?></textarea>
				                      <div class="help-block with-errors"></div>
				                    </div>
				                  </div>
								<?php 
								$size_val='';
								if($hasUser!=""){ $size_val=$items_data['size']; }
								?>	
								<!--<div class="form-group">
				                    <label for="cat_name" class="col-lg-2 control-label">Size</label>
				                    <div class="col-lg-10">
				                      <input type='text' class='form-control custom_textbox_xs text_spacer ' required id='size' name='size' placeholder="size" value='<?php echo $size_val; ?>' />
				                      <div class="help-block with-errors"></div>
				                    </div>
				               </div>-->
								<?php  
								$volume_val='';
								if($hasUser!=""){ $volume_val=$items_data['volume']; }
								?>	
								  <div class="form-group">
				                    <label for="cat_name" class="col-lg-2 control-label">Size/Volume</label>
				                    <div class="col-lg-10">
				                      <input type='text'  class='form-control custom_textbox_xs text_spacer ' required id='volume' name='volume' placeholder="Size/Volume" value='<?php echo $volume_val; ?>' />
				                      <div class="help-block with-errors"></div>
				                    </div>
				                  </div>		
								<?php 
								$price_won_val='';
								if($hasUser!=""){ $price_won_val=$items_data['price_won']; $price_won_val=number_format($price_won_val,2,'.',','); }
								
								?>	
								  <div class="form-group">
				                    <label for="price" class="col-lg-2 control-label">Purchase Price</label>		                    
				                    <div class="col-lg-5">
				                      <input type="text" title='Fill up your desire price' class='form-control text-right custom_textbox_xs text_spacer price' required id='price_won' name='price_won' value="<?php echo $price_won_val; ?>"  placeholder="Purchase Price" />
				                      <div class="help-block with-errors">Price Default is KRW.</div>
				                    </div>
				                  </div>					                  		                  				                  						                  				                  							  						  						                  					  	
								<?php 
								$price_val='';
								if($hasUser!=""){ $price_val=$items_data['price']; }
								?>	
								  <div class="form-group">
				                    <label for="price" class="col-lg-2 control-label">Price</label>		                    
				                    <div class="col-lg-5">
				                      <input type="number" title='Fill up your desire price' class='form-control text-right custom_textbox_xs text_spacer price' required id='price' name='price' value="<?php echo $price_val; ?>"  placeholder="Price" />
				                      <div class="help-block with-errors">Price Default is PHP.</div>
				                    </div>
				                  </div>	
							  <div class="form-group">									  	
			                    <label for="status" class="col-lg-2 control-label">Item Status</label>
			                    <div class="col-lg-5">
									<select  title='Account Status' class='form-control custom_textbox_xs' required="" id='item_status' name='item_status'>
										<option data-b='' value=''>--Item Status--</option>
										<?php
										if($hasUser!=""){													
											if($items_data['is_stock']=='in-stock'){
												echo "
										          <option selected value='in-stock'>In-stock</option>
										          <option value='out-stock'>Out of Stock</option>															
												";								
											} else if($items_data['is_stock']=='out-stock'){
												echo "
										          <option  value='in-stock'>In-stock</option>
										          <option selected value='out-stock'>Out of Stock</option>															
												";													
											}
										} else {
												echo "
										          <option  value='in-stock'>In-stock</option>
										          <option  value='out-stock'>Out of Stock</option>															
												";																
											}													
										?>													
									</select>		
									<div class="help-block with-errors"></div>							                   
			                    </div>
			                  </div>
			                  <?php 
								$is_onhand_val='0';
								$is_onhand_lbl='';
								if($hasUser!=""){
									if($items_data['is_onhand']=='1'){
										$is_onhand_val='1';
										$is_onhand_lbl='checked';										
									}
								}			                  
			                  ?>
			                 <!-- <div class='form-group'>
			                  	<label for="price" class="col-lg-2 control-label">On Hand</label>		
			                  	<div class='col-md-5'>
			                  		<input type='checkbox' id='on_hand' name="on_hand" class="" <?php echo $is_onhand_lbl; ?> /> 
			                  		<input type='hidden' id='on_hand_val' name="on_hand_val" value="<?php echo $is_onhand_val; ?>" class="" />
			                  	</div>
			                  </div>	-->
								<?php  
								$ohprice_val='';
								$isshow='display:none;';
								if($hasUser!=""){
									if($items_data['is_onhand']=='1'){
											$ohprice_val=$items_data['onhand_price']; 
											$isshow='display:block;';	
									} 
								}
								?>	
							<!--	  <div class="form-group isonhand" style='<?php echo $isshow; ?>'>
				                    <label for="price" class="col-lg-2 control-label">On hand Price</label>		                    
				                    <div class="col-lg-5">
				                      <input type="number" title='Fill up your desire price'  class='form-control text-right custom_textbox_xs text_spacer price' id='onhand_price' name='onhand_price' value="<?php echo $ohprice_val; ?>"  placeholder="Price" />
				                      <div class="help-block with-errors">Price Default is PHP.</div>
				                    </div>
				                  </div> --> 
				                <div class='form-group'>
				                <div class="col-lg-offset-2 col-md-10">
				                <?php
				                $is_newitem='';
								$is_bestseller='';
								if($hasUser!=""){
								$items_data['is_newitem']==1 ? 	$is_newitem='checked' : $is_newitem='';
								$items_data['is_bestseller']==1 ? 	$is_bestseller='checked' : $is_bestseller='';	
								}				                
				                ?>	
									<label class="checkbox-inline">
									  <input type="checkbox" id="inlineCheckbox1 form-control" <?php echo $is_newitem; ?> name='is_newitem' value="1"> New Item
									</label>
									<label class="checkbox-inline">
									  <input type="checkbox" id="inlineCheckbox2 form-control" <?php echo $is_bestseller; ?> name='is_bestseller' value="1"> Best Seller
									</label>					                	
				                </div>					                	
				                </div>  	
			                  			                  
							  <div class='col-md-7'>
							  	<button class="btn btn-primary btn-xs" type='button' id='add_option' name='add_option'><i class='glyphicon glyphicon-plus'></i> Add Option</button>
							  	<table class="table table-striped table-bordered" cellspacing="0" width="50%" style="margin-top: 5px;">
							        <thead>
							            <tr>
							                <th width='60%'>Options</th>
							                <th width='30%'>Stock</th>
							                <th width='10%'></th>
							            </tr>
							        </thead>			
							        <tbody id='options'>
							        	<?php
											if($hasUser!=""){
												if($items_data['options']!=''){ 
													$opdata=explode(',',$items_data['options']);
													$opdatastock=explode(',',$items_data['option_stock']);
													$str='';
													for($i=0;$i<count($opdata); $i++){
														if($opdatastock[$i]=='0') { $iop='selected'; $oop='';}else{$iop=''; $oop='selected';}  
														$str='<tr class="io">';
														$str.='<td><input type="text" name="item_options[]" class="form-control text_spacer input-sm" value="'.$opdata[$i].'"/></td>';
														$str.='<td><select  class="form-control text_spacer input-sm" name="option_stock[]">';
														$str.='<option '.$iop.' value="0">In Stock</option>';
														$str.='<option '.$oop.' value="1">Out of Stock</option>';
														$str.='</select></td>';
														$str.='<td><button type="button" class="btn btn-danger btn-sm item_op"><span class="glyphicon glyphicon-trash"></span></button></td>';
														$str.='</tr>';	
														echo $str;														
													}
												} else {
											  echo  "<tr class='io' id='nodata'>
										        	<td colspan='3'>No option added.</td>
										        	</tr>";														
												}
											}else {
											  echo  "<tr class='io' id='nodata'>
										        	<td colspan='3'>No option added.</td>
										        	</tr>";	
											}								        	
							        	?>
							        </tbody>				  		
							  	</table>
							  </div>				                  				                  					  				                  				                   								
							</div>																		        						                  	             			                  				                  
							<input type="hidden" id="hid_base_url" value="<?php echo base_url(); ?>" /> 
							<input type="hidden" id="title" name='title' value="<?php echo $title;?>" /> 
							<input type="hidden" id="brand_id" name="brand_id" value="<?php echo $hasUser != ""  ? $items_data['brand_id'] : ''; ?>"> 
							
							<div class="col-md-12" style="margin-bottom:10px;">
								<div class='pull-right'>	
									<?php 
									$hid_id='';
									if($hasUser!='')
									$hid_id=$items_data['item_id'];
									?>											
									<input type='hidden' name='idx' value='<?php echo  $hid_id; ?>' />										
									<button type="submit" id="submit" name="submit" width='30' title='Save' class="btn btn-primary btn-sm" >
										<?php 
										if($hasUser!='') { echo "<i class='glyphicon glyphicon-edit'></i> Update"; }
										else if ($hasUser=='') {
											 echo "<i class='glyphicon glyphicon-floppy-save'></i> Save"; 
										}
										?>
									</button>	
								</div>
							</div>							                  						                  						                  												                  						                  																		
						  </div>
						</div>							
					</form>
				</div>	
			</div>
			
		</div>
	</div>	
</div>
