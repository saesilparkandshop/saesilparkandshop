<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-md-12'>
				<legend><?php echo $title;?></legend>
				<!-- put info here -->
				<table id="dashboard_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>Activity</th>
			                <th>Action</th>
			                <th>Date</th>
			            </tr>
			        </thead>
					<tbody>
						
			        </tbody> 
			        <tfoot>
			        	
			        </tfoot>			           		        				
				</table>	
			</div>
			
		</div>
	</div>	
</div>
