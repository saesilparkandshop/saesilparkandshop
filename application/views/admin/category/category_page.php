<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-md-12'>
			<!-- put info here -->
				<legend>
					<?php echo strtoupper("MAIN".$title); ?> MANAGEMENT	
					<div class='pull-right'>
						<a href="<?=base_url("admin/form/".$module."/".$this->encryption->encode('a')."")?>" title='add'>
							<span class='glyphicon glyphicon-plus'></span>
						</a>
					</div>												
				</legend>
				<table id="<?php echo $module;?>_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
			       <thead>
			            <tr>
			                <th>Name</th>			                
			                <th>Status</th>
			                <th>Created By</th>
			                <th>Action</th>
			            </tr>
			        </thead>	
					<tbody>								
			        </tbody> 
			        <tfoot>				        	
			        </tfoot>			           		        				
				</table>	
				<hr>
				<legend>
					<?php echo strtoupper("Sub Category"); ?> MANAGEMENT	
					<div class='pull-right'>
						<a href="<?=base_url("admin/form/sub_category/".$this->encryption->encode('a')."")?>" title='add'>
							<span class='glyphicon glyphicon-plus'></span>
						</a>
					</div>												
				</legend>				
				<table id="<?php echo "sub_category";?>_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
			       <thead>
			            <tr>
			            	<th>Category name</th>	
			                <th>Name</th>			                
			                <th>Status</th>
			                <th>Created By</th>
			                <th>Action</th>
			            </tr>
			        </thead>	
					<tbody>								
			        </tbody> 
			        <tfoot>				        	
			        </tfoot>			           		        				
				</table>									    					
			</div>			
		</div>
	</div>	
</div>
