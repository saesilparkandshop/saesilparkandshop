<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-md-12'>
					<legend>
						<?php echo $title;?>
						<div class='pull-right'>
							<a href='<?=base_url()?>admin/grid/<?php echo $module; ?>.aspx'>
								<span class='glyphicon glyphicon-chevron-left'>Back</span>
							</a>
						</div>						
					</legend>
				<div class='col-md-offset-1 col-md-10 col-md-offset-1'>
					<?=$this->session->flashdata('msg');?>	
					<?php
					$hasUser=$category_data;
					if($hasUser!=''){$process ='e'; }
					else if($hasUser==''){$process ='a'; }
					?>					
					<form class='form-horizontal formx' method="post" action="<?=base_url('admin/admin/Process/'.$module.'/'.$process)?>" enctype="multipart/form-data" data-toggle="validator" role="form">
						<div class="media">
						  <div class="media-body">
									<!--	  <div class="form-group">										  	
						                    <label for="status" class="col-lg-2 control-label">Brand</label>
						                    <div class="col-lg-10">
												<select  title='Brand' class='form-control custom_textbox_xs ' required="" id='brand' name='brand' placeholder='Brand' >
													<option value=''>--Brand--</option>
													<?php
													foreach($brand_data as $brand_val){
														if($hasUser!=""){													
															if($category_data['brand_id']==$cat_val['brand_id']){
																echo"
																<option selected value='".$brand_val['brand_id']."'>".$brand_val['brand_name']."</option>
																";																																	
															} else{
																echo"
																<option  value='".$brand_val['brand_id']."'>".$brand_val['brand_name']."</option>
																";																	
															}							
														} else {
															echo"
															<option  value='".$brand_val['brand_id']."'>".$brand_val['brand_name']."</option>
															";																													
														}																
														

													}											
													?>													
												</select>		
												<div class="help-block with-errors"></div>							                   
						                    </div>
						                  </div>	-->						  	
										<!--  <div class="form-group">										  	
						                    <label for="status" class="col-lg-2 control-label">Category Status</label>
						                    <div class="col-lg-10">
												<select  title='Account Status' class='form-control custom_textbox_xs ' required="" id='status' name='status' placeholder='Account status' >
													<option value=''>--Category Status--</option>
													<?php
													if($hasUser!=""){													
														if($category_data['status']=='active'){
															echo "
													          <option selected value='active'>Active</option>
													          <option value='inactive'>Inactive</option>															
															";								
														} else if($category_data['status']=='inactive'){
															echo "
													          <option value='active'>Active</option>
													          <option selected value='inactive'>Inactive</option>															
															";													
														}
													} else {
															echo "
													          <option value='active'>Active</option>
													          <option value='inactive'>Inactive</option>															
															";																
														}													
													?>														
												</select>		
												<div class="help-block with-errors"></div>							                   
						                    </div>
						                  </div>-->						                  					  	
										<?php //check if have data 
										$cat_name_val='';
										if($hasUser!=""){ $cat_name_val=$category_data['cat_name']; }
										?>	
										  <div class="form-group">
						                    <label for="cat_name" class="col-lg-2 control-label">Category name</label>
						                    <div class="col-lg-10">
						                      <input type="text" title='Fill up your desire Username.'  class='form-control custom_textbox_xs text_spacer' required id='cat_name' name='cat_name' value="<?php echo $cat_name_val; ?>"  placeholder="Category Name" />
						                      <div class="help-block with-errors"></div>
						                    </div>
						                  </div>														  							
																						        						                  	             			                  				                  
							<input type="hidden" id="hid_base_url" value="<?php echo base_url(); ?>" /> 
							<input type="hidden" id="title" name='title' value="<?php echo $title;?>" /> 
							<div class="col-md-12" style="margin-bottom:10px;">
								<div class='pull-right'>	
									<?php 
									$hid_id='';
									if($hasUser!='')
									$hid_id=$category_data['cat_id'];
									?>											
									<input type='hidden' name='idx' value='<?php echo  $hid_id; ?>' />										
									<button type="submit" id="submit" name="submit" width='30' title='Save' class="btn btn-primary btn-sm" >
										<?php 
										if($hasUser!='') { echo "<i class='glyphicon glyphicon-edit'></i> Update"; }
										else if ($hasUser=='') {
											 echo "<i class='glyphicon glyphicon-floppy-save'></i> Save"; 
										}
										?>
									</button>	
								</div>
							</div>							                  						                  						                  												                  						                  																		
						  </div>
						</div>							
					</form>
				</div>	
			</div>
			
		</div>
	</div>	
</div>
