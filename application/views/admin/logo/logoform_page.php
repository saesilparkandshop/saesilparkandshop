<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-md-12'>
					
				<div class='col-md-12 '>
						<?=$this->session->flashdata('msg');?>	
						<?php
						$hasUser=$logo_data;
						if($hasUser!=''){$process ='e'; }
						else if($hasUser==''){$process ='a'; }
						?>					
						<form class='form-horizontal formx' method="post" action="<?=base_url('admin/admin/Process/'.$module.'/'.$process)?>" enctype="multipart/form-data" data-toggle="validator" role="form">
						<div class="media">
						  <div class="media-body">
							  	<div class='col-md-4'>
								  		<?php
								  		$img_path='td.PNG'; 
								  		if($hasUser!=''){
								  		if(!empty($logo_data['logo_filename']))
								  		$img_path = $logo_data['logo_filename'];		
								  		}
								  		?>
								  		
										<div class="form-group">
											<center>
											  <input type="hidden" value="<?php echo  $img_path; ?>" name='img_name' />	
											  <a id='upload-item' class="img-thumbnail" title='must  100x200' style=" height:100px; width:200px;  cursor:pointer;">
											      <img style="height:100%; width:100%;" id="img-prev" class="img-responsive" src="<?=assets_url()."img/logo/".$img_path;?>" alt="...">
											  </a>
											  <!--this is the upload button hidden-->
											  <input type="file" id='btn_upload' class="btn-upload" accept="image/*"  name='upload' onchange="showImage(this)" />
											</center>
										</div>						  									  		
							  	</div> 
				<div class='col-md-8'>	
							  <div class='form-group'>
												<?php //check if have data 
												$fb_val='';
												if($hasUser!=""){ $fb_val=$logo_data['facebook_url']; }
												?>								                  	
							                    <label for="facebook_url" class="col-lg-2 control-label">Facebook url</label>
							                    <div class="col-lg-10">
							                      <input type="url" title='Fill up your facebook profile.'  class='form-control custom_textbox_xs text_spacer' required id='facebook_url' name='facebook_url' value="<?php echo $fb_val; ?>"  placeholder="Facebook url" />
							                      <div class="help-block with-errors">Please make sure that it's searchable for verification purpose only.(https://www.facebook.com/ABSCBNnetwork?fref=nf)</div>
							                    </div>							                  	
							    </div> 	
							    
								 <div class='form-group'>
												<?php //check if have data 
												$twitter_val='';
												if($hasUser!=""){ $twitter_val=$logo_data['twitter_url']; }
												?>								                  	
							                    <label for="twitter_url" class="col-lg-2 control-label">Twitter url</label>
							                    <div class="col-lg-10">
							                      <input type="url" title='Fill up your twitter url.'  class='form-control custom_textbox_xs text_spacer' required id='twitter_url' name='twitter_url' value="<?php echo $twitter_val; ?>"  placeholder="Twitter url" />
							                      <div class="help-block with-errors">Please make sure that it's searchable for verification purpose only.(https://www.twitter.com/ABSCBNnetwork?fref=nf)</div>
							                    </div>							                  	
							    </div> 			                  		                  				                  													  							
				</div>	<!-- end of col-md-8-->	
																							        						                  	             			                  				                  
							<input type="hidden" id="hid_base_url" value="<?php echo base_url(); ?>" /> 
							<input type="hidden" id="title" name='title' value="<?php echo $title;?>" /> 
							<div class="col-md-12" style="margin-bottom:10px;">
								<div class='pull-right'>	
									<?php 
									$hid_id='';
									if($hasUser!='')
									$hid_id=$logo_data['logo_id'];
									?>											
									<input type='hidden' name='idx' value='<?php echo  $hid_id; ?>' />										
									<button type="submit" id="submit" name="submit" width='30' title='Save' class="btn btn-primary btn-sm" >
										<?php 
										if($hasUser!='') { echo "<i class='glyphicon glyphicon-edit'></i> Update"; }
										else if ($hasUser=='') {
											 echo "<i class='glyphicon glyphicon-floppy-save'></i> Save"; 
										}
										?>
									</button>	
								</div>
							</div>							                  						                  						                  												                  						                  																		
						  </div> <!--div media body-->
						</div>	<!--div media-->						
					</form>
				</div>	
			</div>
			
		</div>
	</div>	
</div>
