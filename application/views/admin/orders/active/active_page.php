<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-md-12'>
			<!-- put info here -->
				<legend>
					<?php echo strtoupper($title); ?> ORDERS - BATCH (<?php echo $this->encryption->decode($batch_id);?>)	
					<div class='pull-right'>
						<a href="<?=base_url("admin/grid/orders.aspx")?>" title='add'>
							<span class='glyphicon glyphicon-chevron-left'></span> Back
						</a>
					</div>																	
				</legend>
				<table id="<?php echo $module;?>_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
			       <thead>
			            <tr>
			            	<!--change table tile for your need -->
			                <th>Date Ordered</th>			                
			                <th>Reference #</th>
			                <th>Name</th>
			                <th>Quantity</th>
			                <th>Action</th>
			            </tr>
			        </thead>	
					<tbody>								
			        </tbody> 
			        <tfoot>				        	
			        </tfoot>			           		        				
				</table>	
				<input type='hidden' id='bid' value="<?php echo $batch_id;?>" />				    					
			</div>			
		</div>
	</div>	
</div>
