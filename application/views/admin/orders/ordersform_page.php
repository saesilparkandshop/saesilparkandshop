<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
					<legend>
						<?php echo $title;?>
						<div class='pull-right'>
							<a href='<?=base_url()?>admin/orders_grid/<?php echo $status."/".$this->encryption->encode($transaction_mother['batch_id']); ?>.aspx'>
								<span class='glyphicon glyphicon-chevron-left'>Back</span>
							</a>
						</div>						
					</legend>
					<?=$this->session->flashdata('msg');?>				
					<form class='form-horizontal formx' method="post" action="<?=base_url('admin/admin/Process_order/'.$status.'/'.$this->encryption->encode($transaction_mother['order_id']))?>" enctype="multipart/form-data" data-toggle="validator" role="form">
						<div class="media">
						  <div class="media-body">
							<div class='form-group'>
								<label class='form-label col-md-2 text-right'>Date Order</label>
								<div class="col-md-3">
									<input type='text' readonly="" id='order_date' name='order_date' class="form-control custom_textbox_xs text_spacer" value='<?php echo date('m/d/Y',strtotime($transaction_mother['date_created']));?>' />
								</div>
							</div>						  							
							<div class='form-group'>
								<label class='form-label col-md-2 text-right'>Order #</label>
								<div class="col-md-4">
									<input type='text' readonly="" id='order_id' name='order_id' class="form-control custom_textbox_xs text_spacer" value='<?php echo $transaction_mother['order_id'];?>' />
								</div>
							</div>	
							<div class='form-group'>
								<label class='form-label col-md-2 text-right'>Name</label>
								<div class="col-md-4">
									<input type='text' readonly="" id='full_name' name='full_name' class="form-control custom_textbox_xs text_spacer" value='<?php echo ucfirst($transaction_mother['full_name']);?>' />
								</div>
								<label class='form-label col-md-2 text-right'>Username</label>
								<div class="col-md-4">
									<input type='text' readonly="" id='client_name' name='client_name' class="form-control custom_textbox_xs text_spacer" value='<?php echo ucfirst($transaction_mother['client_name']);?>' />
									<input type='text' readonly="" id='user_id' name='user_id' class="form-control custom_textbox_xs text_spacer" value='<?php echo ucfirst($transaction_mother['user_id']);?>' />									
								</div>								
							</div>	
							<div class='form-group'>
								<label class='form-label col-md-2 text-right'>Email</label>
								<div class="col-md-4">
									<input type='text' readonly="" id='email' name='email' class="form-control custom_textbox_xs text_spacer" value='<?php echo ucfirst($transaction_mother['email']);?>' />
								</div>
								<label class='form-label col-md-2 text-right'>Mobile</label>
								<div class="col-md-4">
									<input type='text' readonly="" id='cp_num' name='cp_num' class="form-control custom_textbox_xs text_spacer" value='<?php echo ucfirst($transaction_mother['cp_num']);?>' />
								</div>								
							</div>
													<div class="form-group">
								<label class="form-label col-md-2 text-right">
									Payment Scheme
								</label>
								<?php
								$hid_id=$transaction_mother['id'];
								$payment_tdctr='';
								if($transaction_mother['payment_scheme']=="full"):  
								$is_full="checked" ;
								$payment_tdctr=1;	
								else:
								$is_full='' ;
								endif;	
								if($transaction_mother['payment_scheme']=="partial"):
								 $is_partial="checked"; 
								 $payment_tdctr=2;	
								else:
								$is_partial='';
								endif;
									
								if($transaction_mother['payment_scheme']=="payday"): 
								$is_payday="checked"; 
								$payment_tdctr=3;
								else: 
								$is_payday='';
								endif;	
								?>
								<div class='col-md-10'>
								<label class="radio-inline">
								  <input type="radio" name="inlineRadioOptions" <?php echo $is_full;?> disabled="" name='payscheme' id="inlineRadio1" value="full"> Full
								</label>
								<label class="radio-inline">
								  <input type="radio" name="inlineRadioOptions" <?php echo $is_partial;?> disabled=""  name='payscheme'  id="inlineRadio2" value="installment"> Installment
								</label>
								<label class="radio-inline">
								  <input type="radio" name="inlineRadioOptions" <?php echo $is_payday;?> disabled=""  name='payscheme'  id="inlineRadio3" value="payday"> Pay on Payday
								</label>									
								</div>
							</div>	
							<table class="table table-striped table-bordered table-condensed">
							  <thead>
							  	<th>Image</th>
							  	<th>Transaction Code</th>							  	
							  	<th>Courier</th>
							  	<th>Amount</th>
							  	<th>Status</th>
							  </thead>
							  <tbody>
							  	
							  	<?php 
							  	//preparing data
							  	for($i=0;$i<$payment_tdctr;$i++): 
							  	?>
							  	<tr>
							  		
									<td width="10%">
									<?php
									$img='td.PNG';
									$img_val='';
									$img_thumbs='';
									if(count($transaction_payment)>0 && array_key_exists($i,$transaction_payment)):
									if($transaction_payment[$i]['img_filename']!=''): 
									  $img=$transaction_payment[$i]['img_filename']; 
									  $img_val=$transaction_payment[$i]['img_filename'];
									else:
									  $img='td.PNG'; 
									  $img_val='';
									endif;	
									$transaction_payment[$i]['img_filename_thumbs']!='' ? $img_thumbs=$transaction_payment[$i]['img_filename_thumbs']: $img_thumbs='';	
									endif;
									?>
									<a class="thumbnail payment-thumb" rel="payment-thumb" style="margin-bottom: 10px; border:0px; padding:0px; cursor:pointer;" href="<?=assets_url()."img/payments/".$img?>">																			
									<img src="<?=assets_url()."img/payments/".$img;?>" id='img<?=$i;?>' name='img<?=$i;?>' style='cursor:pointer; height:90px; width:90px;' title='Please Upload Proof of payment.' alt="..." class="img-thumbnail"  >
									</a>
									<input type="file" id='pay<?=$i;?>' name="pay<?=$i; ?>" onchange="showImage(this)" buc='<?=$i; ?>'  style="display:none" />
									<input type="hidden" id='paypic<?=$i;?>' name='paypic[]'  value='<?=$img_val;?>' />
									<input type="hidden" id='paythumbs<?=$i;?>' name='paythumbs[]'  value='<?=$img_thumbs;?>' />											
									</td>	
									<td>
									<?php
									$trans_code='';
									if(count($transaction_payment)>0 && array_key_exists($i,$transaction_payment)):
									$transaction_payment[$i]['control_number']!='' ? $trans_code=$transaction_payment[$i]['control_number']:$trans_code='';
									endif;
									?>											
									<input type="text" id='trans_code1' readonly="" name='trans_code[]' value='<?=$trans_code;?>' class='form-control custom_textbox_xs text_spacer' />	
									<td>
									<select id='courier1' name='courier[]' disabled="" class="form-control custom_textbox_xs">
										<option value="">--Select Courier--</option>
										<?php 	
										print_r($couriers);																		
										foreach($couriers as $cour_val){
											if($cour_val['courier_name']!=''){
												if(count($transaction_payment)>0 && array_key_exists($i,$transaction_payment)):
													if($transaction_payment[$i]['courrier']==$cour_val['courier_name']):
														echo"<option selected value=".$cour_val['courier_name'].">".$cour_val['courier_name']."</option>";
													 else:
														echo"<option  value=".$cour_val['courier_name'].">".$cour_val['courier_name']."</option>";
													 endif;
												else:
														echo"<option  value=".$cour_val['courier_name'].">".$cour_val['courier_name']."</option>";	 
												endif;												
											} else{
												echo"<option  value=".$cour_val['courier_name'].">".$cour_val['courier_name']."</option>";
											}
										}
										?>
									</select>
									<input type="hidden"  name='courrier_val[]' value="<?=$transaction_payment[$i]['courrier']?>" />									
									</td>	
									<td>
									<?php
									$amount='0.00';
									if(count($transaction_payment)>0 && array_key_exists($i,$transaction_payment)):
									$transaction_payment[$i]['amount']!='' ? $amount=$transaction_payment[$i]['amount']:$amount='0.00';
									endif;
									?>											
									<input type="text" id='amount1' name="amount[]" readonly="" value="<?=$amount;?>" class='form-control custom_textbox_xs text_spacer' />
																		
									</td>	
									<td>
									<select id='status' name='status[]'  class="form-control custom_textbox_xs">
										<option value="">--Select Status--</option>
										<?php 
										$s=array(
										array('status'=>'pending'),
										array('status'=>'valid'),
										array('status'=>'invalid'),
										);
										foreach($s as $sts_val){
											if($sts_val['status']!=''){
												if(count($transaction_payment)>0 && array_key_exists($i,$transaction_payment)):
													if($transaction_payment[$i]['status']==$sts_val['status']):
														echo"<option selected value=".$sts_val['status'].">".$sts_val['status']."</option>";				
													 else:
														echo"<option  value=".$sts_val['status'].">".$sts_val['status']."</option>";
													 endif;
												else:
														echo"<option  value=".$sts_val['status'].">".$sts_val['status']."</option>";	 
												endif;												
											} else{
												echo"<option  value=".$sts_val['status'].">".$sts_val['status']."</option>";
											}
										}
										?>
									</select>
									<?php if(count($transaction_payment)>0 && array_key_exists($i,$transaction_payment)): ?>
										<?php if($transaction_payment[$i]['status']=='pending'): ?>
											<!--<center><i class="fa fa-circle-thin fa-4"></i> Pending</center>
											<input type="hidden" name='status[]'  id='status<?=$i;?>' value='<?=$transaction_payment[$i]['status']?>' />-->
										<?php elseif($transaction_payment[$i]['status']=='valid'): ?>
											<!--<center><i class="fa fa-check-circle fa-4"></i> Valid</center>
											<input type="hidden" name='status[]'  id='status<?=$i;?>' value='<?=$transaction_payment[$i]['status']?>' />-->
										<?php elseif($transaction_payment[$i]['status']=='invalid'): ?>
											<!--<center><i class="fa fa-circle-thin fa-4"></i> In Valid</center>
											<input type="hidden" name='status[]'  id='status<?=$i;?>' value='<?=$transaction_payment[$i]['status']?>' />-->
										<?php endif; ?>	
									<?php else: ?>
										<!--<center><i class="fa fa-circle-thin fa-4"></i> No status</center>-->
									<?php endif; ?>	
									</td>
									<?php
									$index='';
									$payment_id='';
									if(count($transaction_payment)>0 && array_key_exists($i,$transaction_payment)):
									$transaction_payment[$i]['index']!='' ? $index=$transaction_payment[$i]['index'] :  $index='' ;
									$transaction_payment[$i]['id']!='' ? $payment_id=$transaction_payment[$i]['id'] :  $payment_id='' ;
									endif;									
									?>
									<input type="hidden" name='index[]'  id='index<?=$i;?>' value='<?=$index?>' />	
									<input type="hidden" name='payment_id[]'  id='payment_id<?=$i;?>' value='<?=$payment_id?>' />																																																			  		
							  	</tr>
					  			<?php endfor;?>					  	
							  </tbody>
							  <tfoot>						  	
							  </tfoot>						
							<div class="form-group">
								<label class='form-label col-md-2 text-right'>Remarks</label>
								<div class="col-md-10">
									<textarea class="form-control custom_textbox_xs text_spacer" style="resize:none;"></textarea>
								</div>
							</div>	
							<table class="table table-striped table-bordered table-condensed">
							  <thead>
							  	<th>Product</th>
							  	<th>Price Each</th>
							  	<th>Options</th>
							  	<th>Total Quantity</th>
							  	<th>Total Price</th>
							  	<th>Action</th>
							  </thead>
							  <tbody>
								<?php
								 $item_ctr=1;
								 $option_ctr=1;	
								 foreach($transaction_baby as $transaction_val):
								?>
									<tr id='itemnum<?php echo $item_ctr;?>' class='itemnum'>
										<td style="width:30%;">
										<input type='text' id='item_name<?php echo $item_ctr;?>' name='item_name[]' class="form-control custom_textbox_xs text_spacer" readonly="" value="<?php echo $transaction_val['item_name'];?>" />
										<input type="hidden" name="item_id[]" value="<?=$transaction_val['item_id']?>"/> 
										</td>
										<td style="width:10%;"><input type='text' id='price_each<?php echo $item_ctr;?>' name='price_each[]' class="form-control custom_textbox_xs text_spacer" readonly="" value="<?php echo $transaction_val['price'];?>" /></td>
										<td style="width:30%;">
										<?php
										$opval='';
										$transaction_val['options']=='' ? $opval='' : $opval=json_decode($transaction_val['options']);
										
										foreach($opval as $optionname => $optionval ):										
										?>
										<div id='option_div<?php echo $option_ctr;?>' class='option_div<?php echo $item_ctr;?>'>
										  <input type='text' class='custom_textbox_xs opname'style="width:65%; display: inline-block;" id='opname' name="opname[]" readonly="" value="<?php echo $optionname; ?>" />
										  <input type='number' min='1' class='custom_textbox_xs opqtyt opqty<?php echo $item_ctr;?>'  oldval="<?php echo $optionval;?>" style="width:20%;display: inline-block;" itemn='<?php echo $item_ctr;?>'   id='opqty' name="opqty[]"  value="<?php echo $optionval;?>" />	
										  <button type='button' itemn='<?php echo $item_ctr;?>' option_ctr='<?php echo $option_ctr;?>' id='opdel' name='opdel' class='btn btn-sm btn-danger deloption'>
										  	<span class="glyphicon glyphicon-trash"></span>
										  </button>	
										</div>  							
										<?php $option_ctr++;endforeach; ?>
										</td>
										<td style="width:15%;"><input type='text' readonly="" id='total_qty<?php echo $item_ctr;?>' name='total_qty[]' class="form-control custom_textbox_xs text_spacer total_qty" value="<?php echo $transaction_val['qty'];?>" /></td>
										<td style="width:15%;"><input type='number' readonly="" id='subtotal<?php echo $item_ctr;?>' name='subtotal[]' class="form-control custom_textbox_xs text_spacer subtotal text-right" value="<?php echo $transaction_val['subtotal'];?>" /></td>
										<td>
										<button type='button' id='itemsdel<?php echo $item_ctr;?>' name="itemsdel" itemn='<?php echo $item_ctr;?>'  class='btn btn-sm btn-danger itemsdel'>
										  	<span class="glyphicon glyphicon-trash"></span>
										</button>
										</td>									
									</tr>
								<?php $item_ctr++; endforeach;?>
							  </tbody>
							  <tfoot>
							  	<tr>
							  		<td colspan="3"></td>
							  		<td>Intl Shipping Fee</td>
							  		<td><input type='number' id='intl_fee'  name='intl_fee' class="form-control text-right" value="<?php echo $transaction_mother['intl_fee']; ?>"  /></td>
							  		<td></td>
							  	</tr>	
							  	<tr>
							  		<td colspan="3"></td>
							  		<td>Local Shipping Fee</td>
							  		<td><input type='number' id='shipping_fee'  name='shipping_fee' class="form-control text-right" value="<?php echo $transaction_mother['shipping_fee']; ?>"  /></td>
							  		<td></td>
							  	</tr>									  							  	
							  	<tr>
							  		<td colspan="3"></td>
							  		<td>Grand Total</td>
							  		<td><input type='number' id='gtotal' readonly="" name='gtotal' class="form-control text-right" value="<?php echo $transaction_mother['total_price']; ?>"  /></td>
							  		<td></td>
							  	</tr>
							  </tfoot>
							</table>																																																	        						                  	             			                  				                  
							<input type="hidden" id="hid_base_url" value="<?php echo base_url(); ?>" /> 
							<input type="hidden" id="title" name='title' value="<?php echo $title;?>" /> 
							<div class="col-md-12" style="margin-bottom:10px;">
								<div class='pull-right'>												
									<!--<input type='hidden' name='idx' value='<?php echo  $hid_id; ?>' />-->										
									<button type="submit" id="update
									" name="submit" width='30' value="update" title='Update' class="btn btn-primary btn-sm" >
									 <i class='glyphicon glyphicon-edit'></i> Update
									</button>	
									<button type="submit" id="predelivery" name="submit" value="predelivery" width='30' title='Pre Delivery' class="btn btn-primary btn-sm" >
									 <i class='glyphicon glyphicon-edit'></i> Pre delivery
									</button>										
								</div>
							</div>							                  						                  						                  												                  						                  																		
						  </div>
						</div>							
					</form>
				
		</div>
	</div>	
</div>
