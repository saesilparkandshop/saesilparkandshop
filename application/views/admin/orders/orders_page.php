<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-md-12'>
			<!-- put info here -->
					<ul class="nav nav-tabs">
					  <li class="active"><a href="#activeorders" data-toggle="tab" aria-expanded="true">Active Orders</a></li>
					  <li class=""><a href="#predelivery" data-toggle="tab" aria-expanded="false">Pre Delivery</a></li>
					  <li class=""><a href="#onshipping" data-toggle="tab" aria-expanded="false">On Shipping</a></li>
					  <li class=""><a href="#ondelivery" data-toggle="tab" aria-expanded="false">On Delivery</a></li>
					  <li class=""><a href="#completed" data-toggle="tab" aria-expanded="false">Completed</a></li>
					  <li class=""><a href="#void" data-toggle="tab" aria-expanded="false">Void</a></li>
					</ul>
					<div id="myTabContent" class="tab-content">
					  <div class="tab-pane fade active in" id="activeorders">
						<table id="activeorders_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
					       <thead>
					            <tr>
					            	<!--change table tile for your need -->
					                <th>Batch ID</th>			                
					                <th>Date From</th>
					                <th>Date To</th>
					                <th>Action</th>	
					            </tr>
					        </thead>	
							<tbody>								
					        </tbody> 
					        <tfoot>				        	
					        </tfoot>			           		        				
						</table>						    
					  </div>
					  <div class="tab-pane fade" id="predelivery">
						<table id="predelivery_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
					       <thead>
					            <tr>
					            	<!--change table tile for your need -->
					                <th>Batch ID</th>			                
					                <th>Date From</th>
					                <th>Date To</th>
					                <th>Action</th>	
					            </tr>
					        </thead>	
							<tbody>								
					        </tbody> 
					        <tfoot>				        	
					        </tfoot>			           		        				
						</table>	
					  </div>
					  <div class="tab-pane fade" id="onshipping">
						<table id="onshipping_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
					       <thead>
					            <tr>
					            	<!--change table tile for your need -->
					                <th>Batch ID</th>			                
					                <th>Date From</th>
					                <th>Date To</th>
					                <th>Action</th>	
					            </tr>
					        </thead>	
							<tbody>								
					        </tbody> 
					        <tfoot>				        	
					        </tfoot>			           		        				
						</table>	
					  </div>
					  <div class="tab-pane fade" id="ondelivery">
						<table id="ondelivery_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
					       <thead>
					            <tr>
					            	<!--change table tile for your need -->
					                <th>Batch ID</th>			                
					                <th>Date From</th>
					                <th>Date To</th>
					                <th>Action</th>	
					            </tr>
					        </thead>	
							<tbody>								
					        </tbody> 
					        <tfoot>				        	
					        </tfoot>			           		        				
						</table>	
					  </div>
					  <div class="tab-pane fade" id="completed">
						<table id="completed_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
					       <thead>
					            <tr>
					            	<!--change table tile for your need -->
					                <th>Batch ID</th>			                
					                <th>Date From</th>
					                <th>Date To</th>
					                <th>Action</th>	
					            </tr>
					        </thead>	
							<tbody>								
					        </tbody> 
					        <tfoot>				        	
					        </tfoot>			           		        				
						</table>	
					  </div>	
					  <div class="tab-pane fade" id="void">
						<table id="void_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
					       <thead>
					            <tr>
					            	<!--change table tile for your need -->
					                <th>Batch ID</th>			                
					                <th>Date From</th>
					                <th>Date To</th>
					                <th>Action</th>					            
					            </tr>
					        </thead>	
							<tbody>								
					        </tbody> 
					        <tfoot>				        	
					        </tfoot>			           		        				
						</table>	
					  </div>						  				  
					</div>				    					
			</div>			
		</div>
	</div>	
</div>
