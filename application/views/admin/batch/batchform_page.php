<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-md-12'>
					<legend>
						<?php echo $title;?>
						<div class='pull-right'>
							<a href='<?=base_url()?>admin/grid/<?php echo $module; ?>.aspx'>
								<span class='glyphicon glyphicon-chevron-left'>Back</span>
							</a>
						</div>						
					</legend>
				<div class='col-md-offset-1 col-md-10 col-md-offset-1'>
					<?=$this->session->flashdata('msg');?>	
					<?php
					$hasUser=$batch_data;
					if($hasUser!=''){$process ='e'; }
					else if($hasUser==''){$process ='a'; }
					?>					
					<form class='form-horizontal formx' method="post" action="<?=base_url('admin/admin/Process/'.$module.'/'.$process)?>" enctype="multipart/form-data" data-toggle="validator" role="form">
						<div class="media">
						  <div class="media-body">
							<?php //check if have data 
							$batch_datefrom='';
							if($hasUser!=""){ $batch_datefrom=$batch_data['date_from']; }
							?>							  	
							  <div class="form-group">										  	
			                    <label for="status" class="col-lg-2 control-label">Date From</label>
			                    <div class="col-lg-10">
									<input type="text" class="form-control custom_textbox_xs text_spacer date" required="" id="batch_datefrom" name="batch_datefrom" value="<?php echo $batch_datefrom; ?>" placeholder="Date from">
									<div class="help-block with-errors"></div>							                   
			                    </div>
			                  </div>	
							<?php //check if have data 
							$batch_dateto='';
							if($hasUser!=""){ $batch_dateto=$batch_data['date_to']; }
							?>				                  
							  <div class="form-group">										  	
			                    <label for="status" class="col-lg-2 control-label">Date To</label>
			                    <div class="col-lg-10">
									<input type="text" class="form-control custom_textbox_xs text_spacer date" required="" id="batch_dateto" name="batch_dateto" value="<?php echo $batch_dateto; ?>" placeholder="Date to">
									<div class="help-block with-errors"></div>							                   
			                    </div>
			                  </div>							                  					                  					  									        						                  	             			                  				                  
							<input type="hidden" id="hid_base_url" value="<?php echo base_url(); ?>" /> 
							<input type="hidden" id="title" name='title' value="<?php echo $title;?>" /> 
							<div class="col-md-12" style="margin-bottom:10px;">
								<div class='pull-right'>	
									<?php 
									$hid_id='';
									if($hasUser!='')
									$hid_id=$batch_data['id'];
									?>											
									<input type='hidden' name='idx' value='<?php echo  $hid_id; ?>' />										
									<button type="submit" id="submit" name="submit" width='30' title='Save' class="btn btn-primary btn-sm" >
										<?php 
										if($hasUser!='') { echo "<i class='glyphicon glyphicon-edit'></i> Update"; }
										else if ($hasUser=='') {
											 echo "<i class='glyphicon glyphicon-floppy-save'></i> Save"; 
										}
										?>
									</button>	
								</div>
							</div>							                  						                  						                  												                  						                  																		
						  </div>
						</div>							
					</form>
				</div>	
			</div>
			
		</div>
	</div>	
</div>
