<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<!DOCTYPE html>
	<html lang="en">
	  <head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <title><?=$title?></title>
	
	    <!-- CSS -->
		<link href = "<?=assets_url();?>css/bootstrap.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/dataTables.bootstrap.min.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/bootstrap-datepicker.min.css" rel = "stylesheet">
		<link href = "<?=assets_url();?>css/css/font-awesome.min.css" rel = "stylesheet">
		<link href = "<?=assets_url();?>css/bootstrap-submenu.min.css" rel = "stylesheet">
		<link href = "<?=assets_url();?>css/custom.css" rel = "stylesheet">	
		<!--<link href = "<?=assets_url();?>css/jquery.dataTables.min.css" rel = "stylesheet">	-->
		<!--<link href = "<?=assets_url();?>css/jquery.dataTables_themeroller.css" rel = "stylesheet">	-->
		<link href = "<?=assets_url();?>css/dataTables.tableTools.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/responsive.bootstrap.min.css" rel = "stylesheet">
		<link href = "<?=assets_url();?>css/select2.min.css" rel = "stylesheet">
		<link href = "<?=assets_url();?>css/select2-bootstrap.min.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/jquery.fancybox.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/jquery.fancybox-buttons.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/jquery.fancybox-thumbs.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/yamm.css" rel = "stylesheet">						
					
		<!-- favicon -->
		<link rel="apple-touch-icon" sizes="57x57" href="<?=assets_url();?>img/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?=assets_url();?>img/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?=assets_url();?>img/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?=assets_url();?>img/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?=assets_url();?>img/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?=assets_url();?>img/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?=assets_url();?>img/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?=assets_url();?>img/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?=assets_url();?>img/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?=assets_url();?>img/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?=assets_url();?>img/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?=assets_url();?>img/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?=assets_url();?>img/favicon-16x16.png">
		<link rel="manifest" href="<?=assets_url();?>img/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?=assets_url();?>img/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">				
	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	                 
            <!-- start of styles auto php -->    
            <?php
            if(isset($styles) && is_array($styles)){
                foreach($styles as $style){
                    ?>
            <link rel="stylesheet" type="text/css" href="<?=$style?>">
            <?php
                }
            }
            ?><!--end code of styles -->	    
	    
	  </head>
<body>
		      <?php 
		    $nav='';
			//for admin login
		    $islog_admin='';
			$islog_admin=$this->session->userdata('admin_username');		
			$level=$this->session->userdata('admin_access_level');	
			//die($level);		     
			$home='';	 
			if($islog_admin){
				$home='admin/dashboard';
			}	  
		      ?>		
	<div class="container" id="top">			
		<div class="row">
			<?php
			// forlogo
			$logo_filename='td.png';
			if(!empty($logo_data['logo_id'])){
				$logo_filename=$logo_data['logo_filename'];
			}
			
			?>
			<div class="col-xs-12">
				<a href='<?=base_url($home);?>'>
				<img src="<?=assets_url()."img/logo/".$logo_filename;?>"  style='height:100px; width:200px; margin-bottom:5px;'  class="img-responsive img_canvas">
				</a>
			</div>
		</div>
	</div>	
		<nav class="navbar navbar-default">
		  <div class="container">
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="glyphicon glyphicon-option-vertical"></span>
		      </button>
		      <!-- for home nav -->

		    	<!--	<a class='navbar-brand' style="width:69px; height:60px;" href="<?php echo base_url($nav); ?>">
		    			<span class="glyphicon glyphicon-home" style="font-size: 32px;" aria-hidden="true"></span>
		    		</a>-->						    
		    </div>
		    <?php
		    //login as admin
		    //die($islog_admin);
			if(!empty($islog_admin)){
		    ?>			
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
		      <ul class="nav navbar-nav">
			    <?php
			      if($level=="admin" || $level=="super-admin" ){
			    ?>			      	
		        <li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						<ul class="list-inline">						
							Management<span class="caret"></span>
						</ul>			          				          	
			          </a>
			          <ul class="dropdown-menu" role="menu">
			          	<?php if ($level=="super-admin"){ ?>	
			            <li>
			            	<a href="<?=base_url('admin/grid/users.aspx');?>">
			             		 Users 
			            	</a>
			            </li>
			            <?php }?>	
			            <li>
			            	<a href="<?=base_url('admin/grid/brand.aspx');?>">
			             		 Brand
			            	</a>
			            </li>				            
			            <li>
			            	<a href="<?=base_url('admin/grid/category.aspx');?>">
			             		 Category
			            	</a>
			            </li>	
			             <?php if ($level=="super-admin"){ ?>		            			            		          			            
			            <li>
			            	<a href="<?=base_url('admin/grid/items.aspx');?>">
			             		 Items 
			            	</a>
			            </li>
			             <?php } ?>				            
			          <!--  <li>
			            	<a href="<?=base_url('admin/grid/options.aspx');?>">
			             		 Item Options
			            	</a>
			            </li> -->
			            <li>
			            	<a href="<?=base_url('admin/grid/promo.aspx');?>">
			             		 Promo 
			            	</a>
			            </li>
			            <li>
			            	<a href="<?=base_url('admin/grid/promo_type.aspx');?>">
			             		 Promo Scheme
			            	</a>
			            </li>
			            <li>
			            	<a href="<?=base_url('admin/grid/batch.aspx');?>">
			             		 Batch
			            	</a>
			            </li>			            
			            <li>
			            	<a href="<?=base_url('admin/form/logo/'.$this->encryption->encode('L0000000001').'');?>">
			             		 Logo
			            	</a>
			            </li>		            
			            <li>
			            	<a href="<?=base_url('admin/grid/announcement.aspx');?>">
			             		 Announcement 
			            	</a>
			            </li>	
			            <li>
			            	<a href="<?=base_url('admin/grid/about_us.aspx');?>">
			             		 About us/ Contact
			            	</a>
			            </li>	
			            <li>
			            	<a href="<?=base_url('admin/grid/terms_conditions.aspx');?>">
			             		 Terms and Condition
			            	</a>
			            </li>				            			            				            		            			            			            			            
			          </ul>
			        </li>    
		        </li>
		        <li>
		        </li>
		        	    <li>
			            	<a href="<?=base_url('admin/grid/orders.aspx');?>">
			             		 Orders 
			            	</a>
			            </li>
			            <?php if ($level=="super-admin"){ ?>	
			            <li>
			            	<a href="<?=base_url('admin/grid/reports.aspx');?>">
			             		 Reports 
			            	</a>
			            </li>	
			            <?php }?>	
			            <li>
			            	<a href="<?=base_url('admin/grid/messages.aspx');?>">
			             		 Messages 
			            	</a>
			            </li>				            	        		        
		        <?php 
				  } else if($level=="encoder"){
		        ?>
		        <li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						<ul class="list-inline">						
							Management<span class="caret"></span>
						</ul>			          				          	
			          </a>
			          <ul class="dropdown-menu" role="menu">
			            <li>
			            	<a href="<?=base_url('admin/grid/items.aspx');?>">
			             		 Items 
			            	</a>
			            </li>					  
					  </ul>
			     	</li> 
			    </li> 	      		        
		        <?php }?>        
		      </ul>		    	
		      <ul class="nav navbar-nav navbar-right">
		        <li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						<ul class="list-inline">
						 <?php /* <li><img src='<?=assets_url().$profilepic; ?>' class="img-circle" style="width:35px; height:35px;"></li> #remove hidden error. that might cause slowdown */ ?>
						  <li><?php echo ucfirst(ucwords(($islog_admin))) ?> <span class="caret"></span></li>
						</ul>			          				          	
			          </a>
			          <ul class="dropdown-menu" role="menu">			            
			            <li><a href="<?=base_url('admin/logout');?>">
			             <span class="glyphicon glyphicon-off"></span> Log Out
			            	</a>
			            </li>
			          </ul>
			        </li>        	
		        </li>
		      </ul>
		    </div>
		    <?php }?> 
		    	    
		    
		  </div>
		</nav>
		
 	
 	
