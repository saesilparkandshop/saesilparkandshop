<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-md-12'>
					<legend>
						<?php echo $title;?>
						<div class='pull-right'>
							<a href='<?=base_url()?>admin/grid/users'>
								<span class='glyphicon glyphicon-chevron-left'>Back</span>
							</a>
						</div>						
					</legend>
				<div class='col-md-offset-1 col-md-10 col-md-offset-1'>
					<?=$this->session->flashdata('msg');?>	
					<?php
					$hasUser=$user_data;
					if($hasUser!=''){$process ='e'; }
					else if($hasUser==''){$process ='a'; }
					?>					
					<form class='form-horizontal formx' method="post" action="<?=base_url('admin/admin/Process/'.$module.'/'.$process)?>" enctype="multipart/form-data" data-toggle="validator" role="form">
						<div class="media">
						  <div class="media-body">
						  				  <legend>Account Status</legend>
										  <div class="form-group">										  	
						                    <label for="status" class="col-lg-2 control-label">Account Status</label>
						                    <div class="col-lg-10">
												<select  title='Account Status' class='form-control custom_textbox_xs ' required="" id='status' name='status' placeholder='Account status' >
													<option value=''>--Account Status--</option>
													<?php
													if($hasUser!=""){													
														if($user_data['status']=='active'){
															echo "
													          <option selected value='active'>Active</option>
													          <option value='inactive'>Inactive</option>															
															";								
														} else if($user_data['status']=='inactive'){
															echo "
													          <option value='active'>Active</option>
													          <option selected value='inactive'>Inactive</option>															
															";													
														}
													} else {
															echo "
													          <option value='active'>Active</option>
													          <option value='inactive'>Inactive</option>															
															";																
														}													
													?>														
												</select>		
												<div class="help-block with-errors"></div>							                   
						                    </div>
						                  </div>	
										  <div class="form-group">										  	
						                    <label for="user_type" class="col-lg-2 control-label">Account Type</label>
						                    <div class="col-lg-10">
												<select  title='Account Status' class='form-control custom_textbox_xs ' required="" id='user_type' name='user_type' placeholder='Account type' >
													<option value=''>--Account Type--</option>
													<?php
													if($hasUser!=""){													
														if($user_data['user_type']=='regular'){
															echo "
													          <option selected value='regular'>Regular</option>
													          <option value='vip'>Vip</option>															
															";								
														} else if($user_data['user_type']=='vip'){
															echo "
													          <option value='regular'>Regular</option>
													          <option selected value='vip'>Vip</option>															
															";													
														}
													} else {
															echo "
													          <option value='regular'>Regular</option>
													          <option value='vip'>Vip</option>															
															";																
														}													
													?>														
												</select>		
												<div class="help-block with-errors"></div>							                   
						                    </div>
						                  </div>						                  					  	
						  				<legend>Login Credentials</legend>
										<?php //check if have data 
										$UserName_val='';
										if($hasUser!=""){ $UserName_val=$user_data['username']; }
										?>	
										  <div class="form-group">
						                    <label for="inputEmail" class="col-lg-2 control-label">Username</label>
						                    <div class="col-lg-10">
						                      <input type="text" title='Fill up your desire Username.'  class='form-control custom_textbox_xs text_spacer' required id='UserName' name='UserName' value="<?php echo $UserName_val; ?>"  placeholder="Username" />
						                      <div class="help-block with-errors"></div>
						                    </div>
						                  </div>														  	
										<?php //check if have data 
										$Password_val='';
										if($hasUser!=""){ $Password_val=$this->encryption->decode($user_data['password']); }
										?>	
										  <div class="form-group">
						                    <label for="Password" class="col-lg-2 control-label">Password</label>
						                    <div class="col-lg-10">
												<input type="password" pattern="^.*(?=.{7,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$" id="Password" name="Password" value="<?php echo $Password_val; ?>"  placeholder="Password" data-toggle="validator" data-minlength="7" title='Fill up your desire Password must 7-20 characters.' class="form-control custom_textbox_xs" maxlength="20" required  />	                 
						                    	<div class="help-block with-errors">Password must be 7-20 characters including one uppercase letter and alphanumeric characters.</div>		
						                    </div>
						                  </div>
										<?php //check if have data 

										if($hasUser==""){  
										?>							                  
										  <div class="form-group">
						                    <label for="Confirm_Password" class="col-lg-2 control-label">Confirm Password</label>
						                    <div class="col-lg-10">
												<input type="password"  data-match="#Password" data-match-error="Password don't match" title='Retype Password' class='form-control custom_textbox_xs ' maxlength="20" required="" id='Confirm_Password' name='Confirm_Password'  placeholder="Confirm Password">	
												<div class="help-block with-errors"></div>							                   
						                    </div>
						                  </div>	
						               <?php } ?> 
										  <div class="form-group">
						                    <label for="Access_level" class="col-lg-2 control-label">Access Level</label>
						                    <div class="col-lg-10">

												<select  title='Access Level' class='form-control custom_textbox_xs ' required="" id='Access_level' name='Access_level' placeholder='Access Level' >
													<option value=''>--Access Level--</option>
													<?php
													if($hasUser!=""){
														 if($user_data['access_level']=='super-admin'){
															echo "
															  <option selected value='super-admin'>Super Admin</option>
													          <option  value='admin'>Admin</option>
													          <option value='user'>User</option>
													          <option value='encoder'>Encoder</option>															
															";								
														} else if($user_data['access_level']=='admin'){
															echo "
															  <option  value='super-admin'>Super Admin</option>
													          <option selected value='admin'>Admin</option>
													          <option  value='user'>User</option>	
													          <option value='encoder'>Encoder</option>														
															";													
														} else if($user_data['access_level']=='user'){
															echo "
													          <option value='admin'>Admin</option>
													          <option selected value='user'>User</option>
													          <option value='encoder'>Encoder</option>															
															";													
														}else if($user_data['access_level']=='encoder'){
															echo "
													          <option value='admin'>Admin</option>
													          <option  value='user'>User</option>
													          <option selected value='encoder'>Encoder</option>															
															";													
														}
													} else {
															echo "
															  <option  value='super-admin'>Super Admin</option>
													          <option value='admin'>Admin</option>
													          <option value='user'>User</option>
													          <option value='encoder'>Encoder</option>															
															";																
														}													
													?>														
												</select>		
												<div class="help-block with-errors"></div>							                   
						                    </div>
						                  </div>					                	
						               	 			                  	
								<legend>Personal Information</legend>		
										<?php
										$Firstname_val='';
										if($hasUser!=""){ $Firstname_val=$user_data['first_name']; }
										$Middlename_val='';
										if($hasUser!=""){ $Middlename_val=$user_data['middle_name']; }										
										$Lastname_val='';
										if($hasUser!=""){ $Lastname_val=$user_data['last_name']; }
										?>	
											<small><b>Name</b></small>
											  <div class="form-group">
							                    <label for="inputEmail" class="col-md-1 control-label">First</label>
							                    <div class="col-md-3">
													<input type="text" class='form-control custom_textbox_xs ' required='' id='FirstName' name='FirstName' value="<?php echo $Firstname_val; ?>" placeholder="Firstname">	
							                    </div>
							                    <label for="inputEmail" class="col-md-1 control-label">Middle</label>
							                    <div class="col-md-3">
													<input type="text" class=' form-control custom_textbox_xs ' required='' id='MiddleName' name='MiddleName' value="<?php echo $Middlename_val; ?>" placeholder="Middlename">	
							                    </div>						                    
							                    <label for="inputEmail" class="col-md-1 control-label">Last</label>
							                    <div class="col-md-3">
													<input type="text" class='form-control custom_textbox_xs ' required='' id='LastName' name='LastName' value="<?php echo $Lastname_val; ?>" placeholder="Lastname">	
							                    </div>						                    
							                  </div>
							                  <div class='form-group'>
												<?php //check if have data 
												
												$addr_val='';
												if($hasUser!=""){ $addr_val=$user_data['address']; }
												?>								                  	
							                    <label for="address" class="col-lg-2 control-label">Address</label>
							                    <div class="col-lg-10">
							                      <textarea type="email" title='Fill up your desire address.'  class='form-control custom_textbox_xs text_spacer' required id='address' name='address' placeholder="Address" style="resize:none;" ><?php echo $addr_val; ?></textarea>
							                      <div class="help-block with-errors"></div>
							                    </div>							                  	
							                  </div> 							                  
							                  <div class='form-group'>
												<?php //check if have data 
												$email_val='';
												if($hasUser!=""){ $email_val=$user_data['email']; }
												?>								                  	
							                    <label for="email" class="col-lg-2 control-label">Email</label>
							                    <div class="col-lg-10">
							                      <input type="email" title='Fill up your desire Email.'  class='form-control custom_textbox_xs text_spacer' required id='email' name='email' value="<?php echo $email_val; ?>"  placeholder="Email" />
							                      <div class="help-block with-errors"></div>
							                    </div>							                  	
							                  </div> 
							                  <div class='form-group'>
												<?php //check if have data 
												$contactno_val='';
												if($hasUser!=""){ $contactno_val=$user_data['contact_no']; }
												?>								                  	
							                    <label for="contact_no" class="col-lg-2 control-label">Contact #</label>
							                    <div class="col-lg-10">
							                      <input type="number" title='Fill up your desire Contact #.'  class='form-control custom_textbox_xs text_spacer' required id='contact_no' name='contact_no' value="<?php echo $contactno_val; ?>"  placeholder="Contact #" />
							                      <div class="help-block with-errors"></div>
							                    </div>							                  	
							                  </div> 
							                  <div class='form-group'>
												<?php //check if have data 
												$fb_val='';
												if($hasUser!=""){ $fb_val=$user_data['fb_url']; }
												?>								                  	
							                    <label for="fb_url" class="col-lg-2 control-label">Facebook Profile</label>
							                    <div class="col-lg-10">
							                      <input type="url" title='Fill up your facebook profile.'  class='form-control custom_textbox_xs text_spacer' required id='fb_url' name='fb_url' value="<?php echo $fb_val; ?>"  placeholder="Facebook Profile" />
							                      <div class="help-block with-errors">Please make sure that it's searchable for verification purpose only.(https://www.facebook.com/ABSCBNnetwork?fref=nf)</div>
							                    </div>							                  	
							                  </div> 							                  							                  
						                  </div>
							<legend>Account Information</legend>	
							                  <div class='form-group'>
												<?php //check if have data 
												
												$account_no_val='';
												if($hasUser!=""){ $account_no_val=$user_data['account_no']; }
												?>								                  	
							                    <label for="account_no" class="col-lg-2 control-label">Account #</label>
							                    <div class="col-lg-10">
							                      <input type="number" title='Fill up your desire Email.'  class='form-control custom_textbox_xs text_spacer' required id='account_no' name='account_no' value="<?php echo $account_no_val; ?>"  placeholder="Account #" />
							                      <div class="help-block with-errors"></div>
							                    </div>							                  	
							                  </div> 							                  
							                  <div class='form-group'>
												<?php //check if have data 
												$account_name_val='';
												if($hasUser!=""){ $account_name_val=$user_data['account_name']; }
												?>								                  	
							                    <label for="account_name" class="col-lg-2 control-label">Account Name</label>
							                    <div class="col-lg-10">
							                      <input type="text" title='Fill up your desire Email.'  class='form-control custom_textbox_xs text_spacer' required id='account_name' name='account_name' value="<?php echo $account_name_val; ?>"  placeholder="Account name" />
							                      <div class="help-block with-errors"></div>
							                    </div>							                  	
							                  </div> 							
																						        						                  	             			                  				                  
							<input type="hidden" id="hid_base_url" value="<?php echo base_url(); ?>" /> 
							<input type="hidden" id="title" name='title' value="<?php echo $title;?>" /> 
							<div class="col-md-12" style="margin-bottom:10px;">
								<div class='pull-right'>	
									<?php 
									$hid_id='';
									if($hasUser!='')
									$hid_id=$user_data['user_id'];
									?>											
									<input type='hidden' name='idx' value='<?php echo  $hid_id; ?>' />										
									<button type="submit" id="submit" name="submit" width='30' title='Save' class="btn btn-primary btn-sm" >
										<?php 
										if($hasUser!='') { echo "<i class='glyphicon glyphicon-edit'></i> Update"; }
										else if ($hasUser=='') {
											 echo "<i class='glyphicon glyphicon-floppy-save'></i> Save"; 
										}
										?>
									</button>	
								</div>
							</div>							                  						                  						                  												                  						                  																		
						  </div>
						</div>							
					</form>
				</div>	
			</div>
			
		</div>
	</div>	
</div>
