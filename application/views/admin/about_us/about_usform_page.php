<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-md-12'>
					<legend>
						<?php echo $title;?>
						<div class='pull-right'>
							<a href='<?=base_url()?>admin/grid/<?php echo $module; ?>.aspx'>
								<span class='glyphicon glyphicon-chevron-left'>Back</span>
							</a>
						</div>						
					</legend>

				<div class='col-md-offset-1 col-md-10 col-md-offset-1'>
					<?=$this->session->flashdata('msg');?>	<!-- eto po para sa mga message na lalabas ntin pag submit ng form
						like error messahge ganun -->
					<?php
					$hasUser=$aboutus_data;// eto naman ng chcheck kung ung form is for editing or new
					if($hasUser!=''){$process ='e'; } // eto it means if may laman ung  $gasuser it  means edit ggwin ng form
					else if($hasUser==''){$process ='a'; }//eto pag add new.
					?>					
					<form class='form-horizontal formx' method="post" action="<?=base_url('admin/admin/Process/'.$module.'/'.$process)?>" enctype="multipart/form-data" data-toggle="validator" role="form">								
						<div class="media">
						  <div class="media-body">
				
    </br>
    <?php 
					$aboutus_date_aboutus='';
					if($hasUser!=''){
					$aboutus_date_aboutus=$aboutus_data['date_aboutus'];
					
					}
					?>	
		 						<div class="form-group">
              						<label class="col-md-3 control-label" for="name">Date</label>
              						<div class="col-md-9">
               								 <input class="date form-control" id="date_aboutus" name="date_aboutus" placeholder="Date" required value='<?php echo $aboutus_date_aboutus; ?>' type='text'>
              								</div>
   				</div>
   			</br>
							<?php 
								$aboutus_about='';
								if($hasUser!=''){
								$aboutus_about=$aboutus_data['about'];
								
								}
								?>		    
           						<div class="form-group">
             						 <label class="col-md-3 control-label" for="about">About</label>
             							 <div class="col-md-9">
               								 <textarea class="form-control" style="resize:none ;" id="about" name="about" placeholder="Please enter your About here..." rows="10" required><?php echo $aboutus_about; ?></textarea>
              							 </div>
              					</div>       
   					 </br>
   					 
   					 		
					</br>
							<?php 
								$aboutus_contacts='';
								if($hasUser!=''){
								$aboutus_contacts=$aboutus_data['contacts'];
								
								}
								?>	
		 						<div class="form-group">
              						<label class="col-md-3 control-label" for="name">Contact</label>
              						<div class="col-md-9">
               								 <textarea class="form-control" id="contacts" name="contacts" placeholder="Contact" required><?php echo $aboutus_contacts; ?></textarea>
              								</div>
   				</div>
					<input type="hidden" id="hid_base_url" value="<?php echo base_url(); ?>" /> 
							<input type="hidden" id="title" name='title' value="<?php echo $title;?>" /> 
							<div class="col-md-12" style="margin-bottom:10px;">
								<div class='pull-right'>	
									<?php 
									$hid_id='';
									if($hasUser!='')
									$hid_id=$aboutus_data['aboutus_id'];
									?>	
									</br>
												
									<input type='hidden' name='idx' value='<?php echo  $hid_id; ?>' />										
									<button type="submit" id="submit" name="submit" width='30' title='Save' class="btn btn-primary btn-sm" >
										
										<?php 
										if($hasUser!='') { echo "<i class='glyphicon glyphicon-edit'></i> Update"; }
										else if ($hasUser=='') {
											 echo "<i class='glyphicon glyphicon-floppy-save'></i> Save"; 
										}
										?>
									</button>	
								</div>
							</div>								                   
						      </div>
						    </div>						                  					  	
										
									
										
					</form>
				</div>	
			</div>
			
		</div>
	</div>	
</div>
