
	<input type='hidden' id='base_url' value='<?php echo base_url(); ?>' />	
	<input type='hidden' id='assets_url' value='<?php echo assets_url(); ?>' />	
	<!--modal-->
	<div class="modal" data-backdrop='static' data-keyboard="false">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	       <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
	        <h4 class="modal-title">Please Wait…</h4>
	      </div>
	      <div class="modal-body">
				<div class="progress">
				  <div class="progress-bar progress-bar-striped active" role="progressbar"
				  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
				  </div>
				</div>	        
	      </div>
	   <!--   <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary">Save changes</button>
	      </div>-->
	    </div>
	  </div>
	</div>	
		
	<!-- loding js files -->

	<script  src="<?=assets_url();?>js/jquery-1.11.3.min.js"></script>
	<script  src="<?=assets_url();?>js/bootstrap.min.js"></script>
	<script  src="<?=assets_url();?>js/bootstrap-datepicker.min.js"></script>
	<script  src="<?=assets_url();?>js/validator.min.js"></script>
	<script  src="<?=assets_url();?>js/jquery.dataTables.min.js"></script>
	<script src="<?=assets_url();?>js/dataTables.bootstrap.min.js"></script>
	<script  src="<?=assets_url();?>js/dataTables.responsive.js"></script>
	<script  src="<?=assets_url();?>js/dataTables.tableTools.js"></script>		
	<script  src="<?=assets_url();?>js/select2.min.js"></script>	
	<script  src="<?=assets_url();?>js/spin.min.js"></script>
	<script  src="<?=assets_url();?>js/currency/curry-0.8.min.js"></script>		
	<script  src="<?=assets_url();?>js/bootstrap-submenu.min.js"></script>	
	<script  src="<?=assets_url();?>js/jquery.elastic.source.js"></script>	
	<script  src="<?=assets_url();?>js/jquery.elevateZoom-3.0.8.min.js"></script>		
	<script  src="<?=assets_url();?>js/jquery.fancybox.js"></script>	
	<script  src="<?=assets_url();?>js/jquery.fancybox.pack.js"></script>	
	<script  src="<?=assets_url();?>js/jquery.fancybox-buttons.js"></script>	
	<script  src="<?=assets_url();?>js/jquery.fancybox-media.js"></script>		
	<script  src="<?=assets_url();?>js/jquery.fancybox-thumbs.js"></script>	
	<script  src="<?=assets_url();?>js/bootbox.min.js"></script>	
			
<!--	<script src="<?=assets_url();?>js/jquery.backstretch.min.js"></script>	-->	
	<script  src="<?=assets_url();?>js/global.js"></script>
            <?php       
            if(isset($scripts) && is_array($scripts)){
               foreach($scripts as $script){
            ?>
            <script    src="<?=$script?>"></script>

            <?php
                }
            }
            ?><!--end code of scripts -->	
	<!-- end script -->	
				
			<footer id="footer">
				<div class='container'>
			        <p class="pull-right"><a href="#top" title="Back to Top"><b class='glyphicon glyphicon-chevron-up'></b></a></p>
			        <p>
			        &copy; Saesil Park and Shop @ 2015. 
			       <!-- | <a style="color:#000;" href="<?php echo base_url("home/index/about"); ?>">About</a>
			        | <a style="color:#000;" href="<?php echo base_url("home/index/terms"); ?>">Terms</a>-->
			        </p>							
				</div>		         
			</footer>				
</body>
</html>