<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-md-12'>
			<!-- put info here -->
				<legend>
					<?php echo strtoupper($title); ?> MANAGEMENT	
					<div class='pull-right'>
						<a href="<?=base_url("admin/form/".$module."/".$this->encryption->encode('a')."")?>" title='add'>
							<span class='glyphicon glyphicon-plus'></span>
						</a>
					</div>												
				</legend>
				<table id="<?php echo $module;?>_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
			       <thead>
			            <tr>
			                <th>Promo Code</th>			                
			                <th>Promo Name</th>
			                <th>Promo Type</th>
			                <th>Action</th>
			            </tr>
			        </thead>	
					<tbody>								
			        </tbody> 
			        <tfoot>				        	
			        </tfoot>			           		        				
				</table>
				<input type='hidden' id='base_urlx' value='<?php echo base_url(); ?>' />										    					
			</div>			
		</div>
	</div>	
</div>
