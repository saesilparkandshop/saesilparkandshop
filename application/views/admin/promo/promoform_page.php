<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-md-12'>
					<legend>
						<?php echo $title;?>
						<div class='pull-right'>
							<a href='<?=base_url()?>admin/grid/<?php echo $module; ?>.aspx'>
								<span class='glyphicon glyphicon-chevron-left'>Back</span>
							</a>
						</div>						
					</legend>
				<div class='col-md-offset-1 col-md-10 col-md-offset-1'>
					<?=$this->session->flashdata('msg');?>	
					<?php
					$hasUser=$promo_data;
					if($hasUser!=''){$process ='e'; }
					else if($hasUser==''){$process ='a'; }
					?>					
					<form class='form-horizontal formx' method="post" action="<?=base_url('admin/admin/Process/'.$module.'/'.$process)?>" enctype="multipart/form-data" data-toggle="validator" role="form">
						<div class="media">
						  <div class="media-body">
							  		<?php
							  		$img_path='td.PNG'; 
							  		if($hasUser!=''){
							  		if(!empty($promo_data['promo_filename']))
							  		$img_path = $promo_data['promo_filename'];		
							  		}
							  		?>
									<div class="form-group">
										<center>
										  <input type="hidden" value="<?php echo  $img_path; ?>" name='img_name' />	
										  <a id='upload-item' class="img-thumbnail" title='must  750x442' style=" height:442px; width:750px;  cursor:pointer;">
										      <img style="height:100%; width:100%;" id="img-prev" class="img-responsive" src="<?=assets_url()."img/promo_banner/".$img_path;?>" alt="...">
										  </a>
										  <input type="file" id='btn_upload' class="btn-upload" accept="image/*"  name='upload' onchange="showImage(this)" />
										</center>
									</div>								  	
										  <div class="form-group">										  	
						                    <label for="status" class="col-lg-2 control-label">Promo Status</label>
						                    <div class="col-lg-10">
												<select  title='Account Status' class='form-control custom_textbox_xs ' required="" id='status' name='status' placeholder='Account status' >
													<option value=''>--Promo Status--</option>
													<?php
													if($hasUser!=""){													
														if($promo_data['status']=='active'){
															echo "
													          <option selected value='active'>Active</option>
													          <option value='inactive'>Inactive</option>															
															";								
														} else if($promo_data['status']=='inactive'){
															echo "
													          <option value='active'>Active</option>
													          <option selected value='inactive'>Inactive</option>															
															";													
														}
													} else {
															echo "
													          <option value='active'>Active</option>
													          <option value='inactive'>Inactive</option>															
															";																
														}													
													?>														
												</select>		
												<div class="help-block with-errors"></div>							                   
						                    </div>
						                  </div>						                  					  	
										<?php //check if have data 
										$promo_code_val='';
										if($hasUser!=""){ $promo_code_val=$promo_data['promo_code']; }
										?>	
										  <div class="form-group">
						                    <label for="promo_code" class="col-lg-2 control-label">Promo code</label>
						                    <div class="col-lg-10">
						                      <input type="text" title='Fill up your desire Promo Code.'  class='form-control custom_textbox_xs text_spacer' required id='promo_code' name='promo_code' value="<?php echo $promo_code_val; ?>"  placeholder="Promo Code" />
						                      <div class="help-block with-errors"></div>
						                    </div>
						                  </div>	
										  <div class="form-group">										  	
						                    <label for="promo_type" class="col-lg-2 control-label">Promo Scheme</label>
						                    <div class="col-lg-10">
												<select  title='Account Status' class='form-control custom_textbox_xs ' required="" id='promo_scheme' name='promo_scheme' placeholder='Account status' >
													<option value=''>--Promo Scheme Type--</option>
													<?php
													foreach($promo_scheme_data as $ps_val){
														if($hasUser!=""){
															if($ps_val['ptype_id']==$promo_data['promo_scheme'] ){
															echo "
													          <option selected value='".$ps_val['ptype_id']."'>".$ps_val['ptype_name']."</option>
													        ";																	
															} else {
															echo "
													          <option  value='".$ps_val['ptype_id']."'>".$ps_val['ptype_name']."</option>
													        ";																
															}
														} else {
															echo "
													          <option  value='".$ps_val['ptype_id']."'>".$ps_val['ptype_name']."</option>
													        ";															
														}														
													}														
												
													?>														
												</select>		
												<div class="help-block with-errors"></div>							                   
						                    </div>
						                  </div>						                  													  							
										<?php //check if have data 
										$promo_name_val='';
										if($hasUser!=""){ $promo_name_val=$promo_data['promo_name']; }
										?>	
										  <div class="form-group">
						                    <label for="promo_name" class="col-lg-2 control-label">Promo name</label>
						                    <div class="col-lg-10">
						                      <input type="text" title='Fill up your desire Username.'  class='form-control custom_textbox_xs text_spacer' required id='promo_name' name='promo_name' value="<?php echo $promo_name_val; ?>"  placeholder="Promo Name" />
						                      <div class="help-block with-errors"></div>
						                    </div>
						                  </div>	
										  <div class="form-group">										  	
						                    <label for="promo_type" class="col-lg-2 control-label">Promo Type</label>
						                    <div class="col-lg-10">
												<select  title='Account Status' class='form-control custom_textbox_xs ' required="" id='promo_type' name='promo_type' placeholder='Account status' >
													<option value=''>--Promo Type--</option>
													<?php
													if($hasUser!=""){													
														if($promo_data['promo_type']=='brand'){
															echo "
													          <option selected value='brand'>Brand</option>
													          <option value='category'>Category</option>
													          <option value='sub-category'>Sub-Category</option>
													          <option value='bundle'>Bundle</option>
													          <option value='item'>Item</option>																          															
															";							
														} else if($promo_data['promo_type']=='category'){
															echo "
													          <option value='brand'>Brand</option>
													          <option selected value='category'>Category</option>
													          <option value='sub-category'>Sub-Category</option>
													          <option value='bundle'>Bundle</option>
													          <option value='item'>Item</option>																          															
															";													
														} else if($promo_data['promo_type']=='sub-category'){
															echo "
													          <option value='brand'>Brand</option>
													          <option value='category'>Category</option>
													          <option selected value='sub-category'>Sub-Category</option>
													          <option value='bundle'>Bundle</option>
													          <option value='item'>Item</option>																          															
															";												
														} else if($promo_data['promo_type']=='bundle'){
															echo "
													          <option value='brand'>Brand</option>
													          <option value='category'>Category</option>
													          <option value='sub-category'>Sub-Category</option>
													          <option selected value='bundle'>Bundle</option>
													          <option value='item'>Item</option>																          															
															";												
														} else if($promo_data['promo_type']=='item'){
															echo "
													          <option value='brand'>Brand</option>
													          <option value='category'>Category</option>
													          <option value='sub-category'>Sub-Category</option>
													          <option value='bundle'>Bundle</option>
													          <option selected value='item'>Item</option>																          															
															";												
														}																		
													} else {
															echo "
													          <option value='brand'>Brand</option>
													          <option value='category'>Category</option>
													          <option value='sub-category'>Sub-Category</option>
													          <option value='bundle'>Bundle</option>
													          <option value='item'>Item</option>																          															
															";																
														}													
													?>														
												</select>		
												<div class="help-block with-errors"></div>							                   
						                    </div>
						                  </div>	
						                  <?php
											$is_hidden = "style='display:none;'";       		
											$is_hidden2="style='display:none;'";
											$is_required = "";
							                  if($hasUser!='' ){
							                  	$is_required = "";
							                  	$is_hidden2 = "style='display:none;'";
							                  	if($promo_data['promo_type']=='bundle' || $promo_data['promo_type']=='item'){
							                  		$is_hidden = "style='display:block;'";
							                  		$is_required = "required";
							                  	}else if($promo_data['promo_type']=='brand'){
							                  		$is_hidden2 =  "style='display:block;'";
							                  	}
							                  	
							                  }						                  
							                  //todo selected brands for edit					                  
						                  ?>
						                  <div id = "brands_div"  <?php echo $is_hidden2; ?> >
						                  <div class="form-group"> 
						                  	<label for="" class="col-lg-2 control-label">Brands</label>
							                <div class="col-lg-10">
							                  	<select  id="brands" name="brands[]" multiple class="form-control custom_textbox_xs widthx">
							                  	 	<?php  foreach ($brands_data as $key => $brand) : ?>
							                  	 		<?php if (isset($brand['promo_id']) && !empty($brand['promo_id']) ) : ?>
														<option value="<?php echo $brand['brand_id']; ?>" selected><?php echo $brand['brand_name']?></option>
														<?php else : ?>
														<option value="<?php echo $brand['brand_id']; ?>"><?php echo $brand['brand_name']?></option>
														<?php endif; ?>
							                  	 	<?php endforeach; ?>
							                  	</select>
						                  	</div>
						                  	</div>
						                  </div>	
<!-- 
						                  <div id = "categories_div" >
						                  <div class="form-group"> 
						                  	<label for="" class="col-lg-2 control-label">Categories</label>
							                <div class="col-lg-10">
							                  	<select  id="brands" name="categories[]" multiple class="form-control custom_textbox_xs widthx">
							                  	 	<?php  foreach ($brands_data as $key => $brand) : ?>
							                  	 		<?php if (isset($brand['promo_id']) && !empty($brand['promo_id']) ) : ?>
														<option value="<?php echo $brand['brand_id']; ?>" selected><?php echo $brand['brand_name']?></option>
														<?php else : ?>
														<option value="<?php echo $brand['brand_id']; ?>"><?php echo $brand['brand_name']?></option>
														<?php endif; ?>
							                  	 	<?php endforeach; ?>
							                  	</select>
						                  	</div>
						                  	</div>
						                  </div>	


						                  <div id = "sub_categories_div" >
						                  <div class="form-group"> 
						                  	<label for="" class="col-lg-2 control-label">Categories</label>
						                  	<div  class=''>	<div class="col-lg-10">
						                  	<select  id="categories" name="categories[]"   class="form-control custom_textbox_xs widthx">
							                  	 	<?php  foreach ($cateogries_data as $key => $brand) : ?>
							                  	 		<?php if (isset($brand['promo_id']) && !empty($brand['promo_id']) ) : ?>
														<option value="<?php echo $brand['brand_id']; ?>" selected><?php echo $brand['brand_name']?></option>
														<?php else : ?>
														<option value="<?php echo $brand['brand_id']; ?>"><?php echo $brand['brand_name']?></option>
														<?php endif; ?>
							                  	 	<?php endforeach; ?>
							                  	</select></div>
						                  	<div >
						                  		<div class="col-lg-10">
							                  	<select  id="brands" name="categories[]" multiple class="form-control custom_textbox_xs widthx">
							                  	 	<?php  foreach ($brands_data as $key => $brand) : ?>
							                  	 		<?php if (isset($brand['promo_id']) && !empty($brand['promo_id']) ) : ?>
														<option value="<?php echo $brand['brand_id']; ?>" selected><?php echo $brand['brand_name']?></option>
														<?php else : ?>
														<option value="<?php echo $brand['brand_id']; ?>"><?php echo $brand['brand_name']?></option>
														<?php endif; ?>
							                  	 	<?php endforeach; ?>
							                  	</select>
						                  	</div>

						                  	</div>
							                
						                  	</div>
						                  </div>	
										</div> -->

						                  	<div id='items_div' <?php echo $is_hidden; ?>>					                  
												<div class="form-group">										  	
							                    <label for="inputEmail" class="col-lg-2 control-label">Items</label>
							                    <div class="col-lg-10">

													<select  title='Items' class='form-control custom_textbox_xs' <?php echo $is_required; ?> id='items' name='items[]' multiple="">
														<option value=''>--Items--</option>
														<?php
														foreach($items_data as $items_val){																									
															if($hasUser!=""){
																// $items_promo_val = explode(',',$promo_data['promo_id']);
																if($promo_data['promo_type'] == "item"){

																	foreach($promo_group as $val){

																		//echo $val;
																		if($val['type'] == "item" && $val['value']==$items_val['item_id']){
																			echo "<option value='".$items_val['item_id']."' selected>".$items_val['item_name']."</option>";	
																		} else {
																			echo "<option value='".$items_val['item_id']."'>".$items_val['item_name']."</option>";
																		}
																	}
																}
															} else {
															  echo "<option value='".$items_val['item_id']."'>".$items_val['item_name']."</option>";
															}													
														}													
														?>														
													</select>		
													<div class="help-block with-errors"></div>							                   
							                    </div>
							                  </div>							                  
						                  </div>
						                  
											<div class="form-group">
												<?php 
												$from='';
												$to='';
												if($hasUser!=""){
												$from=$promo_data['promo_from'];
												$to=$promo_data['promo_to'];													
												}
												?>
												<label class="col-lg-2 control-label">Date</label>
												<div class='col-lg-4'>
											    <input type="text" id='start' required="" value='<?php echo $from; ?>' name='start' class="form-control custom_textbox_xs date"  />
											    </div>
											    <div  class='col-lg-1'>
											    <label >to</label>	
											    </div>												    
											    <div class='col-lg-4'>
											    <input type="text" id='end' required="" value='<?php echo $to; ?>' name='end' class="form-control custom_textbox_xs date" />																									    
												</div>										
											</div>																		                  						                  																						        						                  	             			                  				                  
							<input type="hidden" id="hid_base_url" value="<?php echo base_url(); ?>" /> 
							<input type="hidden" id="title" name='title' value="<?php echo $title;?>" /> 
							<div class="col-md-12" style="margin-bottom:10px;">
								<div class='pull-right'>	
									<?php 
									$hid_id='';
									if($hasUser!='')
									$hid_id=$promo_data['promo_id'];
									?>								
									<?php if(isset($promo_data['id'])) : ?>
									<input type='hidden' name='id' value='<?=$promo_data['id'] ?>' />										
								<?php endif; ?>
									<input type='hidden' name='idx' value='<?php echo  $hid_id; ?>' />										
									<button type="submit" id="submit" name="submit" width='30' title='Save' class="btn btn-primary btn-sm" >
										<?php 
										if($hasUser!='') { echo "<i class='glyphicon glyphicon-edit'></i> Update"; }
										else if ($hasUser=='') {
											 echo "<i class='glyphicon glyphicon-floppy-save'></i> Save"; 
										}
										?>
									</button>	
								</div>
							</div>							                  						                  						                  												                  						                  																		
						  </div>
						</div>							
					</form>
				</div>	
			</div>
			
		</div>
	</div>	
</div>