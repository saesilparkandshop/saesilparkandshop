<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
		 <div class='col-md-12'>
					<legend>
						<?php echo $title;?>
						<div class='pull-right'>
							<a href='<?=base_url()?>admin/grid/<?php echo $module; ?>.aspx'>
								<span class='glyphicon glyphicon-chevron-left'>Back</span>
							</a>
						</div>					
					</legend>
<!--start -->
			<div class='col-md-offset-1 col-md-10 col-md-offset-1'>
					<?=$this->session->flashdata('msg');?>	<!-- eto po para sa mga message na lalabas ntin pag submit ng form
						like error messahge ganun -->
					<?php
					$hasUser=$messages_data;// eto naman ng chcheck kung ung form is for editing or new
					if($hasUser!=''){$process ='e'; } // eto it means if may laman ung  $gasuser it  means edit ggwin ng form
					else if($hasUser==''){$process ='a'; }//eto pag add new.
					?>					
					<form class='form-horizontal formx' method="post" action="<?=base_url('admin/admin/Process/'.$module.'/'.$process)?>" enctype="multipart/form-data" data-toggle="validator" role="form">								
						<div class="media">
						  <div class="media-body">
				
    </br>
    <?php 
					$messages_date='';
					if($hasUser!=''){
					$messages_date=$messages_data['date'];
					
					}
					?>	
		 						<div class="form-group">
              						<label class="col-lg-2 control-label">Date</label>
              						<div class="col-md-9">
               								 <input class="date form-control" id="date" name="date" required value='<?php echo $messages_date; ?>' type='text'>
              								</div>
   				</div>
   			</br>
							<?php 
								$messages_name='';
								if($hasUser!=''){
								$messages_name=$messages_data['name'];
								
								}
								?>		    
           						<div class="form-group">
             						 <label class="col-lg-2 control-label">Name</label>
             							 <div class="col-md-9">
               								 <input id="name" name="name" type="text" value='<?php echo $messages_name; ?>' class="form-control" required>
              							 </div>
              					</div>       
   					 </br>
   					 
   					 <?php 
								$messages_email='';
								if($hasUser!=''){
								$messages_email=$messages_data['email'];
								
								}
								?>		    
           						<div class="form-group">
             						 <label class="col-lg-2 control-label">Email</label>
             							 <div class="col-md-9">
               								 <input id="email" name="email" type="text" value='<?php echo $messages_email; ?>' class="form-control" required>
              							</div>
              					</div>       
   					 </br>
   				
   					 		 <?php 
								$messages_subject='';
								if($hasUser!=''){
								$messages_subject=$messages_data['subject'];
								
								}
								?>		    
           						<div class="form-group">
             						 <label class="col-lg-2 control-label">Subject</label>
             							 <div class="col-md-9">
               								 <input id="subject" name="subject" type="text" value='<?php echo $messages_subject; ?>' class="form-control" required>
              							 </div>
              					</div>       
   					 </br>
   				
			
   					 		 <?php 
								$messages_message='';
								if($hasUser!=''){
								$messages_message=$messages_data['message'];
								
								}
								?>		    
           						<div class="form-group">
             						 <label class="col-lg-2 control-label">Message</label>
             							 <div class="col-md-9">
               								 <textarea class="form-control" id="message" name="message" rows="5"  required><?php echo $messages_message; ?></textarea>
              							 </div>
              					</div>       
   					 </br>
   				
						
									
									</button>	
								</div>
							</div>								                   
						      </div>
						    </div>						                  					  	
										
									
										
					</form>
				</div>	
<!--end-->
			
		</div><!--md12-->
		</div>	<!--mintheight-->
	</div><!--col-xs-12--->
</div><!--container-->