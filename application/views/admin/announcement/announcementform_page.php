<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-md-12'>
					<legend>
						<?php echo $title;?>
						<div class='pull-right'>
							<a href='<?=base_url()?>admin/grid/<?php echo $module; ?>.aspx'>
								<span class='glyphicon glyphicon-chevron-left'>Back</span>
							</a>
						</div>						
					</legend>

				<div class='col-md-offset-1 col-md-10 col-md-offset-1'>
					<?=$this->session->flashdata('msg');?>	<!-- eto po para sa mga message na lalabas ntin pag submit ng form
						like error messahge ganun -->
					<?php
					$hasUser=$announcement_data;// eto naman ng chcheck kung ung form is for editing or new
					if($hasUser!=''){$process ='e'; } // eto it means if may laman ung  $gasuser it  means edit ggwin ng form
					else if($hasUser==''){$process ='a'; }//eto pag add new.
					?>					
					<form class='form-horizontal formx' method="post" action="<?=base_url('admin/admin/Process/'.$module.'/'.$process)?>" enctype="multipart/form-data" data-toggle="validator" role="form">								
						<div class="media">
						  <div class="media-body">
							<!-- sha eto par mag papasok n tau ng ma data natin dun sa table-->
							<?php
							//var_dump($announcement_data);//pang check kung may data tau pang check ng mga array.
							$announce_date = '';//eto alias lng to pde ikaw mag name nyan gngmit yan pang define kung may laman data o wla
							if($hasUser!=''){//it means kung edit view
							$announce_date=	$announcement_data['date_announce'];
							}
							?>			 									  							                 										
								<div class="form-group">
									<label for="status" class="col-md-3 control-label">Announcement Date</label>
									<div class="col-md-9">
										<input class='date form-control' name='announce_date' id='announce_date' placeholder="Announcement Date" value='<?php echo $announce_date; ?>' type='text'/>
									</div>						            
								</div>
          					 </div>
					</br>
							<?php
							//var_dump($announcement_data);//pang check kung may data tau pang check ng mga array.
							$announce_subj = '';//eto alias lng to pde ikaw mag name nyan gngmit yan pang define kung may laman data o wla
							if($hasUser!=''){//it means kung edit view
							$announce_subj=	$announcement_data['subj'];
							}
							?>		
		 						<div class="form-group">
              						<label class="col-md-3 control-label" for="name">Title</label>
              							<div class="col-md-9">
               							 <input id="subj" name="subj" type="text" placeholder="Title" value='<?php echo $announce_subj; ?>' class="form-control" required>
              							</div>
              					</div>
            
    </br>
							<?php
							//var_dump($announcement_data);//pang check kung may data tau pang check ng mga array.
							$announce_message = '';//eto alias lng to pde ikaw mag name nyan gngmit yan pang define kung may laman data o wla
							if($hasUser!=''){//it means kung edit view
							$announce_message=	$announcement_data['message'];
							}
							?>	    
           						<div class="form-group">
             						 <label class="col-md-3 control-label" for="message">Details</label>
             							 <div class="col-md-9">
               								 <textarea class="form-control" style="resize:none ;" id="details" name="details" placeholder="Please enter your details here..." rows="10" required><?php echo $announce_message; ?></textarea>
              							 </div>
              					</div>       
   					 </br>

							<input type="hidden" id="hid_base_url" value="<?php echo base_url(); ?>" /> 
							<input type="hidden" id="title" name='title' value="<?php echo $title;?>" /> 
							<div class="col-md-12" style="margin-bottom:10px;">
								<div class='pull-right'>	
									<?php 
									$hid_id='';
									if($hasUser!='')
									$hid_id=$announcement_data['announcement_id'];
									?>											
									<input type='hidden' name='idx' value='<?php echo  $hid_id; ?>' />										
									<button type="submit" id="submit" name="submit" width='30' title='Save' class="btn btn-primary btn-sm" >
										<?php 
										//eto ng dedefine naman kung save na word o update lalabas 
										//so aun po nakagawa na tau ng isang form na dun na nag add at edit... :) so last nlng ung update
										if($hasUser!='') { echo "<i class='glyphicon glyphicon-edit'></i> Update"; }
										else if ($hasUser=='') {
											 echo "<i class='glyphicon glyphicon-floppy-save'></i> Save"; 
										}
										?>
									</button>	
								</div>
							</div>								                   
						      </div>
						    </div>						                  					  	
										
									
										
					</form>
				</div>	
			</div>
			
		</div>
	</div>	
</div>
