<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-md-12'>
					<legend>
						<?php echo $title;?>
						<div class='pull-right'>
							<a href='<?=base_url()?>admin/grid/<?php echo $module; ?>.aspx'>
								<span class='glyphicon glyphicon-chevron-left'>Back</span>
							</a>
						</div>						
					</legend>
				<div class='col-md-offset-1 col-md-10 col-md-offset-1'>
					<?=$this->session->flashdata('msg');?>	
					<?php
					$hasUser=$brand_data;
					if($hasUser!=''){$process ='e'; }
					else if($hasUser==''){$process ='a'; }
					?>					
					<form class='form-horizontal formx' method="post" action="<?=base_url('admin/admin/Process/'.$module.'/'.$process)?>" enctype="multipart/form-data" data-toggle="validator" role="form">
						<div class="media">
						  <div class="media-body">
										  <div class="form-group">										  	
						                    <label for="status" class="col-lg-2 control-label">Brand Status</label>
						                    <div class="col-lg-10">
												<select  title='Account Status' class='form-control custom_textbox_xs ' required="" id='status' name='status' placeholder='Account status' >
													<option value=''>--Brand Status--</option>
													<?php
													if($hasUser!=""){													
														if($brand_data['status']=='active'){
															echo "
													          <option selected value='active'>Active</option>
													          <option value='inactive'>Inactive</option>															
															";								
														} else if($brand_data['status']=='inactive'){
															echo "
													          <option value='active'>Active</option>
													          <option selected value='inactive'>Inactive</option>															
															";													
														}
													} else {
															echo "
													          <option value='active'>Active</option>
													          <option value='inactive'>Inactive</option>															
															";																
														}													
													?>														
												</select>		
												<div class="help-block with-errors"></div>							                   
						                    </div>
						                  </div>						                  					  	
										<?php //check if have data 
										$brand_name_val='';
										if($hasUser!=""){ $brand_name_val=$brand_data['brand_name']; }
										?>	
										  <div class="form-group">
						                    <label for="cat_name" class="col-lg-2 control-label">Brand name</label>
						                    <div class="col-lg-10">
						                      <input type="text" title='Fill up your desire Username.'  class='form-control custom_textbox_xs text_spacer' required id='brand_name' name='brand_name' value="<?php echo $brand_name_val; ?>"  placeholder="Brand Name" />
						                      <div class="help-block with-errors"></div>
						                    </div>
						                  </div>														  							
																						        						                  	             			                  				                  
							<input type="hidden" id="hid_base_url" value="<?php echo base_url(); ?>" /> 
							<input type="hidden" id="title" name='title' value="<?php echo $title;?>" /> 
							<div class="col-md-12" style="margin-bottom:10px;">
								<div class='pull-right'>	
									<?php 
									$hid_id='';
									if($hasUser!='')
									$hid_id=$brand_data['brand_id'];
									?>											
									<input type='hidden' name='idx' value='<?php echo  $hid_id; ?>' />										
									<button type="submit" id="submit" name="submit" width='30' title='Save' class="btn btn-primary btn-sm" >
										<?php 
										if($hasUser!='') { echo "<i class='glyphicon glyphicon-edit'></i> Update"; }
										else if ($hasUser=='') {
											 echo "<i class='glyphicon glyphicon-floppy-save'></i> Save"; 
										}
										?>
									</button>	
								</div>
							</div>							                  						                  						                  												                  						                  																		
						  </div>
						</div>							
					</form>
				</div>	
			</div>
			
		</div>
	</div>	
</div>
