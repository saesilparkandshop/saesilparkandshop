<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<!DOCTYPE html>
	<html lang="en">
	  <head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <title><?=$title?></title>
	
	    <!-- CSS -->
		<link href = "<?=assets_url();?>css/bootstrap.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/custom.css" rel = "stylesheet">	
	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	                 
            <!-- start of styles auto php -->    
            <?php
            if(isset($styles) && is_array($styles)){
                foreach($styles as $style){
                    ?>
            <link rel="stylesheet" type="text/css" href="<?=$style?>">
            <?php
                }
            }
            ?><!--end code of styles -->	    
	    
	  </head>
<body>
	<div class="container" id="top">	
		<div class="row">
			<div class="col-xs-12">
				<div class="pull-right">
					
					<img src="<?=assets_url();?>img/layout/imus_seal1.png" class="img-responsive img_canvas">
				</div>
				<img src="<?=assets_url();?>img/layout/imus_seal1.png" class="img-responsive img_canvas">
			</div>
		</div>
		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="<?php echo base_url(); ?>"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
		    </div>
		
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
		      <ul class="nav navbar-nav navbar-right">
		        <li>
					<li><a href="#">About</a></li>	        	
		        </li>
		      </ul>
		    </div>
		  </div>
		</nav>	
 	
 	
