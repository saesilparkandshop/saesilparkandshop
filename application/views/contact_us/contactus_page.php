<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
		 <div class='col-md-12'>
					<legend>
						<?php echo $title;?>
					<!--	<div class='pull-right'>
							<a href='<?=base_url()?>admin/grid/<?php echo $module; ?>.aspx'>
								<span class='glyphicon glyphicon-chevron-left'>Back</span>
							</a>
						</div>	-->					
					</legend>

				<div class='col-md-offset-1 col-md-10 col-md-offset-1'>
					<?=$this->session->flashdata('msg');?>	<!-- eto po para sa mga message na lalabas ntin pag submit ng form -->
					<div class='row'>
						<form action="<?=base_url("contact_us/contact_us/saveContacts"); ?>" method='post'>	
					        <div class="well well-sm" style="margin-bottom:50px;">
					          <fieldset>
					            <legend class="text-center">Contact us</legend>
					    
					            <!-- Name input-->
					            <div class="form-group">
					              <label class="col-md-3 control-label" for="name">Name</label>
					              <div class="col-md-9">
					                <input id="name" name="name" type="text" placeholder="Your name" class="form-control" required>
					              </div>
					            </div>
					    
					            <!-- Email input-->
					            <div class="form-group">
					              <label class="col-md-3 control-label" for="email" >Your E-mail</label>
					              <div class="col-md-9">
					                <input id="email" name="email" type="text" placeholder="Your email" class="form-control" required>
					              </div>
					            </div>
					            
					            <!-- subject -->
					            <div class="form-group">
					              <label class="col-md-3 control-label" for="subject">Subject</label>
					              <div class="col-md-9">
					                <input id="subject" name="subject" type="text" placeholder="Subject" class="form-control" required>
					              </div>
					            </div>
					    
					    
					            <!-- Message body -->
					            <div class="form-group">
					              <label class="col-md-3 control-label" for="message">Your message</label>
					              <div class="col-md-9">
					                <textarea class="form-control" style="resize:none ;" id="message" name="message" placeholder="Please enter your message here..." rows="5" required></textarea>
					              </div>
					            </div>
					  
					    
					<!-- submit -->
					 		<div class="form-group pull-right">
					              <div class="col-md-9 ">
					              	<button class="btn btn-success">Submit</button>
					                </div>
							</div>	
							</fieldset>					
							</form>	
							</div>		
					</div>
				</div>
		</div><!--md12-->
		</div>	<!--mintheight-->
	</div><!--col-xs-12--->
</div><!--container-->