	
	<input type='hidden' id='base_url' value='<?php echo base_url(); ?>' />	
	<input type='hidden' id='assets_url' value='<?php echo assets_url(); ?>' />	
	
	<!-- loding js files -->
	<script src="<?=assets_url();?>js/jquery-1.11.3.min.js"></script>
	<script src="<?=assets_url();?>js/bootstrap.min.js"></script>
	<script src="<?=assets_url();?>js/bootstrap-datepicker.min.js"></script>
	<script src="<?=assets_url();?>js/validator.min.js"></script>
	<script src="<?=assets_url();?>js/jquery.dataTables.min.js"></script>
	<script src="<?=assets_url();?>js/dataTables.bootstrap.min.js"></script>	
	<script src="<?=assets_url();?>js/dataTables.responsive.js"></script>	
	<script src="<?=assets_url();?>js/select2.min.js"></script>	
	<script src="<?=assets_url();?>js/spin.min.js"></script>		
	<script src="<?=assets_url();?>js/jquery.backstretch.min.js"></script>	
	<script src="<?=assets_url();?>js/currency/curry-0.8.min.js"></script>
	<script src="<?=assets_url();?>js/bootstrap-submenu.min.js"></script>		
	<script src="<?=assets_url();?>js/global.js"></script>
            <?php
            if(isset($scripts) && is_array($scripts)){
                foreach($scripts as $script){
            ?>
            <script  src="<?=$script?>"> ></script>
            <?php
                }
            }
            ?><!--end code of scripts -->	
	<!-- end script -->		
</html>