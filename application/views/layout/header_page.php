<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<!DOCTYPE html>
	<html lang="en">
	  <head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <title><?=$title?></title>
	
	    <!-- CSS -->
		<link href = "<?=assets_url();?>css/bootstrap.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/dataTables.bootstrap.min.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/bootstrap-datepicker.min.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/bootstrap-submenu.min.css" rel = "stylesheet">
		<link href = "<?=assets_url();?>css/css/font-awesome.min.css" rel = "stylesheet">
		<link href = "<?=assets_url();?>css/custom.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/jquery.dataTables.min.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/jquery.dataTables_themeroller.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/responsive.bootstrap.min.css" rel = "stylesheet">
		<link href = "<?=assets_url();?>css/select2.min.css" rel = "stylesheet">
		<link href = "<?=assets_url();?>css/select2-bootstrap.css" rel = "stylesheet">		
						
		<!-- favicon -->
		<link rel="apple-touch-icon" sizes="57x57" href="<?=assets_url();?>img/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?=assets_url();?>img/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?=assets_url();?>img/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?=assets_url();?>img/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?=assets_url();?>img/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?=assets_url();?>img/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?=assets_url();?>img/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?=assets_url();?>img/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?=assets_url();?>img/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?=assets_url();?>img/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?=assets_url();?>img/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?=assets_url();?>img/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?=assets_url();?>img/favicon-16x16.png">
		<link rel="manifest" href="<?=assets_url();?>img/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?=assets_url();?>img/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">			
		
	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	                 
            <!-- start of styles auto php -->    
            <?php
            if(isset($styles) && is_array($styles)){
                foreach($styles as $style){
                    ?>
            <link rel="stylesheet" type="text/css" href="<?=$style?>">
            <?php
                }
            }
            ?><!--end code of styles -->	    
	    
	  </head>
<body>

 	
 	
