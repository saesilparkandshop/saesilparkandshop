<div class="container" style="margin-bottom:40px;">
	 <div class="row">
        <div class="col-md-12">
        <div class="panel panel-primary">
			  <div class="panel-heading">NEW ARRIVALS</div>
			  <div class="panel-body">
			 

			    	<div class="row">
				 <!-- latest book list -->
				 		<?php 
				 		if(isset($books) && is_array($books) && count($books)) {

				 			foreach ($books as $key => $value) {

				 			?>
				 									<!-- start book -->
							 <div class="item  col-xs-2   ">
							            <div class="thumbnail" style="height:300px;">
							            <a href="<?php echo base_url('books/view/' . $value['id']); ?>">
							                <img class="group   home_book_tm" height="190" style="min-height:190px;height:190px;" width="150" src="<?php echo assets_url() . 'book_covers/' . $value['image_name']; ?>" alt="" style="" />
							                </a>
							                <div class="caption book_caption">
							               
							                    <h4 class="group inner list-group-item-heading">
							                    	   <a href="<?php echo base_url('books/view/' . $value['id']); ?>" style="color:#333333;">
							                         <?php echo ellipsize($value['title'],40,1); ?></h4>
							                     		</a>
							                    <p class="group inner list-group-item-text">
													 <?php echo ellipsize($value['author'],33,1); ?>

				             				   </div>
				            				</div>
				        				</div> 	<!-- end book -->						
				        	<!-- start book -->

				 			<?php

				 			}
				 		}
				 		?>
 
							 
			 </div> <!--.row -->
                  <div class="row">
                      <div class="pull-right col-xs-1" title="Click to see more books"><a href="<?php echo base_url('books/');?>">See More</a></div>
                  </div> <!--.row -->
  		 </div> <!--.panel-body -->
	</div> <!--.panel-heading -->
				
        </div>
     </div>
     <div class="row">
     	<div class="col-md-6" >
     		<div class="panel panel-primary">
			  <div class="panel-heading">ANNOUNCEMENTS</div>
			  <div class="panel-body" style="min-height:200px ;">
					<div class="media">
					  <div class="media-body">
					    <h4 class="media-heading text-capitalize"><a href="<?php echo base_url('announcement/announcement/view/' . $announcement['id']); ?>"><?php echo $announcement['subj']; ?></a></h4>
					    <p class="text-justify text-capitalize"><?php echo $announcement['message']; ?>.</p>
					  	<small>By: <cite title="Source Title"><i><u><?php echo $announcement['announce_by']; ?></u> - <?php echo date("m/d/Y",strtotime($announcement['date_created'])); ?> </i></cite></small>
					  </div>
                	<h5><a href="<?php echo base_url('announcement/announcement'); ?>">See all Announcements</a></h5>  					  
					</div>			  
			  </div>
		</div>
     	</div>
     	<div class="col-md-6">
     	<div class="panel panel-primary">
			  <div class="panel-heading">GIVEAWAYS/PROMOS</div>
			  <div class="panel-body"  style="min-height:200px ;">
					<div class="media">
					  <div class="media-body">
					    <h4 class="media-heading "><a href="<?php echo base_url('promo/promos/view/'.$promo['id']); ?>"><?php echo $promo['subj']; ?></a></h4>
					    <p class="text-justify text-capitalize"><?php echo $promo['message']; ?>.</p>
					  	<small>By: <cite title="Source Title"><i><u><?php echo $promo['announce_by']; ?></u> - <?php echo date("m/d/Y",strtotime($promo['date_created'])); ?> </i></cite></small>
					  </div>
                	<h5><a href="<?php echo base_url('promo/promos'); ?>">See all Promos</a></h5>  					  
					</div>	  
			  </div>
		</div>

     	</div>
     </div>

</div>