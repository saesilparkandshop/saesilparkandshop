<div class='container'>
			<div class="col-md-12">
					<div class='min_height'>
						<?php 
							$issetup=$this->session->userdata('IsSetup');
							if(empty($issetup)){	
						?>
						<div class='col-md-12'>
									<div class="alert alert-warning" role="alert">
										Can't Access the page directly.
									</div>						
						</div>	
						<?php } else { ?>
						<div class='col-md-12'>
							<legend><?php echo $title;?></legend>
						<div class='col-md-offset-1 col-md-10 col-md-offset-1'>
							<?=$this->session->flashdata('msg');?>						
							<form class='form-horizontal formx' method="post" action="<?=base_url('setup/setup/ProcessFirstUser')?>" enctype="multipart/form-data" data-toggle="validator" role="form">
								<div class="media">
								  <div class="media-body">
								  				<legend>Login Credentials</legend>
												<?php //check if have data 
												$UserFName_val='';
												//if($hasUser!=""){ $UserFName_val=$user_data['UserName']; }
												?>	
												  <div class="form-group">
								                    <label for="inputEmail" class="col-lg-2 control-label">Username</label>
								                    <div class="col-lg-10">
								                      <input type="text" title='Fill up your desire Username.'  class='form-control custom_textbox_xs text_spacer' required id='UserName' name='UserName' value="<?php echo $UserFName_val; ?>"  placeholder="Username" />
								                      <div class="help-block with-errors"></div>
								                    </div>
								                  </div>														  	
												<?php //check if have data 
												$Password_val='';
												//if($hasUser!=""){ $Password_val=$user_data['Password']; }
												?>	
												  <div class="form-group">
								                    <label for="inputEmail" class="col-lg-2 control-label">Password</label>
								                    <div class="col-lg-10">
														<input type="password" pattern="^.*(?=.{7,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$" id="Password" name="Password" value="<?php echo $Password_val; ?>"  placeholder="Password" data-toggle="validator" data-minlength="7" title='Fill up your desire Password must 7-20 characters.' class="form-control custom_textbox_xs" maxlength="20" required  />	                 
								                    	<div class="help-block with-errors">Password must be 7-20 characters including one uppercase letter and alphanumeric characters.</div>		
								                    </div>
								                  </div>
												<?php //check if have data 
		
												//if($hasUser==""){  
												?>							                  
												  <div class="form-group">
								                    <label for="inputEmail" class="col-lg-2 control-label">Confirm Password</label>
								                    <div class="col-lg-10">
														<input type="password"  data-match="#Password" data-match-error="Password don't match" title='Retype Password' class='form-control custom_textbox_xs ' maxlength="20" required="" id='Confirm_Password' name='Confirm_Password'  placeholder="Confirm Password">	
														<div class="help-block with-errors"></div>							                   
								                    </div>
								                  </div>	
								               <!-- <?php// } ?>  		 -->			                  	
										<legend>Personal Information</legend>		
												<?php
												$Firstname_val='';
												//if($hasUser!=""){ $Firstname_val=$user_data['FirstName']; }
												$Middlename_val='';
												//if($hasUser!=""){ $Middlename_val=$user_data['MiddleName']; }										
												$Lastname_val='';
												//if($hasUser!=""){ $Lastname_val=$user_data['LastName']; }
												?>	
												<small><b>Name</b></small>
												  <div class="form-group">
								                    <label for="inputEmail" class="col-md-1 control-label">First</label>
								                    <div class="col-md-3">
														<input type="text" class='form-control custom_textbox_xs ' required='' id='FirstName' name='FirstName' value="<?php echo $Firstname_val; ?>" placeholder="Firstname">	
								                    </div>
								                    <label for="inputEmail" class="col-md-1 control-label">Middle</label>
								                    <div class="col-md-3">
														<input type="text" class=' form-control custom_textbox_xs ' required='' id='MiddleName' name='MiddleName' value="<?php echo $Middlename_val; ?>" placeholder="Middlename">	
								                    </div>						                    
								                    <label for="inputEmail" class="col-md-1 control-label">Last</label>
								                    <div class="col-md-3">
														<input type="text" class='form-control custom_textbox_xs ' required='' id='LastName' name='LastName' value="<?php echo $Lastname_val; ?>" placeholder="Lastname">	
								                    </div>						                    
								                  </div>
								                  <?php 
								                  $email_val='';
												  //if($hasUser!=""){ $email_val=$user_data['email']; }
								                  ?>	
												  <div class="form-group">
								                    <label for="inputEmail" class="col-lg-2 control-label">Email</label>
								                    <div class="col-lg-10">
								                      <input type="email" title='Fill up your desire Email.'  class='form-control custom_textbox_xs text_spacer' required id='email' name='email' value="<?php echo $email_val; ?>"  placeholder="Email" />
								                      <div class="help-block with-errors"></div>
								                    </div>
								                  </div>
					                  			  <?php 
								                  $contact_val='';
												  //if($hasUser!=""){ $contact_val=$user_data['email']; }
								                  ?>	
												  <div class="form-group">
								                    <label for="inputEmail" class="col-lg-2 control-label">CONTACT NO.</label>
								                    <div class="col-lg-10">
								                      <input type="number" title='Fill up your desire Contact #.'  class='form-control custom_textbox_xs text_spacer' required id='contact_no' name='contact_no' value="<?php echo $contact_val; ?>"  placeholder="Contact #" />
								                      <div class="help-block with-errors"></div>
								                    </div>
								                  </div>									                  								                  						                  	             			                  				                  
									<input type="hidden" id="hid_base_url" value="<?php echo base_url(); ?>" /> 
									<input type="hidden" id="title" name='title' value="<?php echo $title;?>" /> 
									<div class="col-md-12">
										<div class='pull-right'>											
											<button type="submit" id="submit" name="submit" width='30' title='Save' class="btn btn-primary btn-sm" >
												<?php 
													 echo "<i class='glyphicon glyphicon-floppy-save'></i> Save"; 
												?>
											</button>	
										</div>
									</div>							                  						                  						                  												                  						                  																		
								  </div>
								</div>							
							</form>
						</div>	
						</div>
						<?php } ?>
					</div>									
			</div>
</div>
