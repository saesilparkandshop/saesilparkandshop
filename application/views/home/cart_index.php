<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class="row">
				
			 	<div class="col-lg-12">
			 			
				<legend class="noselect">Shopping Cart</legend>
				<div>
					<h3>Batch ID: <?php echo $batch_id; ?></h3>
				</div>
		 <?php if($this->cart->total_items() > 0) : ?>
		 <table class="table table-hover table-condensed">
		 <thead>
		 	<tr>
		 		<th style="width:20%">Product</th>
		 		<th style="width:10%">Price</th>
		 		<th style="width:8%">Quantity</th>
		 		<th style="width:15%">Options</th>
		 		<th style="width:10%">Subtotal</th>
				<th style="width:10%"></th>
		 	</tr>
		 </thead>
		<tbody>
			<?php echo $table_data; ?>
		</tbody>
			</table>
			 <?php else: ?>
			 <p class="alert alert-warning">Your shopping cart is empty</p>
			<?php endif; ?>
			 </div>		

			</div>			 			
		 <?php if($this->cart->total_items() > 0) : ?>
		<div class="row">
			<div class="col-md-3 pull-left">
				<a href="<?php echo base_url(); ?>cart/clear" class="delete btn btn-warning form-control">Clear</a>
			</div>
			<div class="col-md-3 pull-right">
			<?php if($this->cart->total_items() > 0) : ?>
				<a id="link_checkout" href="<?php echo base_url('checkout'); ?>" class="btn btn-primary form-control">Proceed to Checkout</a>	
			<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>
		</div>
	</div>	
</div>