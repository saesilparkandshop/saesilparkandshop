<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-md-12'>
			<div class='col-md-offset-1 col-md-10 col-md-offset-1'>	
			<div class="panel panel-primary">
			  <div class="panel-heading panel-heading-custom">
			  	<div class="row">
			  	<div class="col-lg-8">
			  		<h3 class="panel-title" style="color:#000;">Login</h3>
			  	</div>
			    <div class="col-lg-4">
				    <div class="pull-right">
				    	<a href="<?=base_url("");?>" class="btn-link"><i class="glyphicon glyphicon-arrow-left"></i> Back</a>
				    </div>			    	
			    </div>
			   </div>
			  </div>
			  <div class="panel-body" style="padding:0px; ">
				<div class="list-group" style="margin-bottom:0px;">
					<form class=' formx' method="post" action="<?=base_url('home/index/login');?>" enctype="multipart/form-data" data-toggle="validator" role="form">
						<?php echo $login_msg;?>	
						<div class="form-group">
							<input type="text" name="username" id="username" required="" tabindex="1" class="form-control col-lg-5" placeholder="Username" value="">
						</div>
						<div class="form-group">
							<input type="password" name="password" id="password" required='' tabindex="2" class="form-control " placeholder="Password">
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-sm-6 col-sm-offset-3">
									<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-pink submit" value="Log In">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center">
										<a href="<?=base_url("home/index/forgot");?>" tabindex="5" class="forgot-password">Forgot Password?</a>
									</div>
								</div>
							</div>
						</div>						
					</form>					
				</div>
			  </div>
			</div>					
											
				</div>	
			</div>
			
		</div>
	</div>	
</div>
