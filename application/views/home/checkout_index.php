<div class='container'>
	<div class="col-xs-12">
		<div class='min_height' style="overflow:visible;">
			 <div class="row">
					
			 	<div class="col-lg-12">

				<legend class="noselect">Checkout</legend>

					  <form method="POST" action="<?=base_url('checkout/order')?>" onsubmit="return confirm('Are you sure you want to proceed to payment?')" >
					  
					  <div class="row">
					   <div class="col-md-12">
					   <h4>Fill Up Form</h4>
							<div class="form-group">
								<label class="form-label col-md-2 text-right">
									Payment Scheme
								</label>
								<div class='col-md-10'>
								<label class="radio-inline">
								  <input type="radio" name='payscheme' checked id="inlineRadio1" value="full"> Full
								</label>
								<label class="radio-inline">
								  <input type="radio" name='payscheme'  id="inlineRadio2" value="partial"> Installment
								</label>
								<label class="radio-inline">
								  <input type="radio" name='payscheme'  id="inlineRadio3" value="payday"> Pay on Payday
								</label>									
								</div>
							</div>						  	
					   </div>
					  	<div class="col-md-6">
					  		 <label>Name:</label>
					  		 <input type="text" name="name"   class="form-control" value="<?php echo  $user['full_name']; ?>" required />


					  		 <label>Contact Number:</label>
					  		 <input type="text" name="contact_number"  class="form-control" value="<?php echo $user['contact_no'];?>" />


					  		 <label>Address:</label>
					  		 <input type="text" name="address" class="form-control" value="<?php echo $user['address'];?>" required />
					  	</div>
					  	<div class="col-md-6">
					  
					 
					<h4>Order</h4>
						<table border="0"  cellpadding="9" cellspacing="2" style="width:100%; " border="0" class="table">
							<thead>
				            <tr>
				                <td>Item Name</td>
				                <td>Price</td>
				                <td>Qty</td>

				                <td class="text-right">Subtotal</td>
				            </tr>
				              </thead>
				              <tbody>
	 <?php $i = 1; ?>
		 <?php  
		  $gran_total = 0;
		  foreach ($this->cart->contents() as $items): 
			  		$is_discount = false;
				    $discount_percent = 0;
			  	foreach ($discount_conditions as $key => $value) {
								 	if($value['promo_type'] == "item"){ ///TODO : conditions for categories and sub category. NEED dropdown for category/sub in promo add/edit page

								 		if($value['value'] == $items['options']['item_id']){
								 			$is_discount = true;
								 			$discount_percent = $value['discount_percent'];
								 		}


								 	}
								 }
								 if($is_discount){
								 	$price = $items['price'] - ($items['price'] * ($discount_percent * 0.01));
								 }else{
								 	$price = $items['price'];
								 }


		  	?>


				<tr>
			 	 <td>

				<?php echo form_hidden('rowid[]', $items['rowid']); ?>
				<?php echo form_hidden('id[]', $items['id']); ?>
					<?php echo $items['name']; ?>
			 <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>

				<p>
					<?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
						<?php if($option_name != "item_id") : ?>
							 <?php echo $option_name; ?>: <?php echo $option_value; ?><br />
						<?php endif; ?>
					<?php endforeach; ?>
				</p>

			<?php endif; ?>	

				  </td>
				 <?php if($is_discount) : ?>
				 	<td  >Php <?php echo $this->cart->format_number($price); ?>
						<s><?php echo $this->cart->format_number($items['price']); ?></s>
				 	</td>
				 <?php else: ?>
				 	<td  >Php <?php echo $this->cart->format_number($items['price']); ?></td>
				 <?php endif; ?>
				  <td class="text-center"><?php echo $items['qty'] ?></td>
				 

			 
				  <td class="text-right">Php <?php echo $this->cart->format_number($price * $items['qty']); ?></td>
				  
				</tr>

			<?php $i++;          $gran_total += $price * $items['qty']  ;?>

			<?php endforeach; ?>

  				  <tr style="border-top:solid 1px #000;">
				                <td><b>Total: </b> </td>
				                <td></td>

				                <td></td>
				                <td class="text-right"  ><b>Php <?=$this->cart->format_number($gran_total) ?></b></td>
				            </tr>
</tbody>
				        </table>

					        
					</div> <!--.col-md-6 -->
					 </div><!--.row -->
					 <div class="row">
					 <div class="text-center">
					 <br/>
					 <br/>
					 	<a class="btn btn-default" href="<?php echo base_url('cart'); ?>">Back</a> <button class="btn btn-primary" type="submit">Proceed to Payment</button>
					 	</div>
					 </div>
					 </form>
				 
					
			 	</div>
			 </div>
				
		</div>
	</div>
</div>