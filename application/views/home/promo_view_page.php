<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class="panel panel-primary">
				  <div class="panel-heading panel-heading-custom">
				  	<div class='row'>
				  	<div class='col-xs-8'>
				  		<h3 class="panel-title" style='color:#000;'>Promo</h3>
				  	</div>
				    <div class='col-xs-4'>
					    <div class="pull-right">
					    	<a href='<?=base_url('home')?>' class='btn-link'><i class="glyphicon glyphicon-arrow-left"></i> Back</a>
					    </div>			    	
				    </div>
				   </div>
				  </div>
				  
<div class="panel-body" style="padding:0px; ">
	<div class="list-group" style="margin-bottom:0px;">
		<div class='col-md-12'>
			<div class="row">
				<?php $hasUser=$promo_data;?>
				
				<div class='col-md-offset-1 col-md-10 col-md-offset-1'>			
						<div class="media" style="margin:20px;">
						  <div class="media-body">
							  		<?php
							  		$img_path='td.PNG'; 
							  		if($hasUser!=''){
							  		if(!empty($promo_data['promo_filename']))
							  		$img_path = $promo_data['promo_filename'];		
							  		}
							  		?>
									<div class="form-group">
										<center>
										  <input type="hidden" value="<?php echo  $img_path; ?>" name='img_name' />	
										  <a id='upload-item' class="img-thumbnail" title='must  750x442' style=" height:442px; width:750px;  cursor:pointer;">
										      <img style="height:100%; width:100%;" id="img-prev" class="img-responsive" src="<?=assets_url()."img/promo_banner/".$img_path;?>" alt="...">
										  </a>
										  <input type="file" id='btn_upload' class="btn-upload" accept="image/*"  name='upload' onchange="showImage(this)" />
										</center>
									</div>								  					                  					  	
												                  													  							
										<?php //check if have data 
										$promo_name_val='';
										if($hasUser!=""){ $promo_name_val=$promo_data['promo_name']; }
										?>	
										
										  <div class="form-group">
						                    <label for="promo_name" class="col-lg-2 control-label">Promo name</label>
						                    <div class="col-lg-10">
						                      <?php echo $promo_name_val; ?>
						                      <div class="help-block with-errors"></div>
						                    </div>
						                  </div>	
						                  
						                  <?php //check if have data 
											$promo_type_val='';
											if($hasUser!=""){ $promo_type_val=$promo_data['promo_type']; }
										  ?>	
						                  
										  <div class="form-group">										  	
						                    <label for="promo_type" class="col-lg-2 control-label">Promo Type</label>
						                    <div class="col-lg-10">
						                      <?php echo $promo_type_val; ?>
						                      <div class="help-block with-errors"></div>
						                    </div>
						                  </div>	
						           
									    <?php 
											$from='';
											$to='';
											if($hasUser!=""){
											$from=$promo_data['promo_from'];
											$to=$promo_data['promo_to'];													
											}
										?>             
										<div class="form-group">
											<label class="col-lg-2 control-label">Date</label>
												<div class='col-lg-4'>
											    	<?php echo date('m/d/Y', strtotime($from)); ?>
											    </div>
											    <div  class='col-lg-1'>
											  		  <label >to</label>	
											    </div>												    
											    <div class='col-lg-4'>
										    		<?php echo date('m/d/Y', strtotime($to)); ?>																								    
												</div>										
										</div>	
										
						
							
										<?php //check if have data 
											$promo_desc_val='';
										if($hasUser!=""){ $promo_desc_val=$promo_data['promo_desc']; }
										?>	
										  <div class="form-group">										  	
						                    <label for="promo_desc" class="col-lg-10 control-label">Promo Info</label>
						                    <div class="col-lg-12">
						                      <?php echo $promo_desc_val; ?>
						                      <div class="help-block with-errors"></div>
						                    </div>
						                  </div>																				                  						                  																						        						                  	             			                  				                  					                  						                  						                  												                  						                  																		
						  </div>
					</div>							
				</div>	
			</div>
		</div>
	</div>	
</div>	

		  </div>
		</div>											
	</div>
</div>	



