<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-md-12'>
				<div class='row'>
					<div class='col-lg-8'>
						<div class="flexslider">
						  <ul class="slides">
						  	<?php 
						  	foreach($promo_data as $prom_data){
						  	?>
						    <li>
						      <a href="<?=base_url('promo/'.$this->encryption->encode($prom_data['promo_id']));?>">
						      	<img src="<?=assets_url()."img/promo_banner/".$prom_data['promo_filename'];?>" />
						      </a>	
						      
						      <p class="flex-caption" style="width:100%;">
						      	<a href='<?=base_url('promo/'.$this->encryption->encode($prom_data['promo_id']));?>' style="color:#fff !important;" class="btn btn-link"><?php echo ucfirst(ucwords(($prom_data['promo_name']))); ?></a>					      	
						      </p>
						    </li>
						    <?php } ?>
						  </ul>
						</div>						
					</div>	
					<div class='col-lg-4'>
						<div class="panel panel-primary">
						  <div class="panel-heading panel-heading-custom">
						    <h3 class="panel-title">Announcement</h3>
						  </div>
						  <div class="panel-body" style="padding:0px; ">
							<div class="list-group" style="margin-bottom:0px;">
							  	<?php 
							  	foreach($announcement_data as $announce_data){
							  	?>							
							  <a href="<?=base_url('announcement/'.$this->encryption->encode($announce_data['announcement_id']));?>" class="list-group-item">
							    <h4 class="list-group-item-heading">
							    	<?php echo $announce_data['subj']; ?>
							    </h4>
							    <p class="list-group-item-text">
							    	<?php echo character_limiter($announce_data['message'],70); ?>
							    </p>
							    <small>By: <cite title="Source Title"><i><u><?php echo $announce_data['announce_by']; ?></u> - <?php echo $announce_data['date_announce']; ?> </i></cite></small>
							  </a>
							  	<?php 
								}
							  	?>	
							  	<div class="list-group-item" style='padding:20px;'>
								  	<div class='list-group-item-heading' style='margin:0px;'>
								  		<a href='<?=base_url('announcement/all');?>'>
								  			See More....
								  		</a>
								  	</div>								  		
							  	</div>				  
							</div>	
						  </div>
						</div>					
					</div>				
				</div>
				<div class='row'>
					<div class='col-lg-12'>
						<ul class="nav nav-tabs">
						  <li class="active pink-tabs">						  	
						  	<a href="#new_items" data-toggle="tab">New Items</a>
						  </li>
						  <li>
						  	<a href="#best_sell" data-toggle="tab">Best Sellers</a>						  	
						  </li>
						  <!--<li>
						  	<a href="#on_hand" data-toggle="tab">On Hand</a>
						  </li>-->
						</ul>
						<div id="myTabContent" class="tab-content">
						  <div class="tab-pane fade active in" id="new_items">
							<div class="row">						  	
						  	<?php 
						  	foreach($newitems_data as $new_data){
						  	?>							  	
							<?php 
							$img_name='';
							$new_data['thumbs']!='' ?  $img_name=$new_data['thumbs'] : $img_name='td.png';
 
 


							?>
							  <div class="col-md-3">
							  	<a href="<?=base_url('items_view/'.$this->encryption->encode($new_data['item_id']));?>" style='margin-bottom:10px;' role="button">
							    <div class="thumbnail" style="border:0px;">
							      <img src="<?=assets_url()."img/item_covers/".$img_name?>" alt="..." class='img-minheight' >
							      <div class="caption" style="height:150px;">
							        <h4 class='btn-link'style="word-break: break-all; white-space:normal;">
							        	<center>
							        		<b class="title_head"><?php echo wordwrap($new_data['item_name']); ?></b>
							        	</center>
							        </h4>
							        <?php if( $new_data['is_discount'] && $new_data['ps_qty'] == 1 ) : ?>
									<p class='price'>Php<?php echo number_format($new_data['price'] - ($new_data['price'] * ($new_data['discount_percent']* 0.01)),2); ?> <s>Php<?php echo number_format($new_data['price'],2); ?></s>
									<span class="label label-danger label-as-badge"><?=floor($new_data['discount_percent'])?>% Off!</span>
									</p>  	
							        <?php elseif( $new_data['is_discount'] && $new_data['ps_qty'] > 1 ) : ?>
							        		<p class='price'>Php<?php echo number_format($new_data['price'],2); ?><span class="label label-danger label-as-badge"><?=$new_data['scheme'];?></span></p>  
							        <?php else: ?>	
							        	 	<p class='price'>Php<?php echo number_format($new_data['price'],2); ?></p>  	
							        <?php endif; ?>


							      
							      </div>
							    </div>
							   </a>
							  </div>

							<?php
								}
							?>	
							</div>														
						  </div>
						  <div class="tab-pane fade" id="best_sell">
							<div class="row">						  	
						  	<?php 
						  	foreach($bestitems_data as $new_data){
							$new_data['thumbs']!='' ?  $img_name=$new_data['thumbs'] : $img_name='td.png';

								//discount check
							$is_discount = false;
							$discount_percent = 0;
							foreach ($discount_conditions as $key => $value) {
								 	if($value['promo_type'] == "item"){ ///TODO : conditions for categories and sub category. NEED dropdown for category/sub in promo add/edit page

								 		if($value['value'] == $new_data['item_id']){
								 			$is_discount = true;
								 			$discount_percent = $value['discount_percent'];
								 			break;
								 		}


								 	}else if($value['promo_type'] == "brand"){  
								 		if($value['value'] == $new_data['brand_id']){
								 			$is_discount = true;
								 			$discount_percent = $value['discount_percent'];
								 			break;
								 		}


								 	}


								}


						  		
						  	?>							  	
							  <div class="col-md-3">
							  	<a href="<?=base_url('items_view/'.$this->encryption->encode($new_data['item_id']));?>" style='margin-bottom:10px ;' role="button">
							    <div class="thumbnail" style="border:0px;">
							      <img src="<?=assets_url()."img/item_covers/".$img_name?>" alt="..." class='img-minheight'>
							      <div class="caption" style="height:150px;">
							        <h4 class='btn-link'style="word-break: break-all; white-space:normal;">
							        	<center>
							        		<b class="title_head"><?php echo wordwrap($new_data['item_name']); ?></b>
							        	</center>
							        </h4>
							        <?php if($is_discount) : ?>
									<p class='price'>Php<?php echo number_format($new_data['price'] - ($new_data['price'] * ($discount_percent * 0.01)),2); ?> <s>Php<?php echo number_format($new_data['price'],2); ?></s>
									<span class="label label-danger label-as-badge"><?=floor($discount_percent)?>% Off!</span>
									</p>  	
							        <?php else: ?>
							        <p class='price'>Php<?php echo number_format($new_data['price'],2); ?></p>  	
							        <?php endif; ?>
							      </div>
							    </div>
							   </a>
							  </div>

							<?php
								}
							?>	
							</div>	
						  </div>
						<!--  <div class="tab-pane fade" id="on_hand">
							<div class="row">						  	
						  	<?php 
						  	foreach($onhanditems_data as $onhand_data){
						  	?>							  	

							  <div class="col-md-3">
							  	<a href="<?=base_url('items_view/'.$this->encryption->encode($onhand_data['item_id']));?>" style='margin-bottom:10px ;' class="btn btn-pink" role="button">
							    <div class="thumbnail" style="margin-bottom: 0px;">
							      <img src="<?=assets_url()."img/item_covers/".$img_name?>" alt="..." class='img-minheight'>
							      <div class="caption" style="height:150px;">
							        <h4 class='btn-link'style="word-break: break-all; height:80px;  white-space:normal;">
							        	<center>
							        		<b><?php echo wordwrap($new_data['item_name']); ?></b>
							        	</center>
							        </h4>
							        <p class='price'><?php echo number_format($onhand_data['price'],2); ?></p>
							      </div>
							    </div>
							   </a>
							  </div>


							<?php
								}
							?>	
							</div>	
						  </div>-->
						</div>					
					</div>
				</div>
				
				<!--end-->
			</div>			
		</div>
	</div>	
</div>