<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-md-12'>
				<legend><?php echo $title;?></legend>
				<form class="form-horizontal formx" id='formx' action="<?=base_url("home/index/resend_email");?>" method='post'> 
					<?=$this->session->flashdata('msg');?>	
					<div class='form-group'>
	                    <label for="email" class="col-lg-2 control-label">Email</label>
	                    <div class="col-lg-10">
	                      <input type="email" title='Fill up your desire Email.'  class='form-control custom_textbox_xs text_spacer required' required id='email' name='email' value=""  placeholder="Email" />
	                      <div class="help-block with-errors"></div>
	                    </div>
					</div>
					<div class='form-group'>
						<div class='col-md-12'>
							<div class='pull-right'>						
								<button type="submit" id="resend" name="resend" width='30' title='Resend' class="btn btn-pink btn-sm submit" >
									Resend
								</button>										
							</div>							
						</div>							
					</div>	
				</form>
			</div>
			
		</div>
	</div>	
</div>
