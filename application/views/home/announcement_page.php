<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class="panel panel-primary">
			  <div class="panel-heading panel-heading-custom">
			  	<div class='row'>
			  	<div class='col-xs-8'>
			  		<h3 class="panel-title" style='color:#000;'>Announcement</h3>
			  	</div>
			    <div class='col-xs-4'>
				    <div class="pull-right">
				    	<a href='<?=base_url('home')?>' class='btn-link'><i class="glyphicon glyphicon-arrow-left"></i> Back</a>
				    </div>			    	
			    </div>
			   </div>
			  </div>
			  <div class="panel-body" style="padding:0px; ">
				<div class="list-group" style="margin-bottom:0px;">
				  	<?php if($filter=='all'){ foreach($announce_all as $announce_data) {?>							
				  <a href="<?=base_url('announcement/'.$this->encryption->encode($announce_data['announcement_id']));?>" class="list-group-item">
				    <h4 class="list-group-item-heading">
				    	<?php echo $announce_data['subj']; ?>
				    </h4>
				    <p class="list-group-item-text">
				    	<?php echo $announce_data['message']; ?>
				    </p>
				    <small>By: <cite title="Source Title"><i><u><?php echo $announce_data['announce_by']; ?></u> - <?php echo $announce_data['date_announce']; ?> </i></cite></small>
				  </a>
				  	<?php }//for foreach?>
				  	<div class="list-group-item" style='padding:5px;'>
				  		<?php echo $pagination; ?>					  		
				  	</div>				  		
				  	<?php } else{ ?>
					  <a href="<?=base_url('announcement/'.$this->encryption->encode($announce_byid['announcement_id']));?>" class="list-group-item">
					    <h4 class="list-group-item-heading">
					    	<?php echo $announce_byid['subj']; ?>
					    </h4>
					    <p class="list-group-item-text">
					    	<?php echo $announce_byid['message']; ?>
					    </p>
					    <small>By: <cite title="Source Title"><i><u><?php echo $announce_byid['announce_by']; ?></u> - <?php echo $announce_byid['date_announce']; ?> </i></cite></small>
					  </a>				  	
				  	<?php } ?>
				</div>	
			  </div>
			</div>											
		</div>
	</div>	
</div>