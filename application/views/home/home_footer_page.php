<!-- Modal -->
<div class="modal fade" id="login-modal" data-backdrop='static' tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header" style="padding: 15px 15px 0px 0px;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<ul class="nav nav-tabs" style="border:0px;">
			  <li id='login'><a href="#loginx" data-toggle="tab">Login</a></li>
			  <li id='register'><a href="#registerx" data-toggle="tab">Register</a></li>
			</ul>
     </div>
      <div class="modal-body"> 
			<div id="myTabContent" class="tab-content">
			  <div class="tab-pane fade active" id="loginx">
				<div class='row'>
					<form id="login-form" class="formx" action="<?=base_url("home/index/login"); ?>" method="post" enctype="multipart/form-data" role="form">
						<?=$this->session->flashdata('login_msg');?>	
						<div class="form-group">
							<input type="text" name="username" id="username" required="" tabindex="1" class="form-control " placeholder="Username" value="">
						</div>
						<div class="form-group">
							<input type="password" name="password" id="password" required='' tabindex="2" class="form-control " placeholder="Password">
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-sm-6 col-sm-offset-3">
									<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-pink submit" value="Log In">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center">
										<a href="<?=base_url();?>" tabindex="5" class="forgot-password">Forgot Password?</a>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" name="redirect_to" id="redirect_to" value=""/>
					</form>												
				</div>
			  </div>
			  <div class="tab-pane fade" id="registerx">
				<div class='row'>
							<?=$this->session->flashdata('msg');?>	
							<?php
							$user_data='';
							$hasUser=$user_data;
							if($hasUser!=''){$process ='e'; }
							else if($hasUser==''){$process ='a'; }
							?>					
							<form class='form-horizontal formx' id='register-form' method="post" action="<?=base_url('home/index/Process')?>" enctype="multipart/form-data" data-toggle="validator" role="form">
								<div class="media">
								  <div class="media-body">	
								  	<div class="msgx">
								  		
								  	</div>					                  					  	
								  				<legend>Login Credentials</legend>
												<?php //check if have data 
												$UserName_val='';
												if($hasUser!=""){ $UserName_val=$user_data['username']; }
												?>	
												  <div class="form-group">
								                    <label for="inputEmail" class="col-lg-2 control-label">Username</label>
								                    <div class="col-lg-10">
								                      <input type="text" title='Fill up your desire Username.'  class='form-control custom_textbox_xs text_spacer required' required id='UserName' name='UserName' value="<?php echo $UserName_val; ?>"  placeholder="Username" />
								                      <div class="help-block with-errors"></div>
								                    </div>
								                  </div>														  	
												<?php //check if have data 
												$Password_val='';
												if($hasUser!=""){ $Password_val=$user_data['password']; }
												?>	
												  <div class="form-group">
								                    <label for="Password" class="col-lg-2 control-label">Password</label>
								                    <div class="col-lg-10">
														<input type="password" pattern="^.*(?=.{7,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$" id="Password" name="Password" value="<?php echo $Password_val; ?>"  placeholder="Password" data-toggle="validator" data-minlength="7" title='Fill up your desire Password must 7-20 characters.' class="form-control custom_textbox_xs required" maxlength="20" required  />	                 
								                    	<div class="help-block with-errors">Password must be 7-20 characters including one uppercase letter and alphanumeric characters.</div>		
								                    </div>
								                  </div>
												<?php //check if have data 
		
												if($hasUser==""){  
												?>							                  
												  <div class="form-group">
								                    <label for="Confirm_Password" class="col-lg-2 control-label">Confirm Password</label>
								                    <div class="col-lg-10">
														<input type="password"  data-match="#Password" data-match-error="Password don't match" title='Retype Password' class='form-control custom_textbox_xs required' maxlength="20" required="" id='Confirm_Password' name='Confirm_Password'  placeholder="Confirm Password">	
														<div class="help-block with-errors"></div>							                   
								                    </div>
								                  </div>	
								               <?php } ?> 
								                								               	 			                  	
										<legend>Personal Information</legend>		
												<?php
												$Firstname_val='';
												if($hasUser!=""){ $Firstname_val=$user_data['first_name']; }
												$Middlename_val='';
												if($hasUser!=""){ $Middlename_val=$user_data['middle_name']; }										
												$Lastname_val='';
												if($hasUser!=""){ $Lastname_val=$user_data['last_name']; }
												?>	
													<small><b>Name</b></small>
													  <div class="form-group">
									                    <label for="inputEmail" class="col-md-1 control-label">First</label>
									                    <div class="col-md-3">
															<input type="text" class='form-control custom_textbox_xs required' required='' id='FirstName' name='FirstName' value="<?php echo $Firstname_val; ?>" placeholder="Firstname">	
									                    </div>
									                    <label for="inputEmail" class="col-md-1 control-label">Middle</label>
									                    <div class="col-md-3">
															<input type="text" class=' form-control custom_textbox_xs required' required='' id='MiddleName' name='MiddleName' value="<?php echo $Middlename_val; ?>" placeholder="Middlename">	
									                    </div>						                    
									                    <label for="inputEmail" class="col-md-1 control-label">Last</label>
									                    <div class="col-md-3">
															<input type="text" class='form-control custom_textbox_xs required' required='' id='LastName' name='LastName' value="<?php echo $Lastname_val; ?>" placeholder="Lastname">	
									                    </div>						                    
									                  </div>
										    			<div class="form-group">										  	
									                    <label for="status" class="col-lg-2 control-label">Gender</label>
									                    <div class="col-lg-10">
															<select  title='Account Status' class='form-control custom_textbox_xs required' required="" id='gender' name='gender' placeholder='Gender' >
																<option value=''>--Gender--</option>
																<option value='male'>Male</option>
																<option value='female'>Female</option>	
															</select>		
															<div class="help-block with-errors"></div>							                   
									                    </div>
									                  </div>             
									                  <div class='form-group'>
														<?php //check if have data 
														
														$addr_val='';
														if($hasUser!=""){ $addr_val=$user_data['address']; }
														?>								                  	
									                    <label for="address" class="col-lg-2 control-label">Address</label>
									                    <div class="col-lg-10">
									                      <textarea type="email" title='Fill up your desire address.'  class='form-control custom_textbox_xs text_spacer required' required id='address' name='address' placeholder="Address" style="resize:none;" ><?php echo $addr_val; ?></textarea>
									                      <div class="help-block with-errors"></div>
									                    </div>							                  	
									                  </div> 							                  
									                  <div class='form-group'>
														<?php //check if have data 
														$email_val='';
														if($hasUser!=""){ $email_val=$user_data['email']; }
														?>								                  	
									                    <label for="email" class="col-lg-2 control-label">Email</label>
									                    <div class="col-lg-10">
									                      <input type="email" title='Fill up your desire Email.'  class='form-control custom_textbox_xs text_spacer required' required id='email' name='email' value="<?php echo $email_val; ?>"  placeholder="Email" />
									                      <div class="help-block with-errors"></div>
									                    </div>							                  	
									                  </div> 
									                  <div class='form-group'>
														<?php //check if have data 
														$contactno_val='';
														if($hasUser!=""){ $contactno_val=$user_data['contact_no']; }
														?>								                  	
									                    <label for="contact_no" class="col-lg-2 control-label">Contact #</label>
									                    <div class="col-lg-10">
									                      <input type="number" title='Fill up your desire Contact #.'  class='form-control custom_textbox_xs text_spacer required' required id='contact_no' name='contact_no' value="<?php echo $contactno_val; ?>"  placeholder="Contact #" />
									                      <div class="help-block with-errors"></div>
									                    </div>							                  	
									                  </div> 
									                  <div class='form-group'>
														<?php //check if have data 
														$fb_val='';
														if($hasUser!=""){ $fb_val=$user_data['fb_url']; }
														?>								                  	
									                    <label for="fb_url" class="col-lg-2 control-label">Facebook Profile</label>
									                    <div class="col-lg-10">
									                      <input type="url" title='Fill up your facebook profile.'  class='form-control custom_textbox_xs text_spacer required' required id='fb_url' name='fb_url' value="<?php echo $fb_val; ?>"  placeholder="Facebook Profile" />
									                      <div class="help-block">Please make sure that it's searchable for verification purpose only.(https://www.facebook.com/ABSCBNnetwork?fref=nf)</div>
									                    </div>							                  	
									                  </div> 							                  							                  
								                  </div>
									<legend>Account Information</legend>	
					                  <div class='form-group'>
										<?php //check if have data 
										
										$account_no_val='';
										if($hasUser!=""){ $account_no_val=$user_data['account_no']; }
										?>								                  	
					                    <label for="account_no" class="col-lg-2 control-label">Account #</label>
					                    <div class="col-lg-10">
					                      <input type="number" title='Fill up your desire Email.'  class='form-control custom_textbox_xs text_spacer required' required id='account_no' name='account_no' value="<?php echo $account_no_val; ?>"  placeholder="Account #" />
					                      <div class="help-block with-errors"></div>
					                    </div>							                  	
					                  </div> 							                  
					                  <div class='form-group'>
										<?php //check if have data 
										$account_name_val='';
										if($hasUser!=""){ $account_name_val=$user_data['account_name']; }
										?>								                  	
					                    <label for="account_name" class="col-lg-2 control-label">Account Name</label>
					                    <div class="col-lg-10">
					                      <input type="text" title='Fill up your desire Email.'  class='form-control custom_textbox_xs text_spacer required' required id='account_name' name='account_name' value="<?php echo $account_name_val; ?>"  placeholder="Account name" />
					                      <div class="help-block with-errors"></div>
					                    </div>							                  	
					                  </div> 
					                  <div class="form-group">
					                  	<div class='col-lg-offset-2 col-lg-10'>
					                  		<div class="g-recaptcha required" data-sitekey="6LfPfxETAAAAAN8BS04RRDE9lu7VgfbfmR_PjUq_"></div>
					                  		<span id="captcha" style="margin-left:100px;color:red" />	
					                  	</div>
														                  	
					                  </div>																															        						                  	             			                  				                  						                  						                  						                  												                  						                  																		
								  </div>
						      	<input type="hidden" id="hid_base_url" value="<?php echo base_url(); ?>" /> 
								<input type="hidden" id="title" name='title' value="<?php echo $title;?>" /> 
								<hr>
								<div class='col-md-8'>
									<a type="button" class="btn btn-default btn-sm" href='<?=base_url('home/index/resend_email');?>'>Resend Confirmation</a>
								</div>
								<div class="col-md-4">
									<div class='pull-right'>
						        		<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>								
										<button type="button" id="submit" name="submit" width='30' title='Register' class="btn btn-pink btn-sm" >
											Submit
										</button>										
									</div>									
								</div>
	
						
								</form>		

								  														
				</div><!--row-->
			  </div>
			</div>		
     </div>

    </div>
  </div>
</div>

<div  class="modal fade" id="cart_modal" data-backdrop='static' tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header" style="padding: 15px 15px 0px 0px;"> 
             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="margin-left:15px;">Success</h4>
     </div>
      <div class="modal-body"> 
      Your item(s) had been added to the cart
      </div>
    	<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="continue_shopping">Continue Shopping</button>
        <button type="button" class="btn btn-primary" id="proceed_to_cart"><i class="fa fa-shopping-cart"></i> Proceed to Cart</button>
      </div>
	</div>
	</div>
</div>
<div  class="modal fade " id="preview_modal" data-backdrop='static' tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header" style="padding: 15px 15px 0px 0px;"> 
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <!--<h4 class="modal-title" style="margin-left:15px;">Success</h4>-->
     </div>
      <div class="modal-body"> 
      	<img id='prev_item' height="400" width='100%' />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-pink" data-dismiss="modal">OK</button>
       <!-- <button type="button" class="btn btn-primary" id="proceed_to_cart"><i class="fa fa-shopping-cart"></i> Proceed to Cart</button>-->
      </div>
	</div>
	</div>
</div>

	<input type='hidden' id='base_url' value='<?php echo base_url(); ?>' />	
	<input type='hidden' id='assets_url' value='<?php echo assets_url(); ?>' />	
	
	<!-- loding js files -->

	<script   src="<?=assets_url();?>js/jquery-1.11.3.min.js"></script>
	<script   src="<?=assets_url();?>js/bootstrap.min.js"></script>
	<script   src="<?=assets_url();?>js/bootstrap-datepicker.min.js"></script>
	<script   src="<?=assets_url();?>js/validator.min.js"></script>
	<script   src="<?=assets_url();?>js/jquery.dataTables.min.js"></script>
	<script   src="<?=assets_url();?>js/dataTables.bootstrap.min.js"></script>
	<script   src="<?=assets_url();?>js/dataTables.responsive.js"></script>
	<script   src="<?=assets_url();?>js/dataTables.tableTools.js"></script>		
	<script   src="<?=assets_url();?>js/select2.min.js"></script>	
	<script   src="<?=assets_url();?>js/spin.min.js"></script>
	<script   src="<?=assets_url();?>js/currency/curry-0.8.min.js"></script>	
	<script   src="<?=assets_url();?>js/bootstrap-submenu.min.js"></script>	
	<script   src="<?=assets_url();?>js/jquery.flexslider-min.js"></script>	
	<script  async="true" src='https://www.google.com/recaptcha/api.js'></script> <!-- cause of loading?  -->
	<script   src="<?=assets_url();?>js/jquery.elastic.source.js"></script>
	<script   src="<?=assets_url();?>js/jquery.elevateZoom-3.0.8.min.js"></script>		
	<script  src="<?=assets_url();?>js/jquery.fancybox.js"></script>	
	<script  src="<?=assets_url();?>js/jquery.fancybox.pack.js"></script>	
	<script  src="<?=assets_url();?>js/jquery.fancybox-buttons.js"></script>	
	<script  src="<?=assets_url();?>js/jquery.fancybox-media.js"></script>		
	<script  src="<?=assets_url();?>js/jquery.fancybox-thumbs.js"></script>					
<!--	<script  src="<?=assets_url();?>js/jquery.backstretch.min.js"></script>	-->	
	<script  src="<?=assets_url();?>js/global.js"></script>

            <?php       
            if(isset($scripts) && is_array($scripts)){
               foreach($scripts as $script){
            ?>
            <script    src="<?=$script?>"></script>
            <?php
                }
            }
            ?><!--end code of scripts -->	
	<!-- end script -->	
				
			<footer id="footer">
				<div class='container'>
			        <p class="pull-right"><a href="#top" style="color:#fff;" title="Back to Top"><b class='glyphicon glyphicon-chevron-up'></b></a></p>
			        <p style="color:#fff;">
			        &copy; Saesil Park and Shop @ 2015. 
			        | <a style="color:#fff;" href="<?php echo base_url("home/index/about"); ?>">About</a>
			        | <a style="color:#fff;" href="<?php echo base_url("home/index/terms"); ?>">Terms</a>
			        | <a style="color:#fff;" href="<?php echo base_url("home/index/developer"); ?>">Developers</a>
			        </p>							
				</div>		         
			</footer>			
</body>
</html>