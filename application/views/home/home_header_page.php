<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<!DOCTYPE html>
	<html lang="en">
	  <head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <title><?=$title?></title>
	
	    <!-- CSS -->
		<link href = "<?=assets_url();?>css/bootstrap.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/dataTables.bootstrap.min.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/bootstrap-datepicker.min.css" rel = "stylesheet">
		<link href = "<?=assets_url();?>css/css/font-awesome.min.css" type="text/css" rel = "stylesheet">
		<link href = "<?=assets_url();?>css/bootstrap-submenu.min.css" rel = "stylesheet">
		<link href = "<?=assets_url();?>css/flexslider.css" rel = "stylesheet">
		<link href = "<?=assets_url();?>css/custom.css" rel = "stylesheet">	
	<!--	<link href = "<?=assets_url();?>css/jquery.dataTables.min.css" rel = "stylesheet">	-->
	<!--	<link href = "<?=assets_url();?>css/jquery.dataTables_themeroller.css" rel = "stylesheet">	-->
		<link href = "<?=assets_url();?>css/dataTables.tableTools.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/responsive.bootstrap.min.css" rel = "stylesheet">
		<link href = "<?=assets_url();?>css/select2.min.css" rel = "stylesheet">
		<link href = "<?=assets_url();?>css/select2-bootstrap.css" rel = "stylesheet">		
		<link href = "<?=assets_url();?>css/jquery.fancybox.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/jquery.fancybox-buttons.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/jquery.fancybox-thumbs.css" rel = "stylesheet">	
		<link href = "<?=assets_url();?>css/yamm.css" rel = "stylesheet">									
		<!-- favicon -->
		<link rel="apple-touch-icon" sizes="57x57" href="<?=assets_url();?>img/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?=assets_url();?>img/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?=assets_url();?>img/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?=assets_url();?>img/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?=assets_url();?>img/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?=assets_url();?>img/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?=assets_url();?>img/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?=assets_url();?>img/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?=assets_url();?>img/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?=assets_url();?>img/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?=assets_url();?>img/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?=assets_url();?>img/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?=assets_url();?>img/favicon-16x16.png">
		<link rel="manifest" href="<?=assets_url();?>img/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?=assets_url();?>img/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">				
	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	                 
            <!-- start of styles auto php -->    
            <?php
            if(isset($styles) && is_array($styles)){
                foreach($styles as $style){
            ?>
            <link rel="stylesheet" type="text/css" href="<?=$style?>">
            <?php
                }
            }
            ?><!--end code of styles -->	    
	    
	  </head>
<body>
		      <?php 
		    $nav=''; //for user login
			$islog=''; $islog=$this->session->userdata('username');
			/*$pix=$this->session->userdata('user_img');
			$profilepic='img/td.PNG';
			if(!empty($pix)){ $profilepic="uploads/users_pic/".$pix;}	*/			
			$level=$this->session->userdata('admin_access_level');			     
			$home='';	 
			if($islog){
				$home='home';
			}		  
		      ?>	
	<div id="top" class='top'>
		<div class="container">
		<div class='col-lg-12'>
			<?php
			// forlogo
			$logo_fb='#';
			if(!empty($logo_data['logo_id'])){
				$logo_fb=$logo_data['facebook_url'];
			}
			$logo_twitter='#';
			if(!empty($logo_data['logo_id'])){
				$logo_twitter=$logo_data['twitter_url'];
			}			
			?>					
			<ul class="list-inline" style="margin-bottom:0px;">
			  <li>
				<a class="btn btn-primary btn-xs" href="<?php echo $logo_fb; ?>">
					<i class="fa fa-facebook-official fa-lg"></i>
				</a>
			  </li>
			  <li>
				<a class="btn btn-primary btn-xs" href="<?php echo $logo_twitter;?>">
					<i class="fa fa-twitter fa-lg"></i>
				</a>
			  </li>			  
			  <li class='pull-right'>
				<p class="text-capitalize small ">
					<a class='btn-link' href='<?=base_url("cart");?>'><span clsas='glyphicon glyphicon-shopping-cart'></span>CART(<span id="cart_count"><?php echo $this->cart->total_items();?></span>) </a> 
					<?php
					$islog=$this->session->userdata('username');
					if($islog==''){
					?>
					| <a class='btn-link prof_modal'  data-toggle="modal" href="#loginx"  id='login-btn' data-target="#login-modal">LOGIN</a> 
					| <a class='btn-link prof_modal'  data-toggle="modal" href="#registerx" id='register-btn'data-target="#login-modal">REGISTER </a></p>
					<?php 
					}
					?>						  	
			  </li>
			</ul>	
		</div>
		</div>
	</div>		      	
	<div class="container">			
		<div class="row">
			<?php
			// forlogo
			$logo_filename='td.png';
			if(!empty($logo_data['logo_id'])){
				$logo_filename=$logo_data['logo_filename'];
			}
			
			?>		
			<div class="col-sm-2">
				<a href='<?=base_url("home");?>'>
				<img src="<?=assets_url()."img/logo/".$logo_filename;?>" style='height:100px; width:200px; margin-bottom:5px;'  class="img-responsive img_canvas">
				</a>
			</div>
		
			<div class="col-lg-7">
				<form action='<?=base_url("search/items/0");?>' method='post' data-toggle="validator"  class='form'>
					<div class="form-group">
					  <div style="margin-top:6%;">
					  	<?php
					  	$search_val ='';
						if(!empty($search_value)){
							$search_val=$search_value;
						}
					  	?>	
		                  <div class="input-group">
		                    <input type="text" name='search' required value="<?php echo $search_val;?>" class="form-control text-pink" placeholder="Search Items">
		                    <span class="input-group-btn">
		                      <button class="btn btn-pink" type="submit">
		                      	<span class="glyphicon glyphicon-search"></span>
		                      	</button>
		                    </span>
		                  </div>					  	
					  </div>					  	
	                </div>									
				</form>
			<div class="col-lg-3">			
				<div style="">
				</div>
				</div>
			</div>			
		</div>
	</div>	
		<nav class="navbar navbar-default yamm">
		  <div class="container">
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="glyphicon glyphicon-option-vertical"></span>
		      </button>					    
		    </div>
		     		
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
		      <ul class="nav navbar-nav">	
				<!--<li class="dropdown">
			        <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
			          ALL PRODUCTS<span class="caret"></span>
			        </a>
			        <ul class='dropdown-menu'>
			      <?php 
			      //var_dump($menu);
			      echo $menu;
		  		  ?> 
		  		  </ul>   
	  		  	</li>-->
				<li class="dropdown">
			        <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
			          CATEGORY<span class="caret"></span>
			        </a>
			        <ul class='dropdown-menu'>
			        <?php foreach ($category_data as $key => $value) {
			        list($cname,$cid)=explode('||',$key);
			        ?>			        
			        	<li class='dropdown-submenu'>
			        	<a tabindex='0'><?=$cname?></a>
			        		<ul class='dropdown-menu'>
			        		<?php 
			        		foreach ($value as $key1 => $value1) {
			        		?>
			        			<li>
								<a tabindex='0' href='<?php base_url('items_bytitle/sub_category/'.$this->encryption->encode($value1['scat_name']));?>' ><?=$value1['scat_name']?></a>
			        			</li>
			        		<?php } ?>	
			        		</ul>
			        	</li>
			        <?php	} ?>	
		  		  	</ul> 
			        <?php 
			         ///$i=0;
					 //var_dump($home_model);
/*				    foreach($category_data as $cat_data){
				    echo " <li >
				            <a tabindex='0' href='".base_url('items_bytitle/category/'.$this->encryption->encode($cat_data['cat_name']))."' >".$cat_data['cat_name']."</a>";												
					echo"   </li> "; 		
				  	}*/
		  		  ?> 		  		    
	  		  	</li>	  		  	
	  		  	<li>
	  		  		<a href='<?=base_url('items/new');?>'>NEW ITEMS</a>
	  		  	</li>	
	  		  	<li>
	  		  		<a href='<?=base_url('items/best_seller');?>'>BEST SELLER</a>
	  		  	</li>	
				<li class="dropdown yamm-fw">
			        <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
			          BRAND<span class="caret"></span>
			        </a>
			      <ul class='dropdown-menu'>
					<li>
					<div class="yamm-content">
						<div class="row">
							<?php
							$ctr=1;
							foreach($brand_data as $key => $brd_data) {
							if($ctr%10==1 )	{
							echo"<ul class='col-sm-2 list-unstyled' style='margin-top:10px;'>";
							}
						    echo " <li >
						            <a tabindex='0' style='color:#fff !important;' href='".base_url('items_bytitle/brand/'.$this->encryption->encode($brd_data['brand_name']))."' >".$brd_data['brand_name']."</a>";													
							echo"   </li> "; 
							if($ctr%10==0){
		                    echo"</ul>";
		                    //echo"<ul class='col-sm-2 list-unstyled'>";
		                    }
		                    $ctr++;
							}
							if ($ctr%10 != 1) echo "</ul>";									
							?>
						</div>
					</div>
					</li>	 
		  		  </ul>   
	  		  	</li>	  		  	
	  		  	<li>
	  		  		<a href='<?=base_url('contact_us/contact_us');?>'>Contact Us</a>
	  		  	</li>	  		  		  		  		  		  			             
		      </ul>
		      <?php
		       //login as users
			  if(!empty($islog)){
			  ?>			    	
		      <ul class="nav navbar-nav navbar-right">
		        <li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						<ul class="list-inline">
						 <!-- <li><img src='<?=assets_url().$profilepic; ?>' class="img-circle" style="width:35px; height:35px;"></li>-->
						  <li><?php echo ucfirst(ucwords($islog)); ?><span class="caret"></span></li>
						</ul>			          				          	
			          </a>
			          <ul class="dropdown-menu" role="menu">
			            <li><a href="<?=base_url('home/index/profile');?>">
			             <i class="glyphicon glyphicon-user"></i> Profile
			            </a>
			            </li>	
			            <li><a href="<?=base_url('current_transaction');?>">
			             <i class="fa fa-bars fa-sm" aria-hidden="true"></i> My Page
			            	</a>
			            </li>				            	            
			            <li><a href="<?=base_url('home/index/logout');?>">
			             <i class="glyphicon glyphicon-off"></i> Log Out
			            	</a>
			            </li>
			          </ul>
			        </li>        	
		        </li>
		      </ul>
		    </div>
		    <?php }?>  		    
		    
		  </div>
		</nav>
		
 	
 	
