<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-lg-12'>
				<legend>
					<?php echo $title_filter; ?>
				</legend>
				<div class="row">						  	
			  	<?php 
			  	if(!empty($items_data)){		  	
			  	foreach($items_data as $itemsx_data){
			  	?>							  	
				<?php 
				$img_name='';
				$itemsx_data['thumbs']!='' ?  $img_name=$itemsx_data['thumbs'] : $img_name='td.png';

								//discount check
							$is_discount = false;
							$discount_percent = 0;
								foreach ($discount_conditions as $key => $value) {
								 	if($value['promo_type'] == "item"){ ///TODO : conditions for categories and sub category. NEED dropdown for category/sub in promo add/edit page

								 		if($value['value'] == $itemsx_data['item_id']){
								 			$is_discount = true;
								 			$discount_percent = $value['discount_percent'];
								 			break;
								 		}


								 	}else if($value['promo_type'] == "brand"){  
								 		if($value['value'] == $itemsx_data['brand_id']){
								 			$is_discount = true;
								 			$discount_percent = $value['discount_percent'];
								 			break;
								 		}


								 	}


								}

				?>
							  <div class="col-md-3">
							  	<a href="<?=base_url('items_view/'.$this->encryption->encode($itemsx_data['item_id']));?>" style='margin-bottom:10px ;'  role="button">
							    <div class="thumbnail"style="margin-bottom: 10px; border:0px; padding:0px;">
							      <img src="<?=assets_url()."img/item_covers/".$img_name?>" alt="..." class='img-minheight'>
							      <div class="caption" style="height:150px;">
							        <h4 class='btn-link'style="word-break: break-all; white-space:normal;">
							        	<center>
							        		<b class='title_head'><?php echo wordwrap(ucfirst(ucwords($itemsx_data['item_name']))); ?></b>
							        	</center>
							        </h4>
							        <?php if($is_discount) : ?>
									<p class='price'>Php<?php echo number_format($itemsx_data['price'] - ($itemsx_data['price'] * ($discount_percent * 0.01)),2); ?> <s>Php<?php echo number_format($itemsx_data['price'],2); ?></s>
									<span class="label label-danger label-as-badge"><?=floor($discount_percent)?>% Off!</span>
									</p>  	
							        <?php else: ?>
							        <p class='price'>Php<?php echo number_format($itemsx_data['price'],2); ?></p>  	
							        <?php endif; ?>
							      </div>
							    </div>
							   </a>
							  </div>
		
				<?php
					}
				}else{
				?>
				<div class='col-lg-12'>
					<center>
						<p style="padding-top:.33em"> No Result.  </p>						
					</center>
				</div>	
				<?php
				}
				?>
				<?php
				if(!empty($pagination))
				?>
				<div class='col-lg-12'>
					<?php echo $pagination; ?>
				</div>					
				<?php
				?>
				</div>					
			</div>								
		</div>
	</div>	
</div>