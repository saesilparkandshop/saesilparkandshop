<div class='container'>
	<div class="col-xs-12">
		<div class='min_height'>
			<div class='col-lg-12'>
				<legend class="noselect">
					<?php echo $item_data['item_name']; ?> ( <?php echo $item_data['volume']; ?> )
				</legend>
				<div class="row">
					<div class='col-md-6'>	
						<?php 
						//echo var_dump($discount_conditions);
						$img_namet1='td.PNG';
						$img_namet2='td.PNG';
						$img_namet3='td.PNG';
						$img_name1='td.PNG';
						$img_name2='td.PNG';
						$img_name3='td.PNG';						
						if($img_data){
							foreach ($img_data as $img_data_val ) {
								if($img_data_val['index']=='1'){
								$img_namet1=$img_data_val['thumbs'];
								$img_name1=$img_data_val['filename'];	
								}
								if($img_data_val['index']=='2'){
								$img_namet2=$img_data_val['thumbs'];
								$img_name2=$img_data_val['filename'];	
								}
								if($img_data_val['index']=='3'){
								$img_namet3=$img_data_val['thumbs'];
								$img_name3=$img_data_val['filename'];	
								}																
							}
						}
						//$item_data['image_name']!='' ?  $img_name=$item_data['image_thumbs_filename'] : $img_name='td.png';
						?>							
						<a class="thumbnail fancybox-thumb" rel="fancybox-thumb" style="margin-bottom: 10px; border:0px; padding:0px; cursor:pointer;" href="<?=assets_url()."img/item_covers/".$img_name1?>">
					    <center style='padding:0px;'>
							<img src="<?=assets_url()."img/item_covers/".$img_name1?>" alt="" width='100%' id='itm_img' class='img-minheight'>
					    </center>					      
					    </a>
					 <div class='row'>
					 	<div class='col-xs-12'>
					 	<ul class="list-inline text-center">
						<li>							
						<a class="thumbnail fancybox-thumb" rel="fancybox-thumb" style="margin-bottom: 10px; border:0px; padding:0px; cursor:pointer;" href="<?=assets_url()."img/item_covers/".$img_name2?>">
					    <center style='padding:0px;'>
							<img src="<?=assets_url()."img/item_covers/".$img_name2?>" alt="" id='itm_img' width='130'  class='img-minheight'>
					    </center>					      
					    </a>
					    </li>
						<li>							
						<a class="thumbnail fancybox-thumb" rel="fancybox-thumb" style="margin-bottom: 10px; border:0px; padding:0px; cursor:pointer;" href='<?=assets_url()."img/item_covers/".$img_name3?>'>
					    <center style='padding:0px;'>
							<img src="<?=assets_url()."img/item_covers/".$img_name3?>" alt="" id='itm_img'  width='130' class='img-minheight'>
					    </center>					      
					 	</a>
					 	</li>
					 	</ul>
					 	</div>					 	
					 </div>   
					</div>
					
					<div class='col-lg-6'>
					<form class="form-horizontal">
						<div class="panel panel-primary" style="min-height:667px;">
						  <div class="panel-heading panel-heading-custom">
						  	<div class="row">
						  	<div class="col-lg-8">
							<?php if( $item_data['is_discount'] && $item_data['ps_qty'] == 1 ) : ?>
							<h3 class="panel-title">
							<b>Price
							<span class="price"> 
								<?php echo "Php ".number_format($item_data['price'] - ($item_data['price'] * ($item_data['discount_percent'] * 0.01)),2); ?></b> 
								<s style="color:white;"><?php echo "Php ".number_format($item_data['price'],2); ?></b></s>
								<span class="label label-danger label-as-badge"><?=floor($item_data['discount_percent'])?>% Off!</span>
							</span>
							</h3>
							<input type='hidden' id='price' name='price' value='<?php echo  $item_data['price'] - ($item_data['price'] * ($item_data['discount_percent'] * 0.01)); ?>' />
							 <?php elseif( $item_data['is_discount'] && $item_data['ps_qty'] > 1 ) : ?>
								<h3 class="panel-title">
								<b>Price
								<span class="price"><?php echo "Php ".number_format($item_data['price']); ?></span> </b>
									<span class="label label-danger label-as-badge"><?=$item_data['scheme']?></span>
							</span>
							</h3>
							<input type='hidden' id='price' name='price' value='<?php echo  $item_data['price'] - ($item_data['price'] * ($item_data['discount_percent']* 0.01)); ?>' />
						  	<?php else: ?>
							<h3 class="panel-title"><b>Price<span class="price"> <?php echo "Php ".number_format($item_data['price']); ?></b></span></h3>						  		
						  	<input type='hidden' id='price' name='price' value='<?php echo $item_data['price']; ?>' />
						  	<?php endif; ?>
						  		
						  		
						  	</div>
						   </div>
						  </div>
						  <div class="panel-body" style="padding:0px; ">
							<div class="list-group" style="margin:10px;">							  		
								    <div class='col-md-12'>						
								    		<div class="form-group">
								    		<label class="col-md-2"><b>Availability</b></label>	
									    		<div class='col-md-5'>
								    				<span style="font-size:14px; color:#2780e3;"><?php echo ucfirst($item_data['is_stock']); ?></span>
									    		</div>							    			
								    		</div>
								    		<div class='form-group'>
									    		<label class='col-md-2'>
									    		</label>
									    		<div class="col-md-10">
												<?php if($item_data['options']!='' && $item_data['option_stock']!='') : ?>
									    			<select id="options_menu" class="form-control input-sm custom_input options_menu">
									    				<option value=''>--Options--</option>
														<?php
														$opdata=explode(',',$item_data['options']); $opdatastock=explode(',',$item_data['option_stock']);
															$str='';
															for($i=0;$i<count($opdata); $i++){
																if($opdatastock[$i]=='1'){
																	echo "<option class='custom_inputd' disabled value='".$opdata[$i]."'>".$opdata[$i]." OUT OF STOCK</option>";	
																}else{
																	echo "<option  value='".$opdata[$i]."'>".$opdata[$i]."</option>";	
																}										    														    					
															}
									    				
									    				?>	
									    			</select>
									    		<?php endif; ?>
									    		<input type="hidden" value="<?php echo $item_data['id']; ?>" id="item_id">
									    		</div>									    			
								    		</div>
								    		<div class='form-group'>
									    		<label class='col-md-2'>
									    		<?php $item_data['options']!=''? $t="Options" : $t="Quantity"; ?>	
									    			<b><?php echo $t; ?></b>
									    		</label>
									    		<div class='col-md-10'>
												  	<table class="table table-striped table-bordered" cellspacing="0" width="50%" style="margin-top: 5px;">		
												        <tbody id='options' class="options_body">
												        </tbody>			
												        <footer>
													  		<tr>
													  		<td width="70%">Total Quantity</td>
												        	<td><input type="number" name="total_qty" id="total_qty" class="form-control text_spacer input-sm tqty" value="0"   min="0"/></td>
												        	</tr>												
												
													  		<tr>
												        	<td>Total Price</td>
												        	<td>
												        	<span name="total_price" id="total_price"></span>
												        	<input type="hidden" id="ototal" name="ototal" value="0" />
												        	</td>
												        	</tr>													        	
												        </footer>	  		
												  	</table>									    				
									    		</div>
								    		</div>
								    		<?php
								    		if($item_data['is_onhand']=='1'){
								    		?>
								    		<div class="form-group">
								    		<label class="col-md-2"><b></b></label>	
									    		<div class='col-md-5'>
									    			<?php $item_data['is_onhand']=='1' ? $is_onhand="On-Hand": $is_onhand=''; ?>
								    				<span style="font-size:16px;"><?php echo $is_onhand; ?></php?></span>
									    		</div>							    			
								    		</div>	
								    		<?php } ?>	
								    		<div class="form-group">	
								    			<div class="col-md-12">							    					
								    				<button class="btn btn-pink pull-right" id="add_to_cart" type="button">
								    					<span class="glyphicon glyphicon-shopping-cart"></span><b>ADD TO CART</b>
								    				</button>								    				
								    			</div>
								    		</div>									    		
											<ul class="nav nav-tabs">
											  <li class="active"><a href="#description" data-toggle="tab">Item Description</a></li>
											<!--  <li><a href="#ingredients" data-toggle="tab">Ingredients</a></li>
											  <li><a href="#volume" data-toggle="tab">Volume</a></li>-->
											</ul>
											<div id="myTabContent" class="tab-content">
											  <div class="tab-pane fade active in" id="description">
										    		<div class="form-group">
											    		<div class='col-md-12'>
										    				<textarea readonly="" id='desc' class='elastic custom_textbox_xs text_spacer noselect' style="font-size:16px; min-height:250px; resize:none;"><?php echo $item_data['description']; ?></textarea>
											    		</div>							    			
										    		</div>												    
											  </div>
											  <div class="tab-pane fade" id="ingredients">
										    		<div class="form-group">
											    		<div class='col-md-12'>
										    				<textarea readonly="" id='ingrid' class='elastic custom_textbox_xs text_spacer noselect' style="font-size:16px; min-height:250px; resize:none;"><?php echo $item_data['ingredients']; ?></textarea>
											    		</div>							    			
										    		</div>													    
											  </div>
											  <div class="tab-pane fade" id="volume">
										    		<div class="form-group">
											    		<div class='col-md-12'>
										    				<textarea readonly="" id='vol' class='elastic custom_textbox_xs text_spacer noselect' style="font-size:16px; min-height:250px; resize:none;"><?php echo $item_data['volume']; ?></textarea>
											    		</div>							    			
										    		</div>		
											  </div>
											</div>								    								    								   							
								  </a>	
								  </div>			  	
							 </div>	
						  </div>
						</div>											
						</form>
					</div>												  	
				</div>
				<div class='row'>
     				<div class="col-md-12">
					<div id="disqus_thread"></div>
					    <script type="text/javascript">
					        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
					        var disqus_shortname = 'saesilparknshop'; 
					        var disqus_identifier = '<?php echo $item_data['id']; ?>';
					        var disqus_title = '<?php echo $item_data['item_name'];?>';
					        /* * * DON'T EDIT BELOW THIS LINE * * */
					        (function() {
					            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
					            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
					            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
					        })();
					    </script>
					    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
					    
					</div>					
				</div>
			</div>								
		</div>
	</div>	
</div>




<div  class="modal fade" id="bonus_cart_modal" data-backdrop='static' tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header" style="padding: 15px 15px 0px 0px;"> 
             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="margin-left:15px;">Promo</h4>
     </div>
      <div class="modal-body"> 
		Congratulations! This item is on promo. Please enter <b id="add_more"></b> more item(s).

	<div class="row">
		<div class="col-md-12">
			<div class="list-group" style="margin:10px;">	
											    		<div class='form-group'>
									    		<label class='col-md-2'>
									    		</label>
									    		<div class="col-md-10">
												<?php if($item_data['options']!='' && $item_data['option_stock']!='') : ?>
									    			<select id="options_menu2" class="form-control input-sm custom_input options_menu">
									    				<option value=''>--Options--</option>
														<?php
														$opdata=explode(',',$item_data['options']); $opdatastock=explode(',',$item_data['option_stock']);
															$str='';
															for($i=0;$i<count($opdata); $i++){
																if($opdatastock[$i]=='1'){
																	echo "<option class='custom_inputd' disabled value='".$opdata[$i]."'>".$opdata[$i]." OUT OF STOCK</option>";	
																}else{
																	echo "<option  value='".$opdata[$i]."'>".$opdata[$i]."</option>";	
																}										    														    					
															}
									    				
									    				?>	
									    			</select>
									    		<?php endif; ?>
									    		<input type="hidden" value="<?php echo $item_data['id']; ?>" id="item_id">
									    		</div>									    			
								    		</div>
								    		<div class='form-group'>
									    		<label class='col-md-2'>
									    		<?php $item_data['options']!=''? $t="Options" : $t="Quantity"; ?>	
									    			<b><?php echo $t; ?></b>
									    		</label>
									    		<div class='col-md-10'>
												  	<table class="table table-striped table-bordered" cellspacing="0" width="50%" style="margin-top: 5px;">		
												        <tbody id='options2' class="options_body">
												        </tbody>			
												        <footer>
													  		<tr>
													  		<td width="70%">Quantity</td>
												        	<td><input type="number" name="total_qty2" id="" class="form-control text_spacer input-sm tqty" value="0"   min="0"/></td>
												        	</tr>												
												 													        	
												        </footer>	  		
												  	</table>									    				
									    		</div>
								    		</div>
				 </div> <!--.list-group -->
		</div>
	</div>

      </div>
    	<div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="add_to_cart2"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
        <button type="button" class="btn btn-default" class="close" data-dismiss="modal" aria-label="Cancel">Cancel</button>
      </div>
	</div>
	</div>
</div>