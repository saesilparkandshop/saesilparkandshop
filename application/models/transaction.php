<?php

class transaction extends CI_Model{
	
	function generate_order_id(){

		$this->db->insert('order_id',array('content'=>''));

		return $this->db->insert_id();
	}

	function insert($data){

		$this->db->insert('transactions',$data);


		 
		return $this->db->insert_id();	
	}

	function update($order_id,$data){

		$this->db->where('order_id' , $order_id);

		$this->db->update('transactions', $data);

	}

    function selectItemsEditOrdersSummary($id){
        $this->db->select('*');
        $this->db->from('transaction_summary');
        $this->db->where(array("order_id"=>$id));
        $query = $this->db->get();
        return $query->row_array();

    }
    function selectItemsEditOrders($id){
        $this->db->select('*');
        $this->db->from('transactions');
        $this->db->where(array("order_id"=>$id));
        $query = $this->db->get();
        return $query->result_array();

    }


    function updateItemsPending($data,$id){
        $this->db->where('order_id', $id);
        $update_data=$this->db->update('transaction_summary', $data);


        if($update_data){

            /*if($trx_type==0){
            $this->db->select('qty');
            $this->db->from('books');
            $this->db->where(array("id"=>$book_id));
            $query = $this->db->get();
            $result=$query->row_array();
            $new_qty=intval($result['qty'])-$qty;
            $this->db->where('id', $book_id);
            $update_data_book=$this->db->update('books',array("qty"=>$new_qty));

            } else if($trx_type==3){

            $this->db->select('qty');
            $this->db->from('books');
            $this->db->where(array("id"=>$book_id));
            $query = $this->db->get();
            $result=$query->row_array();
            $new_qty=intval($result['qty'])+$qty;
            $this->db->where('id', $book_id);
            $update_data_book=$this->db->update('books',array("qty"=>$new_qty));
            }	*/


            return 1;
        }else {
            return 0;
        }
    }

}