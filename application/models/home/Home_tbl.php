<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Home_tbl extends CI_Model {
	 function CheckUserIfLogin(){
		
		$user = $this->session->userdata('username');
		//echo $user."userx";
		if($user==''){
		$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">You Must Login.</div>');						
			return  redirect(base_url(''));									
		} else {
			return  "";					
		}
	 }	 
	 function getlogo(){
 		$this->db->select('*');
		$this->db->from('logo');
		$this->db->order_by('logo_id','desc');
		$query = $this->db->get();
		return $query->row_array();				
	}
	function getBrand(){
		$this->db->select("id,brand_id,brand_name,status");
		$this->db->from("brand");
		$this->db->where("status","active");
		$this->db->order_by('brand_name','asc');
		return $this->db->get()->result_array();
	}	 		
/*	function getCategory(){
		$this->db->select("id,cat_id,cat_name,brand_id,status");
		$this->db->from("category");
		$this->db->where("status","active");
		$this->db->order_by('cat_name','asc');
		return $this->db->get()->result_array();
	}
	function getsCategory(){
		$this->db->select("id,cat_id,scat_id,scat_name,status");
		$this->db->from("sub_category");
		//$this->db->where("cat_id",$cat);
		$this->db->where("status","active");
		$this->db->order_by('scat_name','asc');
		return $this->db->get()->result_array();
	}*/
	
	function getCategoryMenu(){
		$this->db->select("category.*,sub_category.scat_name,sub_category.scat_id");
		$this->db->from("category");
		$this->db->join("sub_category","sub_category.cat_id = category.cat_id","inner");
		$this->db->where("category.status","active");
		$this->db->order_by('category.cat_name','asc');
		$catdata=$this->db->get()->result_array();
		//var_dump($catdata);
	$arr=[];
	foreach ($catdata as $key => $value) {
		$arr[trim($value['cat_name'])."||".trim($value['cat_id'])][$value['scat_id']]=$value;
	}

		return $arr;
	}						
	function promoData(){
		$date = date("Y-m-d");
 
		$this->db->select("*");
		$this->db->from("promo");
		$this->db->where("status","active");
		$this->db->where("promo_from <=", $date);
		$this->db->where("promo_to >=", $date);

		return $this->db->get()->result_array();
	}

	/*
	# returns list of discounted conditions. Should be called once for every product listing. Then compare individual result if condition matches. If match found, apply discount.
	# Also applies to cart
		//sample 
		returns [
			[	
				promo_name :'XXXX'
				promo_type: 'category',
				value : '1',
				discount_percent: '40'
			],
			[
				promo_name : 'YYYY'
				'promo_type' : 'item_id',
				value : ['ITEM00000001', 'ITEM00002'] ,  ///better use in array 
				discount_percent: '30',
			]
			....
		]
	*/
	function getDiscountConditions(){

		$date = date("Y-m-d"); //should be within current date
 		

		$query = $this->db->query("SELECT p.promo_name,p.promo_type, ps.disc_percent AS discount_percent,pg.value, pg.`type` AS item_promo_type, ps.qty AS `ps_qty`,ps.ptype_name FROM 
									promo p INNER JOIN promo_scheme ps ON 
									p.promo_scheme = ps.ptype_id
									LEFT JOIN promo_group pg ON 
									p.id = pg.`promo_id`
								  WHERE p.status='active' AND '{$date}' BETWEEN p.`promo_from` AND p.`promo_to`"); 
		//todo date range
		return $query->result_array();

	}
	function countAnnouncement(){
		return $this->db->count_all('announcement');
	}
	function announcementData($filter,$limit='',$offset=''){
		$this->db->select("*");
		$this->db->from("announcement");
		if($filter=='top'){
		$this->db->order_by('date_announce','desc');
		$this->db->limit(4);			
		}else if($filter=='all'){		
		$this->db->order_by('date_announce','desc');
		$this->db->limit($limit,$offset);			
		} else {
		$this->db->where('announcement_id',$filter);
		$this->db->order_by('date_announce','desc');
		return $this->db->get()->row_array();
		//$this->db->limit($limit,$offset);				
		}
		return $this->db->get()->result_array();
	}	
	function itemsData($filter){
		$this->db->select("*");
		$this->db->from("items");
		$this->db->join('images', 'images.item_id = items.item_id');
		$this->db->where('index','1');
		//for where 
		if($filter=='new'){
		$this->db->where('is_newitem','1');
		$this->db->limit(4);			
		} else if($filter=='best_seller'){
		$this->db->where('is_bestseller','1');
		$this->db->limit(4);			
		} else if($filter=='on_hand'){
		$this->db->where('is_onhand','1');
		$this->db->order_by('date_created','desc');
		$this->db->limit(4);			
		}
		//$this->db->where('is_stock','in-stock');
		$this->db->where('status','active');		
		return $this->db->get()->result_array();
	}
	function itemsDatafilteredcountf($bfilter,$cfilter,$filter){
		$this->db->select("count(*) as cnt");
		$this->db->from("items");	
		$this->db->join('images', 'images.item_id = items.item_id');
		$this->db->where('index','1');				
		$this->db->where('brand',$bfilter);	
		$this->db->where('category',$cfilter);		
		$this->db->where('sub_category',$filter);		
		$this->db->order_by('date_created','desc');			
		
		
		return $this->db->get()->row_array();			
	}
	function itemsDatafilteredf($bfilter,$cfilter,$filter,$limit,$offset){
		$this->db->select("items.item_id,item_name,brand,category,sub_category,description,price,brand_id,images.*");
		$this->db->from("items");	
		$this->db->join('images', 'images.item_id = items.item_id');
		$this->db->where('index','1');		
		$this->db->where('brand',$bfilter);	
		$this->db->where('category',$cfilter);	
		$this->db->where('sub_category',$filter);			
		$this->db->order_by('date_created','desc');		
		$this->db->limit($limit,$offset);	
		
		$this->db->where('status','active');
		return $this->db->get()->result_array();			
	}	
	
	function itemsDatafilteredcount($filter){
		$this->db->select("count(*) as cnt");
		$this->db->from("items");
		$this->db->join('images', 'images.item_id = items.item_id');
		$this->db->where('index','1');				
		//for where 
		if($filter=='brand'){
		//$this->db->where('brand',$filter);		
		$this->db->order_by('brand','asc');			
		} else if($filter=='new'){
		$this->db->where('is_newitem','1');	
		$this->db->limit(10);		
		}else if($filter=='best_seller'){
		$this->db->where('is_bestseller','1');	
		$this->db->limit(10);				 
		} else {
		$this->db->where('brand',$bfilter);	
		$this->db->where('category',$cfilter);		
		$this->db->where('sub_category',$filter);		
		$this->db->order_by('date_created','desc');			
		}
		
		return $this->db->get()->row_array();			
	}	
	function itemsDatafiltered($filter,$limit,$offset){
		$this->db->select("items.item_id,item_name,brand,category,sub_category,description,brand_id,price,images.*");
		$this->db->from("items");	
		$this->db->join('images', 'images.item_id = items.item_id');
		$this->db->where('index','1');		
		//for where 
		if($filter=='brand'){
		//$this->db->where('brand',$filter);		
		$this->db->order_by('brand','asc');	
		$this->db->limit($limit,$offset);		
		} else if($filter=='new'){
		$this->db->where('is_newitem','1');	
		$this->db->limit(8);		
		}else if($filter=='best_seller'){
		$this->db->where('is_bestseller','1');
		$this->db->limit(100);				 
		} else {
		$this->db->where('sub_category',$filter);		
		$this->db->order_by('date_created','desc');		
		$this->db->limit($limit,$offset);	
		}
		$this->db->where('status','active');
		return $this->db->get()->result_array();			
	}
	function itemBytypeCount($filter,$id){
		$this->db->select("count(*) as cnt");
		$this->db->from("items");
		$this->db->join('images', 'images.item_id = items.item_id');
		$this->db->where('index','1');				
		//for where 
		if($filter=='brand'){
		$this->db->where('brand',$id);		
		$this->db->order_by('brand','asc');
		$this->db->order_by('date_created','desc');				
		} else if($filter=='category'){
		$this->db->where('category',$id);		
		$this->db->order_by('brand','asc');	
		$this->db->order_by('date_created','desc');			
		}	
		return $this->db->get()->row_array();			
	}
	function itemBytype($filter,$id,$limit,$offset){
		$this->db->select("items.item_id,item_name,brand,category,sub_category,description,brand_id,price,images.*");
		$this->db->from("items");	
		$this->db->join('images', 'images.item_id = items.item_id');
		$this->db->where('index','1');		
		//for where 
		if($filter=='brand'){
		$this->db->where('brand',$id);		
		$this->db->order_by('brand','asc');
		$this->db->order_by('date_created','desc');		
		$this->db->limit($limit,$offset);		
		} else if($filter=='category'){
		$this->db->where('category',$id);		
		$this->db->order_by('date_created','desc');		
		$this->db->limit($limit,$offset);	
		}
		$this->db->where('status','active');
		return $this->db->get()->result_array();		
	}		
	function getItem($id){
		$this->db->select("*");
		$this->db->from("items");
		$this->db->where("item_id",$id);
		return $this->db->get()->row_array();		
	}	

	function getItem2($id){
		$this->db->select("*");
		$this->db->from("items");
		$this->db->where("id",$id);
		return $this->db->get()->row_array();		
	}	

	function getImg($id){
		$this->db->select("*");
		$this->db->from("images");
		$this->db->where("item_id",$id);
		return $this->db->get()->result_array();		
	}		
	function fetchItem($id){
		$this->db->select("*");
		$this->db->from("items");
		$this->db->where("id",$id);
		return $this->db->get()->row_array();		
	}	
	function search($search,$limit,$offset){
		$this->db->select("items.item_id,item_name,brand,category,sub_category,description,price,images.*");
		$this->db->from("items");
		$this->db->join('images', 'images.item_id = items.item_id');
		$this->db->where('index','1');			
		$this->db->like('item_name', trim($search));
		$this->db->or_like("brand",trim($search));	
		$this->db->or_like("category",trim($search));	
		$this->db->or_like("sub_category",trim($search));	
		$this->db->where('status','active');		
		$this->db->limit($limit,$offset);
		return $this->db->get()->result_array();			
	}	
	function countSearch($search){
		$this->db->select("count(*) as cnt");
		$this->db->from("items");
		$this->db->join('images', 'images.item_id = items.item_id');
		$this->db->where('index','1');			
		$this->db->like('item_name', $search);
		$this->db->or_like("brand",$search);	
		$this->db->or_like("category",$search);	
		$this->db->or_like("sub_category",$search);	
		$this->db->where('status','active');		
		return $this->db->get()->row_array();	
	}
	function getUserByEmail($email){
		$this->db->select('user_id,email,status');
		$this->db->from("users");
		$this->db->where("email",$email);
		
		return $this->db->get()->row_array();
	}

	function verifyAccount($id,$email){
		$this->db->select('user_id,email');
		$this->db->from("users");
		$this->db->where("email",$email);
		$this->db->where("user_id",$id);	
		return $this->db->get()->row_array();		
	}
	function activateAccount($id){
		$update_data=$this->db->where('user_id', $id);
		$update_data->update('users', 
		[
		'status'=>"active",
		'activation_date'=>date('Y-m-d'),
		'activated_by'=>"online",
		]
		);               				
				if($update_data){
					return 1;
				}else {
					return 0;
				}			
	}
	function AccountCheck($username){
		$this->db->select('*');
		$this->db->from("users");
		$this->db->where("username",$username);	
		return $this->db->get()->row_array();		
	}
	function ActiveCheck($status){
		$this->db->select('status');
		$this->db->from("users");
		$this->db->where("status",$status);	
		return $this->db->get()->row_array();		
	}		

	function getAbout(){
		$this->db->select('*');
		$this->db->from("about");
		$this->db->order_by("about_id",'desc');
		
		return $this->db->get()->row_array();		
	}	
	function getTerms(){
		$this->db->select('*');
		$this->db->from("terms_and_conditions");
		$this->db->order_by("terms_id",'desc');
		
		return $this->db->get()->row_array();		
	}
	function getPromo($id){
		// /$this->encryption->decode($id);
		$this->db->select('*');
		$this->db->from("promo");
		$this->db->where("promo_id",$id);
		$this->db->order_by("promo_id",'desc');		
		return $this->db->get()->row_array();	
		
	}
	function getProfile($user_id){
		$this->db->select('*');
		$this->db->from("users");
		$this->db->order_by("user_id",'desc');
		
		return $this->db->get()->row_array();	
		
	}

	function getLatestBatch(){
		$this->db->select('*');
		$this->db->from("batch");
		$this->db->order_by("date_created",'desc');
		$this->db->limit(1);
		return $this->db->get()->row_array();	
	}
	function recent_orders_data($status,$user_id){
		$this->db->select('*');
		$this->db->from("transaction_summary");
		$this->db->where("trx_type",$status);
		$this->db->where("user_id",$user_id);
		$this->db->order_by("date_created",'desc');
		$this->db->limit(10);
		return $this->db->get()->result_array();		
	}
	function transaction_data_payment($id){
		$this->db->select('*');
		$this->db->from("payment_table");
		$this->db->where("order_id",$id);	
		return $this->db->get()->result_array();	
	}
	function savePayment($data){
		$insert_data=$this->db->insert('payment_table',$data);			
			if($insert_data){
				return 1;
			}else {
				return 0;
			}			
	}
	function updatePayment($data,$id){
		$update_data=$this->db->where('id', $id);
		$update_data->update('payment_table', $data);               				
				if($update_data){
					return 1;
				}else {
					return 0;
				}			
	}
	function updateorder_summary($data,$id){
		$update_data=$this->db->where('id', $id);
		$update_data->update('transaction_summary',$data);               				
				if($update_data){
					return 1;
				}else {
					return 0;
				}	
	}
	function getrecent($user_id){
		$this->db->select('*');
		$this->db->from("transaction_summary");
		$this->db->where_in("trx_type",['active','predelivery']);
		$this->db->where('user_id',$user_id);
		$this->db->order_by("date_created",'desc');
		$this->db->limit(1);
		return $this->db->get()->row_array();
	}
}