<?php
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Category extends CI_Model { 


	public function getCateogories(){
			$this->db->select('*');
			$this->db->from("category");
			$this->db->where('status', 'active');
			$query = $this->db->get();
			return $query->result_array();	

	}

	public function getSubCategories($id){
			$this->db->select('*');
			$this->db->from("sub_category");
			$this->db->where("cat_id",$id);
			$query = $this->db->get();
			return $query->row_array();
	}

}