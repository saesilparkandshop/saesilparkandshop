<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Admin_tbl extends CI_Model { 
			
		function CheckIfhaveUser(){
			$this->db->select('id');
	        $this->db->from('users');
			$this->db->limit(1);
			$this->db->order_by('id');
	        $query = $this->db->get();	
			//check if setup.....	
			$check_info=$query->row_array();
			//$this->output->enable_profiler(TRUE);
			// check user
			//check if setup.....	
				
			if(empty($check_info)){
			 $data=$this->session->set_userdata('IsSetup','1');	
			 return	redirect(site_url('user_setup',$data));
			} else {
			 $data=$this->session->set_userdata('IsSetup','');		
			}	
		}
		public function getlogo(){
			$this->db->trans_start();
	 		$this->db->select('logo_id,logo_path,facebook_url,twitter_url,logo_filename');
			$this->db->from('logo');
			$this->db->order_by('logo_id','desc');
			$query = $this->db->get();
			$this->db->trans_complete();
			return $query->row_array();				
		}		
	 	public function AccountCheck($username){
	 		$this->db->select('*');
			$this->db->from('users');
			$this->db->where("username",$username);
			$this->db->group_by("username");
			$query = $this->db->get();
			return $query->row_array();				 		
	 	}
	 	public function ActiveCheck($status){
	 		$this->db->select('status');
			$this->db->from('users');
			$this->db->where('status',$status);
			$query = $this->db->get();
			return $query->row_array();				 		
	 	}
    	function verifyUser($username = "", $password = ""){
        
        
        /*hardcoded for testing*/
        if($username != "" || $password != ""){
            $this->db->select("*")  -> from('users') -> where(
                array(
                    'username' => $username,
                    'password' => $password,
                )
            ) 
			->order_by('username');
			

            $query = $this -> db -> get();

            return $query->first_row('array');


            }else{

            return false;
            }

            return false;

            }				
		function CheckIfLogin(){
			
			$user = $this->session->userdata('admin_username');
			//echo $user."userx";
			if($user==''){
			$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">You Must Login.</div>');						
				return  redirect(base_url('admin'));									
			} else {
				return  "";					
			}
		}
		function activityLog($data){
		$insert_data=$this->db->insert('activity_table',$data);			
			if($insert_data){
				return 1;
			}else {
				return 0;
			}				
		}		
		function table_data($module,$isactive=''){
			//die($module);
			if($module=='activity'){
			$this->db->trans_start();	
			$this->db->select('*');
			$this->db->from("activity_table");
			$this->db->order_by("action_date","desc");
			$this->db->trans_complete();
			return $this->db->get()->result_array();				
			}
			else if($module=='users'){
			$this->db->select('*');
			$this->db->from("users");
			if(!empty($isactive)){
			$this->db->where("status",$isactive);
			}
			$this->db->order_by("full_name","asc");			
			return $this->db->get()->result_array();
			}
			else if($module=='items'){
			$this->db->select('items.*,category.cat_name as cat_name,sub_category.scat_name as scat_name');
			$this->db->join("category","category.cat_name = items.category","left");
			$this->db->join("sub_category","sub_category.scat_name = items.sub_category","left");	
			$this->db->from("items");
			if(!empty($isactive)){
			$this->db->where("status",$isactive);
			}
			$this->db->order_by("item_name","asc");			
			return $this->db->get()->result_array();				
			}
			else if($module=='brand'){
			$this->db->select('*');
			$this->db->from("brand");
			if(!empty($isactive)){
			$this->db->where("status",$isactive);
			}	
			$this->db->order_by("brand_name","asc");		
			return $this->db->get()->result_array();			
			}			
			else if($module=='category'){
			$this->db->select('cat_id,cat_name,status,created_by');
			$this->db->from("category");
			if(!empty($isactive)){
			$this->db->where("status",$isactive);
			}
			$this->db->order_by("cat_name","asc");
			return $this->db->get()->result_array();		
			}	
			else if($module=='sub_category'){
			$this->db->select('sub_category.*,category.cat_name as cat_name');
			$this->db->join("category","category.cat_id = sub_category.cat_id");
			$this->db->from("sub_category");
			if(!empty($isactive)){
			$this->db->where("sub_category.status",$isactive);
			}
			$this->db->order_by("scat_name","asc");			
			return $this->db->get()->result_array();								
			}						
			else if($module=='order'){
		
			}
			else if($module=='promo'){
			$this->db->select('*');
			$this->db->from("promo");
			if(!empty($isactive)){
			$this->db->where("status",$isactive);
			}			
			return $this->db->get()->result_array();				
			}
			else if($module=='promo_type'){

				$this->db->select('ptype_id,ptype_name,qty,disc_percent,status,created_by');
				$this->db->from("promo_scheme");
				if(!empty($isactive)){
				$this->db->where("status",$isactive);
			}			
			return $this->db->get()->result_array();					
			}
			else if($module=='logo'){
				$this->db->select('*');
				$this->db->from('logo');
				$this->db->order_by('logo_id desc');
				//shannele here		
				return $this->db->get()->result_array();			
			}
			else if($module=='announcement'){
				$this->db->select('*');
				$this->db->from('announcement');
				$this->db->order_by('date_announce desc');
				//shannele here		
				return $this->db->get()->result_array();			
			}
			else if($module=='reports'){
			
			}
			else if($module=='messages'){
				$this->db->select('*');
				$this->db->from('contactus');
				$this->db->order_by('id desc');
				//shannele here		
				return $this->db->get()->result_array();			
			}
			else if($module=='about_us'){
				$this->db->select('about_id,date_aboutus,about,contacts');
				$this->db->from('about');
				$this->db->order_by('date_aboutus desc');
				return $this->db->get()->result_array();			
			}
			else if($module=='terms_conditions'){
				$this->db->select('terms_id,date_terms,terms');
				$this->db->from('terms_and_conditions');
				$this->db->order_by('date_terms desc');
				return $this->db->get()->result_array();			
			} 
			else if($module=='options'){
				$this->db->select('option_name,status,created_by,id');
				$this->db->from('options');
				$this->db->order_by('option_name asc');
				return $this->db->get()->result_array();	
			}
			else if($module == 'batch'){
				$this->db->select('id, date_from, date_to, date_created, created_by, created_by_id');
				$this->db->from('batch');
				$this->db->order_by('id','desc');
				return $this->db->get()->result_array();
			}
							
		}
		function getdata($module,$id){
			if($module=='users'){
			$this->db->select('*');
			$this->db->from("users");
			$this->db->where("user_id",$id);
			$query = $this->db->get();
			return $query->row_array();		
					
			}
			else if($module=='items'){
			$this->db->select('*');
			$this->db->from("items");
			$this->db->where("item_id",$id);
			$query = $this->db->get();
			return $query->row_array();				
			} else if($module=='brand'){
			$this->db->select('*');
			$this->db->from("brand");
			$this->db->where("brand_id",$id);
			$query = $this->db->get();
			return $query->row_array();	
							
			} else if($module=='category'){
			$this->db->select('*');
			$this->db->from("category");
			$this->db->where("cat_id",$id);
			$query = $this->db->get();
			return $query->row_array();	
							
			}	
			else if($module=='sub_category'){
			$this->db->select('*');
			$this->db->from("sub_category");
			$this->db->where("scat_id",$id);
			$query = $this->db->get();
			return $query->row_array();	
							
			}						
			else if($module=='order'){
		
			}
			else if($module=='promo'){
			$this->db->select('*');
			$this->db->from("promo");
			$this->db->where("promo_id",$id);
			$query = $this->db->get();
			return $query->row_array();			
			}
			else if($module=='announcement'){
				//eto nakuha ng data papunta sa form
			$this->db->select('*');
			$this->db->from("announcement");
			$this->db->where("announcement_id",$id);
			$query = $this->db->get();
			return $query->row_array();	
				//shannele here					
			}
			else if($module=='reports'){
			
			}
			else if($module=='about_us'){				
			$this->db->select('about_id,date_aboutus,about,contacts');
			$this->db->from("about");
			$this->db->where("about_id",$id);
			$query = $this->db->get();
			return $query->row_array();	
								
			}
			//messages
			else if($module=='messages'){				
			$this->db->select('*');
			$this->db->from("contactus");
			$this->db->where("id",$id);
			$query = $this->db->get();
			return $query->row_array();	
								
			}
			else if($module=='logo'){				
			$this->db->select('*');
			$this->db->from("logo");
			$this->db->where("logo_id",$id);
			$query = $this->db->get();
			return $query->row_array();	
								
			}
			else if($module=='terms_conditions'){				
			$this->db->select('terms_id,date_terms,terms');
			$this->db->from("terms_and_conditions");
			$this->db->where("terms_id",$id);
			$query = $this->db->get();
			return $query->row_array();	
								
			}
			else if($module=='options'){				
			$this->db->select('id,option_name,status,created_by');
			$this->db->from("options");
			$this->db->where("id",$id);
			$query = $this->db->get();
			return $query->row_array();	
								
			}	
			else if($module=='batch'){				
			$this->db->select('id,date_from,date_to');
			$this->db->from("batch");
			$this->db->where("id",$id);
			$query = $this->db->get();
			return $query->row_array();	
								
			}
			else if($module=='promo_type'){

				$this->db->select('ptype_id,ptype_name,qty,disc_percent,status,created_by');
				$this->db->from("promo_scheme");

				$this->db->where("ptype_id",$id);
				$query = $this->db->get();
			return $query->row_array();	
			}							
		}
		//for promo admin items.
		function active_items(){
			$this->db->select('*');
			$this->db->from("items");
			$this->db->where("status","active");
			return $this->db->get()->result_array();				
		}
		
		//for user form
		function getUserid(){
		$this -> db -> select('id');
		$this -> db -> from('users_id');
		$this -> db -> order_by('id desc');
		$this -> db -> limit(1);
		return $this -> db -> get() -> row_array();					
		}
		function UserExist($username){
			$this -> db -> select('username');
			$this -> db -> from('users');
			$this -> db -> where('username',$username);
			return $this -> db -> get() -> row_array();		
		}
		function EmailExist($email){
			$this -> db -> select('email');
			$this -> db -> from('users');
			$this -> db -> where('email',$email);
			return $this -> db -> get() -> row_array();		
		}				
				
		function saveUser($data){
		$insert_data=$this->db->insert('users',$data);			
			if($insert_data){
				$insert_id=$this->db->insert('users_id',array("id"=>''));	
				return 1;
			}else {
				return 0;
			}				
		}					
		function UpdateUser($data,$id){
		$update_data=$this->db->where('user_id', $id);
		$update_data->update('users', $data);               				
				if($update_data){
					return 1;
				}else {
					return 0;
				}				
		}
		//for user verification
		function verifyAccount($id, $email){
			$this -> db -> select('id,email');
			$this -> db -> from('users');
			$this -> db -> where('user_id',$id);
			$this -> db -> where('email',$email);
			return $this -> db -> get() -> row_array();				
		}
							
		//for category form
		function getCatid(){
		$this -> db -> select('cat_id');
		$this -> db -> from('category');
		$this -> db -> order_by('cat_id desc');
		$this -> db -> limit(1);
		return $this -> db -> get() -> row_array();					
		}
		function CategoryExist($cat_name){
			$this -> db -> select('cat_name');
			$this -> db -> from('category');
			$this -> db -> where('cat_name',$cat_name);
			return $this -> db -> get() -> row_array();		
		}
		function saveCategory($data){
		$insert_data=$this->db->insert('category',$data);			
			if($insert_data){
				return 1;
			}else {
				return 0;
			}				
		}					
		function UpdateCategory($data,$id){
		$update_data=$this->db->where('cat_id', $id);
		$update_data->update('category', $data);               				
				if($update_data){
					return 1;
				}else {
					return 0;
				}				
		}	
		//for sub category form
		function getSCatid(){
		$this -> db -> select('scat_id');
		$this -> db -> from('sub_category');
		$this -> db -> order_by('scat_id desc');
		$this -> db -> limit(1);
		return $this -> db -> get() -> row_array();					
		}
		function SCategoryExist($cat_name){
			$this -> db -> select('scat_name');
			$this -> db -> from('sub_category');
			$this -> db -> where('scat_name',$cat_name);
			return $this -> db -> get() -> row_array();		
		}
		function saveSCategory($data){
		$insert_data=$this->db->insert('sub_category',$data);			
			if($insert_data){
				return 1;
			}else {
				return 0;
			}				
		}					
		function UpdateSCategory($data,$id){
		$update_data=$this->db->where('scat_id', $id);
		$update_data->update('sub_category', $data);               				
				if($update_data){
					return 1;
				}else {
					return 0;
				}				
		}	
		function getCategory($brand_id){
			$this -> db -> select('*');
			$this -> db -> from('category');
			$this -> db -> where('brand_id',$brand_id);
			$this -> db -> where('status','active');
			return $this -> db -> get() -> result_array();				
		}		
		function getSubCategory($cat_id){
			$this -> db -> select('*');
			$this -> db -> from('sub_category');
			$this -> db -> where('cat_id',$cat_id);
			$this -> db -> where('status','active');
			return $this -> db -> get() -> result_array();				
		}
		//for  brand form
		function getBrandid(){
		$this -> db -> select('brand_id');
		$this -> db -> from('brand');
		$this -> db -> order_by('brand_id desc');
		$this -> db -> limit(1);
		return $this -> db -> get() -> row_array();					
		}

		function getBrand(){
		$this->db->select("id,brand_id,brand_name,status");
		$this->db->from("brand");
		$this->db->where("status","active");
		return $this->db->get()->result_array();
		}


		function getBrandPromo($promo_id){
		$query = $this->db->query("SELECT b.*, p.promo_id FROM brand b
						LEFT JOIN (SELECT promo_id, promo_group.`value` FROM promo_group WHERE promo_id = '{$promo_id}') AS p ON b.brand_id = p.value");
						return $query->result_array();
		}


		function getPromoGroup($promo_id){ 

			$query = $this->db->query("SELECT * from promo_group where promo_id = '" . $promo_id . "'");

			return $query->result_array();	
		}


		function BrandExist($name){
			$this -> db -> select('brand_name');
			$this -> db -> from('brand');
			$this -> db -> where('brand_name',$name);
			return $this -> db -> get() -> row_array();		
		}
		function saveBrand($data){
		$insert_data=$this->db->insert('brand',$data);			
			if($insert_data){
				return 1;
			}else {
				return 0;
			}				
		}					
		function UpdateBrand($data,$id){
		$update_data=$this->db->where('brand_id', $id);
		$update_data->update('brand', $data);               				
				if($update_data){
					return 1;
				}else {
					return 0;
				}				
		}		
		//for  item form
		function getitemid(){
		$this -> db -> select('item_id');
		$this -> db -> from('items');
		$this -> db -> order_by('item_id desc');
		$this -> db -> limit(1);
		return $this -> db -> get() -> row_array();					
		}
		function ItemExist($name){
			$this -> db -> select('item_name');
			$this -> db -> from('items');
			$this -> db -> where('item_name',$name);
			return $this -> db -> get() -> row_array();		
		}
		function saveItem($data){
		$insert_data=$this->db->insert('items',$data);			
			if($insert_data){
				return 1;
			}else {
				return 0;
			}				
		}	
		function saveItemImages($data){
		$insert_data=$this->db->insert('images',$data);			
			if($insert_data){
				return 1;
			}else {
				return 0;
			}				
		}						
		function UpdateItem($data,$id){
		$update_data=$this->db->where('item_id', $id);
		$update_data->update('items', $data);               				
				if($update_data){
					return 1;
				}else {
					return 0;
				}				
		}	
		function DeleteItem($id){
		//delete image
		$this -> db -> select('*');
		$this -> db -> from('images');
		$this->db->where('item_id', $id);
		$this -> db -> limit(1);
		$query = $this->db->get();
		//$query->row_array();	
		foreach($query->result() as $result){
		//die($result->image_name);	
		unlink("./assets/img/item_covers/".$result->filename);	
		unlink("./assets/img/item_covers/".$result->thumbs);			
		}			
		$delete_data=$this->db->where('item_id', $id);
		$delete_data->delete('items');               				
				if($delete_data){
					return 1;
				}else {
					return 0;
				}				
		}
		function DeleteUser($id){
		$delete_data=$this->db->where('user_id', $id);
		$delete_data->delete('users');               				
				if($delete_data){
					return 1;
				}else {
					return 0;
				}
		}
		function getimgdata($id){
		$this -> db -> select('*');
		$this -> db -> from('images');
		$this->db->where('item_id', $id);
		$this->db->order_by('index', 'asc');
		$query = $this->db->get();	
		return $query->result_array();		
		}
		function deleteItemImages($id){
		$delete_data=$this->db->where('item_id', $id);
		$delete_data->delete('images');               				
				if($delete_data){
					return 1;
				}else {
					return 0;
				}				
		}
		//for  brand form
		function getptypeid(){
		$this -> db -> select('ptype_id');
		$this -> db -> from('promo_scheme');
		$this -> db -> order_by('ptype_id desc');
		$this -> db -> limit(1);
		return $this -> db -> get() -> row_array();					
		}
		function PtypeExist($name){
			$this -> db -> select('ptype_name');
			$this -> db -> from('promo_scheme');
			$this -> db -> where('ptype_name',$name);
			return $this -> db -> get() -> row_array();		
		}
		function savePtype($data){
		$insert_data=$this->db->insert('promo_scheme',$data);			
			if($insert_data){
				return $this->db->insert_id();
			}else {
				return 0;
			}				
		}					
		function UpdatePtype($data,$id){
		$update_data=$this->db->where('ptype_id', $id);
		$update_data->update('promo_scheme', $data);               				
				if($update_data){
					return 1;
				}else {
					return 0;
				}				
		}	
		//for  announcement form
		function getannouncementid(){
		$this -> db -> select('announcement_id');
		$this -> db -> from('announcement');
		$this -> db -> order_by('announcement_id desc');
		$this -> db -> limit(1);
		return $this -> db -> get() -> row_array();					
		}
		function AnnouncementExist($name){
			$this -> db -> select('subj');
			$this -> db -> from('announcement');
			$this -> db -> where('subj',$name);
			return $this -> db -> get() -> row_array();		
		}
		function saveAnnouncement($data){
		$insert_data=$this->db->insert('announcement',$data);			
			if($insert_data){
				return 1;
			}else {
				return 0;
			}				
		}			
		//eto n part ng sql ntin sha since may pattern n tau ggwin nlng ntin papalitan ung mga values nyan kng ano table mo pansinin mo lng gngwa ko		
		function UpdateAnnouncement($data,$id){
		$update_data=$this->db->where('announcement_id', $id);
		$update_data->update('announcement', $data);               				
				if($update_data){
					return 1;
				}else {
					return 0;
				}				
		}	
		
		//terms
		function gettermsid(){
		$this -> db -> select('terms_id');
		$this -> db -> from('terms_and_conditions');
		$this -> db -> order_by('terms_id desc');
		$this -> db -> limit(1);
		return $this -> db -> get() -> row_array();					
		}
		function termsconditionsExist($name){
			$this -> db -> select('terms');
			$this -> db -> from('terms_and_conditions');
			$this -> db -> where('terms',$name);
			return $this -> db -> get() -> row_array();		
		}
		function savetermsconditions($data){
		$insert_data=$this->db->insert('terms_and_conditions',$data);			
			if($insert_data){
				return 1;
			}else {
				return 0;
			}				
		}			
		//eto n part ng sql ntin sha since may pattern n tau ggwin nlng ntin papalitan ung mga values nyan kng ano table mo pansinin mo lng gngwa ko		
		function Updatetermsconditions($data,$id){
		$update_data=$this->db->where('terms_id', $id);
		$update_data->update('terms_and_conditions', $data);               				
				if($update_data){
					return 1;
				}else {
					return 0;
				}				
		}	
		
		//end
			
		//about
		function getaboutusid(){
		$this -> db -> select('about_id');
		$this -> db -> from('about');
		$this -> db -> order_by('about_id desc');
		$this -> db -> limit(1);
		return $this -> db -> get() -> row_array();					
		}
		function aboutusExist($name){
			$this -> db -> select('about');
			$this -> db -> from('about');
			$this -> db -> where('about',$name);
			return $this -> db -> get() -> row_array();		
		}
		function saveaboutus($data){
		$insert_data=$this->db->insert('about',$data);			
			if($insert_data){
				return 1;
			}else {
				return 0;
			}				
		}			
		function Updateaboutus($data,$id){
		$update_data=$this->db->where('about_id', $id);
		$update_data->update('about', $data);               				
				if($update_data){
					return 1;
				}else {
					return 0;
				}				
		}
		//messages
		function getmessagesid(){
		$this -> db -> select('id');
		$this -> db -> from('contactus');
		$this -> db -> order_by('id desc');
		$this -> db -> limit(1);
		return $this -> db -> get() -> row_array();					
		}
		function messagesExist($name){
			$this -> db -> select('id');
			$this -> db -> from('contactus');
			$this -> db -> where('id',$name);
			return $this -> db -> get() -> row_array();		
		}
		function savemessages($data){
		$insert_data=$this->db->insert('contactus',$data);			
			if($insert_data){
				return 1;
			}else {
				return 0;
			}				
		}			
		function Updatemessages($data,$id){
		$update_data=$this->db->where('id', $id);
		$update_data->update('contactus', $data);               				
				if($update_data){
					return 1;
				}else {
					return 0;
				}				
		}
		//end messages
		
		//logo
		function getlogoid(){
		$this -> db -> select('logo_id');
		$this -> db -> from('logo');
		$this -> db -> order_by('logo_id desc');
		$this -> db -> limit(1);
		return $this -> db -> get() -> row_array();					
		}
		function logoExist($name){
			$this -> db -> select('logo_id');
			$this -> db -> from('logo');
			$this -> db -> where('logo_id',$name);
			return $this -> db -> get() -> row_array();		
		}
		function savelogo($data){
		$insert_data=$this->db->insert('logo',$data);			
			if($insert_data){
				return 1;
			}else {
				return 0;
			}				
		}			
		function Updatelogo($data,$id){
		$update_data=$this->db->where('logo_id', $id);
		$update_data->update('logo', $data);               				
				if($update_data){
					return 1;
				}else {
					return 0;
				}				
		}
		
		//end logo

		//promo
		function getpromoid(){
		$this -> db -> select('promo_id');
		$this -> db -> from('promo');
		$this -> db -> order_by('promo_id desc');
		$this -> db -> limit(1);
		return $this -> db -> get() -> row_array();					
		}
		function promoExist($name){
			$this -> db -> select('promo_code');
			$this -> db -> from('promo');
			$this -> db -> where('promo_code',$name);
			return $this -> db -> get() -> row_array();		
		}
		function savePromo($data){
		$insert_data=$this->db->insert('promo',$data);		
		$id =  $this->db->insert_id();	
			if($insert_data){
				return $id;
			}else {
				return 0;
			}				
		}			
		function UpdatePromo($data,$id){
		$update_data=$this->db->where('promo_id', $id);
		$update_data->update('promo', $data);               				
				if($update_data){
					return 1;
				}else {
					return 0;
				}				
		}		
		//option
		function optionExist($name){
			$this -> db -> select('option_name');
			$this -> db -> from('options');
			$this -> db -> where('option_name',$name);
			return $this -> db -> get() -> row_array();		
		}
		function saveOption($data){
		$insert_data=$this->db->insert('options',$data);			
			if($insert_data){
				return 1;
			}else {
				return 0;
			}				
		}			
		function UpdateOption($data,$id){
		$update_data=$this->db->where('id', $id);
		$update_data->update('options', $data);               				
				if($update_data){
					return 1;
				}else {
					return 0;
				}				
		}		
 

		function savePromoGroup($data){
			$this->db->trans_start();
			$insert_data=$this->db->insert_batch('promo_group',$data);			
		
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
			   	return 0;
			}else{
				return 1;
			}	

		}

		function updatePromoGroup($data, $promo_id){
			$this->db->trans_start();
			$delete_data=$this->db->where('promo_id', $promo_id);

			$delete_data->delete('promo_group');               				

			$insert_data=$this->db->insert_batch('promo_group',$data);		
			$this->db->trans_complete();		

			if ($this->db->trans_status() === FALSE)
			{
			   	return 0;
			}else{
				return 1;
			}	
		}
 
		function DeleteCategory($id){		
		$delete_data=$this->db->where('cat_id', $id);
		$delete_data->delete('category');               				
				if($delete_data){
					return 1;
				}else {
					return 0;
				}				
		}
		function DeteleSCategory($id){		
		$delete_data=$this->db->where('scat_id', $id);
		$delete_data->delete('sub_category');               				
				if($delete_data){
					return 1;
				}else {
					return 0;
				}				
 
		}
		//batch
		function BatchExist($date_from,$date_to){
			$this -> db -> select('date_from,date_to');
			$this -> db -> from('batch');
			$this -> db -> where('date_from',$date_from);
			$this -> db -> where('date_from',$date_to);
			return $this -> db -> get() -> row_array();		
		}
		function saveBatch($data){
		$insert_data=$this->db->insert('batch',$data);			
			if($insert_data){
				return 1;
			}else {
				return 0;
			}				
		}	
		function UpdateBatch($data,$id){
		$update_data=$this->db->where('id', $id);
		$update_data->update('batch', $data);               				
				if($update_data){
					return 1;
				}else {
					return 0;
				}				
		}
		function orders_data($status,$b_id){
		$orders_data=$this->db->select('transaction_summary.*,batch_id,date_from,date_to,transaction_summary.qty as qty');
		$this->db->from('batch');
		$this->db->join("transaction_summary","transaction_summary.batch_id = batch.id","inner");
		$this->db->where('transaction_summary.trx_type',$status);
		if($b_id!='g'){
		$this->db->where('transaction_summary.batch_id',$b_id);	
		}
		$this->db->order_by('batch.id','desc');
		return $this -> db -> get() -> result_array();	
		}
		function transaction_data_mother($status,$id){
		$this->db->select('*');	
		$this->db->from('transaction_summary');	
		//$this->db->join("users","users.user_id = transaction_summary.user_id","left");,users.full_name'		
		$this->db->where('trx_type',$status);		
		$this->db->where('order_id',$id);			
		return $this -> db -> get() -> row_array();			
		}
		function transaction_data_baby($status,$id){
		$this->db->select('*');	
		$this->db->from('transactions');	
		//$this->db->where('trx_type',$status);		
		$this->db->where('order_id',$id);			
		return $this -> db -> get() -> result_array();			
		}
		function transaction_data_payment($id){
			$this->db->select('*');
			$this->db->from("payment_table");
			$this->db->where("order_id",$id);	
			return $this->db->get()->result_array();	
		}		
		function getCourier(){
			$this->db->select('*');
			$this->db->from('couriers');
			$this->db->order_by('courier_name','asc');
			return $this -> db -> get() -> result_array();	
		}
		function deleteBaby($order_id){
		$delete_data=$this->db->where('order_id', $id);
		$delete_data->delete('transactions');               				
				if($delete_data){
					return 1;
				}else {
					return 0;
				}			
		}											
} // end model