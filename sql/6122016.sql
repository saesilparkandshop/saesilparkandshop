-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.26 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.5083
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for ssp
CREATE DATABASE IF NOT EXISTS `ssp` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ssp`;

-- Dumping structure for table ssp.about
CREATE TABLE IF NOT EXISTS `about` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `about_id` varchar(50) NOT NULL,
  `date_aboutus` varchar(50) NOT NULL,
  `about` text NOT NULL,
  `contacts` varchar(50) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.about: ~0 rows (approximately)
/*!40000 ALTER TABLE `about` DISABLE KEYS */;
INSERT INTO `about` (`id`, `about_id`, `date_aboutus`, `about`, `contacts`, `created_by`, `created_at`) VALUES
	(1, 'A0000000001', '12/01/2015', 'This is about saesil info.', 'Globe:09152222333', 'superadmin', '2015-12-04');
/*!40000 ALTER TABLE `about` ENABLE KEYS */;

-- Dumping structure for table ssp.activity_table
CREATE TABLE IF NOT EXISTS `activity_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(150) NOT NULL,
  `action_by` varchar(150) NOT NULL,
  `action_date` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `action` (`action`),
  KEY `action_by` (`action_by`),
  KEY `action_date` (`action_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.activity_table: ~0 rows (approximately)
/*!40000 ALTER TABLE `activity_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `activity_table` ENABLE KEYS */;

-- Dumping structure for table ssp.announcement
CREATE TABLE IF NOT EXISTS `announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `announcement_id` varchar(50) NOT NULL DEFAULT '0',
  `date_announce` varchar(255) NOT NULL,
  `type` int(11) NOT NULL COMMENT '0=announcement 1=promo',
  `subj` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `announce_by` varchar(255) NOT NULL,
  `isdelete` int(11) NOT NULL COMMENT '0=active 1=deleted',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `announcement_id` (`announcement_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.announcement: ~0 rows (approximately)
/*!40000 ALTER TABLE `announcement` DISABLE KEYS */;
INSERT INTO `announcement` (`id`, `announcement_id`, `date_announce`, `type`, `subj`, `message`, `announce_by`, `isdelete`, `date_created`, `created_by`, `created_at`) VALUES
	(1, 'A0000000001', '12/01/2015', 0, 'Welcome to Saesil parknshop ', 'We are announcing our new website is on beta.', 'superadmin', 0, '2015-12-04 10:50:35', 'superadmin', '2015-12-04');
/*!40000 ALTER TABLE `announcement` ENABLE KEYS */;

-- Dumping structure for table ssp.batch
CREATE TABLE IF NOT EXISTS `batch` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(90) DEFAULT NULL,
  `created_at` date NOT NULL,
  `created_by_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.batch: 2 rows
/*!40000 ALTER TABLE `batch` DISABLE KEYS */;
INSERT INTO `batch` (`id`, `date_from`, `date_to`, `date_created`, `created_by`, `created_at`, `created_by_id`) VALUES
	(2, '2016-02-10', '2016-02-20', '2016-02-10 12:24:26', '1', '0000-00-00', 1),
	(3, '2016-03-21', '2016-03-22', '2016-03-07 14:22:49', 'superadmin', '2016-03-07', NULL);
/*!40000 ALTER TABLE `batch` ENABLE KEYS */;

-- Dumping structure for table ssp.brand
CREATE TABLE IF NOT EXISTS `brand` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brand_id` varchar(100) NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_by` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `brand_id` (`brand_id`),
  KEY `brand_name` (`brand_name`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.brand: ~4 rows (approximately)
/*!40000 ALTER TABLE `brand` DISABLE KEYS */;
INSERT INTO `brand` (`id`, `brand_id`, `brand_name`, `status`, `created_by`, `created_at`) VALUES
	(1, 'BRD0000000001', 'ARDECO', 'inactive', 'superadmin', '2016-03-06'),
	(2, 'BRD0000000002', 'MAC', 'active', 'superadmin', '2015-12-03'),
	(3, 'BRD0000000003', 'REVLON', 'active', 'superadmin', '2015-12-03'),
	(4, 'BRD0000000004', 'MISSHA', 'active', 'superadmin', '2016-01-23');
/*!40000 ALTER TABLE `brand` ENABLE KEYS */;

-- Dumping structure for table ssp.category
CREATE TABLE IF NOT EXISTS `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brand_id` varchar(100) NOT NULL,
  `cat_id` varchar(100) NOT NULL,
  `cat_name` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_by` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cat_id` (`cat_id`),
  KEY `cat_name` (`cat_name`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='list of categories';

-- Dumping data for table ssp.category: ~3 rows (approximately)
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`id`, `brand_id`, `cat_id`, `cat_name`, `status`, `created_by`, `created_at`) VALUES
	(2, 'BRD0000000001', 'CAT0000000001', 'Base makeup', 'active', 'superadmin', '2015-12-19'),
	(3, '', 'CAT0000000002', 'Makeup', 'active', 'superadmin', '2016-01-10');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Dumping structure for table ssp.consignee
CREATE TABLE IF NOT EXISTS `consignee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consignee_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `status` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `branch_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `consignee_id` (`consignee_id`),
  KEY `name` (`name`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.consignee: 0 rows
/*!40000 ALTER TABLE `consignee` DISABLE KEYS */;
/*!40000 ALTER TABLE `consignee` ENABLE KEYS */;

-- Dumping structure for table ssp.contactus
CREATE TABLE IF NOT EXISTS `contactus` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  `date` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.contactus: ~0 rows (approximately)
/*!40000 ALTER TABLE `contactus` DISABLE KEYS */;
INSERT INTO `contactus` (`id`, `name`, `email`, `subject`, `message`, `date`) VALUES
	(1, 'rjmoris', '123@yahoo.com', 'hi', 'hi', '2016-02-15');
/*!40000 ALTER TABLE `contactus` ENABLE KEYS */;

-- Dumping structure for table ssp.couriers
CREATE TABLE IF NOT EXISTS `couriers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courier_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.couriers: ~6 rows (approximately)
/*!40000 ALTER TABLE `couriers` DISABLE KEYS */;
INSERT INTO `couriers` (`id`, `courier_name`) VALUES
	(1, 'BPI'),
	(2, 'BDO'),
	(3, 'Cebuana'),
	(4, 'LBC'),
	(5, 'Palawan Pawnshop'),
	(6, 'PayPal');
/*!40000 ALTER TABLE `couriers` ENABLE KEYS */;

-- Dumping structure for table ssp.images
CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` varchar(100) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `thumbs` varchar(255) NOT NULL,
  `index` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1 COMMENT='stored images';

-- Dumping data for table ssp.images: ~6 rows (approximately)
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` (`id`, `item_id`, `filename`, `thumbs`, `index`) VALUES
	(49, 'ITM0000000002', '2016_14589074391_1.png', '2016_14589074391_1-thumb.png', 1),
	(50, 'ITM0000000002', '2016_14589074392_2.png', '2016_14589074392_2-thumb.png', 2),
	(51, 'ITM0000000002', '2016_14589074393_3.png', '2016_14589074393_3-thumb.png', 3),
	(52, 'ITM0000000001', '2016_14572397821_1.png', '2016_14572397821_1-thumb.png', 1),
	(53, 'ITM0000000001', '2016_14572397822_2.png', '2016_14572397822_2-thumb.png', 2),
	(54, 'ITM0000000001', '2016_14572397823_3.png', '2016_14572397823_3-thumb.png', 3),
	(55, 'ITM0000000003', '2016_14632750881_1.png', '2016_14632750881_1-thumb.png', 1);
/*!40000 ALTER TABLE `images` ENABLE KEYS */;

-- Dumping structure for table ssp.items
CREATE TABLE IF NOT EXISTS `items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` varchar(50) NOT NULL,
  `brand_id` varchar(50) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `short_desc` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `sub_category` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `size` text NOT NULL,
  `volume` text NOT NULL,
  `price` float(11,2) NOT NULL,
  `price_won` float(11,2) NOT NULL,
  `status` enum('active','inactive') NOT NULL COMMENT 'status 0 available 1 not available',
  `qty` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isdelete` int(11) NOT NULL DEFAULT '0' COMMENT '0 not delete 1 delete',
  `isbnew` int(11) NOT NULL COMMENT '0=brand new; 1=used book',
  `ispromo` int(11) NOT NULL COMMENT '0=nonpromo 1=promo',
  `promo_id` varchar(50) NOT NULL,
  `promo_disc` float(11,2) NOT NULL,
  `format` varchar(255) NOT NULL,
  `is_stock` enum('in-stock','out-stock') NOT NULL,
  `is_onhand` int(2) NOT NULL DEFAULT '0' COMMENT '0=not on hand 1= onhand',
  `onhand_price` float(11,2) NOT NULL DEFAULT '0.00',
  `options` text,
  `option_stock` text,
  `sell_count` int(11) NOT NULL,
  `is_bestseller` int(11) NOT NULL COMMENT '0=no 1=yes',
  `is_newitem` int(11) NOT NULL DEFAULT '0' COMMENT '0=no 1=yes',
  `created_by` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  KEY `item_name` (`item_name`),
  KEY `brand` (`brand`),
  KEY `category` (`category`),
  KEY `short_desc` (`short_desc`),
  KEY `price` (`price`),
  KEY `status` (`status`),
  KEY `qty` (`qty`),
  KEY `date_created` (`date_created`),
  KEY `isdelete` (`isdelete`),
  KEY `isbnew` (`isbnew`),
  KEY `ispromo` (`ispromo`),
  KEY `promo_id` (`promo_id`),
  KEY `promo_disc` (`promo_disc`),
  KEY `sell_count` (`sell_count`),
  KEY `format` (`format`),
  KEY `sub_category` (`sub_category`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.items: ~1 rows (approximately)
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` (`id`, `item_id`, `brand_id`, `item_name`, `short_desc`, `brand`, `category`, `sub_category`, `description`, `size`, `volume`, `price`, `price_won`, `status`, `qty`, `date_created`, `isdelete`, `isbnew`, `ispromo`, `promo_id`, `promo_disc`, `format`, `is_stock`, `is_onhand`, `onhand_price`, `options`, `option_stock`, `sell_count`, `is_bestseller`, `is_newitem`, `created_by`, `created_at`) VALUES
	(29, 'ITM0000000001', 'BRD0000000002', 'Sample 1', '', 'MAC', 'Base makeup', 'sampe sub category', '123', '123', '333', 123.00, 10.00, 'active', 0, '2016-03-05 14:04:58', 0, 0, 0, '', 0.00, '', 'in-stock', 0, 0.00, 'red,blue', '0,0', 0, 1, 1, 'superadmin', '2016-03-05'),
	(31, 'ITM0000000002', 'BRD0000000003', 'Sample2', '', 'REVLON', 'Base makeup', 'sampe sub category', '123', '', '', 333.00, 333.00, 'active', 0, '2016-03-25 20:03:59', 0, 0, 0, '', 0.00, '', 'in-stock', 0, 0.00, 'black,blue', '0,0', 0, 1, 1, 'superadmin', '2016-03-25'),
	(32, 'ITM0000000003', 'BRD0000000002', 'asd', '', 'MAC', 'Base makeup', 'sampe sub category', '11111', '', '123', 123.00, 123.00, 'active', 0, '2016-05-15 09:18:09', 0, 0, 0, '', 0.00, '', 'in-stock', 0, 0.00, '1', '0', 0, 0, 0, 'superadmin', '2016-05-15');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;

-- Dumping structure for table ssp.logo
CREATE TABLE IF NOT EXISTS `logo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `logo_id` varchar(100) NOT NULL,
  `logo_filename` varchar(100) NOT NULL,
  `logo_path` varchar(100) NOT NULL,
  `facebook_url` varchar(255) NOT NULL,
  `twitter_url` varchar(255) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.logo: ~0 rows (approximately)
/*!40000 ALTER TABLE `logo` DISABLE KEYS */;
INSERT INTO `logo` (`id`, `logo_id`, `logo_filename`, `logo_path`, `facebook_url`, `twitter_url`, `created_by`, `created_at`) VALUES
	(1, 'L0000000001', '2016_1458887300.png', 'http://localhost/ssp/assets/img/logo/2016_1458887300.png', 'https://www.facebook.com/saesil.parknshop', 'https://twitter.com/saesilparknshop', '', '2016-03-25');
/*!40000 ALTER TABLE `logo` ENABLE KEYS */;

-- Dumping structure for table ssp.options
CREATE TABLE IF NOT EXISTS `options` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `item_id` varchar(50) NOT NULL,
  `image_path` varchar(255) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `option_name` varchar(255) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_by` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `option_name_created_by` (`option_name`,`created_by`),
  KEY `status` (`status`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='this is for options';

-- Dumping data for table ssp.options: ~0 rows (approximately)
/*!40000 ALTER TABLE `options` DISABLE KEYS */;
/*!40000 ALTER TABLE `options` ENABLE KEYS */;

-- Dumping structure for table ssp.order_id
CREATE TABLE IF NOT EXISTS `order_id` (
  `id` int(12) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT 'For Order ID Generation',
  `content` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1 COMMENT='For Order ID Generation';

-- Dumping data for table ssp.order_id: ~16 rows (approximately)
/*!40000 ALTER TABLE `order_id` DISABLE KEYS */;
INSERT INTO `order_id` (`id`, `content`) VALUES
	(000000000001, ''),
	(000000000002, ''),
	(000000000003, ''),
	(000000000004, ''),
	(000000000005, ''),
	(000000000006, ''),
	(000000000007, ''),
	(000000000008, ''),
	(000000000009, ''),
	(000000000010, ''),
	(000000000011, ''),
	(000000000012, ''),
	(000000000013, ''),
	(000000000014, ''),
	(000000000015, ''),
	(000000000016, '');
/*!40000 ALTER TABLE `order_id` ENABLE KEYS */;

-- Dumping structure for table ssp.payment_table
CREATE TABLE IF NOT EXISTS `payment_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `control_number` varchar(255) NOT NULL,
  `courrier` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `img_filename` varchar(255) NOT NULL,
  `img_filename_thumbs` varchar(255) NOT NULL,
  `status` enum('pending','valid','invalid') NOT NULL DEFAULT 'pending',
  `index` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1 COMMENT='table for payments made.';

-- Dumping data for table ssp.payment_table: ~2 rows (approximately)
/*!40000 ALTER TABLE `payment_table` DISABLE KEYS */;
INSERT INTO `payment_table` (`id`, `order_id`, `control_number`, `courrier`, `amount`, `img_filename`, `img_filename_thumbs`, `status`, `index`) VALUES
	(31, 35, '1', 'BDO', '12', '2016_14614876540_0.png', '2016_14614876540_0-thumb.png', 'pending', 0),
	(32, 35, '2', 'BPI', '33', '2016_14614876541_1.png', '2016_14614876541_1-thumb.png', 'pending', 1);
/*!40000 ALTER TABLE `payment_table` ENABLE KEYS */;

-- Dumping structure for table ssp.promo
CREATE TABLE IF NOT EXISTS `promo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `promo_id` varchar(50) NOT NULL,
  `promo_code` varchar(255) NOT NULL,
  `promo_path` varchar(255) NOT NULL,
  `promo_filename` varchar(255) NOT NULL,
  `promo_scheme` varchar(100) NOT NULL,
  `promo_name` varchar(255) NOT NULL,
  `promo_desc` text NOT NULL,
  `items` varchar(255) NOT NULL,
  `promo_type` enum('brand','category','sub-category','bundle','item') NOT NULL,
  `promo_disc` float(11,2) NOT NULL,
  `promo_from` varchar(50) NOT NULL,
  `promo_to` varchar(50) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `status` enum('active','inactive') NOT NULL COMMENT 'status 0 available 1 not available',
  PRIMARY KEY (`id`),
  KEY `promo_id` (`promo_id`),
  KEY `promo_type` (`promo_type`),
  KEY `promo_name` (`promo_name`),
  KEY `promo_code` (`promo_code`),
  KEY `promo_disc` (`promo_disc`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.promo: ~0 rows (approximately)
/*!40000 ALTER TABLE `promo` DISABLE KEYS */;
INSERT INTO `promo` (`id`, `promo_id`, `promo_code`, `promo_path`, `promo_filename`, `promo_scheme`, `promo_name`, `promo_desc`, `items`, `promo_type`, `promo_disc`, `promo_from`, `promo_to`, `created_by`, `created_at`, `status`) VALUES
	(1, 'PRM0000000001', 'QWE123', 'http://localhost/ssp/assets/img/promo_banner/1456681823.png', '1456681823.png', 'PS0000000001', 'DOLL FACE SALE', '', 'ITM0000000001', 'item', 0.00, '2016-02-29', '2016-04-29', 'superadmin', '2016-02-29', 'active'),
	(2, 'PRM0000000002', '9080989', 'http://localhost/ssp/assets/img/promo_banner/1457247921.png', '1457247921.png', 'PS0000000002', 'misha promo', '', '', 'brand', 0.00, '2016-03-06', '2016-03-07', 'superadmin', '2016-03-06', 'active');
/*!40000 ALTER TABLE `promo` ENABLE KEYS */;

-- Dumping structure for table ssp.promo_group
CREATE TABLE IF NOT EXISTS `promo_group` (
  `id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `promo_id` int(9) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `value` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `value` (`value`),
  KEY `promo_id` (`promo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.promo_group: ~4 rows (approximately)
/*!40000 ALTER TABLE `promo_group` DISABLE KEYS */;
INSERT INTO `promo_group` (`id`, `promo_id`, `type`, `value`) VALUES
	(1, 1, 'item', 'ITM0000000001'),
	(2, 2, 'brand', 'BRD0000000002'),
	(3, 2, 'brand', 'BRD0000000003'),
	(4, 2, 'brand', 'BRD0000000004');
/*!40000 ALTER TABLE `promo_group` ENABLE KEYS */;

-- Dumping structure for table ssp.promo_scheme
CREATE TABLE IF NOT EXISTS `promo_scheme` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ptype_id` varchar(50) NOT NULL,
  `ptype_name` varchar(50) NOT NULL,
  `qty` int(11) NOT NULL,
  `disc_percent` float(11,2) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_by` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ptype_id` (`ptype_id`),
  KEY `ptype_name` (`ptype_name`),
  KEY `qty` (`qty`),
  KEY `disc_percent` (`disc_percent`),
  KEY `status` (`status`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.promo_scheme: ~0 rows (approximately)
/*!40000 ALTER TABLE `promo_scheme` DISABLE KEYS */;
INSERT INTO `promo_scheme` (`id`, `ptype_id`, `ptype_name`, `qty`, `disc_percent`, `status`, `created_by`, `created_at`) VALUES
	(1, 'PS0000000001', 'Buy one Take one', 1, 10.00, 'active', 'superadmin', '2015-12-04'),
	(2, 'PS0000000002', 'misha', 1, 30.00, 'active', 'superadmin', '2016-03-06');
/*!40000 ALTER TABLE `promo_scheme` ENABLE KEYS */;

-- Dumping structure for table ssp.sub_category
CREATE TABLE IF NOT EXISTS `sub_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cat_id` varchar(100) NOT NULL,
  `scat_id` varchar(100) NOT NULL,
  `scat_name` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_by` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `scat_id` (`scat_id`),
  KEY `scat_name` (`scat_name`),
  KEY `status` (`status`),
  KEY `cat_id` (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.sub_category: ~3 rows (approximately)
/*!40000 ALTER TABLE `sub_category` DISABLE KEYS */;
INSERT INTO `sub_category` (`id`, `cat_id`, `scat_id`, `scat_name`, `status`, `created_by`, `created_at`) VALUES
	(6, 'CAT0000000002', 'SCAT0000000003', 'Blush on', 'active', 'superadmin', '2016-01-23'),
	(7, 'CAT0000000001', 'SCAT0000000004', 'sampe sub category', 'active', 'superadmin', '2016-02-15'),
	(8, 'CAT0000000001', 'SCAT0000000005', 'sampe sub category2', 'active', 'superadmin', '2016-02-15');
/*!40000 ALTER TABLE `sub_category` ENABLE KEYS */;

-- Dumping structure for table ssp.terms_and_conditions
CREATE TABLE IF NOT EXISTS `terms_and_conditions` (
  `terms_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_terms` varchar(50) NOT NULL,
  `terms` text NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`terms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.terms_and_conditions: ~0 rows (approximately)
/*!40000 ALTER TABLE `terms_and_conditions` DISABLE KEYS */;
INSERT INTO `terms_and_conditions` (`terms_id`, `date_terms`, `terms`, `created_by`, `created_at`) VALUES
	(1, '12/01/2015', 'This is about the terms and conditions.', 'superadmin', '2015-12-04');
/*!40000 ALTER TABLE `terms_and_conditions` ENABLE KEYS */;

-- Dumping structure for table ssp.transactions
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) NOT NULL,
  `item_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `batch_id` int(11) DEFAULT NULL,
  `trx_type` enum('active','predelivery','onshipping','ondelivery','completed','void') NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `remarks` text NOT NULL,
  `qty` int(11) NOT NULL,
  `price` float(11,2) NOT NULL,
  `subtotal` float(11,2) NOT NULL,
  `vat` float(11,2) NOT NULL,
  `disc` float(11,2) NOT NULL,
  `g_total` float(11,2) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_promo` int(11) NOT NULL COMMENT '0= not in promo 1= in promo',
  `courier` varchar(255) NOT NULL,
  `track_num` varchar(255) NOT NULL,
  `shipping_fee` float(11,2) NOT NULL,
  `delivery_sched` varchar(255) NOT NULL,
  `isdelete` int(11) NOT NULL COMMENT '0=not deleted 1=deleted',
  `name` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `options` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.transactions: 1 rows
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` (`id`, `order_id`, `item_id`, `user_id`, `batch_id`, `trx_type`, `user_name`, `user_email`, `item_name`, `remarks`, `qty`, `price`, `subtotal`, `vat`, `disc`, `g_total`, `date_created`, `is_promo`, `courier`, `track_num`, `shipping_fee`, `delivery_sched`, `isdelete`, `name`, `phone_number`, `address`, `options`) VALUES
	(57, 'ORD00015', '31', 'USER0000000003', 3, 'active', 'saesiluser', '111@yahoo.com', 'Sample2', '', 11, 333.00, 3663.00, 0.00, 0.00, 0.00, '2016-05-29 18:40:07', 0, '', '', 0.00, '', 0, 'sample encoder user', '123456789', '111', '{"black":"11"}');
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;

-- Dumping structure for table ssp.transaction_summary
CREATE TABLE IF NOT EXISTS `transaction_summary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `batch_id` int(10) DEFAULT NULL,
  `client_name` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cp_num` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `sub_total` float(11,2) NOT NULL,
  `vat` float(11,2) NOT NULL,
  `shipping_fee` float(11,2) NOT NULL,
  `intl_fee` float(11,2) NOT NULL,
  `total_price` float(11,2) NOT NULL,
  `payment_scheme` enum('full','partial','payday') NOT NULL DEFAULT 'full',
  `admin_control_num` varchar(255) NOT NULL,
  `courier` varchar(255) NOT NULL,
  `remarks` text NOT NULL,
  `trx_type` enum('active','predelivery','onshipping','ondelivery','completed','void') NOT NULL DEFAULT 'active',
  `isdelete` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.transaction_summary: 1 rows
/*!40000 ALTER TABLE `transaction_summary` DISABLE KEYS */;
INSERT INTO `transaction_summary` (`id`, `order_id`, `user_id`, `batch_id`, `client_name`, `full_name`, `email`, `cp_num`, `qty`, `sub_total`, `vat`, `shipping_fee`, `intl_fee`, `total_price`, `payment_scheme`, `admin_control_num`, `courier`, `remarks`, `trx_type`, `isdelete`, `date_created`) VALUES
	(37, 'ORD00015', 'USER0000000003', 3, 'saesiluser', 'sample encoder user', '111@yahoo.com', '123456789', 11, 0.00, 0.00, 0.00, 0.00, 3663.00, 'full', '', '', '', 'active', 0, '2016-05-29 18:40:07');
/*!40000 ALTER TABLE `transaction_summary` ENABLE KEYS */;

-- Dumping structure for table ssp.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) NOT NULL,
  `full_name` varchar(200) DEFAULT '',
  `first_name` varchar(100) DEFAULT '',
  `middle_name` varchar(100) DEFAULT '',
  `last_name` varchar(100) DEFAULT '',
  `email` varchar(255) DEFAULT '',
  `address` text,
  `contact_no` varchar(255) DEFAULT '',
  `gender` enum('male','female') DEFAULT NULL,
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) DEFAULT '',
  `user_type` enum('regular','vip') NOT NULL,
  `access_level` enum('user','admin','super-admin','encoder') DEFAULT 'user',
  `account_name` varchar(255) DEFAULT '',
  `account_no` varchar(255) DEFAULT '',
  `fb_url` varchar(255) DEFAULT '',
  `created_by` varchar(255) DEFAULT '',
  `status` enum('active','inactive') DEFAULT NULL,
  `activation_date` date DEFAULT NULL,
  `activated_by` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `status` (`status`),
  KEY `username` (`username`),
  KEY `access_level` (`access_level`),
  KEY `user_type` (`user_type`),
  KEY `email` (`email`),
  KEY `full_name` (`full_name`),
  KEY `account_name_account_no` (`account_name`,`account_no`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.users: ~5 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `user_id`, `full_name`, `first_name`, `middle_name`, `last_name`, `email`, `address`, `contact_no`, `gender`, `username`, `password`, `user_type`, `access_level`, `account_name`, `account_no`, `fb_url`, `created_by`, `status`, `activation_date`, `activated_by`) VALUES
	(1, 'USER0000000002', 'Saesil Parkn Shop', 'Saesil', 'Parkn', 'Shop', 'saesil.parkenshop2015@gmail.com', NULL, '09152222111', NULL, 'superadmin', 'Sv7pYMypRIBNQMULoP9cLguNFu20xVQIMerJtlBFrgA', 'regular', 'super-admin', '', '', '', 'super-admin', 'active', '2015-12-03', 'super-admin'),
	(2, 'USER0000000003', 'Saesil sample User', 'Saesil', 'sample', 'User', 'saesil.parkenshop2015@gmail.com', '091 home bacoor, cavite', '09151111222', NULL, 'saesiluser', '1DdsJ8V1KfZCR0p7sc3DlpcsswTsivuZ11Xx87SxITM', 'regular', 'user', '12345', '12345', 'http://www.123contactform.com/form-654309/QUOTATION-FORM', 'superadmin', 'active', '2015-12-03', 'superadmin'),
	(3, 'USER0000000004', 'rj s moris', 'rj', 's', 'moris', '123@123.com', '123', '123123123', 'male', 'minamikaite', '1DdsJ8V1KfZCR0p7sc3DlpcsswTsivuZ11Xx87SxITM', 'regular', 'user', '123', '123', 'http://www.123contactform.com/form-654309/QUOTATION-FORM', 'online', 'inactive', '2016-01-02', 'online'),
	(5, 'USER0000000006', 'rj s moris', 'rj', 's', 'moris', '12312@12312.com', '123', '123', NULL, 'sampleuser1', 'm0C3FgJID2EnAT07m_rF9dYlb7QBs5Gas_Sqpu8jK9w', 'regular', 'admin', '123123', '123', 'http://www.123contactform.com/form-654309/QUOTATION-FORM', 'superadmin', 'active', '2016-02-10', 'superadmin'),
	(6, 'USER0000000007', 'sample encoder user', 'sample', 'encoder', 'user', '111@yahoo.com', '111', '123456789', NULL, 'encoder1', 'Sv7pYMypRIBNQMULoP9cLguNFu20xVQIMerJtlBFrgA', 'regular', 'encoder', 'sampleaccount', '2131212312312', 'http://getbootstrap.com/components/', 'superadmin', 'active', '2016-03-28', 'superadmin');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table ssp.users_id
CREATE TABLE IF NOT EXISTS `users_id` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.users_id: ~6 rows (approximately)
/*!40000 ALTER TABLE `users_id` DISABLE KEYS */;
INSERT INTO `users_id` (`id`) VALUES
	(1),
	(2),
	(3),
	(4),
	(5),
	(6),
	(7);
/*!40000 ALTER TABLE `users_id` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
