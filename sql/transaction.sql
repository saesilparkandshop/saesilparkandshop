/*
SQLyog Ultimate v11.5 (64 bit)
MySQL - 5.6.21 : Database - vanilla
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `transaction_summary` */

DROP TABLE IF EXISTS `transaction_summary`;

CREATE TABLE `transaction_summary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cp_num` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `sub_total` float(11,2) NOT NULL,
  `vat` float(11,2) NOT NULL,
  `shipping_fee` float(11,2) NOT NULL,
  `total_price` float(11,2) NOT NULL,
  `client_control_num` varchar(255) NOT NULL,
  `courier_client` varchar(255) NOT NULL,
  `admin_control_num` varchar(255) NOT NULL,
  `courier` varchar(255) NOT NULL,
  `remarks` text NOT NULL,
  `trx_type` int(11) NOT NULL COMMENT '0=active 1=paid 2=ondelivery 3=summary 4=void',
  `isdelete` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Table structure for table `transactions` */

DROP TABLE IF EXISTS `transactions`;

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) NOT NULL,
  `book_id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `trx_type` int(11) NOT NULL COMMENT '0=active order 1=on delivery  2=delivered order 3= returned order',
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `book_title` varchar(255) NOT NULL,
  `remarks` text NOT NULL,
  `qty` int(11) NOT NULL,
  `price` float(11,2) NOT NULL,
  `subtotal` float(11,2) NOT NULL,
  `vat` float(11,2) NOT NULL,
  `disc` float(11,2) NOT NULL,
  `g_total` float(11,2) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_promo` int(11) NOT NULL COMMENT '0= not in promo 1= in promo',
  `courier` varchar(255) NOT NULL,
  `track_num` varchar(255) NOT NULL,
  `shipping_fee` float(11,2) NOT NULL,
  `delivery_sched` varchar(255) NOT NULL,
  `isdelete` int(11) NOT NULL COMMENT '0=not deleted 1=deleted',
  `name` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `address` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
