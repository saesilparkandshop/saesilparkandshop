/*
SQLyog Ultimate v11.5 (64 bit)
MySQL - 5.6.21 : Database - ssp
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `promo_group` */

DROP TABLE IF EXISTS `promo_group`;

CREATE TABLE `promo_group` (
  `id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `promo_id` int(9) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `value` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `value` (`value`),
  KEY `promo_id` (`promo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
