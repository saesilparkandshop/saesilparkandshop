-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.26 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for ssp
CREATE DATABASE IF NOT EXISTS `ssp` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ssp`;


-- Dumping structure for table ssp.about
CREATE TABLE IF NOT EXISTS `about` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `about` text NOT NULL,
  `contacts` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.about: ~0 rows (approximately)
/*!40000 ALTER TABLE `about` DISABLE KEYS */;
/*!40000 ALTER TABLE `about` ENABLE KEYS */;


-- Dumping structure for table ssp.announcement
CREATE TABLE IF NOT EXISTS `announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_announce` varchar(255) NOT NULL,
  `type` int(11) NOT NULL COMMENT '0=announcement 1=promo',
  `subj` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `announce_by` varchar(255) NOT NULL,
  `isdelete` int(11) NOT NULL COMMENT '0=active 1=deleted',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.announcement: ~0 rows (approximately)
/*!40000 ALTER TABLE `announcement` DISABLE KEYS */;
/*!40000 ALTER TABLE `announcement` ENABLE KEYS */;


-- Dumping structure for table ssp.items
CREATE TABLE IF NOT EXISTS `items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` varchar(50) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `short_desc` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image_path` varchar(255) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `price` float(11,2) NOT NULL,
  `status` int(11) NOT NULL COMMENT 'status 0 available 1 not available',
  `qty` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isdelete` int(11) NOT NULL DEFAULT '0' COMMENT '0 not delete 1 delete',
  `category` varchar(255) NOT NULL,
  `isbnew` int(11) NOT NULL COMMENT '0=brand new; 1=used book',
  `ispromo` int(11) NOT NULL COMMENT '0=nonpromo 1=promo',
  `promo_id` varchar(50) NOT NULL,
  `promo_disc` float(11,2) NOT NULL,
  `format` varchar(255) NOT NULL,
  `sell_count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.items: ~0 rows (approximately)
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
/*!40000 ALTER TABLE `items` ENABLE KEYS */;


-- Dumping structure for table ssp.terms_and_conditions
CREATE TABLE IF NOT EXISTS `terms_and_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `terms` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.terms_and_conditions: ~0 rows (approximately)
/*!40000 ALTER TABLE `terms_and_conditions` DISABLE KEYS */;
/*!40000 ALTER TABLE `terms_and_conditions` ENABLE KEYS */;


-- Dumping structure for table ssp.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) NOT NULL,
  `full_name` varchar(200) DEFAULT '',
  `first_name` varchar(100) DEFAULT '',
  `middle_name` varchar(100) DEFAULT '',
  `last_name` varchar(100) DEFAULT '',
  `email` varchar(255) DEFAULT '',
  `contact_no` varchar(255) DEFAULT '',
  `username` varchar(255) DEFAULT '',
  `password` varchar(255) DEFAULT '',
  `user_type` enum('regular','vip') NOT NULL,
  `access_level` varchar(255) DEFAULT '',
  `created_by` varchar(255) DEFAULT '',
  `status` varchar(8) DEFAULT '',
  `activation_date` date DEFAULT NULL,
  `activated_by` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT IGNORE INTO `users` (`id`, `user_id`, `full_name`, `first_name`, `middle_name`, `last_name`, `email`, `contact_no`, `username`, `password`, `user_type`, `access_level`, `created_by`, `status`, `activation_date`, `activated_by`) VALUES
	(1, 'USER0000000001', 'rj S moris', 'rj', 'S', 'moris', 'rolandjay@gmail.com', '091512222333', 'superadmin', 'Chii1221', 'regular', 'super-admin', 'super-admin', 'active', '2015-10-01', 'super-admin'),
	(2, 'USER0000000002', 'rj s moris2', 'rj', 's', 'moris2', '123@yahoo.com', '1234444444', 'chii12', 'Chii1221', 'vip', 'admin', '0', 'active', '2015-10-04', '0');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Dumping structure for table ssp.users_id
CREATE TABLE IF NOT EXISTS `users_id` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table ssp.users_id: ~2 rows (approximately)
/*!40000 ALTER TABLE `users_id` DISABLE KEYS */;
INSERT IGNORE INTO `users_id` (`id`) VALUES
	(1),
	(2);
/*!40000 ALTER TABLE `users_id` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
